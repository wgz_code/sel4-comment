/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#ifdef HAVE_AUTOCONF
#include <autoconf.h>
#endif

#ifndef __ASSEMBLER__
#define LIBSEL4_BIT(n) (1ul<<(n))   /*Z 2^n */

#ifdef CONFIG_HARDWARE_DEBUG_API
/*Z 断点类型 */
/* API arg values for breakpoint API, "type" arguments. */
typedef enum {
    seL4_DataBreakpoint = 0,    /*Z INT1数据读写断点 */
    seL4_InstructionBreakpoint, /*Z INT1指令执行断点 */
    seL4_SingleStep,            /*Z INT1单步执行 */
    seL4_SoftwareBreakRequest,  /*Z INT3。不用于断点预置 */
    SEL4_FORCE_LONG_ENUM(seL4_BreakpointType)
} seL4_BreakpointType;
/*Z 断点条件类型 */
/* API arg values for breakpoint API, "access" arguments. */
typedef enum {
    seL4_BreakOnRead = 0,       /*Z 读(执行)指令 */
    seL4_BreakOnWrite,          /*Z 写数据 */
    seL4_BreakOnReadWrite,      /*Z 读写数据 */
    seL4_MaxBreakpointAccess,
    SEL4_FORCE_LONG_ENUM(seL4_BreakpointAccess)
} seL4_BreakpointAccess;

/* Format of a debug-exception message. */
enum {
    seL4_DebugException_FaultIP,
    seL4_DebugException_ExceptionReason,
    seL4_DebugException_TriggerAddress,
    seL4_DebugException_BreakpointNumber,
    seL4_DebugException_Length,
    SEL4_FORCE_LONG_ENUM(seL4_DebugException_Msg)
} seL4_DebugException_Msg;
#endif
/*Z 调度优先级常量。默认配置为256级 */
enum priorityConstants {
    seL4_InvalidPrio = -1,                      /*Z 无效的或不使用优先级 */
    seL4_MinPrio = 0,                           /*Z 最小优先级0 */
    seL4_MaxPrio = CONFIG_NUM_PRIORITIES - 1    /*Z 最大优先级255 */
};

/* seL4_MessageInfo_t defined in api/shared_types.bf */

enum seL4_MsgLimits {
    seL4_MsgLengthBits = 7,     /*Z seL4_MessageInfo_t中消息长度位数 */
    seL4_MsgExtraCapBits = 2    /*Z seL4_MessageInfo_t中消息extraCaps位数 */
};

enum {
    seL4_MsgMaxLength = 120,    /*Z 消息最大长度，超过的自动丢弃 */
};
#define seL4_MsgMaxExtraCaps (LIBSEL4_BIT(seL4_MsgExtraCapBits)-1)  /*Z 3。最大extraCaps数量 */

/* seL4_CapRights_t defined in shared_types_*.bf */
#define seL4_CapRightsBits 4

typedef enum {
    seL4_NoFailure = 0,
    seL4_InvalidRoot,
    seL4_MissingCapability,
    seL4_DepthMismatch,
    seL4_GuardMismatch,
    SEL4_FORCE_LONG_ENUM(seL4_LookupFailureType),
} seL4_LookupFailureType;
#endif /* !__ASSEMBLER__ */

#ifdef CONFIG_KERNEL_MCS
/* Minimum size of a scheduling context (2^{n} bytes) */
#define seL4_MinSchedContextBits 8      /*Z 最小的调度上下文(对数)。256B，实际上下文大小在x86_64是88B */
#ifndef __ASSEMBLER__
/* the size of a scheduling context, excluding extra refills */
#define seL4_CoreSchedContextBytes (10 * sizeof(seL4_Word) + (6 * 8))
/* the size of a single extra refill */
#define seL4_RefillSizeBytes (2 * 8)

/*
 * @brief Calculate the max extra refills a scheduling context can contain for a specific size.
 *
 * @param  size of the schedulding context. Must be >= seL4_MinSchedContextBits
 * @return the max number of extra refills that can be passed to seL4_SchedControl_Configure for
 *         this scheduling context
 */
static inline seL4_Word seL4_MaxExtraRefills(seL4_Word size)
{
    return (LIBSEL4_BIT(size) -  seL4_CoreSchedContextBytes) / seL4_RefillSizeBytes;
}
#endif /* !__ASSEMBLER__ */
#endif /* CONFIG_KERNEL_MCS */

#ifdef CONFIG_KERNEL_INVOCATION_REPORT_ERROR_IPC
#define DEBUG_MESSAGE_START 6
#define DEBUG_MESSAGE_MAXLEN 50
#endif

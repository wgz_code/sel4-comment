/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#ifdef HAVE_AUTOCONF
#include <autoconf.h>
#endif

/* caps with fixed slot positions in the root CNode */
/*Z 根CNode中有固定位置的能力索引 */
enum {
    seL4_CapNull                =  0, /* null cap *//*Z 空 */
    seL4_CapInitThreadTCB       =  1, /* initial thread's TCB cap *//*Z TCB访问能力 */
    seL4_CapInitThreadCNode     =  2, /* initial thread's root CNode cap *//*Z 访问initrd根CNode的能力 */
    seL4_CapInitThreadVSpace    =  3, /* initial thread's VSpace cap *//*Z 访问VSpace(一级页表)的能力 */
    seL4_CapIRQControl          =  4, /* global IRQ controller cap *//*Z 全局中断控制器访问能力 */
    seL4_CapASIDControl         =  5, /* global ASID controller cap *//*Z ASID(PCID)控制能力 */
    seL4_CapInitThreadASIDPool  =  6, /* initial thread's ASID pool cap *//*Z ASID(PCID) pool能力 */
    seL4_CapIOPortControl       =  7, /* global IO port control cap (null cap if not supported) *//*Z 全局I/O端口控制能力 */
    seL4_CapIOSpace             =  8, /* global IO space cap (null cap if no IOMMU support) *//*Z 全局I/O空间控制能力 */
    seL4_CapBootInfoFrame       =  9, /* bootinfo frame cap *//*Z bootinfo物理页的映射能力 */
    seL4_CapInitThreadIPCBuffer = 10, /* initial thread's IPC buffer frame cap *//*Z IPC buffer页访问能力 */
    seL4_CapDomain              = 11, /* global domain controller cap *//*Z 调度域控制能力 */
#ifdef CONFIG_KERNEL_MCS
    seL4_CapInitThreadSC        = 12, /* initial thread's scheduling context cap *//*Z SC访问能力 */
    seL4_NumInitialCaps         = 13
#else /* CONFIG_KERNEL_MCS *//*Z 后续有对每个cpu进行调度控制、各页表访问、bootinfo页映射、额外bootinfo页映射能力 */
    seL4_NumInitialCaps         = 12    /*Z 初始能力数量；也是自定义能力的第1个可用CSlot索引 */
#endif /* !CONFIG_KERNEL_MCS */
};

/* Legacy code will have assumptions on the vspace root being a Page Directory
 * type, so for now we define one to the other */
#define seL4_CapInitThreadPD seL4_CapInitThreadVSpace
/*Z 能力槽位置索引。word_t */
/* types */
typedef seL4_Word seL4_SlotPos;
/*Z CSlot索引范围（不含尾索引）*/
typedef struct seL4_SlotRegion {
    seL4_SlotPos start; /* first CNode slot position OF region */
    seL4_SlotPos end;   /* first CNode slot position AFTER region */
} seL4_SlotRegion;
/*Z 未映射物理内存描述 */
typedef struct seL4_UntypedDesc {
    seL4_Word  paddr;   /* physical address of untyped cap  *//*Z 物理地址 */
    seL4_Uint8 sizeBits;/* size (2^n) bytes of each untyped *//*Z 大小（位数表示），每个都是2的指数大小 */
    seL4_Uint8 isDevice;/* whether the untyped is a device  *//*Z 是否设备内存 */
    seL4_Uint8 padding[sizeof(seL4_Word) - 2 * sizeof(seL4_Uint8)];
} seL4_UntypedDesc;
/*Z 映射到用户空间的bootinfo，地址放到寄存器中。4K大小，根据配置不一定有 */
typedef struct seL4_BootInfo {
    seL4_Word         extraLen;        /* length of any additional bootinfo information *//*Z 额外的bootinfo大小(实际占用的空间是2^n个页)，0-无 */
    seL4_NodeId       nodeID;          /* ID [0..numNodes-1] of the seL4 node (0 if uniprocessor) *//*Z cpu id */
    seL4_Word         numNodes;        /* number of seL4 nodes (1 if uniprocessor) *//*Z cpu数量 */
    seL4_Word         numIOPTLevels;   /* number of IOMMU PT levels (0 if no IOMMU support) *//*Z IOMMU页表级数 */
    seL4_IPCBuffer   *ipcBuffer;       /* pointer to initial thread's IPC buffer *//*Z initrd的IPC buffer地址 */
    seL4_SlotRegion   empty;           /* empty slots (null caps) *//*Z 启动结束后根CNode中空CSlot索引范围，位于CNode的后部 */
    seL4_SlotRegion   sharedFrames;    /* shared-frame caps (shared between seL4 nodes) *//*Z 未实现 */
    seL4_SlotRegion   userImageFrames; /* userland-image frame caps *//*Z initrd映像页访问能力 */
    seL4_SlotRegion   userImagePaging; /* userland-image paging structure caps *//*Z initrd各页表访问能力(除一级)，能力按级、按虚拟地址升序排列 */
    seL4_SlotRegion   ioSpaceCaps;     /* IOSpace caps for ARM SMMU */
    seL4_SlotRegion   extraBIPages;    /* caps for any pages used to back the additional bootinfo information *//*Z initrd额外bootinfo页访问能力 */
    seL4_Word         initThreadCNodeSizeBits; /* initial thread's root CNode size (2^n slots) *//*Z initrd的根CSlot数量(对数) */
    seL4_Domain       initThreadDomain; /* Initial thread's domain ID *//*Z initrd所属的调度域 */
#ifdef CONFIG_KERNEL_MCS
    seL4_SlotRegion   schedcontrol; /* Caps to sched_control for each node *//*Z 每cpu SchedControl能力CSlot范围 */
#endif
    seL4_SlotRegion   untyped;         /* untyped-object caps (untyped caps) *//*Z 未映射内存列表能力索引范围。一一对应 */
    seL4_UntypedDesc  untypedList[CONFIG_MAX_NUM_BOOTINFO_UNTYPED_CAPS]; /* information about each untyped *//*Z 设备RAM、ROM(1M以下内存均标记为设备内存)，内核及initrd以外的可用RAM */
    /* the untypedList should be the last entry in this struct, in order
     * to make this struct easier to represent in other languages */
} seL4_BootInfo;
/*Z 额外的bootinfo头，每条一个 */
/* If extraLen > 0 then 4K after the start of bootinfo is a region of extraLen additional
 * bootinfo structures. Bootinfo structures are arch/platform specific and may or may not
 * exist in any given execution. */
typedef struct seL4_BootInfoHeader {
    /* identifier of the following chunk. IDs are arch/platform specific */
    seL4_Word id;       /*Z 本条额外bootinfo的类别 */
    /* length of the chunk, including this header */
    seL4_Word len;      /*Z 本条额外bootinfo的大小（含头）*/
} seL4_BootInfoHeader;
/*Z 额外bootinfo类别 */
/* Bootinfo identifiers share a global namespace, even if they are arch or platform specific
 * and are enumerated here */
#define SEL4_BOOTINFO_HEADER_PADDING 0          /*Z padding信息 */
#define SEL4_BOOTINFO_HEADER_X86_VBE 1          /*Z 来自Multiboot的VBE信息 */
#define SEL4_BOOTINFO_HEADER_X86_MBMMAP 2       /*Z 来自Multiboot的RAM区域图 */
#define SEL4_BOOTINFO_HEADER_X86_ACPI_RSDP 3    /*Z 来自ACPI的RSDP表 */
#define SEL4_BOOTINFO_HEADER_X86_FRAMEBUFFER 4  /*Z 来自Multiboot的framebuffer信息 */
#define SEL4_BOOTINFO_HEADER_X86_TSC_FREQ 5 // frequency is in mhz  /*Z 自行计算的TSC计数频率：MHZ */
#define SEL4_BOOTINFO_HEADER_FDT 6
#define SEL4_BOOTINFO_HEADER_NUM SEL4_BOOTINFO_HEADER_FDT + 1


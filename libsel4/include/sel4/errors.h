/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

typedef enum {
    seL4_NoError = 0,
    seL4_InvalidArgument,       /*Z 能力以外的无效参数。.msg[0]指明参数序号 */
    seL4_InvalidCapability,     /*Z 能力无效。.msg[0]指明序号 */
    seL4_IllegalOperation,      /*Z 非法操作，包括页未映射等 */
    seL4_RangeError,            /*Z 范围越界。.msg[0]、[1]分别指明允许的下、上界 */
    seL4_AlignmentError,        /*Z 地址对齐错误 */
    seL4_FailedLookup,          /*Z 能力查找失败。.msg[0]值1指明是源能力、0目标能力，[1]失败类型,[2]以后是有关描述 */
    seL4_TruncatedMessage,      /*Z 系统调用设置的消息参数太少 */
    seL4_DeleteFirst,           /*Z 目标slot被占用 */
    seL4_RevokeFirst,           /*Z 需要先撤销CSlot */
    seL4_NotEnoughMemory,       /*Z seL4_Untyped_Retype()请求的内存不足。.msg[0]指明可用的字节数 */

    /* This should always be the last item in the list
     * so it gives a count of the number of errors in the
     * enum.
     */
    seL4_NumErrors
} seL4_Error;

/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <stdint.h>

#ifdef HAVE_AUTOCONF
#include <autoconf.h>
#endif

#if (defined CONFIG_BENCHMARK_TRACK_KERNEL_ENTRIES || defined CONFIG_DEBUG_BUILD)
/*Z 内核进入点路径(原因) */
/* the following code can be used at any point in the kernel
 * to determine detail about the kernel entry point */
typedef enum {
    Entry_Interrupt,            /*Z #32~159 硬件中断 */
    Entry_UnknownSyscall,       /*Z #160~254 自定义trap */
    Entry_UserLevelFault,       /*Z #0~31(除以下的) 例外 */
    Entry_DebugFault,           /*Z #1 #3 DEBUG */
    Entry_VMFault,              /*Z #14 页错误 */
    Entry_Syscall,
    Entry_UnimplementedDevice,  /*Z #7 无FPU */
#ifdef CONFIG_ARCH_ARM
    Entry_VCPUFault,
#endif
#ifdef CONFIG_ARCH_X86
    Entry_VMExit,
#endif
} entry_type_t;
/*Z 内核进入点信息 */
/**
 * @brief Kernel entry logging
 *
 * Encapsulates useful info about the cause of the kernel entry
 */
typedef struct SEL4_PACKED kernel_entry {
    seL4_Word path: 3;                      /*Z 进入路径(原因) */
    union {
        struct {
            seL4_Word core: 3;
            seL4_Word word: 26;             /*Z 路径有关的信息 */
        };
        /* Tracked kernel entry info filled from outside this file */
        struct {
            seL4_Word syscall_no: 4;
            seL4_Word cap_type: 5;
            seL4_Word is_fastpath: 1;
            seL4_Word invocation_tag: 19;
        };
    };
} kernel_entry_t;

#endif /* CONFIG_BENCHMARK_TRACK_KERNEL_ENTRIES || DEBUG */

#ifdef CONFIG_BENCHMARK_TRACK_KERNEL_ENTRIES

typedef struct benchmark_syscall_log_entry {
    uint64_t  start_time;
    uint32_t  duration;
    kernel_entry_t entry;
} benchmark_track_kernel_entry_t;

#endif /* CONFIG_BENCHMARK_TRACK_KERNEL_ENTRIES || CONFIG_DEBUG_BUILD */


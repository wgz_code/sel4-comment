/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#define SEL4_MULTIBOOT_MAX_MMAP_ENTRIES 50
#define SEL4_MULTIBOOT_RAM_REGION_TYPE 1
/*Z VESA控制器信息 */
typedef struct seL4_VBEInfoBlock {
    char        signature[4];           /*Z "VESA" */
    seL4_Uint16 version;                /*Z 版本 */
    seL4_Uint32 oemStringPtr;           /*Z OEM串指针 */
    seL4_Uint32 capabilities;           /*Z 图形控制器能力 */
    seL4_Uint32 modeListPtr;            /*Z 显示模式列表指针 */
    seL4_Uint16 totalMemory;            /*Z 显存大小，以64KB为单位 */
    seL4_Uint16 oemSoftwareRev;         /*Z 修订版本号 */
    seL4_Uint32 oemVendorNamePtr;       /*Z 厂商名字串指针 */
    seL4_Uint32 oemProductNamePtr;      /*Z 产品名字串指针 */
    seL4_Uint32 oemProductRevPtr;       /*Z 产品修订版本名字串指针 */
    seL4_Uint8  reserved[222];
    seL4_Uint8  oemData[256];
} SEL4_PACKED seL4_VBEInfoBlock_t;
/*Z VESA显示模式信息，版本均需实现 */
/* the seL4_VBEModeInfoBlock struct is split into multiple parts to aid the C parser */
typedef struct seL4_VBEModeInfoCommon {
    seL4_Uint16 modeAttr;               /*Z 显示模式属性 */
    seL4_Uint8  winAAttr;               /*Z 窗口A属性 */
    seL4_Uint8  winBAttr;               /*Z 窗口B属性 */
    seL4_Uint16 winGranularity;         /*Z 窗口粒度 */
    seL4_Uint16 winSize;                /*Z 窗口大小 */
    seL4_Uint16 winASeg;                /*Z 窗口A开始段 */
    seL4_Uint16 winBSeg;                /*Z 窗口B开始段 */
    seL4_Uint32 winFuncPtr;             /*Z 实模式窗口函数指针 */
    seL4_Uint16 bytesPerScanLine;       /*Z 每个扫描行字节数 */
} SEL4_PACKED seL4_VBEModeInfoCommon_t;
/*Z VESA显示模式信息，1.2及以上版本 */
typedef struct  seL4_VBEInfo12Part1 {
    seL4_Uint16 xRes;                   /*Z 水平分辨率（像素或字符）*/
    seL4_Uint16 yRes;                   /*Z 垂直分辨率 */
    seL4_Uint8  xCharSize;              /*Z 像素表示的字符宽度 */
    seL4_Uint8  yCharSize;              /*Z 像素表示的字符高度 */
    seL4_Uint8  planes;                 /*Z 内存层数 */
    seL4_Uint8  bitsPerPixel;           /*Z 每像素位数 */
    seL4_Uint8  banks;                  /*Z 内存槽数 */
    seL4_Uint8  memoryModel;            /*Z 内存型号类型 */
    seL4_Uint8  bankSize;               /*Z 每槽大小（KB）*/
    seL4_Uint8  imagePages;             /*Z 映像页数量 */
    seL4_Uint8  reserved1;
} SEL4_PACKED seL4_VBEInfo12Part1_t;
/*Z 接上 */
typedef struct seL4_VBEInfo12Part2 {
    seL4_Uint8  redLen;                 /*Z 红色掩码位数 */
    seL4_Uint8  redOff;                 /*Z 红色掩码位置 */
    seL4_Uint8  greenLen;               /*Z 绿色掩码位数 */
    seL4_Uint8  greenOff;               /*Z 绿色掩码位置 */
    seL4_Uint8  blueLen;                /*Z 蓝色掩码位数 */
    seL4_Uint8  blueOff;                /*Z 蓝色掩码位置 */
    seL4_Uint8  rsvdLen;                /*Z 保留掩码位数 */
    seL4_Uint8  rsvdOff;                /*Z 保留掩码位置 */
    seL4_Uint8  directColorInfo;  /* direct color mode attributes *//*Z 顔色模式属性 */
} SEL4_PACKED seL4_VBEInfo12Part2_t;
/*Z VESA显示模式信息，2.0及以上版本 */
typedef struct  seL4_VBEInfo20 {
    seL4_Uint32 physBasePtr;            /*Z frame buffer物理地址 */
    seL4_Uint8  reserved2[6];
} SEL4_PACKED seL4_VBEInfo20_t;
/*Z VESA显示模式信息，3.0及以上版本 */
typedef struct seL4_VBEInfo30 {
    seL4_Uint16 linBytesPerScanLine;    /*Z linear模式每扫描行字节数 */
    seL4_Uint8  bnkImagePages;          /*Z banked模式映像页数 */
    seL4_Uint8  linImagePages;          /*Z linear模式映像页数 */
    seL4_Uint8  linRedLen;              /*Z 红色掩码位数(linear模式) */
    seL4_Uint8  linRedOff;              /*Z 红色掩码位置(linear模式) */
    seL4_Uint8  linGreenLen;            /*Z 绿色掩码位数(linear模式) */
    seL4_Uint8  linGreenOff;            /*Z 绿色掩码位置(linear模式) */
    seL4_Uint8  linBlueLen;             /*Z 蓝色掩码位数(linear模式) */
    seL4_Uint8  linBlueOff;             /*Z 蓝色掩码位置(linear模式) */
    seL4_Uint8  linRsvdLen;             /*Z 保留掩码位数(linear模式) */
    seL4_Uint8  linRsvdOff;             /*Z 保留掩码位置(linear模式) */
    seL4_Uint32 maxPixelClock;          /*Z 图形模式最大像素时钟（HZ）*/
    seL4_Uint16 modeId;
    seL4_Uint8  depth;
} SEL4_PACKED seL4_VBEInfo30_t;
/*Z VESA显示模式信息 */
typedef struct seL4_VBEModeInfoBlock {
    /* All VBE revisions */
    seL4_VBEModeInfoCommon_t vbe_common;
    /* VBE 1.2+ */
    seL4_VBEInfo12Part1_t vbe12_part1;
    seL4_VBEInfo12Part2_t vbe12_part2;

    /* VBE 2.0+ */
    seL4_VBEInfo20_t vbe20;

    /* VBE 3.0+ */
    seL4_VBEInfo30_t vbe30;

    seL4_Uint8 reserved3[187];
} SEL4_PACKED seL4_VBEModeInfoBlock_t;
/*Z VESA图形显示信息，拷贝自Multiboot */
typedef struct _seL4_X86_BootInfo_VBE {
    seL4_BootInfoHeader header;         /*Z 额外的bootinfo信息头 */
    seL4_VBEInfoBlock_t vbeInfoBlock;   /*Z 以下拷贝自Multiboot提供的VBE信息 */
    seL4_VBEModeInfoBlock_t vbeModeInfoBlock;
    seL4_Uint32 vbeMode;                /*Z seL4用-1表示未取到VESA信息 */
    seL4_Uint32 vbeInterfaceSeg;
    seL4_Uint32 vbeInterfaceOff;
    seL4_Uint32 vbeInterfaceLen;
} SEL4_PACKED seL4_X86_BootInfo_VBE;
/*Z 与multiboot_mmap相同的结构 */
/**
 * Copy of multiboot mmap fields.
 * https://www.gnu.org/software/grub/manual/multiboot/multiboot.html
 */
typedef struct seL4_X86_mb_mmap {
    uint32_t size; // size of this struct in bytes
    uint64_t base_addr; // physical address of start of this region
    uint64_t length; // length of the region this struct represents in bytes
    uint32_t type; // memory type of region. Type 1 corresponds to RAM.
} SEL4_PACKED seL4_X86_mb_mmap_t;
/*Z 来自Multiboot的RAM区域图 */
typedef struct seL4_X86_BootInfo_mmap {
    seL4_BootInfoHeader header; /*Z 额外的bootinfo信息头 */
    seL4_Uint32 mmap_length;    /*Z 拷贝自Multiboot的mmap_length，RAM区域图大小（字节），指下面数组的大小 */
    seL4_X86_mb_mmap_t mmap[SEL4_MULTIBOOT_MAX_MMAP_ENTRIES];/*Z 拷贝自Muitlboot的multiboot_mmap，RAM区域图 */
} SEL4_PACKED seL4_X86_BootInfo_mmap_t;

typedef struct multiboot2_fb seL4_X86_BootInfo_fb_t;


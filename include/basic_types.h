/*Z OK 主要是内存区域类型定义 */

/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <stdint.h>
#include <arch/types.h>

enum _bool {
    false = 0,
    true  = 1
};
typedef word_t bool_t;

typedef struct region {
    pptr_t start;
    pptr_t end;
} region_t;     /*Z 物理区域在内核空间中的线性映射。就引导时用 */

typedef struct p_region {
    paddr_t start;
    paddr_t end;    /*Z 不含 */
} p_region_t;       /*Z 物理内存地址区域 */

typedef struct v_region {
    vptr_t start;
    vptr_t end;
} v_region_t;   /*Z 虚拟内存地址区域 */

#define REG_EMPTY (region_t){ .start = 0, .end = 0 }        /*Z 起止地址均为0的空内核空间区域 */
#define P_REG_EMPTY (p_region_t){ .start = 0, .end = 0 }    /*Z 起止地址均为0的空物理内存区域 */

/* equivalent to a word_t except that we tell the compiler that we may alias with
 * any other type (similar to a char pointer) *//*Z 这个Gcc属性是防止指针类型大跨度转换 */
typedef word_t __attribute__((__may_alias__)) word_t_may_alias;

/*Z OK */

/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <util.h>
#include <arch/types.h>
#include <arch/machine/registerset.h>
#include <arch/object/structures.h>
/*Z IPC错误类消息寄存器索引转换用的部分错误分类 */
typedef enum {
    MessageID_Syscall,
    MessageID_Exception,
#ifdef CONFIG_KERNEL_MCS
    MessageID_TimeoutReply,
#endif
} MessageID_t;

#ifdef CONFIG_KERNEL_MCS
#define MAX_MSG_SIZE MAX(n_syscallMessage, MAX(n_timeoutMessage, n_exceptionMessage))
#else
#define MAX_MSG_SIZE MAX(n_syscallMessage, n_exceptionMessage)  /*Z 未知系统调用、例外错误消息字段数量的最大值 */
#endif
extern const register_t fault_messages[][MAX_MSG_SIZE] VISIBLE;
/*Z 在TCB上下文中保存寄存器reg的值w */
static inline void setRegister(tcb_t *thread, register_t reg, word_t w)
{   /*Z TCB->架构有关->用户进程上下文->寄存器 */
    thread->tcbArch.tcbContext.registers[reg] = w;
}
/*Z 返回TCB上下文中保存的寄存器值 */
static inline word_t PURE getRegister(tcb_t *thread, register_t reg)
{
    return thread->tcbArch.tcbContext.registers[reg];
}

#ifdef CONFIG_KERNEL_MCS
word_t getNBSendRecvDest(void);
#endif


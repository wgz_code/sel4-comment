/*Z OK 定义汇编函数的宏 */

/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

/* This file contains useful macros for assembly code. */

#ifdef __ASSEMBLER__

/*
 * Use BEGIN_FUNC(), END_FUNC() around assembly functions to annotate them
 * correctly to the assembler.
 *//*Z 全局函数：声明一个全局符号_name，类型是function，开始_name */
#define BEGIN_FUNC(_name) \
    .global _name ; \
    .type _name, %function ; \
_name:
/*Z 指示_name的大小是函数体大小 */
#define END_FUNC(_name) \
    .size _name, .-_name

/*
 * BEGIN_FUNC_STATIC() and END_FUNC_STATIC() do as above, but without making a
 * global declaration. (c.f. static functions in C).
 *//*Z 静态函数：声明一个function类型符号_name，开始_name */
#define BEGIN_FUNC_STATIC(_name) \
    .type _name, %function ; \
_name:
/*Z 指示_name的大小是函数体大小 */
#define END_FUNC_STATIC(_name) \
    .size _name, .-_name

#else /* !__ASSEMBLER__ */
#warning "Including assembly-specific header in C code"
#endif




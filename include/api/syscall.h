/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h> // for arch/api/syscall.h
#include <machine.h>
#include <api/failures.h>
#include <model/statedata.h>
#include <kernel/vspace.h>
#include <arch/api/syscall.h>
#include <api/debug.h>
/*Z ticks_t的机器字数。64位的1，32位的2 */
#define TIME_ARG_SIZE (sizeof(ticks_t) / sizeof(word_t))

#ifdef CONFIG_KERNEL_MCS
/*Z 记账运行时间。如果没有预算了，置线程重启状态(以等待充值预算的到来) */
#define MCS_DO_IF_BUDGET(_block) \
    updateTimestamp(); \
    if (likely(checkBudgetRestart())) { \
        _block \
    }
#else
#define MCS_DO_IF_BUDGET(_block) \
    { \
        _block \
    }
#endif

exception_t handleSyscall(syscall_t syscall);
exception_t handleInterruptEntry(void);
exception_t handleUnknownSyscall(word_t w);
exception_t handleUserLevelFault(word_t w_a, word_t w_b);/*Z 处理<32的其它例外(中断)：第1个参数中断向量号，第2个参数错误码，将例外信息发送给当前线程的错误处理对象，线程阻塞并等待处理结果，引入调度 */
exception_t handleVMFaultEvent(vm_fault_type_t vm_faultType);/*Z 处理页错误：将错误发送给当前线程的错误处理对象，线程阻塞并等待处理结果，引入调度 */
/*Z 返回当前线程第i个消息寄存器(IPC buffer)字 */
static inline word_t PURE getSyscallArg(word_t i, word_t *ipc_buffer)
{
    if (i < n_msgRegisters) {
        return getRegister(NODE_STATE(ksCurThread), msgRegisters[i]);
    }

    assert(ipc_buffer != NULL);
    return ipc_buffer[i + 1];
}

extern extra_caps_t current_extra_caps;


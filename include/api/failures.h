/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <types.h>
#include <sel4/errors.h>
#include <sel4/constants.h>
/* These datatypes differ markedly from haskell, due to the
 * different implementation of the various fault monads */


enum exception {
    EXCEPTION_NONE,         /*Z 无例外 */
    EXCEPTION_FAULT,        /*Z 其它错误 */
    EXCEPTION_LOOKUP_FAULT, /*Z 查询错误 */
    EXCEPTION_SYSCALL_ERROR,/*Z 系统调用错误 */
    EXCEPTION_PREEMPTED     /*Z 抢占点错误。指示内核连续处理时间太长了，该处理中断了 */
};
typedef word_t exception_t; /*Z 例外类型 */

typedef word_t syscall_error_type_t;    /*Z 系统调用错误类型 */

struct syscall_error {
    word_t invalidArgumentNumber;   /*Z 无效的参数数量(序号) */
    word_t  invalidCapNumber;       /*Z 无效的能力数量 */
    word_t rangeErrorMin;           /*Z 要求的范围最小值 */
    word_t rangeErrorMax;           /*Z 要求的范围最大值 */
    word_t memoryLeft;              /*Z 可用的空闲内存(分配内存时) */
    bool_t failedLookupWasSource;   /*Z 是否IPC的源方查询失败 */

    syscall_error_type_t type;      /*Z 错误类型 */
};
typedef struct syscall_error syscall_error_t;/*Z 系统调用错误 */

#ifdef CONFIG_KERNEL_INVOCATION_REPORT_ERROR_IPC
struct debug_syscall_error {
    word_t errorMessage[DEBUG_MESSAGE_MAXLEN];
};
typedef struct debug_syscall_error debug_syscall_error_t;

extern debug_syscall_error_t current_debug_error;
#endif
extern lookup_fault_t current_lookup_fault;
extern seL4_Fault_t current_fault;
extern syscall_error_t current_syscall_error;


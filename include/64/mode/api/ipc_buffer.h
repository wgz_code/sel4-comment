/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <types.h>
#include <api/syscall.h>
/*Z 返回当前线程第i个消息寄存器(IPC buffer)字 */
static inline time_t mode_parseTimeArg(word_t i, word_t *buffer)
{
    return getSyscallArg(i, buffer);
}
/*Z 设置接收者的消息寄存器值，或写在其IPC buffer的+1字偏移处，返回i+1 */
static inline word_t mode_setTimeArg(word_t i, time_t time, word_t *buffer, tcb_t *thread)
{
    return setMR(thread, buffer, i, time);
}

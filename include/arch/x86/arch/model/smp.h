/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h>
#include <arch/types.h>
#include <arch/model/statedata.h>
#include <model/statedata.h>
#include <model/smp.h>
#include <mode/model/smp.h>

#ifdef ENABLE_SMP_SUPPORT
/*Z cpu索引到真正cpuid的映射 */
typedef struct cpu_id_mapping {
    cpu_id_t index_to_cpu_id[CONFIG_MAX_NUM_NODES];
/*Z 逻辑id用于IPI目的寻址方式为逻辑模式时的id比较 */
#ifdef CONFIG_USE_LOGICAL_IDS
    logical_id_t index_to_logical_id[CONFIG_MAX_NUM_NODES];/*Z cpu索引到逻辑id的映射 */
    word_t other_indexes_in_cluster[CONFIG_MAX_NUM_NODES];/*Z 同一cluster中cpu索引位图(不含自身)，每cpu一个 */
#endif /* CONFIG_USE_LOGICAL_IDS */
} cpu_id_mapping_t;
/*Z cpu索引到真正cpu id的映射 */
extern cpu_id_mapping_t cpu_mapping;
/*Z 返回cpu索引对应的真正cpu id */
static inline cpu_id_t cpuIndexToID(word_t index)
{
    return cpu_mapping.index_to_cpu_id[index];
}

static inline PURE word_t getCurrentCPUID(void)
{
    return cpu_mapping.index_to_cpu_id[getCurrentCPUIndex()];
}
/*Z 原子地将new_val(是个地址)写入ptr指向的内存，并将其原值写入prev指向的内存 */
static inline bool_t try_arch_atomic_exchange_rlx(void *ptr, void *new_val, void **prev)
{
    *prev = __atomic_exchange_n((void **) ptr, new_val, __ATOMIC_RELAXED);
    return true;
}

#endif /* ENABLE_SMP_SUPPORT */

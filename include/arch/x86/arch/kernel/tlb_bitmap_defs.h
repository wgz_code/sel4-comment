/*Z OK 利用页表项存储当前TLB对应的CPU */

/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h>

#ifdef ENABLE_SMP_SUPPORT
/* Bit zero for entries in PD are present bits and should always be 0 */
#define TLBBITMAP_ENTRIES_PER_ROOT (wordBits - 1)   /*Z 63。1:63位表示各cpu，0位恒0(防止MMU访问) */

/* Number of entries in PD reserved for TLB bitmap */
#define TLBBITMAP_ROOT_ENTRIES (((CONFIG_MAX_NUM_NODES - 1) / TLBBITMAP_ENTRIES_PER_ROOT) + 1)  /*Z 配置的cpu数所需要的页表项数 */
#define TLBBITMAP_RESERVED_VSPACE (TLBBITMAP_ROOT_ENTRIES * BIT(TLBBITMAP_ROOT_ENTRY_SIZE))/*Z 应该是TLB(cpu)位图占用的虚拟内存空间大小，未找到SIZE定义???? */
#define TLBBITMAP_ROOT_INDEX GET_VSPACE_ROOT_INDEX(TLBBITMAP_PPTR)  /*Z TLB(cpu)位图在一级页表中的起始索引 */
/*Z 保留的TLB cpu位图项可用位数 */
/* Total number of usable bits in TLBbitmap */
#define TLBBITMAP_ROOT_BITS (TLBBITMAP_ENTRIES_PER_ROOT * TLBBITMAP_ROOT_ENTRIES)

#define TLBBITMAP_ROOT_MAKE_INDEX(_cpu) (TLBBITMAP_ROOT_INDEX + ((_cpu) / TLBBITMAP_ENTRIES_PER_ROOT))/*Z 指定cpu的TLB位图在一级页表中的索引 */
#define TLBBITMAP_ROOT_MAKE_BIT(_cpu) BIT(((_cpu) % TLBBITMAP_ENTRIES_PER_ROOT) + 1)/*Z 指定cpu在TLB位图字中的索引 */

#else
#define TLBBITMAP_ROOT_ENTRIES 0
#endif /* ENABLE_SMP_SUPPORT */



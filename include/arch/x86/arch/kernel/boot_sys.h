/*Z OK 通过boot loader、BIOS等获取的各类引导信息 */

/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <arch/kernel/multiboot.h>
#include <arch/kernel/multiboot2.h>
/*Z 各类引导信息 */
/* type definitions (directly corresponding to abstract specification) */
typedef struct boot_state {
    p_region_t   avail_p_reg; /* region of available physical memory on platform *//*Z x86未用此字段 */
    p_region_t   ki_p_reg;    /* region where the kernel image is in *//*Z linker加载的内核映像地址范围 */
    ui_info_t    ui_info;     /* info about userland images *//*Z 第1个加载模块(initrd)的最终加载信息(尾页对齐) */
    uint32_t     num_ioapic;  /* number of IOAPICs detected *//*Z ACPI提供的I/O APIC中断控制器数量 */
    paddr_t      ioapic_paddr[CONFIG_MAX_NUM_IOAPIC];/*Z ACPI提供的I/O APIC中断控制器物理地址列表 */
    uint32_t     num_drhu; /* number of IOMMUs *//*Z ACPI提供的IOMMU数量 */
    paddr_t      drhu_list[MAX_NUM_DRHU]; /* list of physical addresses of the IOMMUs *//*Z ACPI提供的IOMMU寄存器组起始物理地址列表 */
    acpi_rmrr_list_t rmrr_list;/*Z ACPI提供的IOMMU管理下的用于老旧设备的DMA保留内存区列表，每设备一项 */
    acpi_rsdp_t  acpi_rsdp; /* copy of the rsdp *//*Z BIOS的ACPI-RSDP表 */
    paddr_t      mods_end_paddr; /* physical address where boot modules end *//*Z boot loader加载的模块最大结束地址 */
    paddr_t      boot_module_start; /* physical address of first boot module *//*Z boot loader加载的第一个模块开始地址(initrd) */
    uint32_t     num_cpus;    /* number of detected cpus *//*Z ACPI提供的cpu数量 */
    uint32_t     mem_lower;   /* lower memory size for boot code of APs to run in real mode *//*Z boot loader提供的640K以下内存的实际大小（KB）*/
    cpu_id_t     cpus[CONFIG_MAX_NUM_NODES];/*Z ACPI提供的cpu id列表 */
    mem_p_regs_t mem_p_regs;  /* physical memory regions *//*Z Multiboot提供的类型1内存区域信息。1M以上可用物理内存 */
    seL4_X86_BootInfo_VBE vbe_info; /* Potential VBE information from multiboot *//*Z Multiboot1提供的图形显示信息。不好 */
    seL4_X86_BootInfo_mmap_t mb_mmap_info; /* memory map information from multiboot *//*Z Multiboot提供的内存区域信息。原封不动 */
    seL4_X86_BootInfo_fb_t fb_info; /* framebuffer information as set by bootloader *//*Z Multiboot2提供的frame buffer */
} boot_state_t;
/*Z 从boot loader、BIOS等获取的各类引导信息 */
extern boot_state_t boot_state;

void boot_sys(
    unsigned long multiboot_magic,
    void *multiboot
);


/*Z OK GNU Multiboot2规范 */

/*
 * Copyright 2017, Genode Labs GmbH
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once
/*Z Multiboot2 boot loader通过EAX传递给OS的记号 */
#define MULTIBOOT2_MAGIC 0x36d76289

#include <types.h>
/*Z Multiboot2 boot infomation起始信息 */
typedef struct multiboot2_header {
    uint32_t total_size;            /*Z 所有boot information总大小 */
    uint32_t unknown;               /*Z 保留 */
} PACKED multiboot2_header_t;
/*Z 信息头，8字节对齐 */
typedef struct multiboot2_tag {
    uint32_t type;                  /*Z 信息分类。也是type=0,size=8表示信息结束 */
    uint32_t size;                  /*Z 包括头在内的信息大小（不包括8字节对齐的padding） */
} PACKED multiboot2_tag_t;
/*Z 内存区域图信息。在信息头和本结构之间还有两个u32大小的entry_size和entry_version */
typedef struct multiboot2_memory {
    uint64_t addr;                  /*Z 起始物理地址 */
    uint64_t size;                  /*Z 大小 */
    uint32_t type;                  /*Z 1-可用RAM，3-ACPI，4-hibernation，5-缺陷，其它-保留 */
    uint32_t reserved;
} PACKED multiboot2_memory_t;
/*Z 加载的模块信息 */
typedef struct multiboot2_module {
    uint32_t start;                 /*Z 起始物理地址 */
    uint32_t end;                   /*Z 结束地址 */
    char     string [1];            /*Z 模块名字符串 */
} PACKED multiboot2_module_t;
/*Z framebuffer信息 */
typedef struct multiboot2_fb {
    uint64_t addr;                  /*Z 帧缓冲区物理地址 */
    uint32_t pitch;                 /*Z 字节大小的pitch */
    uint32_t width;                 /*Z 单位：像素或字符 */
    uint32_t height;
    uint8_t  bpp;                   /*Z 每像素位数 */
    uint8_t  type;                  /*Z 1-RGB，2-文本模式。根据模式不同，后面可能还有信息 */
} PACKED multiboot2_fb_t;
/*Z Multiboot2传递给OS的信息分类 */
enum multiboot2_tags {
    MULTIBOOT2_TAG_END     = 0,     /*Z 结束标记 */
    MULTIBOOT2_TAG_CMDLINE = 1,     /*Z 命令行字符串 */
    MULTIBOOT2_TAG_MODULE  = 3,     /*Z 加载的模块 */
    MULTIBOOT2_TAG_MEMORY  = 6,     /*Z 内存区域图 */
    MULTIBOOT2_TAG_FB      = 8,     /*Z framebuffer */
    MULTIBOOT2_TAG_ACPI_1  = 14,    /*Z ACPI 1.0 */
    MULTIBOOT2_TAG_ACPI_2  = 15,    /*Z ACPI 2.0 ~ */
};


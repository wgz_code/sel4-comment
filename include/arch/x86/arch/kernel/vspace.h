/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h>
#include <types.h>
#include <api/failures.h>
#include <object/structures.h>
#include <plat/machine.h>
/*Z ASID是ARM的叫法，Intel叫PCID(Process-context identifier)，用于在TLB中区分进程 */
#define IT_ASID 1 /* initial thread's ASID */


struct lookupPTSlot_ret {
    exception_t status;
    pte_t      *ptSlot;
};
typedef struct lookupPTSlot_ret lookupPTSlot_ret_t;/*Z 查四级页表项结果 */

struct lookupPDSlot_ret {
    exception_t status;
    pde_t      *pdSlot;
};
typedef struct lookupPDSlot_ret lookupPDSlot_ret_t;/*Z 查三级页表项结果 */

struct findVSpaceForASID_ret {
    exception_t status;
    vspace_root_t *vspace_root; /*Z 一级页表线性地址 */
};
typedef struct findVSpaceForASID_ret findVSpaceForASID_ret_t;/*Z 查找根VSpace的结果 */

void init_boot_pd(void);
void enable_paging(void);
bool_t map_kernel_window(
    uint32_t num_ioapic,
    paddr_t   *ioapic_paddrs,
    uint32_t   num_drhu,
    paddr_t   *drhu_list
);
bool_t map_skim_window(vptr_t skim_start, vptr_t skim_end);
bool_t map_kernel_window_devices(
    pte_t *pt,
    uint32_t num_ioapic,
    paddr_t   *ioapic_paddrs,
    uint32_t   num_drhu,
    paddr_t   *drhu_list
);

void init_tss(tss_t *tss);
void init_gdt(gdt_entry_t *gdt, tss_t *tss);
void init_idt_entry(idt_entry_t *idt, interrupt_t interrupt, void(*handler)(void));
vspace_root_t *getValidNativeRoot(cap_t vspace_cap);
pde_t *get_boot_pd(void);
void *map_temp_boot_page(void *entry, uint32_t large_pages);
bool_t init_vm_state(void);
void init_dtrs(void);
void map_it_pt_cap(cap_t vspace_cap, cap_t pt_cap);
void map_it_pd_cap(cap_t vspace_cap, cap_t pd_cap);
void map_it_frame_cap(cap_t vspace_cap, cap_t frame_cap);
void write_it_asid_pool(cap_t it_ap_cap, cap_t it_vspace_cap);
bool_t init_pat_msr(void);
cap_t create_it_address_space(cap_t root_cnode_cap, v_region_t it_v_reg);

/* ==================== BOOT CODE FINISHES HERE ==================== */

void idle_thread(void);
#define idleThreadStart (&idle_thread)  /*Z idle线程 */

bool_t isVTableRoot(cap_t cap);
/*Z 在全局pool中查找ASID(PCID)对应的映射项 */
asid_map_t findMapForASID(asid_t asid);

lookupPTSlot_ret_t lookupPTSlot(vspace_root_t *vspace, vptr_t vptr);/*Z 查找虚拟地址的三级页表项地址 */
lookupPDSlot_ret_t lookupPDSlot(vspace_root_t *vspace, vptr_t vptr);/*Z 查找虚拟地址的三级页表项地址 */
void copyGlobalMappings(vspace_root_t *new_vspace);
word_t *PURE lookupIPCBuffer(bool_t isReceiver, tcb_t *thread);/*Z 查找指定线程的IPC buffer线性地址。isReceiver指示该线程是否为接收者 */
exception_t handleVMFault(tcb_t *thread, vm_fault_type_t vm_faultType);/*Z 设置全局错误变量为页错误并返回EXCEPTION_FAULT */
void unmapPageDirectory(asid_t asid, vptr_t vaddr, pde_t *pd);/*Z 在二级页表中删除三级页表pd所在的项，失效相关(TLB位图指示)的TLB和paging缓存 */
void unmapPageTable(asid_t, vptr_t vaddr, pte_t *pt);/*Z 在三级页表中删除四级页表pt所在的项，失效相关(TLB位图指示)的TLB和paging缓存 */

exception_t performASIDPoolInvocation(asid_t asid, asid_pool_t *poolPtr, cte_t *vspaceCapSlot);
exception_t performASIDControlInvocation(void *frame, cte_t *slot, cte_t *parent, asid_t asid_base);
void hwASIDInvalidate(asid_t asid, vspace_root_t *vspace);/*Z 失效TLB cpu位图指定的cpu上ASID(PCID)所属的TLB和paging缓存，如果不是当前在用页表还清除TLB位图中的当前cpu位，等待所有的核处理完毕 */
void deleteASIDPool(asid_t asid_base, asid_pool_t *pool);/*Z 置空指定ASID(PCID) pool，失效所属的TLB和paging缓存 */
void deleteASID(asid_t asid, vspace_root_t *vspace);/*Z 置空ASID(PCID) pool中的相关项，清缓存 */
findVSpaceForASID_ret_t findVSpaceForASID(asid_t asid);/*Z 在全局pool中查找ASID(PCID)对应的一级页表线性地址 */
/*Z 清除内存页的页表映射，清缓存。page_size页大小，vptr该页虚拟地址，pptr该页线性地址 */
void unmapPage(vm_page_size_t page_size, asid_t asid, vptr_t vptr, void *pptr);
/* returns whether the translation was removed and needs to be flushed from the hardware (i.e. tlb) */
/*Z 清除内存页的页表映射。page_size页大小，vroot一级页表线性地址，vptr该页虚拟地址，pptr该页线性地址 */
bool_t modeUnmapPage(vm_page_size_t page_size, vspace_root_t *vroot, vptr_t vptr, void *pptr);
exception_t decodeX86ModeMapPage(word_t invLabel, vm_page_size_t page_size, cte_t *cte, cap_t cap,
                                 vspace_root_t *vroot, vptr_t vptr, paddr_t paddr, vm_rights_t vm_rights, vm_attributes_t vm_attr);
void setVMRoot(tcb_t *tcb);/*Z 把线程的页表设置为当前页表 */
bool_t CONST isValidVTableRoot(cap_t cap);
bool_t CONST isValidNativeRoot(cap_t cap);/*Z 能力是否一级页表访问能力且已映射(有效) */
exception_t checkValidIPCBuffer(vptr_t vptr, cap_t cap);
vm_rights_t CONST maskVMRights(vm_rights_t vm_rights, seL4_CapRights_t cap_rights_mask);
void flushTable(vspace_root_t *vspace, word_t vptr, pte_t *pt, asid_t asid);/*Z 如果vspace属于当前线程，则失效TLB位图指定cpu上其所有四级页表项对应的TLB和页缓存 */

exception_t decodeX86MMUInvocation(word_t invLabel, word_t length, cptr_t cptr, cte_t *cte,
                                   cap_t cap, extra_caps_t excaps, word_t *buffer);

exception_t decodeX86ModeMMUInvocation(word_t invLabel, word_t length, cptr_t cptr, cte_t *cte,
                                       cap_t cap, extra_caps_t excaps, word_t *buffer);

exception_t decodeIA32PageDirectoryInvocation(word_t invLabel, word_t length, cte_t *cte, cap_t cap,
                                              extra_caps_t excaps, word_t *buffer);

/* common functions for x86 */
exception_t decodeX86FrameInvocation(word_t invLabel, word_t length, cte_t *cte, cap_t cap, extra_caps_t excaps,
                                     word_t *buffer);

uint32_t CONST WritableFromVMRights(vm_rights_t vm_rights);
uint32_t CONST SuperUserFromVMRights(vm_rights_t vm_rights);

/* the following functions have the same names, but different
 * implementations for 32-bit and 64-bit.
 */

pte_t CONST makeUserPTE(paddr_t paddr, vm_attributes_t vm_attr, vm_rights_t vm_rights);
pte_t CONST makeUserPTEInvalid(void);/*Z 创建一个全0的页表项 */
pde_t CONST makeUserPDELargePage(paddr_t paddr, vm_attributes_t vm_attr, vm_rights_t vm_rights);
pde_t CONST makeUserPDEPageTable(paddr_t paddr, vm_attributes_t vm_attr);
pde_t CONST makeUserPDEInvalid(void);/*Z 生成一个全0三级页表项 */


#ifdef CONFIG_PRINTING
void Arch_userStackTrace(tcb_t *tptr);/*Z 打印线程的栈内容 */
#endif
/*Z 检查字是否对齐枚举类型指代的页大小 */
static inline bool_t checkVPAlignment(vm_page_size_t sz, word_t w)
{
    return IS_ALIGNED(w, pageBitsForSize(sz));
}


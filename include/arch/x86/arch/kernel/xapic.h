/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h>
#include <plat_mode/machine/hardware.h>

#ifdef CONFIG_XAPIC
typedef enum _apic_reg_t {
    APIC_ID             = 0x020,
    APIC_VERSION        = 0x030,    /*Z APIC内存Version Register */
    APIC_TASK_PRIO      = 0x080,
    APIC_ARBITR_PRIO    = 0x090,
    APIC_PROC_PRIO      = 0x0A0,
    APIC_EOI            = 0x0B0,    /*Z 中断服务完成寄存器。32位，应该是写任何值都可以 */
    APIC_LOGICAL_DEST   = 0x0D0,    /*Z Logical Destination Register(LDR)，标识本APIC逻辑ID。32位中的高8位 */
    APIC_DEST_FORMAT    = 0x0E0,    /*Z Destination Format Register(DFR)，标识APIC逻辑ID格式。32位中的高4位：全1-flat 全0-cluster */
    APIC_SVR            = 0x0F0,    /*Z APIC内存Spurious-Interrupt Vector Register(SVR)，与任务和中断优先级有关 */
    APIC_ISR_BASE       = 0x100,
    APIC_TMR_BASE       = 0x180,
    APIC_IRR_BASE       = 0x200,    /*Z 中断请求寄存器(pending)。32位 x 8 */
    APIC_ERR_STATUS     = 0x280,
    APIC_ICR1           = 0x300,    /*Z 中断命令寄存器（低32位）。此寄存器写完即开始发送中断 */
    APIC_ICR2           = 0x310,    /*Z 中断命令寄存器（高32位）*/
    APIC_LVT_TIMER      = 0x320,    /*Z APIC内存LVT timer register，配置APIC定时器模式 */
    APIC_LVT_THERMAL    = 0x330,
    APIC_LVT_PERF_CNTR  = 0x340,
    APIC_LVT_LINT0      = 0x350,    /*Z APIC内存LVT LINT0 Register，配置LINT0引脚中断 */
    APIC_LVT_LINT1      = 0x360,    /*Z APIC内存LVT LINT1 Register，配置LINT1引脚中断 */
    APIC_LVT_ERROR      = 0x370,
    APIC_TIMER_COUNT    = 0x380,    /*Z APIC内存Initial Count Register，配置本APIC定时器初值 */
    APIC_TIMER_CURRENT  = 0x390,    /*Z APIC内存Current Count Register，本APIC定时器当前计数值 */
    APIC_TIMER_DIVIDE   = 0x3E0     /*Z APIC内存Divide Configuration Register，配置本APIC定时器分频因子 */
} apic_reg_t;

#define XAPIC_LDR_SHIFT             24          /*Z APIC逻辑ID位偏移量：LDR >> 24 */
#define XAPIC_DFR_FLAT              0xFFFFFFFF  /*Z APIC逻辑ID为平坦格式：LDR中每位代表一个APIC */
/*Z 获取APIC内存reg偏移处的值，u32 */
static inline uint32_t apic_read_reg(apic_reg_t reg)
{
    return *(volatile uint32_t *)(PPTR_APIC + reg);
}
/*Z 向APIC内存reg偏移处写入值val */
static inline void apic_write_reg(apic_reg_t reg, uint32_t val)
{
    *(volatile uint32_t *)(PPTR_APIC + reg) = val;
}
/*Z 获取逻辑cpu id */
static inline logical_id_t apic_get_logical_id(void)
{
    return apic_read_reg(APIC_LOGICAL_DEST) >> XAPIC_LDR_SHIFT;
}
/*Z seL4在xAPIC时未使用cluster结构，因此总是返回0 */
static inline word_t apic_get_cluster(logical_id_t logical_id)
{
    return 0; /* always return 0 as 'init_xapic_ldr' uses flat cluster */
}

static inline void apic_write_icr(word_t high, word_t low)
{
    apic_write_reg(APIC_ICR2, high);
    apic_write_reg(APIC_ICR1, low);
}

#define IPI_ICR_BARRIER  asm volatile("" ::: "memory")  /*Z xapic为编译器内存屏障 */
#define IPI_MEM_BARRIER IPI_ICR_BARRIER                 /*Z xapic为编译器内存屏障 */
#endif  /* CONFIG_XAPIC */


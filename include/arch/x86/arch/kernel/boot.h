/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h>
#include <types.h>
#include <plat/machine/acpi.h>
#include <kernel/boot.h>
#include <sel4/arch/bootinfo_types.h>
/*Z 内存区域列表 */
typedef struct mem_p_regs {
    word_t count;                           /*Z 区域数量 */
    p_region_t list[MAX_NUM_FREEMEM_REG];   /*Z 每个区域的起止地址 */
} mem_p_regs_t;
/*Z init程序最终加载信息 */
typedef struct ui_info {
    p_region_t p_reg;     /* region where the userland image lies in *//*Z 最终加载物理区域 */
    sword_t    pv_offset; /* UI virtual address + pv_offset = UI physical address *//*Z 最终加载地址与ELF虚拟地址的偏移量 */
    vptr_t     v_entry;   /* entry point (virtual address) of userland image *//*Z ELF中的入口点地址 */
} ui_info_t;

cap_t create_unmapped_it_frame_cap(pptr_t pptr, bool_t use_large);/*Z 为物理页创建不能映射的能力 */
cap_t create_mapped_it_frame_cap(cap_t pd_cap, pptr_t pptr, vptr_t vptr, asid_t asid, bool_t use_large,
                                 bool_t executable);/*Z 为物理页创建映射能力，建立普通权限的页表项 */

bool_t init_sys_state(
    cpu_id_t      cpu_id,
    mem_p_regs_t  *mem_p_regs,
    ui_info_t     ui_info,
    p_region_t    boot_mem_reuse_p_reg,
    /* parameters below not modeled in abstract specification */
    uint32_t      num_drhu,
    paddr_t      *drhu_list,
    acpi_rmrr_list_t *rmrr_list,
    acpi_rsdp_t      *acpi_rsdp,
    seL4_X86_BootInfo_VBE *vbe,
    seL4_X86_BootInfo_mmap_t *mb_mmap,
    seL4_X86_BootInfo_fb_t *fb_info
);

bool_t init_cpu(
    bool_t   mask_legacy_irqs
);

bool_t add_allocated_p_region(p_region_t reg);
void init_allocated_p_regions(void);


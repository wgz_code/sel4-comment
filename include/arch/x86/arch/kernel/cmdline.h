/*Z OK 内核启动参数 */

/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */
#pragma once
#include <config.h>
/*Z 内核启动参数，实际只有一个是否禁用IOMMU */
typedef struct cmdline_opt {
#ifdef CONFIG_PRINTING
    uint16_t console_port;  /*Z 内核打印的控制台端口，默认0x3f8 */
#endif
#ifdef CONFIG_DEBUG_BUILD
    uint16_t debug_port;    /*Z 基本同上 */
#endif
    bool_t   disable_iommu; /*Z 默认启用IOMMU */
} cmdline_opt_t;

void cmdline_parse(const char *cmdline, cmdline_opt_t *cmdline_opt);


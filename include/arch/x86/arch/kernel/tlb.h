/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <arch/smp/ipi_inline.h>
/*Z 失效掩码指定的cpu页缓存，根据配置也可能失效ASID(PCID)相关的TLB */
static inline void invalidatePageStructureCacheASID(paddr_t root, asid_t asid, word_t mask)
{   /*Z 失效页缓存，根据配置也可能失效ASID(PCID)相关的TLB */
    invalidateLocalPageStructureCacheASID(root, asid);
    SMP_COND_STATEMENT(doRemoteInvalidatePageStructureCacheASID(root, asid, mask));
}

static inline void invalidateTranslationSingle(vptr_t vptr, word_t mask)
{
    invalidateLocalTranslationSingle(vptr);
    SMP_COND_STATEMENT(doRemoteInvalidateTranslationSingle(vptr, mask));
}
/*Z 失效掩码指定的cpu上指定ASID(PCID)指定虚拟地址TLB和页缓存IPI，等待所有的核处理完毕 */
static inline void invalidateTranslationSingleASID(vptr_t vptr, asid_t asid, word_t mask)
{   /*Z 失效指定ASID(PCID)的虚拟地址的TLB和paging缓存 */
    invalidateLocalTranslationSingleASID(vptr, asid);
    SMP_COND_STATEMENT(doRemoteInvalidateTranslationSingleASID(vptr, asid, mask));
}

static inline void invalidateTranslationAll(word_t mask)
{
    invalidateLocalTranslationAll();
    SMP_COND_STATEMENT(doRemoteInvalidateTranslationAll(mask));
}



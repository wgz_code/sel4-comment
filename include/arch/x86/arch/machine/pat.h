/*Z OK PAGE ATTRIBUTE TABLE - 控制页缓存模式，如写通、写回等，页表项中的PAT、PCD、PWT位可通过IA32_PAT MSR选择页属性 */

/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#define IA32_PAT_MSR            0x277       /*Z 页属性。供页表项选择物理内存属性 */

#define IA32_PAT_MT_UNCACHEABLE     0x00    /*Z 不能缓存。适用于MMIO */
#define IA32_PAT_MT_WRITE_COMBINING 0x01    /*Z 写合并，无缓存。在专门暂存区积累后再写，适用于视频 */
#define IA32_PAT_MT_WRITE_THROUGH   0x04    /*Z 写通，有缓存。写时一并写内存 */
#define IA32_PAT_MT_WRITE_PROTECTED 0x05    /*Z 写传播，有缓存。写时无效所有核的cacheline */
#define IA32_PAT_MT_WRITE_BACK      0x06    /*Z 写回，有缓存。延迟写内存 */
#define IA32_PAT_MT_UNCACHED        0x07    /*Z 无缓存，但可改为写合并 */


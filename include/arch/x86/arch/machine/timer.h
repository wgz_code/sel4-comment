/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h>
#include <arch/model/statedata.h>
#include <plat/machine.h>

#ifdef CONFIG_KERNEL_MCS
#include <mode/util.h>
#include <arch/kernel/xapic.h>
/*Z 获取内核WCET(猜测是“最坏情况下执行时间”：线程调度的内核用时，从而不能计算到线程时间消耗中)：10us */
static inline CONST time_t getKernelWcetUs(void)
{
    return  10u;
}
/*Z us转换成tick(TSC计数) */
static inline PURE ticks_t usToTicks(time_t us)
{
    assert(x86KStscMhz > 0);
    return us * x86KStscMhz;
}
/*Z 8字节能表示的最大微秒值 */
static inline PURE time_t getMaxUsToTicks(void)
{
    return div64(UINT64_MAX, x86KStscMhz);
}
/*Z 1us的TICKs */
static inline PURE ticks_t getTimerPrecision(void)
{
    return usToTicks(1u);
}
/*Z 空函数 */
static inline void ackDeadlineIRQ(void)
{
}
/*Z 读TSC计数值 */
static inline ticks_t getCurrentTime(void)
{
    return x86_rdtsc();
}
/*Z UINT64_MAX */
static inline CONST ticks_t getMaxTicksToUs(void)
{
    return UINT64_MAX;
}
/*Z TICKs转换成微秒 */
static inline PURE time_t ticksToUs(ticks_t ticks)
{
    return div64(ticks, x86KStscMhz);
}
/*Z 设定TSC deadline */
static inline void setDeadline(ticks_t deadline)
{
    assert(deadline > NODE_STATE(ksCurTime));
    if (likely(x86KSapicRatio == 0)) {
        x86_wrmsr(IA32_TSC_DEADLINE_MSR, deadline);
    } else {
        /* convert deadline from tscKhz to apic khz */
        deadline -= getCurrentTime();
        apic_write_reg(APIC_TIMER_COUNT, div64(deadline, x86KSapicRatio));
    }
}
#else
/*Z 空函数 */
static inline void resetTimer(void)
{
    /* nothing to do */
}
#endif /* CONFIG_KERNEL_MCS */

/*Z 获取TSC的计数频率：MHZ */
BOOT_CODE uint32_t tsc_init(void);


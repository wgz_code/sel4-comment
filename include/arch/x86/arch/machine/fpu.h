/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h>
#include <types.h>
#include <arch/object/vcpu.h>
#include <object/structures.h>
#include <model/statedata.h>
#include <arch/machine/cpu_registers.h>

#define MXCSR_INIT_VALUE               0x1f80       /*Z SSE指令控制寄存器的初值 */
#define XCOMP_BV_COMPACTED_FORMAT      (1ull << 63) /*Z XSAVE扩展区格式为压缩格式 */
/*Z XSAVE老旧区，512字节。BUG：大小和字段名与手册对不上?????????? */
/* The state format, as saved by FXSAVE and restored by FXRSTOR instructions. */
typedef struct i387_state {
    uint16_t cwd;               /* control word */
    uint16_t swd;               /* status word */
    uint16_t twd;               /* tag word */
    uint16_t fop;               /* last instruction opcode */
    uint32_t reserved[4];       /* instruction and data pointers */
    uint32_t mxcsr;             /* MXCSR register state */
    uint32_t mxcsr_mask;        /* MXCSR mask */
    uint32_t st_space[32];      /* FPU registers */
    uint32_t xmm_space[64];     /* XMM registers */
    uint32_t padding[13];
} PACKED i387_state_t;

/* The state format, as saved by XSAVE and restored by XRSTOR instructions. */
typedef struct xsave_state {
    i387_state_t i387;          /*Z 老旧512字节区域 */
    struct {
        uint64_t xfeatures;     /*Z 状态组件位图xstate_bv */
        uint64_t xcomp_bv;      /* state-component bitmap *//*Z 扩展区格式。不好：这个注释应是上条的，上条的名字也不对 */
        uint64_t reserved[6];
    } header;                   /*Z 64字节XSAVE头。后面可能跟变长的扩展区 */
} PACKED xsave_state_t;
/*Z 初始化FPU状态、配置 */
/* Initialise the FPU. */
bool_t Arch_initFpu(void);

/* Initialise the FPU state of the given user context. */
void Arch_initFpuContext(user_context_t *context);
/*Z 返回XSAVE指令相关的性能集掩码高32位 */
static inline uint32_t xsave_features_high(void)
{
    uint64_t features = config_ternary(CONFIG_XSAVE, CONFIG_XSAVE_FEATURE_SET, 1);
    return (uint32_t)(features >> 32);
}
/*Z 返回XSAVE指令相关的性能集掩码低32位。从seL4配置情况看，这部分实现还是临时的 */
static inline uint32_t xsave_features_low(void)
{
    uint64_t features = config_ternary(CONFIG_XSAVE, CONFIG_XSAVE_FEATURE_SET, 1);
    return (uint32_t)(features & 0xffffffff);
}
/*Z 保存FPU性能状态集到XSAVE区 */
/* Store state in the FPU registers into memory. */
static inline void saveFpuState(user_fpu_state_t *dest)
{
    if (config_set(CONFIG_FXSAVE)) {
        asm volatile("fxsave %[dest]" : [dest] "=m"(*dest));
    } else if (config_set(CONFIG_XSAVE_XSAVEOPT)) {
        asm volatile("xsaveopt %[dest]" : [dest] "=m"(*dest) : "d"(xsave_features_high()), "a"(xsave_features_low()));
    } else if (config_set(CONFIG_XSAVE_XSAVE)) {
        asm volatile("xsave %[dest]" : [dest] "=m"(*dest) : "d"(xsave_features_high()), "a"(xsave_features_low()));
    } else if (config_set(CONFIG_XSAVE_XSAVEC)) {
        asm volatile("xsavec %[dest]" : [dest] "=m"(*dest) : "d"(xsave_features_high()), "a"(xsave_features_low()));
    } else if (config_set(CONFIG_XSAVE_XSAVES)) {
        asm volatile("xsaves %[dest]" : [dest] "=m"(*dest) : "d"(xsave_features_high()), "a"(xsave_features_low()));
    }
}
/*Z 加载XSAVE区到FPU */
/* Load FPU state from memory into the FPU registers. */
static inline void loadFpuState(user_fpu_state_t *src)
{
    if (config_set(CONFIG_FXSAVE)) {
        asm volatile("fxrstor %[src]" :: [src] "m"(*src));
    } else if (config_set(CONFIG_XSAVE)) {
        if (config_set(CONFIG_XSAVE_XSAVES)) {
            asm volatile("xrstors %[src]" :: [src] "m"(*src), "d"(xsave_features_high()), "a"(xsave_features_low()));
        } else {
            asm volatile("xrstor %[src]" :: [src] "m"(*src), "d"(xsave_features_high()), "a"(xsave_features_low()));
        }
    }
}
/*Z 设置FPU内部控制、状态、寄存器等为默认值 */
/* Reset the FPU registers into their initial blank state. */
static inline void finit(void)
{
    asm volatile("finit" :: "m"(control_reg_order));
}
/*Z 清除任务切换标志，从而使能FPU正常应用*/
/*
 * Enable the FPU to be used without faulting.
 * Required even if the kernel attempts to use the FPU.
 */
static inline void enableFpu(void)
{
    asm volatile("clts" :: "m"(control_reg_order));
}
/*Z CR0设置TS位时，执行浮点指令会导致一个#NM例外，由例外例程清标志、保存XSAVE区、继续执行等 */
/*
 * Disable the FPU so that usage of it causes a fault
 */
static inline void disableFpu(void)
{
    write_cr0(read_cr0() | CR0_TASK_SWITCH);
}

#ifdef CONFIG_VTX
/*Z 当前FPU是否为虚拟机上的线程所用 */
static inline bool_t vcpuThreadUsingFPU(tcb_t *thread)
{
    return thread->tcbArch.tcbVCPU && &thread->tcbArch.tcbVCPU->fpuState == NODE_STATE(ksActiveFPUState);
}
#endif /* CONFIG_VTX */


/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

static inline unsigned long read_cr3(void)
{
    word_t cr3;
    asm volatile("movq %%cr3, %0" : "=r"(cr3), "=m"(control_reg_order));
    return cr3;
}
/*Z 将val写入CR3寄存器 */
static inline void write_cr3(unsigned long val)
{
    asm volatile("movq %0, %%cr3" :: "r"(val), "m"(control_reg_order));
}
/*Z 返回CR0寄存器值 */
static inline unsigned long read_cr0(void)
{
    unsigned long val;
    asm volatile("movq %%cr0, %0" : "=r"(val), "=m"(control_reg_order));
    return val;
}
/*Z 将val写入CR0寄存器 */
static inline void write_cr0(unsigned long val)
{
    asm volatile("movq %0, %%cr0" :: "r"(val), "m"(control_reg_order));
}
/*Z 读CR2寄存器值 */
static inline unsigned long read_cr2(void)
{
    unsigned long val;
    asm volatile("movq %%cr2, %0" : "=r"(val), "=m"(control_reg_order));
    return val;
}
/*Z 返回CR4寄存器值 */
static inline unsigned long read_cr4(void)
{
    unsigned long val;
    asm volatile("movq %%cr4, %0" : "=r"(val), "=m"(control_reg_order));
    return val;
}
/*Z 将val写入CR4寄存器 */
static inline void write_cr4(unsigned long val)
{
    asm volatile("movq %0, %%cr4" :: "r"(val), "m"(control_reg_order));
}


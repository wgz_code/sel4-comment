/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <sel4/macros.h>
/* x86-64 specific object types */
/* sysexitq (64-bit) user code = cs + 32, user data = cs + 40.
 * sysexit user code = cs + 16, user data = cs + 24, so we need to arrange
 * user CS and DS as 5 and 6.
 * *//*Z GDT表中各段描述符偏移量（选择子）*/
#define GDT_NULL    0
#define GDT_CS_0    1   /*Z 特权CS段 */
#define GDT_DS_0    2
#define GDT_TSS     3 //TSS is two slots in x86-64  /*Z TSS系统段 */
#define GDT_CS_3    5   /*Z 用户CS段 */
#define GDT_DS_3    6   /*Z 用户DS段 */
#define GDT_FS      7
#define GDT_GS      8
#define GDT_ENTRIES 9   /*Z GDT表项数量 */

compile_assert(gdt_idt_ptr_packed,
               sizeof(gdt_idt_ptr_t) == sizeof(uint16_t) * 5)

compile_assert(unsigned_long_size_64,
               sizeof(unsigned long) == 8)

compile_assert(unsinged_int_size_32,
               sizeof(unsigned int) == 4)

compile_assert(uint64_t_size_64,
               sizeof(uint64_t) == 8)

#ifdef CONFIG_KERNEL_SKIM_WINDOW
#define X86_GLOBAL_VSPACE_ROOT x64KSSKIMPML4    /*Z 根VSpace，即内核滑动窗口空间，一级页表，1页 */
#else
#define X86_GLOBAL_VSPACE_ROOT x64KSKernelPML4  /*Z 没配置内核滑动窗口时，根VSpace即内核一级页表，1页 */
#endif

#define X86_KERNEL_VSPACE_ROOT x64KSKernelPML4  /*Z 根VSpace就是内核一级页表 */

#define PML4E_SIZE_BITS seL4_PML4EntryBits      /*Z 3。一级页表项8字节 */
#define PML4_INDEX_BITS seL4_PML4IndexBits      /*Z 9。一级页表512项 */
#define PDPTE_SIZE_BITS seL4_PDPTEntryBits      /*Z 3。二级页表项8字节 */
#define PDPT_INDEX_BITS seL4_PDPTIndexBits      /*Z 9。二级页表512项 */
#define PDE_SIZE_BITS   seL4_PageDirEntryBits   /*Z 3。三级页表项8字节 */
#define PD_INDEX_BITS   seL4_PageDirIndexBits   /*Z 9。三级页表512项 */
#define PTE_SIZE_BITS   seL4_PageTableEntryBits /*Z 3。四级页表项8字节 */
#define PT_INDEX_BITS   seL4_PageTableIndexBits /*Z 9。四级页表512项 */

#define PT_INDEX_OFFSET (seL4_PageBits)                         /*Z 12。地址中四级页表索引位偏移 */
#define PD_INDEX_OFFSET (PT_INDEX_OFFSET + PT_INDEX_BITS)       /*Z 21。地址中三级页表索引位偏移 */
#define PDPT_INDEX_OFFSET (PD_INDEX_OFFSET + PD_INDEX_BITS)     /*Z 30。地址中二级页表索引位偏移 */
#define PML4_INDEX_OFFSET (PDPT_INDEX_OFFSET + PDPT_INDEX_BITS) /*Z 39。地址中一级页表索引位偏移 */

typedef pml4e_t vspace_root_t;  /*Z 根VSpace，即一级页表项(pml4e) */

#define GET_PML4_INDEX(x) ( ((x) >> (PML4_INDEX_OFFSET)) & MASK(PML4_INDEX_BITS))   /*Z 获取一级页表索引值 */
#define GET_VSPACE_ROOT_INDEX GET_PML4_INDEX    /*Z 获取一级页表索引值 */
#define GET_PDPT_INDEX(x) ( ((x) >> (PDPT_INDEX_OFFSET)) & MASK(PDPT_INDEX_BITS))   /*Z 获取二级页表索引值 */
#define GET_PD_INDEX(x)   ( ((x) >> (PD_INDEX_OFFSET)) & MASK(PD_INDEX_BITS))       /*Z 获取三级页表索引值 */
#define GET_PT_INDEX(x)   ( ((x) >> (PT_INDEX_OFFSET)) & MASK(PT_INDEX_BITS))       /*Z 获取四级页表索引值 */

#define PML4E_PTR(r)     ((pml4e_t *)(r))   /*Z 强制转换成一级页表指针 */
#define PML4E_PTR_PTR(r) ((pml4e_t **)(r))
#define PML4E_REF(p)     ((word_t)(p))      /*Z 强制转换成word_t */

#define PML4_PTR(r)     ((pml4e_t *)(r))    /*Z 强制转换成一级页表指针 */
#define PML4_REF(p)     ((word_t)(r))       /*Z 强制转换成word_t */

#define PDPTE_PTR(r)   ((pdpte_t *)(r))     /*Z 强制转换成二级页表项指针 */
#define PDPTE_PTR_PTR(r) ((pdpte_t **)(r))
#define PDPTE_REF(p)   ((word_t)(p))        /*Z 强制转换成word_t */

#define PDPT_PTR(r)    ((pdpte_t *)(r))     /*Z 强制转换成二级页表指针 */
#define PDPT_PREF(p)   ((word_t)(p))        /*Z 强制转换成word_t */

#define PDE_PTR(r)     ((pde_t *)(r))       /*Z 强制转换成三级页表项指针 */
#define PDE_PTR_PTR(r) ((pde_t **)(r))
#define PDE_REF(p)     ((word_t)(p))        /*Z 强制转换成word_t */

#define PD_PTR(r)    ((pde_t *)(r))         /*Z 强制转换成三级页表指针 */
#define PD_REF(p)    ((word_t)(p))          /*Z 强制转换成word_t */

#define PTE_PTR(r)    ((pte_t *)(r))        /*Z 强制转换成四级页表指针 */
#define PTE_REF(p)    ((word_t)(p))         /*Z 强制转换成word_t */

#define PT_PTR(r)    ((pte_t *)(r))         /*Z 强制转换成四级页表指针 */
#define PT_REF(p)    ((word_t)(p))          /*Z 强制转换成word_t */

/* there are 1^12 hardware PCID; now we match the software ASID
 * to the available PCID. Since each ASID pool is 4K in size,
 * it contains 512 vroots.
 */

struct asid_pool {
    asid_map_t array[BIT(asidLowBits)];/*Z 每pool的512个项目，每个元素是struct{u64} */
};
/*Z ASID(PCID) pool结构 */
typedef struct asid_pool asid_pool_t;

#define ASID_POOL_INDEX_BITS  seL4_ASIDPoolIndexBits    /*Z ASID(PCID) pool内索引位(每池最多512项，占4K) */
#define ASID_POOL_SIZE_BITS (seL4_ASIDPoolBits + WORD_SIZE_BITS)
#define ASID_POOL_PTR(r)    ((asid_pool_t*)r)   /*Z 强制转换成ASID(PCID) pool结构指针 */
#define ASID_POOL_REF(p)    ((word_t)p)         /*Z 强制转换成word_t */
#define ASID_BITS           (asidHighBits + asidLowBits)    /*Z 12。ASID(PCID)位数 */
#define nASIDPools          BIT(asidHighBits)               /*Z 8。ASID(PCID) pool总数量 */
#define ASID_LOW(a)         (a & MASK(asidLowBits))         /*Z ASID(PCID)的池内索引值 */
#define ASID_HIGH(a)        ((a >> asidLowBits) & MASK(asidHighBits))   /*Z ASID(PCID)的池索引值 */
/*Z 返回页表能力对应的ASID(PCID)。注意：不用于四级页表和EPT二、三、四级页表 */
static inline asid_t PURE cap_get_capMappedASID(cap_t cap)
{
    cap_tag_t ctag;

    ctag = cap_get_capType(cap);

    switch (ctag) {

    case cap_pml4_cap:
        return cap_pml4_cap_get_capPML4MappedASID(cap);

    case cap_pdpt_cap:
        return cap_pdpt_cap_get_capPDPTMappedASID(cap);

    case cap_page_directory_cap:
        return cap_page_directory_cap_get_capPDMappedASID(cap);

#ifdef CONFIG_VTX
    case cap_ept_pml4_cap:
        return cap_ept_pml4_cap_get_capPML4MappedASID(cap);
#endif

    default:
        fail("Invalid arch cap type");
    }
}
/*Z 返回能力所指代的对象大小(位数表示) */
static inline word_t CONST cap_get_modeCapSizeBits(cap_t cap)
{
    cap_tag_t ctag;

    ctag = cap_get_capType(cap);

    switch (ctag) {
    case cap_pml4_cap:
        return seL4_PML4Bits;

    case cap_pdpt_cap:
        return seL4_PDPTBits;

    default:
        return 0;
    }
}
/*Z 能力是否与内存有关 */
static inline bool_t CONST cap_get_modeCapIsPhysical(cap_t cap)
{
    cap_tag_t ctag;

    ctag = cap_get_capType(cap);

    switch (ctag) {

    case cap_pml4_cap:
        return true;

    case cap_pdpt_cap:
        return true;

    default:
        return false;
    }
}
/*Z 根据能力类型获取其所指对象的线性地址(架构相关的) */
static inline void *CONST cap_get_modeCapPtr(cap_t cap)
{
    cap_tag_t ctag;

    ctag = cap_get_capType(cap);

    switch (ctag) {
    case cap_pml4_cap:
        return PML4_PTR(cap_pml4_cap_get_capPML4BasePtr(cap));

    case cap_pdpt_cap:
        return PDPT_PTR(cap_pdpt_cap_get_capPDPTBasePtr(cap));

    default:
        return NULL;
    }
}


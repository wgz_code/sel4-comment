/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h>
#include <hardware.h>
#include <arch/model/statedata.h>
#include <arch/machine/cpu_registers.h>
#include <arch/model/smp.h>
#include <arch/machine.h>
/*Z 以物理地址addr和用户进程标识pcid为基础，构建CR3值 */
static inline cr3_t makeCR3(paddr_t addr, word_t pcid)
{
    return cr3_new(addr, config_set(CONFIG_SUPPORT_PCID) ? pcid : 0);
}
/*Z 获取当前cpu保存的CR3值 */
/* Address space control */
static inline cr3_t getCurrentCR3(void)
{
#ifdef CONFIG_KERNEL_SKIM_WINDOW
    /* If we're running in the kernel to call this function, then by definition
     * this must be the current cr3 *//*Z 用内核一级页表基地址生成一个CR3寄存器值 */
    return cr3_new(kpptr_to_paddr(x64KSKernelPML4), 0);
#else   /*Z 获取当前cpu的CR3值 */
    return MODE_NODE_STATE(x64KSCurrentCR3);
#endif
}
/*Z 获取CR3值 */
static inline cr3_t getCurrentUserCR3(void)
{
#ifdef CONFIG_KERNEL_SKIM_WINDOW
    // Construct a cr3_t from the state word, dropping any command information
    // if needed
    word_t cr3_word = MODE_NODE_STATE(x64KSCurrentUserCR3);
    cr3_t cr3_ret;
    if (config_set(CONFIG_SUPPORT_PCID)) {
        cr3_word &= ~BIT(63);/*Z 当第63位为0且PCID使能时，将该值写入CR3会失效与该PCID相关的TLB和paging缓存 */
    }
    cr3_ret.words[0] = cr3_word;
    return cr3_ret;
#else
    return getCurrentCR3();
#endif
}
/*Z 获取当前一级页表物理地址 */
static inline paddr_t getCurrentUserVSpaceRoot(void)
{
    return cr3_get_pml4_base_address(getCurrentUserCR3());
}
/*Z 将cr3值写入寄存器，preserve_translation如果为0会导致失效相关的TLB、paging缓存 */
static inline void setCurrentCR3(cr3_t cr3, word_t preserve_translation)
{
#ifdef CONFIG_KERNEL_SKIM_WINDOW
    /* we should only ever be enabling the kernel window, as the bulk of the
     * cr3 loading when using the SKIM window will happen on kernel entry/exit
     * in assembly stubs */
    assert(cr3_get_pml4_base_address(cr3) == kpptr_to_paddr(x64KSKernelPML4));
#else
    MODE_NODE_STATE(x64KSCurrentCR3) = cr3;/*Z 保存在ksSMP[]中 */
#endif
    word_t cr3_word = cr3.words[0];
    if (config_set(CONFIG_SUPPORT_PCID)) {
        if (preserve_translation) {
            cr3_word |= BIT(63);/*Z 当第63位为1且PCID使能时，不失效 */
        }
    } else {
        assert(cr3_get_pcid(cr3) == 0);
    }
    write_cr3(cr3_word);
}
/*Z 将cr3值写入寄存器，不失效相关的TLB、paging缓存 */
/* there is no option for preservation translation when setting the user cr3
   as it is assumed you want it preserved as you are doing a context switch.
   If translation needs to be flushed then setCurrentCR3 should be used instead */
static inline void setCurrentUserCR3(cr3_t cr3)
{
#ifdef CONFIG_KERNEL_SKIM_WINDOW
    // To make the restore stubs more efficient we will set the preserve_translation
    // command in the state. If we look at the cr3 later on we need to remember to
    // remove that bit
    word_t cr3_word = cr3.words[0];
    if (config_set(CONFIG_SUPPORT_PCID)) {
        cr3_word |= BIT(63);
    }
    MODE_NODE_STATE(x64KSCurrentUserCR3) = cr3_word;/*Z 保存cr3值 */
#else
    setCurrentCR3(cr3, 1);
#endif
}
/*Z 将物理地址addr作为一级页表，和用户进程标识pcid一起写入CR3寄存器，从而确定一个虚拟地址空间 */
static inline void setCurrentVSpaceRoot(paddr_t addr, word_t pcid)
{
    setCurrentCR3(makeCR3(addr, pcid), 1);
}
/*Z 将物理地址作为一级页表地址写入cr3寄存器 */
static inline void setCurrentUserVSpaceRoot(paddr_t addr, word_t pcid)
{
#ifdef CONFIG_KERNEL_SKIM_WINDOW
    setCurrentUserCR3(makeCR3(addr, pcid));
#else
    setCurrentVSpaceRoot(addr, pcid);
#endif
}
/*Z 将参数指明的限长和基地址加载到GDTR寄存器中，DS、ES、SS段均指向GDT_DS_0段描述符，FS、GS段均置0 */
/* GDT installation */
void x64_install_gdt(gdt_idt_ptr_t *gdt_idt_ptr);
/*Z 将参数指明的限长和基地址加载到IDTR寄存器中 */
/* IDT installation */
void x64_install_idt(gdt_idt_ptr_t *gdt_idt_ptr);
/*Z 将参数指明的段选择子（16位偏移量）加载到LDTR寄存器的选择子域，然后cpu自动从GDT中索引得到LDT段基地址和限长 */
/* LDT installation */
void x64_install_ldt(uint32_t ldt_sel);
/*Z 将参数指明的段选择子（16位偏移量）加载到TR寄存器的选择子域，然后cpu自动从GDT中索引得到TSS段基地址和限长 */
/* TSS installation */
void x64_install_tss(uint32_t tss_sel);

void handle_fastsyscall(void);
/*Z 初始化syscall/sysret指令有关MSR寄存器 */
void init_syscall_msrs(void);

/* Get current stack pointer */
static inline void *get_current_esp(void)
{
    word_t stack;
    void *result;
    asm volatile("movq %[stack_address], %[result]" : [result] "=r"(result) : [stack_address] "r"(&stack));
    return result;
}
/*Z INVPCID的操作描述符 */
typedef struct invpcid_desc {
    uint64_t    asid;   /*Z 低12位是PCID */
    uint64_t    addr;   /*Z 类型0时要失效的地址 */
} invpcid_desc_t;
/*Z INVPCID指令类型，失效不同范围的TLB和paging缓存 */
#define INVPCID_TYPE_ADDR           0   /*Z 失效单个地址 */
#define INVPCID_TYPE_SINGLE         1   /*Z 失效PCID所属的 */
#define INVPCID_TYPE_ALL_GLOBAL     2   /* also invalidate global *//*Z 失效所有的 */
#define INVPCID_TYPE_ALL            3   /*Z 失效所有的，除全局映射 */
/*Z 根据类型失效TLB和paging缓存，type是失效操作类型，vaddr是要失效的虚拟地址，asid是PCID */
static inline void invalidateLocalPCID(word_t type, void *vaddr, asid_t asid)
{
    if (config_set(CONFIG_SUPPORT_PCID)) {
        invpcid_desc_t desc;
        desc.asid = asid & 0xfff;   /*Z PCID */
        desc.addr = (uint64_t)vaddr;
        asm volatile("invpcid %1, %0" :: "r"(type), "m"(desc));
    } else {
        switch (type) {
        case INVPCID_TYPE_ADDR:/*Z 失效地址所属页的TLB项 */
            asm volatile("invlpg (%[vptr])" :: [vptr] "r"(vaddr));
            break;
        case INVPCID_TYPE_SINGLE:
        case INVPCID_TYPE_ALL:/*Z 失效所有的TLB、paging缓存，除全局项 */
            /* reload CR3 to perform a full flush */
            setCurrentCR3(getCurrentCR3(), 0);
            break;
        case INVPCID_TYPE_ALL_GLOBAL: {
            /* clear and reset the global bit to flush global mappings */
            unsigned long cr4 = read_cr4();
            write_cr4(cr4 & ~BIT(7));/*Z 全局页特性位：0-禁用 */
            write_cr4(cr4);/*Z 清除后再恢复 */
        }
        break;
        }
    }
}
/*Z 失效所有的（包括全局的）TLB和paging缓存。不好：已经有一个All函数了 */
static inline void invalidateLocalTranslationSingle(vptr_t vptr)
{
    /* As this may be used to invalidate global mappings by the kernel,
     * and as its only used in boot code, we can just invalidate
     * absolutely everything form the tlb */
    invalidateLocalPCID(INVPCID_TYPE_ALL_GLOBAL, (void *)0, 0);
}
/*Z 失效指定ASID(PCID)的虚拟地址的TLB和paging缓存 */
static inline void invalidateLocalTranslationSingleASID(vptr_t vptr, asid_t asid)
{
    invalidateLocalPCID(INVPCID_TYPE_ADDR, (void *)vptr, asid);
}
/*Z 失效所有的（包括全局的）TLB和paging缓存 */
static inline void invalidateLocalTranslationAll(void)
{
    invalidateLocalPCID(INVPCID_TYPE_ALL_GLOBAL, (void *)0, 0);
}
/*Z 失效页缓存，根据配置也可能失效ASID(PCID)相关的TLB。不好：root参数没用 */
static inline void invalidateLocalPageStructureCacheASID(paddr_t root, asid_t asid)
{
    if (config_set(CONFIG_SUPPORT_PCID)) {
        /* store our previous cr3 */
        cr3_t cr3 = getCurrentCR3();
        /* we load the new vspace root, invalidating translation for it
         * and then switch back to the old CR3. We do this in a single
         * asm block to ensure we only rely on the code being mapped in
         * the temporary address space and not the stack. We preserve the
         * translation of the old cr3 */
        asm volatile(
            "mov %[new_cr3], %%cr3\n"
            "mov %[old_cr3], %%cr3\n"
            ::  /*Z 失效ASID(PCID)相关的TLB和页缓存 */
            [new_cr3] "r"(makeCR3(root, asid).words[0]),
            [old_cr3] "r"(cr3.words[0] | BIT(63))/*Z 最高位置1不失效 */
        );
    } else {
        /* just invalidate the page structure cache as per normal, by
         * doing a dummy invalidation of a tlb entry *//*Z 失效页缓存 */
        asm volatile("invlpg (%[vptr])" :: [vptr] "r"(0));
    }
}
/*Z 与IA32_KERNEL_GS_BASE_MSR交换GS段寄存器值 */
static inline void swapgs(void)
{
    asm volatile("swapgs");
}
/*Z 读取reg的MSR，如果机器不支持这个MSR，则返回结果标识失败 */
static inline rdmsr_safe_result_t x86_rdmsr_safe(const uint32_t reg)
{
    uint32_t low;
    uint32_t high;
    word_t returnto;
    word_t temp;
    rdmsr_safe_result_t result;
    asm volatile(
        "movabs $1f, %[temp] \n"
        "movq %[temp], (%[returnto_addr]) \n\
         rdmsr \n\
         1: \n\
         movq (%[returnto_addr]), %[returnto] \n\
         movq $0, (%[returnto_addr])"
        : [returnto] "=&r"(returnto),
        [temp] "=&r"(temp),
        [high] "=&d"(high),
        [low] "=&a"(low)        /*Z 预设GP例外处理地址 */
        : [returnto_addr] "r"(&ARCH_NODE_STATE(x86KSGPExceptReturnTo)),
        [reg] "c"(reg)
        : "memory"
    );/*Z 如果有GP例外发生，说明机器不支持这个MSR，例外处理例程会置零x86KSGPExceptReturnTo，从而returnto得到0 */
    result.success = returnto != 0;
    result.value = ((uint64_t)high << 32) | (uint64_t)low;
    return result;
}

#ifdef CONFIG_FSGSBASE_INST
/*Z 写FS段基地址寄存器 */
static inline void x86_write_fs_base_impl(word_t base)
{
    asm volatile("wrfsbase %0"::"r"(base));
}
/*Z 读FS段基地址寄存器 */
static inline word_t x86_read_fs_base_impl(void)
{
    word_t base = 0;
    asm volatile("rdfsbase %0":"=r"(base));
    return base;
}
/*Z 将当前cpu的FSGS段基地址保存在TCB上下文中。不好：忽略参数 */
static inline void x86_save_fsgs_base(tcb_t *thread, cpu_id_t cpu)
{
    /*
     * Store the FS and GS base registers.
     *
     * These should only be accessed inside the kernel, between the
     * entry and exit calls to swapgs if used.
     */
#ifdef CONFIG_VTX
    if (thread_state_ptr_get_tsType(&thread->tcbState) == ThreadState_RunningVM) {
        /*
         * Never save the FS/GS of a thread running in a VM as it will
         * be garbage values.
         */
        return;
    }
#endif
    word_t cur_fs_base = x86_read_fs_base(cpu);/*Z 读当前cpu的FS段基地址寄存器。不好：参数忽略 */
    setRegister(thread, FS_BASE, cur_fs_base);/*Z 保存在TCB上下文中 */
    word_t cur_gs_base = x86_read_gs_base(cpu);
    setRegister(thread, GS_BASE, cur_gs_base);
}

#endif

#if defined(ENABLE_SMP_SUPPORT)

/*
 * Under x86_64 with SMP support, the GS.Base register and the
 * IA32_KERNEL_GS_BASE MSR are swapped so the actual user-level copy of
 * GS is stored in IA32_KERNEL_GS_BASE between the call to swapgs in the
 * kernel entry and the call to swapgs in the user restore.
 */
/*Z 写可用于swapgs指令的GS段基地址寄存器 */
static inline void x86_write_gs_base_impl(word_t base)
{
    x86_wrmsr(IA32_KERNEL_GS_BASE_MSR, base);
}

static inline word_t x86_read_gs_base_impl(void)
{
    return x86_rdmsr(IA32_KERNEL_GS_BASE_MSR);
}

#elif defined(CONFIG_FSGSBASE_INST)
/*Z 写GS段基地址寄存器 */
static inline void x86_write_gs_base_impl(word_t base)
{
    asm volatile("wrgsbase %0"::"r"(base));
}
/*Z 读GS段基地址寄存器 */
static inline word_t x86_read_gs_base_impl(void)
{
    word_t base = 0;
    asm volatile("rdgsbase %0":"=r"(base));
    return base;
}

#elif defined(CONFIG_FSGSBASE_MSR)

static inline void x86_write_gs_base_impl(word_t base)
{
    x86_wrmsr(IA32_GS_BASE_MSR, base);
}

static inline word_t x86_read_gs_base_impl(void)
{
    return x86_rdmsr(IA32_GS_BASE_MSR);
}

#endif
/*Z 设置FS段寄存器(线程TLS)值并保存在全局变量中 */
static inline void x86_set_tls_segment_base(word_t tls_base)
{
    x86_write_fs_base(tls_base, SMP_TERNARY(getCurrentCPUIndex(), 0));
}


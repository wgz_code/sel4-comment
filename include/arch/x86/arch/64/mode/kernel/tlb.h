/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <smp/ipi.h>
#include <arch/kernel/tlb.h>
#include <mode/smp/ipi.h>
#include <arch/kernel/tlb_bitmap.h>
/*Z 失效ASID(PCID)所属的TLB和paging缓存，如果不是当前在用页表还清除TLB位图中的当前cpu位 */
/*
 * This is a wrapper around invalidatePCID that can be used to invalidate
 * an ASID and, in the case of SMP, potentially clear a vspace of having
 * any translations on this core
 */
static inline void invalidateLocalASID(vspace_root_t *vspace, asid_t asid)
{   /*Z 失效ASID(PCID)所属的TLB和paging缓存 */
    invalidateLocalPCID(INVPCID_TYPE_SINGLE, (void *)0, asid);
#ifdef ENABLE_SMP_SUPPORT
    if (pptr_to_paddr(vspace) != getCurrentUserVSpaceRoot()) {/*Z 如果不是当前在用页表 */
        tlb_bitmap_unset(vspace, getCurrentCPUIndex());/*Z 清除TLB位图位 */
    }
#endif /* ENABLE_SMP_SUPPORT */
}

static inline void invalidatePCID(word_t type, void *vaddr, asid_t asid, word_t mask)
{   /*Z 失效TLB和paging缓存，type是失效操作类型，vaddr是要失效的虚拟地址，asid是PCID */
    invalidateLocalPCID(type, vaddr, asid);
    SMP_COND_STATEMENT(doRemoteInvalidatePCID(type, vaddr, asid, mask));
}
/*Z 失效掩码指定的cpu上ASID(PCID)所属的TLB和paging缓存，如果不是当前在用页表还清除TLB位图中的当前cpu位，等待所有的核处理完毕 */
static inline void invalidateASID(vspace_root_t *vspace, asid_t asid, word_t mask)
{
    invalidateLocalASID(vspace, asid);
    SMP_COND_STATEMENT(doRemoteInvalidateASID(vspace, asid, mask));
}


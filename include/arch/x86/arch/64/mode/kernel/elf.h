/*Z OK ELF64结构 */

/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <types.h>
/*Z ELF64头结构 */
/* minimal ELF structures needed for loading GRUB boot module */
typedef struct Elf64_Header {
    unsigned char       e_ident[16];
    uint16_t            e_type;         /*Z 1-relocatable, 2-executable, 3-shared, 4-core */
    uint16_t            e_machine;      /*Z 3-x86, 0x3e-x86_64 */
    uint32_t            e_version;      /*Z ELF版本 */
    uint64_t            e_entry;        /*Z 程序入口点 */
    uint64_t            e_phoff;        /*Z 段头表位置 */
    uint64_t            e_shoff;        /*Z 节头表位置 */
    uint32_t            e_flags;
    uint16_t            e_ehsize;       /*Z ELF头大小 */
    uint16_t            e_phentsize;    /*Z 段头表每项大小 */
    uint16_t            e_phnum;        /*Z 段头表表项数量 */
    uint16_t            e_shentsize;    /*Z 节头表每项大小 */
    uint16_t            e_shnum;        /*Z 节头表表项数量 */
    uint16_t            e_shstrndx;     /*Z 节名表项在节头表中的索引 */
} Elf64_Header_t, Elf_Header_t;
/*Z ELF程序段头结构 */
typedef struct Elf64_Phdr {
    uint32_t            p_type;         /*Z 段类型 */
    uint32_t            p_flags;        /*Z 1-executable, 2-writable, 4-readable */
    uint64_t            p_offset;       /*Z 本段在文件中的偏移量，页对齐 */
    uint64_t            p_vaddr;        /*Z 本段应加载在虚拟内存中的地址，页对齐 */
    uint64_t            p_paddr;        
    uint64_t            p_filesz;       /*Z 本段在文件中的大小 */
    uint64_t            p_memsz;        /*Z 本段在内存中的大小 */
    uint64_t            p_align;        /*Z 段对齐 */
} Elf64_Phdr_t, Elf_Phdr_t;


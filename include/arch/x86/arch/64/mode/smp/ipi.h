/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h>
#include <types.h>
#include <plat/machine.h>
#include <smp/ipi.h>

#ifdef ENABLE_SMP_SUPPORT
/*Z IPI远程调用的功能指示之二 */
typedef enum {
    IpiRemoteCall_InvalidatePCID = IpiNumArchRemoteCall,/*Z 失效PCID相关的TLB和paging缓存 */
    IpiRemoteCall_InvalidateASID,   /*Z 失效ASID相关的TLB和paging缓存 */
    IpiNumModeRemoteCall
} IpiModeRemoteCall_t;

void Mode_handleRemoteCall(IpiModeRemoteCall_t call, word_t arg0, word_t arg1, word_t arg2);

static inline void doRemoteInvalidatePCID(word_t type, void *vaddr, asid_t asid, word_t mask)
{
    doRemoteMaskOp3Arg((IpiRemoteCall_t)IpiRemoteCall_InvalidatePCID, type, (word_t)vaddr, asid, mask);
}
/*Z 发送失效ASID相关TLB和paging缓存的IPI，等待所有的核处理完毕。mask为目标cpu索引掩码 */
static inline void doRemoteInvalidateASID(vspace_root_t *vspace, asid_t asid, word_t mask)
{
    doRemoteMaskOp2Arg((IpiRemoteCall_t)IpiRemoteCall_InvalidateASID, (word_t)vspace, asid, mask);
}

#endif /* ENABLE_SMP_SUPPORT */


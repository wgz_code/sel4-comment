/*Z OK */

/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#define wordRadix 6                 /*Z 6。机器字位数的对数 */
#define wordBits (1 << wordRadix)   /*Z 64。机器字位数 */


/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h>
#include <api/failures.h>

#define VCPU_VMCS_SIZE 4096         /*Z 虚拟机控制结构VMCS最大4K大小 */
#define VCPU_IOBITMAP_SIZE 8192     /*Z 虚拟机I/O端口访问控制位图大小。每位对应一个端口，1-禁用 */

#define VMX_CONTROL_VPID 0x00000000         /*Z VMCS的vpid域 */

#define VMX_GUEST_ES_SELECTOR 0x00000800
#define VMX_GUEST_CS_SELECTOR 0x00000802
#define VMX_GUEST_SS_SELECTOR 0x00000804
#define VMX_GUEST_DS_SELECTOR 0x00000806
#define VMX_GUEST_FS_SELECTOR 0x00000808
#define VMX_GUEST_GS_SELECTOR 0x0000080A
#define VMX_GUEST_LDTR_SELECTOR 0x0000080C
#define VMX_GUEST_TR_SELECTOR 0x0000080E

#define VMX_HOST_ES_SELECTOR 0x00000C00
#define VMX_HOST_CS_SELECTOR 0x00000C02
#define VMX_HOST_SS_SELECTOR 0x00000C04
#define VMX_HOST_DS_SELECTOR 0x00000C06
#define VMX_HOST_FS_SELECTOR 0x00000C08
#define VMX_HOST_GS_SELECTOR 0x00000C0A
#define VMX_HOST_TR_SELECTOR 0x00000C0C

#define VMX_CONTROL_IOA_ADDRESS 0x00002000      /*Z I/O端口位图地址(低半部分) */
#define VMX_CONTROL_IOB_ADDRESS 0x00002002      /*Z I/O端口位图地址(高半部分) */
#define VMX_CONTROL_MSR_ADDRESS 0x00002004
#define VMX_CONTROL_EXIT_MSR_STORE_ADDRESS 0x00002006
#define VMX_CONTROL_EXIT_MSR_LOAD_ADDRESS 0x00002008
#define VMX_CONTROL_ENTRY_MSR_LOAD_ADDRESS 0x0000200A
#define VMX_CONTROL_EXECUTIVE_VMCS_POINTER 0x0000200C
#define VMX_CONTROL_TSC_OFFSET 0x00002010
#define VMX_CONTROL_VIRTUAL_APIC_ADDRESS 0x00002012
#define VMX_CONTROL_APIC_ACCESS_ADDRESS 0x00002014
#define VMX_CONTROL_EPT_POINTER 0x0000201A              /*Z EPT指针域 */

#define VMX_DATA_GUEST_PHYSICAL 0x00002400              /*Z 客户物理地址域 */

#define VMX_GUEST_VMCS_LINK_POINTER 0x00002800          /*Z 指向VMCS自身的指针 */
#define VMX_GUEST_VMCS_LINK_POINTER_HIGH 0x00002801     /*Z 指向VMCS自身的指针，高32位 */
#define VMX_GUEST_DEBUGCTRL 0x00002802
#define VMX_GUEST_PAT 0x00002804
#define VMX_GUEST_EFER 0x00002806
#define VMX_GUEST_PERF_GLOBAL_CTRL 0x00002808
#define VMX_GUEST_PDPTE0 0x0000280A
#define VMX_GUEST_PDPTE1 0x0000280C
#define VMX_GUEST_PDPTE2 0x0000280E
#define VMX_GUEST_PDPTE3 0x00002810

#define VMX_HOST_PAT 0x00002C00                 /*Z 主机页属性 */
#define VMX_HOST_EFER 0x00002C02                /*Z 主机使能扩展特性 */
#define VMX_HOST_PERF_GLOBAL_CTRL 0x00002C04    /*Z 主机性能监测全局控制 */

#define VMX_CONTROL_PIN_EXECUTION_CONTROLS 0x00004000       /*Z PIN执行控制 */
#define VMX_CONTROL_PRIMARY_PROCESSOR_CONTROLS 0x00004002   /*Z 主处理器指令执行控制 */
#define VMX_CONTROL_EXCEPTION_BITMAP 0x00004004             /*Z 例外控制位图域 */
#define VMX_CONTROL_PAGE_FAULT_MASK 0x00004006
#define VMX_CONTROL_PAGE_FAULT_MATCH 0x00004008
#define VMX_CONTROL_CR3_TARGET_COUNT 0x0000400A
#define VMX_CONTROL_EXIT_CONTROLS 0x0000400C
#define VMX_CONTROL_EXIT_MSR_STORE_COUNT 0x0000400E
#define VMX_CONTROL_EXIT_MSR_LOAD_COUNT 0x00004010
#define VMX_CONTROL_ENTRY_CONTROLS 0x00004012
#define VMX_CONTROL_ENTRY_MSR_LOAD_COUNT 0x00004014
#define VMX_CONTROL_ENTRY_INTERRUPTION_INFO 0x00004016      /*Z VMentry后向cpu发送中断通知的方式 */
#define VMX_CONTROL_ENTRY_EXCEPTION_ERROR_CODE 0x00004018
#define VMX_CONTROL_ENTRY_INSTRUCTION_LENGTH 0x0000401A
#define VMX_CONTROL_TPR_THRESHOLD 0x0000401C
#define VMX_CONTROL_SECONDARY_PROCESSOR_CONTROLS 0x0000401E
#define VMX_CONTROL_PLE_GAP 0x00004020
#define VMX_CONTROL_PLE_WINDOW 0x00004022

#define VMX_DATA_INSTRUCTION_ERROR 0x00004400           /*Z VMX指令错误域 */
#define VMX_DATA_EXIT_REASON 0x00004402                 /*Z VMexit退出原因 */
#define VMX_DATA_EXIT_INTERRUPT_INFO 0x00004404         /*Z VMexit时的中断信息 */
#define VMX_DATA_EXIT_INTERRUPT_ERROR 0x00004406
#define VMX_DATA_IDT_VECTOR_INFO 0x00004408
#define VMX_DATA_IDT_VECTOR_ERROR 0x0000440A
#define VMX_DATA_EXIT_INSTRUCTION_LENGTH 0x0000440C     /*Z VMexit指令长度 */
#define VMX_DATA_EXIT_INSTRUCTION_INFO 0x0000440E

#define VMX_GUEST_ES_LIMIT 0x00004800
#define VMX_GUEST_CS_LIMIT 0x00004802
#define VMX_GUEST_SS_LIMIT 0x00004804
#define VMX_GUEST_DS_LIMIT 0x00004806
#define VMX_GUEST_FS_LIMIT 0x00004808
#define VMX_GUEST_GS_LIMIT 0x0000480A
#define VMX_GUEST_LDTR_LIMIT 0x0000480C
#define VMX_GUEST_TR_LIMIT 0x0000480E
#define VMX_GUEST_GDTR_LIMIT 0x00004810
#define VMX_GUEST_IDTR_LIMIT 0x00004812
#define VMX_GUEST_ES_ACCESS_RIGHTS 0x00004814
#define VMX_GUEST_CS_ACCESS_RIGHTS 0x00004816
#define VMX_GUEST_SS_ACCESS_RIGHTS 0x00004818
#define VMX_GUEST_DS_ACCESS_RIGHTS 0x0000481A
#define VMX_GUEST_FS_ACCESS_RIGHTS 0x0000481C
#define VMX_GUEST_GS_ACCESS_RIGHTS 0x0000481E
#define VMX_GUEST_LDTR_ACCESS_RIGHTS 0x00004820
#define VMX_GUEST_TR_ACCESS_RIGHTS 0x00004822
#define VMX_GUEST_INTERRUPTABILITY 0x00004824                /*Z 客户的特殊情况中断阻塞能力 */
#define VMX_GUEST_ACTIVITY 0x00004826
#define VMX_GUEST_SMBASE 0x00004828
#define VMX_GUEST_SYSENTER_CS 0x0000482A
#define VMX_GUEST_PREEMPTION_TIMER_VALUE 0x0000482E

#define VMX_HOST_SYSENTER_CS 0x00004C00             /*Z 宿主SYSENTER代码段 */

#define VMX_CONTROL_CR0_MASK 0x00006000             /*Z CR0访问掩码域 */
#define VMX_CONTROL_CR4_MASK 0x00006002
#define VMX_CONTROL_CR0_READ_SHADOW 0x00006004      /*Z CR0读影子值域 */
#define VMX_CONTROL_CR4_READ_SHADOW 0x00006006
#define VMX_CONTROL_CR3_TARGET0 0x00006008
#define VMX_CONTROL_CR3_TARGET1 0x0000600A
#define VMX_CONTROL_CR3_TARGET2 0x0000600C
#define VMX_CONTROL_CR3_TARGET3 0x0000600E

#define VMX_DATA_EXIT_QUALIFICATION 0x00006400      /*Z VMexit退出时的附加信息 */
#define VMX_DATA_IO_RCX 0x00006402
#define VMX_DATA_IO_RSI 0x00006404
#define VMX_DATA_IO_RDI 0x00006406
#define VMX_DATA_IO_RIP 0x00006408
#define VMX_DATA_GUEST_LINEAR_ADDRESS 0x0000640A

#define VMX_GUEST_CR0 0x00006800                /*Z 客户的CR0 */
#define VMX_GUEST_CR3 0x00006802                /*Z 客户的CR3 */
#define VMX_GUEST_CR4 0x00006804
#define VMX_GUEST_ES_BASE 0x00006806
#define VMX_GUEST_CS_BASE 0x00006808
#define VMX_GUEST_SS_BASE 0x0000680A
#define VMX_GUEST_DS_BASE 0x0000680C
#define VMX_GUEST_FS_BASE 0x0000680E
#define VMX_GUEST_GS_BASE 0x00006810
#define VMX_GUEST_LDTR_BASE 0x00006812
#define VMX_GUEST_TR_BASE 0x00006814
#define VMX_GUEST_GDTR_BASE 0x00006816
#define VMX_GUEST_IDTR_BASE 0x00006818
#define VMX_GUEST_DR7 0x0000681A
#define VMX_GUEST_RSP 0x0000681C
#define VMX_GUEST_RIP 0x0000681E                /*Z 客户的RIP */
#define VMX_GUEST_RFLAGS 0x00006820             /*Z 客户的RFLAGS */
#define VMX_GUEST_PENDING_DEBUG_EXCEPTIONS 0x00006822
#define VMX_GUEST_SYSENTER_ESP 0x00006824
#define VMX_GUEST_SYSENTER_EIP 0x00006826

#define VMX_HOST_CR0 0x00006C00             /*Z 宿主CR0 */
#define VMX_HOST_CR3 0x00006C02             /*Z 宿主CR3 */
#define VMX_HOST_CR4 0x00006C04             /*Z 宿主CR4 */
#define VMX_HOST_FS_BASE 0x00006C06         /*Z 宿主FS */
#define VMX_HOST_GS_BASE 0x00006C08         /*Z 宿主GS */
#define VMX_HOST_TR_BASE 0x00006C0A         /*Z 宿主TSS基地址 */
#define VMX_HOST_GDTR_BASE 0x00006C0C       /*Z 宿主GDT基地址 */
#define VMX_HOST_IDTR_BASE 0x00006C0E       /*Z 宿主IDT基地址 */
#define VMX_HOST_SYSENTER_ESP 0x00006C10    /*Z 宿主IA32_SYSENTER_ESP */
#define VMX_HOST_SYSENTER_EIP 0x00006C12    /*Z 宿主IA32_SYSENTER_EIP */
#define VMX_HOST_RSP 0x00006C14             /*Z 宿主RSP */
#define VMX_HOST_RIP 0x00006C16             /*Z 宿主RIP */

/* Exit reasons. */
enum exit_reasons {
    EXCEPTION_OR_NMI = 0x00,    /*Z 例外或NMI导致 */
    EXTERNAL_INTERRUPT = 0x01,  /*Z 外部中断导致VMexit */
    TRIPLE_FAULT = 0x02,
    INIT_SIGNAL = 0x03,
    SIPI = 0x04,
    /*IO_SMI = 0x05,
     *   OTHER_SMI = 0x06,*/
    INTERRUPT_WINDOW = 0x07,
    NMI_WINDOW = 0x08,
    TASK_SWITCH = 0x09,
    CPUID = 0x0A,
    GETSEC = 0x0B,
    HLT = 0x0C,
    INVD = 0x0D,
    INVLPG = 0x0E,
    RDPMC = 0x0F,
    RDTSC = 0x10,
    RSM = 0x11,
    VMCALL = 0x12,
    VMCLEAR = 0x13,
    VMLAUNCH = 0x14,
    VMPTRLD = 0x15,
    VMPTRST = 0x16,
    VMREAD = 0x17,
    VMRESUME = 0x18,
    VMWRITE = 0x19,
    VMXOFF = 0x1A,
    VMXON = 0x1B,
    CONTROL_REGISTER = 0x1C,    /*Z 写CR0、CR3、CR4、CR8导致 */
    MOV_DR = 0x1D,
    IO = 0x1E,
    RDMSR = 0x1F,
    WRMSR = 0x20,
    INVALID_GUEST_STATE = 0x21,
    MSR_LOAD_FAIL = 0x22,
    /* 0x23 */
    MWAIT = 0x24,
    MONITOR_TRAP_FLAG = 0x25,
    /* 0x26 */
    MONITOR = 0x27,
    PAUSE = 0x28,
    MACHINE_CHECK = 0x29,
    /* 0x2A */
    TPR_BELOW_THRESHOLD = 0x2B,
    APIC_ACCESS = 0x2C,
    GDTR_OR_IDTR = 0x2E,
    LDTR_OR_TR = 0x2F,
    EPT_VIOLATION = 0x30,
    EPT_MISCONFIGURATION = 0x31,
    INVEPT = 0x32,
    RDTSCP = 0x33,
    VMX_PREEMPTION_TIMER = 0x34,
    INVVPID = 0x35,
    WBINVD = 0x36,
    XSETBV = 0x37
};

#define VPID_INVALID 0                      /*Z 无效的VPID，16位 */
#define VPID_FIRST 1                        /*Z 第一个有效VPID值 */
#define VPID_LAST (CONFIG_MAX_VPIDS - 1)    /*Z 最后一个有效VPID值 */

typedef uint16_t vpid_t;    /*Z u16 */

#ifdef CONFIG_VTX
#define VTX_TERNARY(vtx, nonvtx) vtx
/*Z 虚拟机的8个通用寄存器枚举 */
enum vcpu_gp_register {
    VCPU_EAX = 0,
    VCPU_EBX,
    VCPU_ECX,
    VCPU_EDX,
    VCPU_ESI,
    VCPU_EDI,
    VCPU_EBP,
    n_vcpu_gp_register,     /*Z 7(除RSP) */
    /* We need to define a sentinal value to detect ESP that is strictly distinct
     * from any of our other GP register indexes, so put that here */
    VCPU_ESP,
};
/*Z 虚拟机的通用寄存器枚举 */
typedef enum vcpu_gp_register vcpu_gp_register_t;;

const vcpu_gp_register_t crExitRegs[];
/*Z 硬件虚拟化数据结构 */
struct vcpu {
    /* Storage for VMCS region. First field of vcpu_t so they share address.
     * Will use at most 4KiB of memory. Statically reserve 4KiB for convenience. */
    char vmcs[VCPU_VMCS_SIZE];  /*Z 虚拟机控制结构VMCS */
    word_t io[VCPU_IOBITMAP_SIZE / sizeof(word_t)];/*Z 虚拟机I/O端口访问控制位图。1-禁用(触发VMexit)，0-允许 */

    /* Place the fpu state here so that it is aligned */
    user_fpu_state_t fpuState;/*Z 当前虚拟cpu的FPU状态 */
    /*Z VMCS外，虚拟机要保存的现场，在VMexit时由handle_vmexit()保存 */
    /* General purpose registers that we have to save and restore as they
     * are not part of the vmcs *//*Z 初始化时VMCS中设置宿主RSP指向本数组后沿，估计就是为了在进出虚拟机时保存这些通用寄存器 */
    word_t gp_registers[n_vcpu_gp_register];/*Z 7个通用寄存器(除RSP) */
#if defined(ENABLE_SMP_SUPPORT) && defined(CONFIG_ARCH_IA32)
    word_t kernelSP;
#endif

    /* TCB associated with this VCPU. */
    struct tcb *vcpuTCB;    /*Z 关联(运行)的线程 */
    bool_t launched;        /*Z 是否已加载 */

    /* Currently assigned VPID */
    vpid_t vpid;            /*Z 当前赋予的vpid */

    /* This is the cr0 value has requested by the VCPU owner. The actual cr0 value set at
     * any particular time may be different to this for lazy fpu management, but we will
     * still respect whatever semantics the user has put into cr0. The cr0 value may
     * additionally be modified by the guest depending on how the VCPU owner has configured
     * the cr0 shadow/mask */
    word_t cr0;             /*Z 客户的CR0 */

    /* These are the values for the shadow and mask that the VCPU owner has requested.
     * Like cr0, they actual values set may be slightly different but semantics will
     * be preserved */
    word_t cr0_shadow;          /*Z CR0影子值 */
    word_t cr0_mask;            /*Z CR0位访问掩码，1-读shadow、写导致VMexit，0-客户正常读写 */
    word_t exception_bitmap;    /*Z 例外控制位图。低32位有效，1-例外导致VMexit，0-正常传递 */

    /* These values serve as a cache of what is presently in the VMCS allowing for
     * optimizing away unnecessary calls to vmwrite/vmread */
    word_t cached_exception_bitmap;
    word_t cached_cr0_shadow;
    word_t cached_cr0_mask;
    word_t cached_cr0;

    /* Last used EPT root */
    word_t last_ept_root;   /*Z 当前(最后)使用的EPT一级页表物理地址 */

#ifndef CONFIG_KERNEL_SKIM_WINDOW
    /* Last set host cr3 */
    word_t last_host_cr3;   /*Z 虚拟cpu当前(最后)之宿主CR3 */
#endif

#ifdef ENABLE_SMP_SUPPORT
    /* Core this VCPU was last loaded on, or is currently loaded on */
    word_t last_cpu;        /*Z 虚拟cpu当前(最后)之所在真实cpu索引 */
#endif /* ENABLE_SMP_SUPPORT */
};
typedef struct vcpu vcpu_t;/*Z 硬件虚拟化数据结构 */

compile_assert(vcpu_size_sane, sizeof(vcpu_t) <= BIT(seL4_X86_VCPUBits))
unverified_compile_assert(vcpu_fpu_state_alignment_valid,
                          OFFSETOF(vcpu_t, fpuState) % MIN_FPU_ALIGNMENT == 0)

/* Initializes a VCPU object with default values. A VCPU object that is not inititlized
 * must not be run/loaded with vmptrld */
void vcpu_init(vcpu_t *vcpu);
/*Z 取消VCPU关联的线程，清除正运行的环境(虚拟cpu不再活跃、相关数据未加载、VMCS清除) */
/* Cleans up the VCPU object such that its memory can be freed */
void vcpu_finalise(vcpu_t *vcpu);

exception_t decodeX86VCPUInvocation(
    word_t invLabel,
    word_t length,
    cptr_t cptr,
    cte_t *slot,
    cap_t cap,
    extra_caps_t excaps,
    word_t *buffer
);

/* Updates the state of the provided VCPU for a SysVMEnter syscall. The state
 * is pulled from the current threads messages registers */
void vcpu_update_state_sysvmenter(vcpu_t *vcpu);

void vcpu_sysvmenter_reply_to_user(tcb_t *tcb);

bool_t vtx_init(void);
exception_t handleVmexit(void);
exception_t handleVmEntryFail(void);/*Z 收集VMentry失败的全部17个(错误)信息，写入ksCurThread的消息寄存器和IPC buffer；控制权交回ksCurThread */
void restoreVMCS(void);/*Z 恢复ksCurThread的VMCS环境：vpid、EPT等 */
void clearCurrentVCPU(void);/*Z 清除当前cpu的虚拟cpu环境(虚拟cpu不再活跃、相关数据未加载、VMCS清除) */

#ifdef ENABLE_SMP_SUPPORT
void VMCheckBoundNotification(tcb_t *tcb);
#endif /* ENABLE_SMP_SUPPORT */
/*Z 失效EPT相关的TLB和页缓存。参数为EPT一级页表线性地址 */
void invept(ept_pml4e_t *ept_pml4);

/* Removes any IO port mappings that have been cached for the given VPID */
void clearVPIDIOPortMappings(vpid_t vpid, uint16_t first, uint16_t last);
/*Z 读VMCS指定域的值 */
static inline word_t vmread(word_t field)
{
    word_t value;
    asm volatile(
        "vmread %1, %0"
        : "=r"(value)
        : "r"(field)
        : "cc"
    );
    return value;
}

#include <machine/io.h>
/*Z 向当前VMCS的指定域写入值 */
static inline void vmwrite(word_t field, word_t value)
{
    asm volatile(
        "vmwrite %0, %1"
        :
        : "r"(value), "r"(field)
        : "cc"
    );
}

#else /* CONFIG_VTX */
#define VTX_TERNARY(vtx, nonvtx) nonvtx
#endif /* CONFIG_VTX */

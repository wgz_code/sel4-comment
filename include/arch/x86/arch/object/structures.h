/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <assert.h>
#include <config.h>
#include <util.h>
#include <api/types.h>
#include <sel4/macros.h>
#include <arch/types.h>
#include <arch/object/structures_gen.h>
#include <arch/machine/hardware.h>
#include <arch/machine/registerset.h>
#include <sel4/arch/constants.h>
/*Z TCB CNode中有固定位置的能力索引 */
enum tcb_arch_cnode_index {
#ifdef CONFIG_VTX
    /* VSpace root for running any associated VCPU in */
    tcbArchEPTRoot = tcbCNodeEntries,   /*Z 访问EPT的能力，即客户机页表产生的“物理地址”再转换成真正的物理地址 */
    tcbArchCNodeEntries
#else
    tcbArchCNodeEntries = tcbCNodeEntries
#endif
};
/*Z TCB架构有关的状态 */
typedef struct arch_tcb {
    user_context_t tcbContext;  /*Z 用户进程上下文 */
#ifdef CONFIG_VTX
    /* Pointer to associated VCPU. NULL if not associated.
     * tcb->tcbVCPU->vcpuTCB == tcb. */
    struct vcpu *tcbVCPU;       /*Z 所在的虚拟cpu */
#endif /* CONFIG_VTX */
} arch_tcb_t;

#define SEL_NULL    GDT_NULL
#define SEL_CS_0    (GDT_CS_0 << 3)         /*Z syscall自动加载的特权CS段 */
#define SEL_DS_0    (GDT_DS_0 << 3)         /*Z syscall自动加载的特权SS段 */
#define SEL_CS_3    ((GDT_CS_3 << 3) | 3)   /*Z GDT中用户CS段 */
#define SEL_DS_3    ((GDT_DS_3 << 3) | 3)   /*Z GDT中用户DS段 */
#define SEL_TSS     (GDT_TSS << 3)
#define SEL_FS      ((GDT_FS << 3) | 3)
#define SEL_GS      ((GDT_GS << 3) | 3)

#define IDT_ENTRIES 256         /*Z IDT表项数量 */

#define VTD_RT_SIZE_BITS  12                /*Z IOMMU根表大小4K */

#define VTD_CTE_SIZE_BITS 4                 /*Z IOMMU上下文表项大小 */
#define VTD_CTE_PTR(r)    ((vtd_cte_t*)(r))
#define VTD_CT_BITS       8                 /*Z IOMMU上下文表项数量 */
#define VTD_CT_SIZE_BITS  (VTD_CT_BITS + VTD_CTE_SIZE_BITS)/*Z 12。IOMMU上下文表大小4K */

#define VTD_PTE_SIZE_BITS 3                 /*Z IOMMU页表项大小 */
#define VTD_PTE_PTR(r)    ((vtd_pte_t*)(r)) /*Z 强制转换成IOMMU页表项指针 */
#define VTD_PT_INDEX_BITS       9           /*Z IOMMU页表每级索引的位数，同正常的4级页表情况。seL4不支持其它页大小 */

compile_assert(vtd_pt_size_sane, VTD_PT_INDEX_BITS + VTD_PTE_SIZE_BITS == seL4_IOPageTableBits)

#ifdef CONFIG_VTX

#define EPT_PML4E_SIZE_BITS seL4_X86_EPTPML4EntryBits   /*Z 3。EPT一级页表项大小(位数) */
#define EPT_PML4_INDEX_BITS seL4_X86_EPTPML4IndexBits   /*Z 9。EPT一级页表索引位数 */
#define EPT_PDPTE_SIZE_BITS seL4_X86_EPTPDPTEntryBits   /*Z 3。EPT二级页表项大小(位数) */
#define EPT_PDPT_INDEX_BITS seL4_X86_EPTPDPTIndexBits   /*Z 9。EPT二级页表索引位数 */
#define EPT_PDE_SIZE_BITS   seL4_X86_EPTPDEntryBits     /*Z 3。EPT三级页表项大小(位数) */
#define EPT_PD_INDEX_BITS   seL4_X86_EPTPDIndexBits     /*Z 9。EPT三级页表索引位数 */
#define EPT_PTE_SIZE_BITS   seL4_X86_EPTPTEntryBits     /*Z 3。EPT四级页表项大小(位数) */
#define EPT_PT_INDEX_BITS   seL4_X86_EPTPTIndexBits     /*Z 9。EPT四级页表索引位数 */

#define EPT_PT_INDEX_OFFSET (seL4_PageBits)                                 /*Z 12。EPT四级页表索引位偏移 */
#define EPT_PD_INDEX_OFFSET (EPT_PT_INDEX_OFFSET + EPT_PT_INDEX_BITS)       /*Z 21。EPT三级页表索引位偏移 */
#define EPT_PDPT_INDEX_OFFSET (EPT_PD_INDEX_OFFSET + EPT_PD_INDEX_BITS)     /*Z 30。EPT二级页表索引位偏移 */
#define EPT_PML4_INDEX_OFFSET (EPT_PDPT_INDEX_OFFSET + EPT_PDPT_INDEX_BITS) /*Z 39。EPT一级页表索引位偏移 */

#define GET_EPT_PML4_INDEX(x) ( (((uint64_t)(x)) >> (EPT_PML4_INDEX_OFFSET)) & MASK(EPT_PML4_INDEX_BITS))   /*Z 获取EPT一级页表索引 */
#define GET_EPT_PDPT_INDEX(x) ( ((x) >> (EPT_PDPT_INDEX_OFFSET)) & MASK(EPT_PDPT_INDEX_BITS))               /*Z 获取EPT二级页表索引 */
#define GET_EPT_PD_INDEX(x) ( ((x) >> (EPT_PD_INDEX_OFFSET)) & MASK(EPT_PD_INDEX_BITS))                     /*Z 获取EPT三级页表索引 */
#define GET_EPT_PT_INDEX(x) ( ((x) >> (EPT_PT_INDEX_OFFSET)) & MASK(EPT_PT_INDEX_BITS))                     /*Z 获取EPT四级页表索引 */

#define EPT_PML4E_PTR(r)     ((ept_pml4e_t *)(r))   /*Z 强制转换成EPT一级页表项指针 */
#define EPT_PML4E_PTR_PTR(r) ((ept_pml4e_t **)(r))
#define EPT_PML4E_REF(p)     ((word_t)(p))          /*Z 强制转换成word_t */

#define EPT_PML4_SIZE_BITS seL4_X86_EPTPML4Bits
#define EPT_PML4_PTR(r)    ((ept_pml4e_t *)(r))     /*Z 强制转换成EPT一级页表指针 */
#define EPT_PML4_REF(p)    ((word_t)(p))            /*Z 强制转换成word_t */

#define EPT_PDPTE_PTR(r)     ((ept_pdpte_t *)(r))   /*Z 强制转换成EPT二级页表项指针 */
#define EPT_PDPTE_PTR_PTR(r) ((ept_pdpte_t **)(r))
#define EPT_PDPTE_REF(p)     ((word_t)(p))          /*Z 强制转换成word_t */

#define EPT_PDPT_SIZE_BITS seL4_X86_EPTPDPTBits
#define EPT_PDPT_PTR(r)    ((ept_pdpte_t *)(r))     /*Z 强制转换成EPT二级页表指针 */
#define EPT_PDPT_REF(p)    ((word_t)(p))            /*Z 强制转换成word_t */

#define EPT_PDE_PTR(r)     ((ept_pde_t *)(r))       /*Z 强制转换成EPT三级页表项指针 */
#define EPT_PDE_PTR_PTR(r) ((ept_pde_t **)(r))
#define EPT_PDE_REF(p)     ((word_t)(p))            /*Z 强制转换成word_t */

#define EPT_PD_SIZE_BITS seL4_X86_EPTPDBits
#define EPT_PD_PTR(r)    ((ept_pde_t *)(r))         /*Z 强制转换成EPT三级页表指针 */
#define EPT_PD_REF(p)    ((word_t)(p))              /*Z 强制转换成word_t */

#define EPT_PTE_PTR(r)    ((ept_pte_t *)(r))        /*Z 强制转换成EPT四级页表项指针 */
#define EPT_PTE_REF(p)    ((word_t)(p))             /*Z 强制转换成word_t */

#define EPT_PT_SIZE_BITS seL4_X86_EPTPTBits
#define EPT_PT_PTR(r)    ((ept_pte_t *)(r))         /*Z 强制转换成EPT四级页表指针 */
#define EPT_PT_REF(p)    ((word_t)(p))              /*Z 强制转换成word_t */

#define VCPU_PTR(r)       ((vcpu_t *)(r))           /*Z 强制转换成vcpu指针 */
#define VCPU_REF(p)       ((word_t)(p))             /*Z 强制转换成word_t */

#endif /* CONFIG_VTX */

struct rdmsr_safe_result {
    uint64_t value;     /*Z rdmsr结果值EDX:EAX */
    bool_t success;     /*Z rdmsr是否执行成功 */
};
/*Z 保存rdmsr结果值，如果失败（不支持这个编号的MSR）则标识.success=false */
typedef struct rdmsr_safe_result rdmsr_safe_result_t;
/*Z GDT、IDT表主要属性 */
/* helper structure for filling descriptor registers */
typedef struct gdt_idt_ptr {
    uint16_t limit;     /*Z 表长 */
    word_t base;        /*Z 表基地址 */
} __attribute__((packed)) gdt_idt_ptr_t;

enum vm_rights {
    VMKernelOnly = 1,   /*Z 仅内核访问 */
    VMReadOnly = 2,     /*Z 只读 */
    VMReadWrite = 3     /*Z 读写 */
};
typedef word_t vm_rights_t; /*Z 虚拟内存读写权限枚举类型 */

#include <mode/object/structures.h>
/*Z 返回能力所指代的对象大小(位数表示) */
static inline word_t CONST cap_get_archCapSizeBits(cap_t cap)
{
    cap_tag_t ctag;

    ctag = cap_get_capType(cap);

    switch (ctag) {
    case cap_frame_cap:
        return pageBitsForSize(cap_frame_cap_get_capFSize(cap));

    case cap_page_table_cap:
        return seL4_PageTableBits;

    case cap_page_directory_cap:
        return seL4_PageDirBits;

    case cap_io_port_cap:
        return 0;

#ifdef CONFIG_IOMMU
    case cap_io_space_cap:
        return 0;
    case cap_io_page_table_cap:
        return seL4_IOPageTableBits;
#endif

    case cap_asid_control_cap:
        return 0;

    case cap_asid_pool_cap:
        return seL4_ASIDPoolBits;

#ifdef CONFIG_VTX
    case cap_vcpu_cap:
        return seL4_X86_VCPUBits;

    case cap_ept_pml4_cap:
        return seL4_X86_EPTPML4Bits;
    case cap_ept_pdpt_cap:
        return seL4_X86_EPTPDPTBits;
    case cap_ept_pd_cap:
        return seL4_X86_EPTPDBits;
    case cap_ept_pt_cap:
        return seL4_X86_EPTPTBits;
#endif /* CONFIG_VTX */

    default:
        return cap_get_modeCapSizeBits(cap);
    }
}
/*Z 能力是否与内存有关 */
static inline bool_t CONST cap_get_archCapIsPhysical(cap_t cap)
{
    cap_tag_t ctag;

    ctag = cap_get_capType(cap);

    switch (ctag) {

    case cap_frame_cap:
        return true;

    case cap_page_table_cap:
        return true;

    case cap_page_directory_cap:
        return true;

    case cap_io_port_cap:
        return false;

#ifdef CONFIG_IOMMU
    case cap_io_space_cap:
        return false;

    case cap_io_page_table_cap:
        return true;
#endif

    case cap_asid_control_cap:
        return false;

    case cap_asid_pool_cap:
        return true;

#ifdef CONFIG_VTX
    case cap_ept_pt_cap:
        return true;

    case cap_ept_pd_cap:
        return true;

    case cap_ept_pdpt_cap:
        return true;

    case cap_ept_pml4_cap:
        return true;
#endif /* CONFIG_VTX */

    default:
        return cap_get_modeCapIsPhysical(cap);
    }
}
/*Z 根据能力类型获取其所指对象的地址(架构相关的) */
static inline void *CONST cap_get_archCapPtr(cap_t cap)
{
    cap_tag_t ctag;

    ctag = cap_get_capType(cap);

    switch (ctag) {

    case cap_frame_cap: /*Z 物理页线性地址 */
        return (void *)(cap_frame_cap_get_capFBasePtr(cap));

    case cap_page_table_cap:
        return PT_PTR(cap_page_table_cap_get_capPTBasePtr(cap));

    case cap_page_directory_cap:
        return PD_PTR(cap_page_directory_cap_get_capPDBasePtr(cap));

    case cap_io_port_cap:
        return NULL;

#ifdef CONFIG_IOMMU
    case cap_io_space_cap:
        return NULL;

    case cap_io_page_table_cap:
        return (void *)(cap_io_page_table_cap_get_capIOPTBasePtr(cap));
#endif

    case cap_asid_control_cap:
        return NULL;

    case cap_asid_pool_cap:
        return ASID_POOL_PTR(cap_asid_pool_cap_get_capASIDPool(cap));

#ifdef CONFIG_VTX   /*Z 以下未配置，无从查看????? */
    case cap_ept_pt_cap:
        return EPT_PT_PTR(cap_ept_pt_cap_get_capPTBasePtr(cap));

    case cap_ept_pd_cap:
        return EPT_PD_PTR(cap_ept_pd_cap_get_capPDBasePtr(cap));

    case cap_ept_pdpt_cap:
        return EPT_PDPT_PTR(cap_ept_pdpt_cap_get_capPDPTBasePtr(cap));

    case cap_ept_pml4_cap:
        return EPT_PML4_PTR(cap_ept_pml4_cap_get_capPML4BasePtr(cap));
#endif /* CONFIG_VTX */

    default:
        return cap_get_modeCapPtr(cap);
    }
}
/*Z 对于IO端口控制能力，导出的IO端口能力是可撤销的 */
static inline bool_t CONST Arch_isCapRevocable(cap_t derivedCap, cap_t srcCap)
{
    switch (cap_get_capType(derivedCap)) {
    case cap_io_port_cap:
        return cap_get_capType(srcCap) == cap_io_port_control_cap;

    default:
        return false;
    }
}

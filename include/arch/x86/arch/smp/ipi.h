/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once
#include <config.h>

#ifdef ENABLE_SMP_SUPPORT
/*Z IPI远程调用的功能指示之一 */
typedef enum {
    IpiRemoteCall_Stall,                            /*Z 停止当前线程 */
#ifdef CONFIG_VTX
    IpiRemoteCall_ClearCurrentVCPU,                 /*Z 清除当前虚拟cpu */
    IpiRemoteCall_VMCheckBoundNotification,         /*Z 检查虚拟机线程绑定通知 */
#endif
    IpiRemoteCall_InvalidatePageStructureCacheASID, /*Z 失效ASID(PCID)相关页缓存 */
    IpiRemoteCall_InvalidateTranslationSingle,
    IpiRemoteCall_InvalidateTranslationSingleASID,  /*Z 失效ASID(PCID)指定虚拟地址TLB和页缓存 */
    IpiRemoteCall_InvalidateTranslationAll,         /*Z 失效所有的(包括全局的)TLB和paging缓存 */
    IpiRemoteCall_switchFpuOwner,                   /*Z 切换FPU属主 */
    IpiNumArchRemoteCall
} IpiRemoteCall_t;

#endif /* ENABLE_SMP_SUPPORT */


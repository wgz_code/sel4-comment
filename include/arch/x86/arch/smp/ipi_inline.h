/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h>
#include <smp/ipi.h>

#ifdef ENABLE_SMP_SUPPORT
/*Z 向指定cpu发送停止当前线程IPI，并等待其处理完毕 */
static inline void doRemoteStall(word_t cpu)
{
    doRemoteOp0Arg(IpiRemoteCall_Stall, cpu);
}
/*Z 向指定cpu发送切换FPU属主的IPI，并等待其处理完毕 */
static inline void doRemoteswitchFpuOwner(user_fpu_state_t *new_owner, word_t cpu)
{
    doRemoteOp1Arg(IpiRemoteCall_switchFpuOwner, (word_t)new_owner, cpu);
}
/*Z 向掩码指定的cpu发送失效页缓存的IPI，等待所有的核处理完毕 */
static inline void doRemoteInvalidatePageStructureCacheASID(paddr_t root, asid_t asid, word_t mask)
{
    doRemoteMaskOp2Arg(IpiRemoteCall_InvalidatePageStructureCacheASID, root, asid, mask);
}

static inline void doRemoteInvalidateTranslationSingle(vptr_t vptr, word_t mask)
{
    doRemoteMaskOp1Arg(IpiRemoteCall_InvalidateTranslationSingle, vptr, mask);
}
/*Z 向掩码指定的cpu发送失效指定ASID(PCID)指定虚拟地址TLB和页缓存IPI，等待所有的核处理完毕 */
static inline void doRemoteInvalidateTranslationSingleASID(vptr_t vptr, asid_t asid, word_t mask)
{
    doRemoteMaskOp2Arg(IpiRemoteCall_InvalidateTranslationSingleASID, vptr, asid, mask);
}

static inline void doRemoteInvalidateTranslationAll(word_t mask)
{
    doRemoteMaskOp0Arg(IpiRemoteCall_InvalidateTranslationAll, mask);
}

#ifdef CONFIG_VTX
/*Z 向指定cpu发送清除当前虚拟cpu的远程调用IPI，并等待其处理完毕 */
static inline void doRemoteClearCurrentVCPU(word_t cpu)
{
    doRemoteOp0Arg(IpiRemoteCall_ClearCurrentVCPU, cpu);
}
/*Z 向指定cpu发送虚拟机绑定通知检查的远程调用IPI，并等待其处理完毕 */
static inline void doRemoteVMCheckBoundNotification(word_t cpu, tcb_t *tcb)
{
    doRemoteOp1Arg(IpiRemoteCall_VMCheckBoundNotification, (word_t)tcb, cpu);
}
#endif

#endif /* ENABLE_SMP_SUPPORT */


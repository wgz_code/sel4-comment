/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h>
#include <assert.h>
#include <stdint.h>

#if defined(CONFIG_ARCH_IA32)
compile_assert(long_is_32bits, sizeof(unsigned long) == 4)
#elif defined(CONFIG_ARCH_X86_64)
compile_assert(long_is_64bits, sizeof(unsigned long) == 8)
#endif

typedef unsigned long word_t;   /*Z 无符号的机器字 */
typedef signed long sword_t;    /*Z 有符号的机器字 */
typedef word_t vptr_t;          /*Z 虚拟地址 */
typedef word_t paddr_t;         /*Z 物理地址 */
typedef word_t pptr_t;          /*Z 内核线性地址：物理地址线性映射到的虚拟地址 */
typedef word_t cptr_t;          /*Z CSlot句柄 */
typedef word_t dev_id_t;
typedef word_t cpu_id_t;
typedef uint32_t logical_id_t;  /*Z 逻辑cpu id */
typedef word_t node_id_t;
typedef word_t dom_t;           /*Z 调度域 */

/* for libsel4 headers that the kernel shares */
typedef word_t seL4_Word;       /*Z 无符号字 */
typedef cptr_t seL4_CPtr;
typedef uint16_t seL4_Uint16;
typedef uint32_t seL4_Uint32;
typedef uint8_t seL4_Uint8;
typedef node_id_t seL4_NodeId;
typedef paddr_t seL4_PAddr;
typedef dom_t seL4_Domain;

typedef uint64_t timestamp_t;


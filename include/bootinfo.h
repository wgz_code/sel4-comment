/*Z OK */

/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h>
#include <types.h>
#include <sel4/bootinfo_types.h>

#define BI_PTR(r) ((seL4_BootInfo*)(r)) /*Z 强制转换成bootinfo指针 */
#define BI_REF(p) ((word_t)(p))         /*Z 强制转换成word_t */
#define BI_FRAME_SIZE_BITS PAGE_BITS    /*Z 12。bootinfo帧大小(对数) */
#define S_REG_EMPTY (seL4_SlotRegion){ .start = 0, .end = 0 }   /*Z 空CSlot索引范围 */

/* adjust constants in config.h if this assert fails */
compile_assert(bi_size, sizeof(seL4_BootInfo) <= BIT(BI_FRAME_SIZE_BITS))


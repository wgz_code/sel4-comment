/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

struct deriveCap_ret {
    exception_t status; /*Z 是否出错 */
    cap_t cap;          /*Z 导出的能力 */
};
typedef struct deriveCap_ret deriveCap_ret_t;/*Z 导出能力结果类型 */

struct finaliseCap_ret {
    cap_t remainder;    /*Z 剩余还能用的 */
    /* potential cap holding information for cleanup that needs to be happen *after* a
     * cap has been deleted. Where deleted here means been removed from the slot in emptySlot */
    cap_t cleanupInfo;  /*Z 需要进一步清理的 */
};
typedef struct finaliseCap_ret finaliseCap_ret_t;/*Z 清除能力最后一个引用后的结果 */


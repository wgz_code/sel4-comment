/*Z OK PCI设备id有关宏 */

/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#define get_pci_bus(x) (((x)>>8u) & 0xffu)  /*Z PCI总线号，8位 */
#define get_pci_dev(x) (((x)>>3u) & 0x1fu)  /*Z PCI设备号，5位 */
#define get_pci_fun(x) ((x) & 0x7u)         /*Z PCI功能号，3位 */
#define get_dev_id(bus, dev, fun) (((bus) << 8u) | ((dev) << 3u) | (fun))   /*Z 拼成PCI设备id */

#define PCI_BUS_MAX     255                 /*Z PCI总线号上限 */
#define PCI_DEV_MAX     31                  /*Z PCI设备号上限 */
#define PCI_FUNC_MAX    7                   /*Z PCI功能号上限 */



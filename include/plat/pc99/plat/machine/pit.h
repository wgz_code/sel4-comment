/*Z OK Programmable Interval Timer (PIT) 系统时钟 */

/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

/* ms after which a wraparound occurs (max. 54) */
#define PIT_WRAPAROUND_MS 50    /*Z IRQ0中断信号周期。先进CPU有APIC定时器、TSC计数器，实际可能不用这个系统时钟 */

void pit_init(void);
void pit_wait_wraparound(void);


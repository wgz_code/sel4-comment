/*Z OK 虚拟地址空间设备信息内存布局，两类：一是APIC、IOAPIC，二是IOMMU */

/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h>
/*Z APIC内存虚拟基地址，也是内核设备虚拟基地址（顶端1G），占1页 */
#define PPTR_APIC KDEV_BASE
/*Z IOAPIC内存虚拟基地址，紧邻APIC，每个1页 */
#define PPTR_IOAPIC_START (PPTR_APIC + BIT(PAGE_BITS))
/*Z IOMMU内存虚拟基地址，紧邻IOAPIC，每个1页，可能直至尽头 */
#define PPTR_DRHU_START (PPTR_IOAPIC_START + BIT(PAGE_BITS) * CONFIG_MAX_NUM_IOAPIC)
/*Z 最大IOMMU设备数量 */
#define MAX_NUM_DRHU    ((-PPTR_DRHU_START) >> PAGE_BITS)


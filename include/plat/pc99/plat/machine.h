/*Z
中断分为两大类：
    Interrupt(中断)：一般异步发生，包括硬件中断(可以屏蔽)、软件的INT类指令(不可以屏蔽)
    Exception(例外)：同步发生，包括程序错误、机器自检例外、软件的INT类指令

IDT表项分为三类：
    Task-gate：以专项任务的方式处理中断
    Interrupt-gate：一般理解上的中断处理入口
    Trap-gate：同上，只是不自动清除IF中断标志位

      0～31 ：32个，Intel架构专用，主要是指令执行引起的各种例外
     32～47 ：16个，为老旧的两级8259A级联中断
     48～155：108个，为用户自定义的interrupt
       (156)：1个，IOMMU
       (157)：1个，APIC定时器
       (158)：1个，远程调用IPI
       (159)：1个，重新调度IPI
    160～254：95个，为用户自定义的trap
         255：1个，伪中断
*/

/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <machine/interrupt.h>

#define PIC_IRQ_LINES 16    /*Z 两个8259A的中断线数量 */
#define IOAPIC_IRQ_LINES 24 /*Z 一个I/O APIC的中断线数量 */

/* interrupt vectors (corresponds to IDT entries) */

#define IRQ_INT_OFFSET 0x20 /*Z 8259 Programmable Interrupt Controller (PIC)重新映射的中断向量表偏移量 */
#define IRQ_CNODE_SLOT_BITS 8   /*Z IRQ的CNode的CSlot数量(位表示) */
/*Z 中断向量号 */
typedef enum _interrupt_t {
    int_invalid                 = -1,   /*Z 无效的中断号 */
    int_debug                   = 1,    /*Z DEBUG。Intel推荐硬件使用 */
    int_software_break_request  = 3,    /*Z 软件DEBUG */
    int_unimpl_dev              = 7,    /*Z 无FPU */
    int_gp_fault                = 13,   /*Z General Protection Fault */
    int_page_fault              = 14,   /*Z 页错误 */
    int_irq_min                 = IRQ_INT_OFFSET, /* First IRQ. */                          /*Z 0~31保留给Intel，主要是指令执行引起的各种例外 */
    int_irq_isa_min             = IRQ_INT_OFFSET, /* Beginning of PIC IRQs */
    int_irq_isa_max             = IRQ_INT_OFFSET + PIC_IRQ_LINES - 1, /* End of PIC IRQs */ /*Z 32~47为老旧的两级8259A级联中断 */
    int_irq_user_min            = IRQ_INT_OFFSET + PIC_IRQ_LINES, /* First user available vector */
    int_irq_user_max            = 155,                                                      /*Z 48~155为用户自定义的interrupt，主要是I/O APIC */
#ifdef CONFIG_IOMMU
    int_iommu                   = 156,  /*Z IOMMU中断 */
#endif
    int_timer                   = 157,  /*Z APIC定时器 */
#ifdef ENABLE_SMP_SUPPORT
    int_remote_call_ipi         = 158,  /*Z 远程调用IPI向量号 */
    int_reschedule_ipi          = 159,  /*Z 重调度IPI向量号 */
    int_irq_max                 = 159, /* int_reschedule_ipi is the max irq *//*Z 最大的纯硬件中断向量号 */
#else
    int_irq_max                 = 157, /* int_timer is the max irq */
#endif
    int_trap_min,                                                                           /*Z seL4设置：此向量号以下，除3和INT1外其它均不能由用户空间INT指令触发 */
    int_trap_max                = 254,                                                      /*Z 160~254为用户自定义的trap */
    int_spurious                = 255,                                                      /*Z 伪中断向量号 */
    int_max                     = 255
} interrupt_t;
/*Z 128个硬件IRQ号(256个中断中除32个保留向量、95个自定义trap、1个伪中断) */
typedef enum _platform_irq_t {
    irq_isa_min                 = int_irq_isa_min     - IRQ_INT_OFFSET,
    irq_isa_max                 = int_irq_isa_max     - IRQ_INT_OFFSET, /*Z 0~15为老旧的两级8259A级联中断 */
    irq_user_min                = int_irq_user_min    - IRQ_INT_OFFSET,
    irq_user_max                = int_irq_user_max    - IRQ_INT_OFFSET, /*Z 16~123为用户自定义中断 */
#ifdef CONFIG_IOMMU
    irq_iommu                   = int_iommu           - IRQ_INT_OFFSET, /*Z IOMMU中断 */
#endif
    irq_timer                   = int_timer           - IRQ_INT_OFFSET, /*Z APIC定时器 */
#ifdef ENABLE_SMP_SUPPORT
    irq_remote_call_ipi         = int_remote_call_ipi - IRQ_INT_OFFSET, /*Z 远程调用IPI。同步阻塞等待处理完成 */
    irq_reschedule_ipi          = int_reschedule_ipi  - IRQ_INT_OFFSET, /*Z 重调度IPI。异步不阻塞 */
#endif
    maxIRQ                      = int_irq_max         - IRQ_INT_OFFSET, /*Z 最大的纯硬件中断数量 - 1 */
    /* This is explicitly 255, instead of -1 like on some other platforms, to ensure
     * that comparisons between an irq_t (a uint8_t) and irqInvalid (some kind of signed int)
     * are well defined and behave as expected */
    irqInvalid                  = 255,  /*Z 无效的IRQ */
} platform_irq_t;
/*Z 以下各内存位置均在头1M空间的顶384K内 */
#define KERNEL_TIMER_IRQ irq_timer
#define BIOS_PADDR_START 0x0e0000                       /*Z ACPI-RSDP信息的起始地址之一，ROM */
#define BIOS_PADDR_END   0x100000                       /*Z ROM和显示内存的结束位置（1M）*/

#define BIOS_PADDR_VIDEO_RAM_START 0x000A0000           /*Z 显示内存起始物理地址 */
/* The text mode framebuffer exists part way into the video ram region */
#define BIOS_PADDR_VIDEO_RAM_TEXT_MODE_START 0x000B8000
#define BIOS_PADDR_IVDEO_RAM_END 0x000C0000             /*Z 显示内存结束物理地址 */


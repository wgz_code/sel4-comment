/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <object.h>

#ifdef CONFIG_KERNEL_MCS
/*Z 线程是否有超时错误通知能力 */
static inline bool_t validTimeoutHandler(tcb_t *tptr)
{
    return cap_get_capType(TCB_PTR_CTE_PTR(tptr, tcbTimeoutHandler)->cap) == cap_endpoint_cap;
}

void handleTimeout(tcb_t *tptr);
void handleNoFaultHandler(tcb_t *tptr);
bool_t sendFaultIPC(tcb_t *tptr, cap_t handlerCap, bool_t can_donate);
#else
exception_t sendFaultIPC(tcb_t *tptr);/*Z 将内核错误以指定线程(应也是导致错误者)为发送方，以其错误处理线程为接收方，按阻塞并等待处理结果方式发送IPC */
void handleDoubleFault(tcb_t *tptr, seL4_Fault_t ex1);/*Z 打印二次错误信息并置线程为不活跃状态。参数为第一次错误，第二次错误在全局变量中 */
#endif
void handleFault(tcb_t *tptr);/*Z 将内核错误发送给线程的错误处理对象，线程阻塞并等待处理结果 */


/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <types.h>
#include <api/failures.h>
#include <api/types.h>
#include <object/structures.h>

struct lookupCap_ret {
    exception_t status;
    cap_t cap;
};
typedef struct lookupCap_ret lookupCap_ret_t;/*Z 查询能力的结果 */

struct lookupCapAndSlot_ret {
    exception_t status;
    cap_t cap;      /*Z 能力 */
    cte_t *slot;    /*Z CSlot */
};
typedef struct lookupCapAndSlot_ret lookupCapAndSlot_ret_t;/*Z 查询CSlot及其能力的结果 */

struct lookupSlot_raw_ret {
    exception_t status;
    cte_t *slot;
};
typedef struct lookupSlot_raw_ret lookupSlot_raw_ret_t;/*Z 查询CSlot的结果 */

struct lookupSlot_ret {
    exception_t status;
    cte_t *slot;
};
typedef struct lookupSlot_ret lookupSlot_ret_t;/*Z 查询CSlot的结果 */

struct resolveAddressBits_ret {
    exception_t status;     /*Z 解析是否成功 */
    cte_t *slot;            /*Z 得到的CSlot地址 */
    word_t bitsRemaining;   /*Z 剩余未解析的位数 */
};
typedef struct resolveAddressBits_ret resolveAddressBits_ret_t;/*Z 解析CSlot指针的结果 */

lookupCap_ret_t lookupCap(tcb_t *thread, cptr_t cPtr);/*Z 查找线程中CSlot句柄指示的CSlot的能力 */
lookupCapAndSlot_ret_t lookupCapAndSlot(tcb_t *thread, cptr_t cPtr);
lookupSlot_raw_ret_t lookupSlot(tcb_t *thread, cptr_t capptr);/*Z 查找线程中CSlot句柄指示的CSlot */
lookupSlot_ret_t lookupSlotForCNodeOp(bool_t isSource,
                                      cap_t root, cptr_t capptr,/*Z 自CNode能力开始，按CSlot句柄中右数depth位数的指示，查找最终CSlot地址。isSource标识是否IPC源方 */
                                      word_t depth);
lookupSlot_ret_t lookupSourceSlot(cap_t root, cptr_t capptr,/*Z 自IPC源方CNode能力开始，按CSlot句柄中右数depth位数的指示，查找最终CSlot地址 */
                                  word_t depth);
lookupSlot_ret_t lookupTargetSlot(cap_t root, cptr_t capptr,/*Z 自IPC目标方CNode能力开始，按CSlot句柄中右数depth位数的指示，查找最终CSlot地址 */
                                  word_t depth);
lookupSlot_ret_t lookupPivotSlot(cap_t root, cptr_t capptr,/*Z 自CNode能力开始，按CSlot句柄中右数depth位数的指示，查找最终CSlot地址 */
                                 word_t depth);
resolveAddressBits_ret_t resolveAddressBits(cap_t nodeCap,/*Z 自nodeCap能力开始，按capptr CSlot句柄中右数n_bits位数的指示，解析CSlot地址，直至达到“叶子”CSlot或指示位用完 */
                                            cptr_t capptr,
                                            word_t n_bits);


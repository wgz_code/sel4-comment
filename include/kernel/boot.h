/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once
#include <bootinfo.h>
#include <arch/bootinfo.h>
/*Z 空闲物理内存区域数量上限 */
#ifndef CONFIG_ARCH_ARM
#define MAX_NUM_FREEMEM_REG 16
#else
/* Modifiers:
 *  + 1: allow the kernel to release its own boot data region
 *  + 1: possible gap between ELF images and rootserver objects;
 *       see arm/arch_init_freemem */
#define MAX_NUM_FREEMEM_REG (ARRAY_SIZE(avail_p_regs) + MODE_RESERVED + 1 + 1)
#endif

/*
 * Resolve naming differences between the abstract specifications
 * of the bootstrapping phase and the runtime phase of the kernel.
 */
typedef cte_t  slot_t;      /*Z CSlot */
typedef cte_t *slot_ptr_t;  /*Z CSlot指针 */
#define SLOT_PTR(pptr, pos) (((slot_ptr_t)(pptr)) + (pos))  /*Z CNode的某个CSlot，pptr[pos] */
#define pptr_of_cap (pptr_t)cap_get_capPtr                  /*Z 获取能力所指对象的线性地址 */

/* (node-local) state accessed only during bootstrapping */
/*Z 引导时用的数据结构，包括内存、bootinfo、CSlot索引等 */
typedef struct ndks_boot {
    p_region_t reserved[MAX_NUM_RESV_REG];  /*Z 1M以上所有RAM，包括APIC、IOMMU占用的。连续的内存记为一项 */
    word_t resv_count;                      /*Z 上述数组长度 */
    region_t   freemem[MAX_NUM_FREEMEM_REG];/*Z 0～内核～initrd以外的物理内存(线性地址)。空闲的 */
    seL4_BootInfo      *bi_frame;           /*Z bootinfo指针 */
    seL4_SlotPos slot_pos_cur;              /*Z 当前空闲CSlot索引 */
    seL4_SlotPos slot_pos_max;              /*Z 最大能力槽(数组)数量(长度)。x86_64是8192 */
} ndks_boot_t;

extern ndks_boot_t ndks_boot;

/* function prototypes */
/*Z 区域参数的起止地址相等则将其视为空区域 */
static inline bool_t is_reg_empty(region_t reg)
{
    return reg.start == reg.end;
}

void init_freemem(word_t n_available, const p_region_t *available,
                  word_t n_reserved, region_t *reserved,
                  v_region_t it_v_reg, word_t extra_bi_size_bits);
bool_t reserve_region(p_region_t reg);/*Z 将内存区域加入到保留区域列表中，返回值1成功，0失败 */
bool_t insert_region(region_t reg);
void write_slot(slot_ptr_t slot_ptr, cap_t cap);
cap_t create_root_cnode(void);/*Z 初始化rootserver的CNode，创建一个指向自身且能操作本身的能力CSlot */
bool_t provide_cap(cap_t root_cnode_cap, cap_t cap);/*Z 在rootserver的根CNode当前空闲CSlot写入能力，并递进空闲CSlot指针 */
cap_t create_it_asid_pool(cap_t root_cnode_cap);
void write_it_pd_pts(cap_t root_cnode_cap, cap_t it_pd_cap);
bool_t create_idle_thread(void);
bool_t create_untypeds_for_region(cap_t root_cnode_cap, bool_t device_memory, region_t reg,
                                  seL4_SlotPos first_untyped_slot);
bool_t create_device_untypeds(cap_t root_cnode_cap, seL4_SlotPos slot_pos_before);
bool_t create_kernel_untypeds(cap_t root_cnode_cap, region_t boot_mem_reuse_reg, seL4_SlotPos first_untyped_slot);
void bi_finalise(void);
void create_domain_cap(cap_t root_cnode_cap);

cap_t create_ipcbuf_frame_cap(cap_t root_cnode_cap, cap_t pd_cap, vptr_t vptr);
word_t calculate_extra_bi_size_bits(word_t extra_size);
void populate_bi_frame(node_id_t node_id, word_t num_nodes, vptr_t ipcbuf_vptr,
                       word_t extra_bi_size_bits);
void create_bi_frame_cap(cap_t root_cnode_cap, cap_t pd_cap, vptr_t vptr);

#ifdef CONFIG_KERNEL_MCS
bool_t init_sched_control(cap_t root_cnode_cap, word_t num_nodes);
#endif
/*Z 创建能力的结果 */
typedef struct create_frames_of_region_ret {
    seL4_SlotRegion region;
    bool_t success;
} create_frames_of_region_ret_t;
/*Z 为线性区域在CNode中创建物理页映射能力，视情在VSpace中创建页表项 */
create_frames_of_region_ret_t
create_frames_of_region(
    cap_t    root_cnode_cap,
    cap_t    pd_cap,
    region_t reg,
    bool_t   do_map,
    sword_t  pv_offset
);

cap_t
create_it_pd_pts(
    cap_t      root_cnode_cap,
    v_region_t ui_v_reg,
    vptr_t     ipcbuf_vptr,
    vptr_t     bi_frame_vptr
);

tcb_t *
create_initial_thread(
    cap_t  root_cnode_cap,
    cap_t  it_pd_cap,
    vptr_t ui_v_entry,
    vptr_t bi_frame_vptr,
    vptr_t ipcbuf_vptr,
    cap_t  ipcbuf_cap
);
/*Z 初始化系统运行状态，设置调度器预选对象 */
void init_core_state(tcb_t *scheduler_action);
/*Z init线程专属重要数据，这是一切力量的源泉。地址均为线性地址 */
/* state tracking the memory allocated for root server objects */
typedef struct {
    pptr_t cnode;       /*Z CNode地址 */
    pptr_t vspace;      /*Z VSpace地址，也即一级页表 */
    pptr_t asid_pool;   /*Z ASID(PCID) pool地址 */
    pptr_t ipc_buf;     /*Z IPC buffer地址 */
    pptr_t boot_info;   /*Z bootinfo地址 */
    pptr_t extra_bi;    /*Z 额外bootinfo的地址 */
    pptr_t tcb;         /*Z TCB地址 */
#ifdef CONFIG_KERNEL_MCS
    pptr_t sc;          /*Z 最小的调度上下文数据结构地址 */
#endif
    region_t paging;    /*Z 页表地址区域（除一级页表），页表使用后该区域递减 */
} rootserver_mem_t;

extern rootserver_mem_t rootserver;
/*Z 区域用bits位页对齐并表示需要几页 */
/* get the number of paging structures required to cover it_v_reg, with
 * the paging structure covering `bits` of the address range - for a 4k page
 * `bits` would be 12 */
static inline BOOT_CODE word_t get_n_paging(v_region_t v_reg, word_t bits)
{
    vptr_t start = ROUND_DOWN(v_reg.start, bits);
    vptr_t end = ROUND_UP(v_reg.end, bits);
    return (end - start) / BIT(bits);/*Z 不好：应该 (end - start) >> bits */
}
/*Z 从rootserver的页表区域分配一页作为页表 */
/* allocate a page table sized structure from rootserver.paging */
static inline BOOT_CODE pptr_t it_alloc_paging(void)
{
    pptr_t allocated = rootserver.paging.start;
    rootserver.paging.start += BIT(seL4_PageTableBits);
    assert(rootserver.paging.start <= rootserver.paging.end);
    return allocated;
}
/*Z 区域需要的页表页总数（除一级页表页）*/
/* return the amount of paging structures required to cover v_reg */
word_t arch_get_n_paging(v_region_t it_veg);

/* Create pptrs for all root server objects, starting at pptr, to cover the
 * virtual memory region v_reg, and any extra boot info. */
void create_rootserver_objects(pptr_t start, v_region_t v_reg, word_t extra_bi_size_bits);

/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>

#ifdef CONFIG_VTX

#include <model/statedata.h>
#include <arch/kernel/ept.h>
#include <arch/api/invocation.h>

struct lookupEPTPDPTSlot_ret {
    exception_t status;
    ept_pdpte_t *pdptSlot;  /*Z EPT二级页表项线性地址 */
};
typedef struct lookupEPTPDPTSlot_ret lookupEPTPDPTSlot_ret_t;/*Z 查找EPT二级页表项线性地址结果 */

struct lookupEPTPDSlot_ret {
    exception_t status;
    ept_pde_t  *pdSlot;     /*Z EPT三级页表项线性地址 */
};
typedef struct lookupEPTPDSlot_ret lookupEPTPDSlot_ret_t;/*Z 查找EPT三级页表项线性地址结果 */

struct lookupEPTPTSlot_ret {
    exception_t status;
    ept_pte_t  *ptSlot;     /*Z EPT四级页表项线性地址 */
};
typedef struct lookupEPTPTSlot_ret lookupEPTPTSlot_ret_t;/*Z 查找EPT四级页表项线性地址结果 */

enum ept_cache_options {
    EPTUncacheable = 0,
    EPTWriteCombining = 1,
    EPTWriteThrough = 4,
    EPTWriteProtected = 5,
    EPTWriteBack = 6
};
typedef enum ept_cache_options ept_cache_options_t;/*Z EPT memory type */
/*Z 清除EPT一级页表在ASID(PCID) pool中的映射项 */
void deleteEPTASID(asid_t asid, ept_pml4e_t *ept)
{
    asid_pool_t *poolPtr;
    /*Z ASID(PCID) pool */
    poolPtr = x86KSASIDTable[asid >> asidLowBits];
    if (poolPtr != NULL) {
        asid_map_t asid_map = poolPtr->array[asid & MASK(asidLowBits)];
        if (asid_map_get_type(asid_map) == asid_map_asid_map_ept &&
            (ept_pml4e_t *)asid_map_asid_map_ept_get_ept_root(asid_map) == ept) {
            poolPtr->array[asid & MASK(asidLowBits)] = asid_map_asid_map_none_new();
        }
    }
}
/*Z 解映射一个EPT内存页：清页表、复位能力。cap为cap-frame-cap能力，cte为其CSlot */
exception_t performX86EPTPageInvocationUnmap(cap_t cap, cte_t *ctSlot)
{
    unmapEPTPage(/*Z 清除指定内存页的EPT页表映射 */
        cap_frame_cap_get_capFSize(cap),
        cap_frame_cap_get_capFMappedASID(cap),
        cap_frame_cap_get_capFMappedAddress(cap),
        (void *)cap_frame_cap_get_capFBasePtr(cap)
    );

    cap_frame_cap_ptr_set_capFMappedAddress(&ctSlot->cap, 0);
    cap_frame_cap_ptr_set_capFMappedASID(&ctSlot->cap, asidInvalid);
    cap_frame_cap_ptr_set_capFMapType(&ctSlot->cap, X86_MappingNone);

    return EXCEPTION_NONE;
}
/*Z 查找ASID(PCID)对应的EPT一级页表线性地址 */
findEPTForASID_ret_t findEPTForASID(asid_t asid)
{
    findEPTForASID_ret_t ret;
    asid_map_t asid_map;
    /*Z 返回ASID(PCID)对应pool的映射项 */
    asid_map = findMapForASID(asid);
    if (asid_map_get_type(asid_map) != asid_map_asid_map_ept) {/*Z 映射项类型 */
        current_lookup_fault = lookup_fault_invalid_root_new();/*Z 标记无效的root错误 */

        ret.ept = NULL;
        ret.status = EXCEPTION_LOOKUP_FAULT;
        return ret;
    }
    /*Z 获取EPT一级页表线性地址 */
    ret.ept = (ept_pml4e_t *)asid_map_asid_map_ept_get_ept_root(asid_map);
    ret.status = EXCEPTION_NONE;
    return ret;
}
/*Z 获取虚拟地址的EPT一级页表项指针 */
static ept_pml4e_t *CONST lookupEPTPML4Slot(ept_pml4e_t *pml4, vptr_t vptr)
{
    return pml4 + GET_EPT_PML4_INDEX(vptr);
}
/*Z 查找虚拟地址的EPT二级页表项线性地址 */
static lookupEPTPDPTSlot_ret_t CONST lookupEPTPDPTSlot(ept_pml4e_t *pml4, vptr_t vptr)
{
    lookupEPTPDPTSlot_ret_t ret;
    ept_pml4e_t *pml4Slot;
    /*Z 获取虚拟地址的EPT一级页表项指针 */
    pml4Slot = lookupEPTPML4Slot(pml4, vptr);

    if (!ept_pml4e_ptr_get_read(pml4Slot)) {/*Z 所指内存不允许读 */
        current_lookup_fault = lookup_fault_missing_capability_new(EPT_PML4_INDEX_OFFSET);

        ret.pdptSlot = NULL;
        ret.status = EXCEPTION_LOOKUP_FAULT;
        return ret;
    }
    /*Z 二级页表线性地址 */
    ept_pdpte_t *pdpt = paddr_to_pptr(ept_pml4e_ptr_get_pdpt_base_address(pml4Slot));
    uint32_t index = GET_EPT_PDPT_INDEX(vptr);
    ret.pdptSlot = pdpt + index;
    ret.status = EXCEPTION_NONE;
    return ret;
}
/*Z 查找虚拟地址的EPT三级页表项线性地址 */
static lookupEPTPDSlot_ret_t lookupEPTPDSlot(ept_pml4e_t *pml4, vptr_t vptr)
{
    lookupEPTPDSlot_ret_t ret;
    lookupEPTPDPTSlot_ret_t lu_ret;
    /*Z 查找虚拟地址的EPT二级页表项线性地址 */
    lu_ret = lookupEPTPDPTSlot(pml4, vptr);
    if (lu_ret.status != EXCEPTION_NONE) {
        current_syscall_error.type = seL4_FailedLookup;
        current_syscall_error.failedLookupWasSource = false;
        /* current_lookup_fault will have been set by lookupEPTPDPTSlot */
        ret.pdSlot = NULL;
        ret.status = EXCEPTION_LOOKUP_FAULT;
        return ret;
    }

    if (!ept_pdpte_ptr_get_read(lu_ret.pdptSlot)) {/*Z 所指内存不允许读。不好：没检查是否1G页，下面那个检查了 */
        current_lookup_fault = lookup_fault_missing_capability_new(EPT_PDPT_INDEX_OFFSET);

        ret.pdSlot = NULL;
        ret.status = EXCEPTION_LOOKUP_FAULT;
        return ret;
    }
    /*Z 三级页表线性地址 */
    ept_pde_t *pd = paddr_to_pptr(ept_pdpte_ptr_get_pd_base_address(lu_ret.pdptSlot));
    uint32_t index = GET_EPT_PD_INDEX(vptr);
    ret.pdSlot = pd + index;
    ret.status = EXCEPTION_NONE;
    return ret;
}
/*Z 查找虚拟地址的EPT四级页表项线性地址 */
static lookupEPTPTSlot_ret_t lookupEPTPTSlot(ept_pml4e_t *pml4, vptr_t vptr)
{
    lookupEPTPTSlot_ret_t ret;
    lookupEPTPDSlot_ret_t lu_ret;
    /*Z 查找虚拟地址的EPT三级页表项线性地址 */
    lu_ret = lookupEPTPDSlot(pml4, vptr);
    if (lu_ret.status != EXCEPTION_NONE) {
        current_syscall_error.type = seL4_FailedLookup;
        current_syscall_error.failedLookupWasSource = false;
        /* current_lookup_fault will have been set by lookupEPTPDSlot */
        ret.ptSlot = NULL;
        ret.status = EXCEPTION_LOOKUP_FAULT;
        return ret;
    }
    /*Z 所指内存是2M页、或不可读 */
    if ((ept_pde_ptr_get_page_size(lu_ret.pdSlot) != ept_pde_ept_pde_pt) ||
        !ept_pde_ept_pde_pt_ptr_get_read(lu_ret.pdSlot)) {
        current_lookup_fault = lookup_fault_missing_capability_new(EPT_PD_INDEX_OFFSET);

        ret.ptSlot = NULL;
        ret.status = EXCEPTION_LOOKUP_FAULT;
        return ret;
    }

    ept_pte_t *pt = paddr_to_pptr(ept_pde_ept_pde_pt_ptr_get_pt_base_address(lu_ret.pdSlot));
    uint32_t index = GET_EPT_PT_INDEX(vptr);

    ret.ptSlot = pt + index;
    ret.status = EXCEPTION_NONE;
    return ret;
}
/*Z 将参数直接转成EPT memory type */
static ept_cache_options_t eptCacheFromVmAttr(vm_attributes_t vmAttr)
{
    /* PAT cache options are 1-1 with ept_cache_options. But need to
       verify user has specified a sensible option */
    ept_cache_options_t option = vmAttr.words[0];
    if (option != EPTUncacheable ||
        option != EPTWriteCombining ||
        option != EPTWriteThrough ||
        option != EPTWriteBack) {
        /* No failure mode is supported here, vmAttr settings should be verified earlier */
        option = EPTWriteBack;
    }
    return option;
}
/*Z 获取EPT二级页表的一级页表项。asid二级页表关联的ASID(PCID)，vptr该页表虚拟地址，pdpt其线性地址 */
EPTPDPTMapped_ret_t EPTPDPTMapped(asid_t asid, vptr_t vptr, ept_pdpte_t *pdpt)
{
    EPTPDPTMapped_ret_t ret;
    findEPTForASID_ret_t asid_ret;
    ept_pml4e_t *pml4Slot;
    /*Z EPT一级页表 */
    asid_ret = findEPTForASID(asid);
    if (asid_ret.status != EXCEPTION_NONE) {
        ret.pml4 = NULL;
        ret.pml4Slot = NULL;
        ret.status = asid_ret.status;
        return ret;
    }
    /*Z EPT一级页表项 */
    pml4Slot = lookupEPTPML4Slot(asid_ret.ept, vptr);

    if (ept_pml4e_ptr_get_read(pml4Slot)
        && ptrFromPAddr(ept_pml4e_ptr_get_pdpt_base_address(pml4Slot)) == pdpt) {
        ret.pml4 = asid_ret.ept;
        ret.pml4Slot = pml4Slot;
        ret.status = EXCEPTION_NONE;
        return ret;
    } else {/*Z 所指内存不可读 */
        ret.pml4 = NULL;
        ret.pml4Slot = NULL;
        ret.status = EXCEPTION_LOOKUP_FAULT;
        return ret;
    }
}
/*Z 清除EPT二级页表的一级页表项，清缓存。asid二级页表关联的ASID(PCID)，vptr该页表虚拟地址，pdpt其线性地址 */
void unmapEPTPDPT(asid_t asid, vptr_t vaddr, ept_pdpte_t *pdpt)
{
    EPTPDPTMapped_ret_t lu_ret;
    /*Z 获取EPT二级页表的一级页表项 */
    lu_ret = EPTPDPTMapped(asid, vaddr, pdpt);

    if (lu_ret.status == EXCEPTION_NONE) {
        *lu_ret.pml4Slot = ept_pml4e_new(0, 0, 0, 0);
        invept(lu_ret.pml4);/*Z 失效EPT相关的TLB和页缓存 */
    }
}
/*Z 清除EPT二级页表的一级页表项，清缓存和内存，置能力未映射标记 */
static exception_t performEPTPDPTInvocationUnmap(cap_t cap, cte_t *cte)
{
    if (cap_ept_pdpt_cap_get_capPDPTIsMapped(cap)) {
        ept_pdpte_t *pdpt = (ept_pdpte_t *)cap_ept_pdpt_cap_get_capPDPTBasePtr(cap);
        unmapEPTPDPT(
            cap_ept_pdpt_cap_get_capPDPTMappedASID(cap),
            cap_ept_pdpt_cap_get_capPDPTMappedAddress(cap),
            pdpt);
        clearMemory((void *)pdpt, cap_get_capSizeBits(cap));
    }
    cap_ept_pdpt_cap_ptr_set_capPDPTIsMapped(&(cte->cap), 0);

    return EXCEPTION_NONE;
}
/*Z 设置CSlot的值，设置EPT一级页表项的值，清缓存 */
static exception_t performEPTPDPTInvocationMap(cap_t cap, cte_t *cte, ept_pml4e_t pml4e, ept_pml4e_t *pml4Slot,
                                               ept_pml4e_t *pml4)
{
    cte->cap = cap;
    *pml4Slot = pml4e;
    invept(pml4);

    return EXCEPTION_NONE;
}
/*Z 引用cap_ept_pdpt_cap能力的系统调用：映射、解映射EPT二级页表 */
static exception_t decodeX86EPTPDPTInvocation(
    word_t invLabel,        /*Z 消息标签(错误类型) */
    word_t length,          /*Z 消息长度 */
    cte_t *cte,             /*Z 其CSlot */
    cap_t cap,              /*Z 其能力 */
    extra_caps_t excaps,    /*Z 额外能力 */
    word_t *buffer          /*Z IPC buffer */
)
{
    word_t          vaddr;
    cap_t           pml4Cap;
    ept_pml4e_t    *pml4;
    ept_pml4e_t     pml4e;
    paddr_t         paddr;
    asid_t          asid;
    findEPTForASID_ret_t find_ret;
    ept_pml4e_t    *pml4Slot;

    if (invLabel == X86EPTPDPTUnmap) {/*Z --------------------------------------------子功能：解映射 */
        if (!isFinalCapability(cte)) {
            current_syscall_error.type = seL4_RevokeFirst;
            return EXCEPTION_SYSCALL_ERROR;
        }
        setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
        return performEPTPDPTInvocationUnmap(cap, cte);/*Z 清除EPT二级页表的一级页表项，清缓存和内存，置能力未映射标记 */
    }

    if (invLabel != X86EPTPDPTMap) {/*Z ----------------------------------------------子功能：映射 */
        userError("X86EPTPDPT Illegal operation.");
        current_syscall_error.type = seL4_IllegalOperation;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (length < 2 || excaps.excaprefs[0] == NULL) {/*Z 不好：这里并未使用另一个消息寄存器，并且x86也不支持EPT指代页表的页属性设置 */
        userError("X86EPTPDPTMap: Truncated message.");
        current_syscall_error.type = seL4_TruncatedMessage;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (cap_ept_pdpt_cap_get_capPDPTIsMapped(cap)) {/*Z 验证EPT二级页表尚未映射 */
        userError("X86EPTPDPTMap: EPT PDPT is already mapped to a PML4.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 0;

        return EXCEPTION_SYSCALL_ERROR;
    }

    vaddr = getSyscallArg(0, buffer);/*Z -------------------------------------------------消息传参：0-负责的首个虚拟地址 */
    /* cannot use ~MASK(EPT_PML4_INDEX_OFFSET) because on 32-bit compilations
     * this results in an error shifting by greater than 31 bits, so we manually
     * force a 64-bit variable to do the shifting with */
    vaddr = vaddr & ~(((uint64_t)1 << EPT_PML4_INDEX_OFFSET) - 1);
    pml4Cap = excaps.excaprefs[0]->cap;                                                         /*Z extraCaps0-要映射的EPT一级页表 */

    if (cap_get_capType(pml4Cap) != cap_ept_pml4_cap) {/*Z 验证EPT一级页表能力类型 */
        userError("X86EPTPDPTMap: Not a valid EPT PML4.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 1;

        return EXCEPTION_SYSCALL_ERROR;
    }

    pml4 = (ept_pml4e_t *)cap_ept_pml4_cap_get_capPML4BasePtr(pml4Cap);
    asid = cap_ept_pml4_cap_get_capPML4MappedASID(pml4Cap);

    find_ret = findEPTForASID(asid);
    if (find_ret.status != EXCEPTION_NONE) {/*Z 验证ASID有效性 */
        current_syscall_error.type = seL4_FailedLookup;
        current_syscall_error.failedLookupWasSource = false;

        return EXCEPTION_SYSCALL_ERROR;
    }

    if (find_ret.ept != pml4) {/*Z 验证EPT一级页表线性地址有效性 */
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 1;

        return EXCEPTION_SYSCALL_ERROR;
    }

    pml4Slot = lookupEPTPML4Slot(pml4, vaddr);

    if (ept_pml4e_ptr_get_read(pml4Slot)) {/*Z 验证一级页表项未被占用 */
        userError("X86EPTPDPTMap: PDPT already mapped here.");
        current_syscall_error.type = seL4_DeleteFirst;
        return EXCEPTION_SYSCALL_ERROR;
    }

    paddr = pptr_to_paddr((void *)cap_ept_pdpt_cap_get_capPDPTBasePtr(cap));
    pml4e = ept_pml4e_new(
                paddr,
                1,
                1,
                1
            );

    cap = cap_ept_pdpt_cap_set_capPDPTIsMapped(cap, 1);
    cap = cap_ept_pdpt_cap_set_capPDPTMappedASID(cap, asid);
    cap = cap_ept_pdpt_cap_set_capPDPTMappedAddress(cap, vaddr);

    setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
    return performEPTPDPTInvocationMap(cap, cte, pml4e, pml4Slot, pml4);/*Z 设置CSlot的值，设置EPT一级页表项的值，清缓存 */
}
/*Z 引用cap_ept_*_cap能力的系统调用：映射、解映射EPT二、三、四级页表 */
exception_t decodeX86EPTInvocation(
    word_t invLabel,
    word_t length,
    cptr_t cptr,
    cte_t *cte,
    cap_t cap,
    extra_caps_t excaps,
    word_t *buffer
)
{
    switch (cap_get_capType(cap)) {
    case cap_ept_pdpt_cap:/*Z 映射、解映射EPT二级页表 */
        return decodeX86EPTPDPTInvocation(invLabel, length, cte, cap, excaps, buffer);
    case cap_ept_pd_cap:/*Z 映射、解映射EPT三级页表 */
        return decodeX86EPTPDInvocation(invLabel, length, cte, cap, excaps, buffer);
    case cap_ept_pt_cap:/*Z 映射、解映射EPT四级页表 */
        return decodeX86EPTPTInvocation(invLabel, length, cte, cap, excaps, buffer);
    default:
        fail("Invalid cap type");
    }
}
/*Z 获取EPT三级页表的二级页表项。asid三级页表关联的ASID(PCID)，vptr该页表虚拟地址，pdpt其线性地址 */
EPTPageDirectoryMapped_ret_t EPTPageDirectoryMapped(asid_t asid, vptr_t vaddr, ept_pde_t *pd)
{
    EPTPageDirectoryMapped_ret_t ret;
    lookupEPTPDPTSlot_ret_t find_ret;
    findEPTForASID_ret_t asid_ret;

    asid_ret = findEPTForASID(asid);
    if (asid_ret.status != EXCEPTION_NONE) {
        ret.pml4 = NULL;
        ret.pdptSlot = NULL;
        ret.status = asid_ret.status;
        return ret;
    }

    find_ret = lookupEPTPDPTSlot(asid_ret.ept, vaddr);
    if (find_ret.status != EXCEPTION_NONE) {
        ret.pml4 = NULL;
        ret.pdptSlot = NULL;
        ret.status = find_ret.status;
        return ret;
    }

    if (ept_pdpte_ptr_get_read(find_ret.pdptSlot)
        && ptrFromPAddr(ept_pdpte_ptr_get_pd_base_address(find_ret.pdptSlot)) == pd) {
        ret.pml4 = asid_ret.ept;
        ret.pdptSlot = find_ret.pdptSlot;
        ret.status = EXCEPTION_NONE;
        return ret;
    } else {
        ret.pml4 = NULL;
        ret.pdptSlot = NULL;
        ret.status = EXCEPTION_LOOKUP_FAULT;
        return ret;
    }
}
/*Z 清除EPT三级页表的二级页表项，清缓存。asid三级页表关联的ASID(PCID)，vptr该页表虚拟地址，pdpt其线性地址 */
void unmapEPTPageDirectory(asid_t asid, vptr_t vaddr, ept_pde_t *pd)
{
    EPTPageDirectoryMapped_ret_t lu_ret;

    lu_ret = EPTPageDirectoryMapped(asid, vaddr, pd);

    if (lu_ret.status == EXCEPTION_NONE) {
        *lu_ret.pdptSlot = ept_pdpte_new(
                               0,  /* pd_base_address  */
                               0,  /* avl_cte_depth    */
                               0,  /* execute          */
                               0,  /* write            */
                               0   /* read             */
                           );
        invept(lu_ret.pml4);
    }
}
/*Z 清除EPT三级页表的二级页表项，清缓存和内存，置能力未映射标记 */
static exception_t performEPTPDInvocationUnmap(cap_t cap, cte_t *cte)
{
    if (cap_ept_pd_cap_get_capPDIsMapped(cap)) {
        ept_pde_t *pd = (ept_pde_t *)cap_ept_pd_cap_get_capPDBasePtr(cap);
        unmapEPTPageDirectory(
            cap_ept_pd_cap_get_capPDMappedASID(cap),
            cap_ept_pd_cap_get_capPDMappedAddress(cap),
            pd);
        clearMemory((void *)pd, cap_get_capSizeBits(cap));
    }
    cap_ept_pd_cap_ptr_set_capPDIsMapped(&(cte->cap), 0);

    return EXCEPTION_NONE;
}
/*Z 设置CSlot能力值，设置二级页表项的值，清缓存 */
static exception_t performEPTPDInvocationMap(cap_t cap, cte_t *cte, ept_pdpte_t pdpte, ept_pdpte_t *pdptSlot,
                                             ept_pml4e_t *pml4)
{
    cte->cap = cap;
    *pdptSlot = pdpte;
    invept(pml4);

    return EXCEPTION_NONE;
}
/*Z 引用cap_ept_pd_cap能力的系统调用：映射、解映射EPT三级页表 */
exception_t decodeX86EPTPDInvocation(
    word_t invLabel,
    word_t length,
    cte_t *cte,
    cap_t cap,
    extra_caps_t excaps,
    word_t *buffer
)
{
    word_t          vaddr;
    cap_t           pml4Cap;
    ept_pml4e_t    *pml4;
    ept_pdpte_t     pdpte;
    paddr_t         paddr;
    asid_t          asid;
    findEPTForASID_ret_t find_ret;
    lookupEPTPDPTSlot_ret_t lu_ret;

    if (invLabel == X86EPTPDUnmap) {/*Z --------------------------------------------------子功能：解映射 */
        if (!isFinalCapability(cte)) {
            current_syscall_error.type = seL4_RevokeFirst;
            return EXCEPTION_SYSCALL_ERROR;
        }
        setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
        return performEPTPDInvocationUnmap(cap, cte);/*Z 清除EPT三级页表的二级页表项，清缓存和内存，置能力未映射标记 */
    }

    if (invLabel != X86EPTPDMap) {/*Z ----------------------------------------------------子功能：映射 */
        userError("X86EPTPD Illegal operation.");
        current_syscall_error.type = seL4_IllegalOperation;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (length < 2 || excaps.excaprefs[0] == NULL) {/*Z 不好：另一个消息寄存器参数未用 */
        userError("X86EPTPDMap: Truncated message.");
        current_syscall_error.type = seL4_TruncatedMessage;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (cap_ept_pd_cap_get_capPDIsMapped(cap)) {
        userError("X86EPTPDMap: EPT Page directory is already mapped to a PDPT.");
        current_syscall_error.type =
            seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 0;

        return EXCEPTION_SYSCALL_ERROR;
    }

    vaddr = getSyscallArg(0, buffer);/*Z -----------------------------------------消息传参：0-负责的首个虚拟地址 */
    vaddr = vaddr & ~MASK(EPT_PDPT_INDEX_OFFSET);
    pml4Cap = excaps.excaprefs[0]->cap;                                                 /*Z extraCaps0-要映射的EPT一级页表 */

    if (cap_get_capType(pml4Cap) != cap_ept_pml4_cap) {
        userError("X86EPTPDMap: Not a valid EPT pml4.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 1;

        return EXCEPTION_SYSCALL_ERROR;
    }

    pml4 = (ept_pml4e_t *)cap_ept_pml4_cap_get_capPML4BasePtr(pml4Cap);
    asid = cap_ept_pml4_cap_get_capPML4MappedASID(pml4Cap);

    find_ret = findEPTForASID(asid);
    if (find_ret.status != EXCEPTION_NONE) {
        userError("X86EPTPDMap: EPT PML4 is not mapped.");
        current_syscall_error.type = seL4_FailedLookup;
        current_syscall_error.failedLookupWasSource = false;

        return EXCEPTION_SYSCALL_ERROR;
    }

    if (find_ret.ept != pml4) {
        userError("X86EPTPDMap: EPT PML4 asid is invalid.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 1;

        return EXCEPTION_SYSCALL_ERROR;
    }

    lu_ret = lookupEPTPDPTSlot(pml4, vaddr);
    if (lu_ret.status != EXCEPTION_NONE) {
        current_syscall_error.type = seL4_FailedLookup;
        current_syscall_error.failedLookupWasSource = false;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (ept_pdpte_ptr_get_read(lu_ret.pdptSlot)) {
        userError("X86EPTPDMap: Page directory already mapped here.");
        current_syscall_error.type = seL4_DeleteFirst;
        return EXCEPTION_SYSCALL_ERROR;
    }

    paddr = pptr_to_paddr((void *)(cap_ept_pd_cap_get_capPDBasePtr(cap)));
    pdpte = ept_pdpte_new(
                paddr,  /* pd_base_address  */
                0,      /* avl_cte_depth    */
                1,      /* execute          */
                1,      /* write            */
                1       /* read             */
            );

    cap = cap_ept_pd_cap_set_capPDIsMapped(cap, 1);
    cap = cap_ept_pd_cap_set_capPDMappedASID(cap, asid);
    cap = cap_ept_pd_cap_set_capPDMappedAddress(cap, vaddr);

    setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
    return performEPTPDInvocationMap(cap, cte, pdpte, lu_ret.pdptSlot, pml4);/*Z 设置CSlot能力值，设置二级页表项的值，清缓存 */
}
/*Z 获取EPT四级页表的三级页表项。asid四级页表关联的ASID(PCID)，vptr该页表虚拟地址，pdpt其线性地址 */
EPTPageTableMapped_ret_t EPTPageTableMapped(asid_t asid, vptr_t vaddr, ept_pte_t *pt)
{
    EPTPageTableMapped_ret_t ret;
    lookupEPTPDSlot_ret_t find_ret;
    findEPTForASID_ret_t asid_ret;

    asid_ret = findEPTForASID(asid);
    if (asid_ret.status != EXCEPTION_NONE) {
        ret.pml4 = NULL;
        ret.pdSlot = NULL;
        ret.status = asid_ret.status;
        return ret;
    }

    find_ret = lookupEPTPDSlot(asid_ret.ept, vaddr);
    if (find_ret.status != EXCEPTION_NONE) {
        ret.pml4 = NULL;
        ret.pdSlot = NULL;
        ret.status = find_ret.status;
        return ret;
    }

    if (ept_pde_ptr_get_page_size(find_ret.pdSlot) == ept_pde_ept_pde_pt
        && ptrFromPAddr(ept_pde_ept_pde_pt_ptr_get_pt_base_address(find_ret.pdSlot)) == pt) {
        ret.pml4 = asid_ret.ept;
        ret.pdSlot = find_ret.pdSlot;
        ret.status = EXCEPTION_NONE;
        return ret;
    } else {
        ret.pml4 = NULL;
        ret.pdSlot = NULL;
        ret.status = EXCEPTION_LOOKUP_FAULT;
        return ret;
    }
}
/*Z 清除EPT四级页表的三级页表项，清缓存。asid四级页表关联的ASID(PCID)，vptr该页表虚拟地址，pdpt其线性地址 */
void unmapEPTPageTable(asid_t asid, vptr_t vaddr, ept_pte_t *pt)
{
    EPTPageTableMapped_ret_t lu_ret;

    lu_ret = EPTPageTableMapped(asid, vaddr, pt);

    if (lu_ret.status == EXCEPTION_NONE) {
        *lu_ret.pdSlot = ept_pde_ept_pde_pt_new(
                             0,  /* pt_base_address  */
                             0,  /* avl_cte_depth    */
                             0,  /* execute          */
                             0,  /* write            */
                             0   /* read             */
                         );
        invept(lu_ret.pml4);
    }
}
/*Z 清除EPT四级页表的三级页表项，清缓存和内存，置能力未映射标记 */
static exception_t performEPTPTInvocationUnmap(cap_t cap, cte_t *cte)
{
    if (cap_ept_pt_cap_get_capPTIsMapped(cap)) {
        ept_pte_t *pt = (ept_pte_t *)cap_ept_pt_cap_get_capPTBasePtr(cap);
        unmapEPTPageTable(
            cap_ept_pt_cap_get_capPTMappedASID(cap),
            cap_ept_pt_cap_get_capPTMappedAddress(cap),
            pt);
        clearMemory((void *)pt, cap_get_capSizeBits(cap));
    }
    cap_ept_pt_cap_ptr_set_capPTIsMapped(&(cte->cap), 0);

    return EXCEPTION_NONE;
}
/*Z 设置CSlot能力值，设置三级页表项的值，清缓存 */
static exception_t performEPTPTInvocationMap(cap_t cap, cte_t *cte, ept_pde_t pde, ept_pde_t *pdSlot, ept_pml4e_t *pml4)
{
    cte->cap = cap;
    *pdSlot = pde;
    invept(pml4);

    return EXCEPTION_NONE;
}
/*Z 引用cap_ept_pt_cap能力的系统调用：映射、解映射EPT四级页表 */
exception_t decodeX86EPTPTInvocation(
    word_t invLabel,
    word_t length,
    cte_t *cte,
    cap_t cap,
    extra_caps_t excaps,
    word_t *buffer
)
{
    word_t          vaddr;
    cap_t           pml4Cap;
    ept_pml4e_t    *pml4;
    ept_pde_t       pde;
    paddr_t         paddr;
    asid_t          asid;
    findEPTForASID_ret_t find_ret;
    lookupEPTPDSlot_ret_t lu_ret;

    if (invLabel == X86EPTPTUnmap) {/*Z --------------------------------------------------子功能：解映射 */
        if (!isFinalCapability(cte)) {
            current_syscall_error.type = seL4_RevokeFirst;
            return EXCEPTION_SYSCALL_ERROR;
        }
        setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
        return performEPTPTInvocationUnmap(cap, cte);/*Z 清除EPT四级页表的三级页表项，清缓存和内存，置能力未映射标记 */
    }

    if (invLabel != X86EPTPTMap) {/*Z ----------------------------------------------------子功能：映射 */
        userError("X86EPTPT Illegal operation.");
        current_syscall_error.type = seL4_IllegalOperation;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (length < 2 || excaps.excaprefs[0] == NULL) {/*Z 不好：另一个消息寄存器参数未用 */
        userError("X86EPTPT: Truncated message.");
        current_syscall_error.type = seL4_TruncatedMessage;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (cap_ept_pt_cap_get_capPTIsMapped(cap)) {
        userError("X86EPTPT EPT Page table is already mapped to an EPT page directory.");
        current_syscall_error.type =
            seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 0;

        return EXCEPTION_SYSCALL_ERROR;
    }

    vaddr = getSyscallArg(0, buffer);/*Z -------------------------------------------------------------消息传参：0-负责的首个虚拟地址 */
    vaddr = vaddr & ~MASK(EPT_PD_INDEX_OFFSET);
    pml4Cap = excaps.excaprefs[0]->cap;                                                                     /*Z extraCaps0-要映射的EPT一级页表 */

    if (cap_get_capType(pml4Cap) != cap_ept_pml4_cap ||
        !cap_ept_pml4_cap_get_capPML4IsMapped(pml4Cap)) {
        userError("X86EPTPTMap: Not a valid EPT pml4.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 1;

        return EXCEPTION_SYSCALL_ERROR;
    }

    pml4 = (ept_pml4e_t *)(cap_ept_pml4_cap_get_capPML4BasePtr(pml4Cap));
    asid = cap_ept_pml4_cap_get_capPML4MappedASID(pml4Cap);

    find_ret = findEPTForASID(asid);
    if (find_ret.status != EXCEPTION_NONE) {
        current_syscall_error.type = seL4_FailedLookup;
        current_syscall_error.failedLookupWasSource = false;

        return EXCEPTION_SYSCALL_ERROR;
    }

    if (find_ret.ept != pml4) {
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 1;

        return EXCEPTION_SYSCALL_ERROR;
    }

    lu_ret = lookupEPTPDSlot(pml4, vaddr);
    if (lu_ret.status != EXCEPTION_NONE) {
        current_syscall_error.type = seL4_FailedLookup;
        current_syscall_error.failedLookupWasSource = false;
        /* current_lookup_fault will have been set by lookupPTSlot */
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (((ept_pde_ptr_get_page_size(lu_ret.pdSlot) == ept_pde_ept_pde_pt) &&/*Z 不好：直接get_read就够了 */
         ept_pde_ept_pde_pt_ptr_get_read(lu_ret.pdSlot)) ||
        ((ept_pde_ptr_get_page_size(lu_ret.pdSlot) == ept_pde_ept_pde_2m) &&
         ept_pde_ept_pde_2m_ptr_get_read(lu_ret.pdSlot))) {
        userError("X86EPTPTMap: Page table already mapped here");
        current_syscall_error.type = seL4_DeleteFirst;
        return EXCEPTION_SYSCALL_ERROR;
    }

    paddr = pptr_to_paddr((void *)(cap_ept_pt_cap_get_capPTBasePtr(cap)));
    pde = ept_pde_ept_pde_pt_new(
              paddr,/* pt_base_address  */
              0,    /* avl_cte_depth    */
              1,    /* execute          */
              1,    /* write            */
              1     /* read             */
          );

    cap = cap_ept_pt_cap_set_capPTIsMapped(cap, 1);
    cap = cap_ept_pt_cap_set_capPTMappedASID(cap, asid);
    cap = cap_ept_pt_cap_set_capPTMappedAddress(cap, vaddr);

    setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
    return performEPTPTInvocationMap(cap, cte, pde, lu_ret.pdSlot, pml4);/*Z 设置CSlot能力值，设置三级页表项的值，清缓存 */
}
/*Z 设置CSlot的能力，设置EPT四级页表的页表项值，失效EPT相关的TLB和页缓存 */
static exception_t performEPTPageMapPTE(cap_t cap, cte_t *cte, ept_pte_t *ptSlot, ept_pte_t pte, ept_pml4e_t *pml4)
{
    *ptSlot = pte;
    cte->cap = cap;
    invept(pml4);/*Z 失效EPT相关的TLB和页缓存。参数为EPT一级页表线性地址 */

    return EXCEPTION_NONE;
}
/*Z 设置CSlot的能力，设置EPT三级页表的页表项值，失效EPT相关的TLB和页缓存 */
static exception_t performEPTPageMapPDE(cap_t cap, cte_t *cte, ept_pde_t *pdSlot, ept_pde_t pde1, ept_pde_t pde2,
                                        ept_pml4e_t *pml4)
{
    pdSlot[0] = pde1;
    if (LARGE_PAGE_BITS == 22) {
        pdSlot[1] = pde2;
    }
    cte->cap = cap;
    invept(pml4);

    return EXCEPTION_NONE;
}
/*Z 映射一个EPT内存页，更新CSlot的能力，建立页表项，失效缓存。必须是未映射过的页 */
exception_t decodeX86EPTPageMap(
    word_t invLabel,        /*Z 消息标签 */
    word_t length,          /*Z 消息长度 */
    cte_t *cte,             /*Z 引用的CSlot */
    cap_t cap,              /*Z 其能力：cap_frame_cap */
    extra_caps_t excaps,    /*Z 传递的额外的能力 */
    word_t *buffer)         /*Z IPC buffer */
{
    word_t          vaddr;          /*Z 要映射的虚拟地址 */
    word_t          w_rightsMask;   /*Z 请求的读写权限 */
    paddr_t         paddr;
    cap_t           pml4Cap;        /*Z 要映射到的EPT一级页表能力 */
    ept_pml4e_t    *pml4;           /*Z EPT一级页表线性地址 */
    vm_rights_t     capVMRights;    /*Z 页已有读写权限 */
    vm_rights_t     vmRights;       
    vm_attributes_t vmAttr;         /*Z 请求的页属性 */
    vm_page_size_t  frameSize;      /*Z 枚举的页大小 */
    asid_t          asid;           /*Z 页表ASID */

    frameSize = cap_frame_cap_get_capFSize(cap);/*Z --------------------------------传递的消息参数：*/
    vaddr = getSyscallArg(0, buffer);                           /*Z 0：要映射的虚拟地址 */
    vaddr = vaddr & ~MASK(EPT_PT_INDEX_OFFSET);
    w_rightsMask = getSyscallArg(1, buffer);                    /*Z 1：读写权限 */
    vmAttr = vmAttributesFromWord(getSyscallArg(2, buffer));    /*Z 2：页属性 */
    pml4Cap = excaps.excaprefs[0]->cap;                         /*Z extraCaps0：EPT一级页表能力 */

    capVMRights = cap_frame_cap_get_capFVMRights(cap);

    if (cap_frame_cap_get_capFMappedASID(cap) != asidInvalid) {/*Z 不支持重映射 */
        userError("X86EPTPageMap: Frame already mapped.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 0;

        return EXCEPTION_SYSCALL_ERROR;
    }

    assert(cap_frame_cap_get_capFMapType(cap) == X86_MappingNone);

    if (cap_get_capType(pml4Cap) != cap_ept_pml4_cap ||
        !cap_ept_pml4_cap_get_capPML4IsMapped(pml4Cap)) {
        userError("X86EPTPageMap: Attempting to map frame into invalid ept pml4.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 1;

        return EXCEPTION_SYSCALL_ERROR;
    }

    pml4 = (ept_pml4e_t *)(cap_ept_pml4_cap_get_capPML4BasePtr(pml4Cap));
    asid = cap_ept_pml4_cap_get_capPML4MappedASID(pml4Cap);

    findEPTForASID_ret_t find_ret = findEPTForASID(asid);/*Z 验证ASID(PCID)有效性 */
    if (find_ret.status != EXCEPTION_NONE) {
        current_syscall_error.type = seL4_FailedLookup;
        current_syscall_error.failedLookupWasSource = false;

        return EXCEPTION_SYSCALL_ERROR;
    }

    if (find_ret.ept != pml4) {/*Z 验证页表地址有效性 */
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 1;

        return EXCEPTION_SYSCALL_ERROR;
    }

    /*Z 确定最终读写权限 */
    vmRights = maskVMRights(capVMRights, rightsFromWord(w_rightsMask));
    /*Z 验证是否对齐 */
    if (!checkVPAlignment(frameSize, vaddr)) {
        current_syscall_error.type = seL4_AlignmentError;

        return EXCEPTION_SYSCALL_ERROR;
    }
    /*Z 该页的物理地址 */
    paddr = pptr_to_paddr((void *)cap_frame_cap_get_capFBasePtr(cap));
    /*Z 更新能力，但还未写入CSlot */
    cap = cap_frame_cap_set_capFMappedASID(cap, asid);
    cap = cap_frame_cap_set_capFMappedAddress(cap, vaddr);
    cap = cap_frame_cap_set_capFMapType(cap, X86_MappingEPT);

    switch (frameSize) {
    /* PTE mappings */
    case X86_SmallPage: {
        lookupEPTPTSlot_ret_t lu_ret;
        ept_pte_t pte;
        /*Z 查找虚拟地址的EPT四级页表项线性地址 */
        lu_ret = lookupEPTPTSlot(pml4, vaddr);
        if (lu_ret.status != EXCEPTION_NONE) {
            current_syscall_error.type = seL4_FailedLookup;
            current_syscall_error.failedLookupWasSource = false;
            /* current_lookup_fault will have been set by lookupEPTPTSlot */
            return EXCEPTION_SYSCALL_ERROR;
        }

        if (ept_pte_ptr_get_read(lu_ret.ptSlot)) {/*Z 页表项已被占用 */
            userError("X86EPTPageMap: Mapping already present.");
            current_syscall_error.type = seL4_DeleteFirst;
            return EXCEPTION_SYSCALL_ERROR;
        }

        pte = ept_pte_new(
                  paddr,
                  0,
                  0,
                  eptCacheFromVmAttr(vmAttr),
                  1,
                  WritableFromVMRights(vmRights),
                  1);

        setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
        return performEPTPageMapPTE(cap, cte, lu_ret.ptSlot, pte, pml4);/*Z 设置CSlot的能力，设置EPT四级页表的页表项值，失效EPT相关的TLB和页缓存 */
    }

    /* PDE mappings */
    case X86_LargePage: {
        lookupEPTPDSlot_ret_t lu_ret;
        /*Z 查找虚拟地址的EPT三级页表项线性地址 */
        lu_ret = lookupEPTPDSlot(pml4, vaddr);
        if (lu_ret.status != EXCEPTION_NONE) {
            userError("X86EPTPageMap: Need a page directory first.");
            current_syscall_error.type = seL4_FailedLookup;
            current_syscall_error.failedLookupWasSource = false;
            /* current_lookup_fault will have been set by lookupEPTPDSlot */
            return EXCEPTION_SYSCALL_ERROR;
        }


        if ((ept_pde_ptr_get_page_size(lu_ret.pdSlot) == ept_pde_ept_pde_pt) &&/*Z 页表项已被一个有效四级页表占用 */
            ept_pde_ept_pde_pt_ptr_get_read(lu_ret.pdSlot)) {
            userError("X86EPTPageMap: Page table already present.");
            current_syscall_error.type = seL4_DeleteFirst;
            return EXCEPTION_SYSCALL_ERROR;
        }
        if (LARGE_PAGE_BITS != EPT_PD_INDEX_OFFSET &&
            (ept_pde_ptr_get_page_size(lu_ret.pdSlot + 1) == ept_pde_ept_pde_pt) &&
            ept_pde_ept_pde_pt_ptr_get_read(lu_ret.pdSlot + 1)) {
            userError("X86EPTPageMap: Page table already present.");
            current_syscall_error.type = seL4_DeleteFirst;
            return EXCEPTION_SYSCALL_ERROR;
        }
        if ((ept_pde_ptr_get_page_size(lu_ret.pdSlot) == ept_pde_ept_pde_2m) &&/*Z 页表项已被一个有效大页占用 */
            ept_pde_ept_pde_2m_ptr_get_read(lu_ret.pdSlot)) {
            userError("X86EPTPageMap: Mapping already present.");
            current_syscall_error.type = seL4_DeleteFirst;
            return EXCEPTION_SYSCALL_ERROR;
        }

        ept_pde_t pde1 = ept_pde_ept_pde_2m_new(
                             paddr,
                             0,
                             0,
                             eptCacheFromVmAttr(vmAttr),
                             1,
                             WritableFromVMRights(vmRights),
                             1);

        ept_pde_t pde2 = ept_pde_ept_pde_2m_new(/*Z 没看，与32位架构有关 */
                             paddr + BIT(EPT_PD_INDEX_OFFSET),
                             0,
                             0,
                             eptCacheFromVmAttr(vmAttr),
                             1,
                             WritableFromVMRights(vmRights),
                             1);

        setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
        return performEPTPageMapPDE(cap, cte, lu_ret.pdSlot, pde1, pde2, pml4);/*Z 设置CSlot的能力，设置EPT三级页表的页表项值，失效EPT相关的TLB和页缓存 */
    }

    default:
        /* When initializing EPT we only checked for support for 4K and 2M
         * pages, so we must disallow attempting to use any other */
        userError("X86EPTPageMap: Attempted to map unsupported page size.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 0;
        return EXCEPTION_SYSCALL_ERROR;
    }
}
/*Z 清除指定内存页的EPT页表映射 */
void unmapEPTPage(vm_page_size_t page_size, asid_t asid, vptr_t vptr, void *pptr)
{                   /*Z page_size页大小，vptr该页映射的虚拟地址，pptr该页线性地址 */
    findEPTForASID_ret_t find_ret;
    paddr_t addr = addrFromPPtr(pptr);/*Z 页物理地址 */
    /*Z 查找ASID(PCID)对应的EPT一级页表线性地址 */
    find_ret = findEPTForASID(asid);
    if (find_ret.status != EXCEPTION_NONE) {
        return;
    }

    switch (page_size) {
    case X86_SmallPage: {
        lookupEPTPTSlot_ret_t lu_ret;
        /*Z 查找虚拟地址的EPT四级页表项线性地址 */
        lu_ret = lookupEPTPTSlot(find_ret.ept, vptr);
        if (lu_ret.status != EXCEPTION_NONE) {
            return;
        }
        if (!ept_pte_ptr_get_read(lu_ret.ptSlot)) {/*Z 所指内存不可读 */
            return;
        }
        if (ept_pte_ptr_get_page_base_address(lu_ret.ptSlot) != addr) {
            return;
        }
        /*Z 四级页表项置0 */
        *lu_ret.ptSlot = ept_pte_new(0, 0, 0, 0, 0, 0, 0);
        break;
    }
    case X86_LargePage: {
        lookupEPTPDSlot_ret_t lu_ret;
        
        lu_ret = lookupEPTPDSlot(find_ret.ept, vptr);
        if (lu_ret.status != EXCEPTION_NONE) {
            return;
        }
        if (ept_pde_ptr_get_page_size(lu_ret.pdSlot) != ept_pde_ept_pde_2m) {
            return;
        }
        if (!ept_pde_ept_pde_2m_ptr_get_read(lu_ret.pdSlot)) {
            return;
        }
        if (ept_pde_ept_pde_2m_ptr_get_page_base_address(lu_ret.pdSlot) != addr) {
            return;
        }

        lu_ret.pdSlot[0] = ept_pde_ept_pde_2m_new(0, 0, 0, 0, 0, 0, 0);

        if (LARGE_PAGE_BITS != EPT_PD_INDEX_OFFSET) {
            assert(ept_pde_ptr_get_page_size(lu_ret.pdSlot + 1) == ept_pde_ept_pde_2m);
            assert(ept_pde_ept_pde_2m_ptr_get_read(lu_ret.pdSlot + 1));
            assert(ept_pde_ept_pde_2m_ptr_get_page_base_address(lu_ret.pdSlot + 1) == addr + BIT(21));

            lu_ret.pdSlot[1] = ept_pde_ept_pde_2m_new(0, 0, 0, 0, 0, 0, 0);
        }
        break;
    }
    default:
        /* we did not allow mapping additional page sizes into EPT objects,
         * so this should not happen. As we have no way to return an error
         * all we can do is assert */
        assert(!"Invalid page size for unmap");
    }
}

#endif /* CONFIG_VTX */

/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>

#ifdef CONFIG_HARDWARE_DEBUG_API

#include <arch/machine/debug.h>
#include <mode/machine/debug.h>
#include <arch/machine.h>
#include <machine/registerset.h>
#include <sel4/plat/api/constants.h> /* seL4_NumHWBReakpoints */

/* Intel manual Vol3, 17.2.4 */
#define X86_DEBUG_BP_SIZE_1B                (0x0u)  /*Z DR7中LENn值：指示断点数据区域1字节 */
#define X86_DEBUG_BP_SIZE_2B                (0x1u)  /*Z DR7中LENn值：指示断点数据区域2字节 */
#define X86_DEBUG_BP_SIZE_4B                (0x3u)  /*Z DR7中LENn值：指示断点数据区域4字节 */
#define X86_DEBUG_BP_SIZE_8B                (0x2u)  /*Z DR7中LENn值：指示断点数据区域8字节或未定义 */

#define X86_DEBUG_BP0_SIZE_SHIFT            (18)    /*Z DR7中LEN0域位偏移。两位 */
#define X86_DEBUG_BP1_SIZE_SHIFT            (22)    /*Z DR7中LEN1域位偏移。两位 */
#define X86_DEBUG_BP2_SIZE_SHIFT            (26)    /*Z DR7中LEN2域位偏移。两位 */
#define X86_DEBUG_BP3_SIZE_SHIFT            (30)    /*Z DR7中LEN3域位偏移。两位 */

/* NOTE: Intel manual 17.2.4:
 * I/O breakpoints are supported by every processor later than i486, but only
 * when CR4.DE=1.
 * When CR4.DE=0, or if processor is earlier than i586, this bit is "Undefined",
 * which is not the same as "Reserved", so it won't trigger an exception - it
 * will just cause an undefined reaction from the CPU.
 */
#define X86_DEBUG_BP_TYPE_IO                (0x2u)          /*Z 断点条件：I/O读写(断点地址)或未定义 */
#define X86_DEBUG_BP_TYPE_INSTR             (0x0u)          /*Z 断点条件：指令执行(到断点地址) */
#define X86_DEBUG_BP_TYPE_DATA_WRITE        (0x1u)          /*Z 断点条件：数据写(断点地址) */
#define X86_DEBUG_BP_TYPE_DATA_READWRITE    (0x3u)          /*Z 断点条件：数据读写(断点地址) */

#define X86_DEBUG_BP0_TYPE_SHIFT            (16)            /*Z DR7中0号断点条件位偏移。两位 */
#define X86_DEBUG_BP1_TYPE_SHIFT            (20)            /*Z DR7中1号断点条件位偏移。两位 */
#define X86_DEBUG_BP2_TYPE_SHIFT            (24)            /*Z DR7中2号断点条件位偏移。两位 */
#define X86_DEBUG_BP3_TYPE_SHIFT            (28)            /*Z DR7中3号断点条件位偏移。两位 */

#define X86_DEBUG_EFLAGS_TRAP_FLAG    ((word_t)BIT(8))      /*Z RFLAGS中的单步执行标志 */
#define X86_DEBUG_EFLAGS_RESUME_FLAG    ((word_t)BIT(16))   /*Z 继续执行标志。下条指令执行后cpu自动清除该标志 */
#define X86_DEBUG_DR6_SINGLE_STEP_FLAG  ((word_t)BIT(14))   /*Z 单步执行标志 */

#define X86_DEBUG_DR6_BP_MASK     (0xFu)
/*Z CPU是否支持8字节的断点区域，见手册3卷17.2.4 Debug Control Register (DR7) */
static bool_t byte8_bps_supported = false;
/*Z CPU是否支持8字节的断点区域 */
bool_t byte8BreakpointsSupported(void)
{
    return byte8_bps_supported;
}
/*Z 按掩码更新DR6寄存器。只在例外发生后 */
static inline void bitwiseAndDr6Reg(word_t mask)
{
    word_t tmp;

    tmp = readDr6Reg() & mask;
    writeDr6Reg(tmp);
}
/*Z 返回TCB上下文保存的DR7值 */
static inline word_t readDr7Context(tcb_t *t)
{
    return t->tcbArch.tcbContext.breakpointState.dr[5];
}
/*Z 为TCB上下文中DR7或位值 */
static inline void bitwiseOrDr7Context(tcb_t *t, word_t val)
{
    t->tcbArch.tcbContext.breakpointState.dr[5] |= val;
}
/*Z TCB上下文中DR7的值用掩码与 */
static inline void bitwiseAndDr7Context(tcb_t *t, word_t mask)
{
    t->tcbArch.tcbContext.breakpointState.dr[5] &= mask;
}
/*Z 断点对应的TCB上下文中DR7断点条件、数据尺寸域置0 */
static void unsetDr7BitsFor(tcb_t *t, uint16_t bp_num)
{
    word_t mask;

    switch (bp_num) {
    case 0:
        mask = (0x3u << X86_DEBUG_BP0_SIZE_SHIFT) | (0x3u << X86_DEBUG_BP0_TYPE_SHIFT);
        break;
    case 1:
        mask = (0x3u << X86_DEBUG_BP1_SIZE_SHIFT) | (0x3u << X86_DEBUG_BP1_TYPE_SHIFT);
        break;
    case 2:
        mask = (0x3u << X86_DEBUG_BP2_SIZE_SHIFT) | (0x3u << X86_DEBUG_BP2_TYPE_SHIFT);
        break;
    default: /* 3 */
        assert(bp_num == 3);
        mask = (0x3u << X86_DEBUG_BP3_SIZE_SHIFT) | (0x3u << X86_DEBUG_BP3_TYPE_SHIFT);
        break;
    }

    mask = ~mask;
    bitwiseAndDr7Context(t, mask);/*Z TCB上下文中DR7的值用掩码与 */
}
/*Z 获取断点编号对应的DR7断点条件值 */
/** Converts an seL4_BreakpointType value into the underlying hardware
 * equivalent.
 * @param bp_num Breakpoint number.
 * @param type One of the values of seL4_BreakpointType.
 * @param rw Access trigger condition (read/write).
 * @return Hardware specific register value representing the inputs.
 */
PURE static inline word_t convertTypeAndAccessToArch(uint16_t bp_num, word_t type, word_t rw)
{
    switch (type) {
    case seL4_InstructionBreakpoint:
        type = X86_DEBUG_BP_TYPE_INSTR;
        break;
    default: /* seL4_DataBreakpoint */
        assert(type == seL4_DataBreakpoint);
        type = (rw == seL4_BreakOnWrite)
               ? X86_DEBUG_BP_TYPE_DATA_WRITE
               : X86_DEBUG_BP_TYPE_DATA_READWRITE;
    }

    switch (bp_num) {
    case 0:
        return type << X86_DEBUG_BP0_TYPE_SHIFT;
    case 1:
        return type << X86_DEBUG_BP1_TYPE_SHIFT;
    case 2:
        return type << X86_DEBUG_BP2_TYPE_SHIFT;
    default: /* 3 */
        assert(bp_num == 3);
        return type << X86_DEBUG_BP3_TYPE_SHIFT;
    }
}
/*Z 断点及断点条件类型 */
/** Reverse of convertTypeAndAccessToArch(): converts hardware values into
 * seL4 API values.
 * @param dr7 Hardware register value as input for conversion.
 * @param bp_num Breakpoint number.
 * @param type[out] Converted type value.
 * @param rw[out] Converted output access trigger value.
 */
typedef struct {
    word_t type, rw;    /*Z type-断点类型，rw-断点条件类型 */
} convertedTypeAndAccess_t;
/*Z 获取断点号的断点及断点条件类型 */
PURE static inline convertedTypeAndAccess_t convertArchToTypeAndAccess(word_t dr7, uint16_t bp_num)
{
    convertedTypeAndAccess_t ret;
    /*Z 获取断点号对应的触发条件位 */
    switch (bp_num) {
    case 0:
        dr7 &= 0x3u << X86_DEBUG_BP0_TYPE_SHIFT;
        dr7 >>= X86_DEBUG_BP0_TYPE_SHIFT;
        break;
    case 1:
        dr7 &= 0x3u << X86_DEBUG_BP1_TYPE_SHIFT;
        dr7 >>= X86_DEBUG_BP1_TYPE_SHIFT;
        break;
    case 2:
        dr7 &= 0x3u << X86_DEBUG_BP2_TYPE_SHIFT;
        dr7 >>= X86_DEBUG_BP2_TYPE_SHIFT;
        break;
    default: /* 3 */
        assert(bp_num == 3);
        dr7 &= 0x3u << X86_DEBUG_BP3_TYPE_SHIFT;
        dr7 >>= X86_DEBUG_BP3_TYPE_SHIFT;
    }

    switch (dr7) {
    case X86_DEBUG_BP_TYPE_INSTR:
        ret.type = seL4_InstructionBreakpoint;
        ret.rw = seL4_BreakOnRead;
        break;
    case X86_DEBUG_BP_TYPE_DATA_WRITE:
        ret.type = seL4_DataBreakpoint;
        ret.rw = seL4_BreakOnWrite;
        break;
    default: /* Read-write */
        assert(dr7 == X86_DEBUG_BP_TYPE_DATA_READWRITE);/*Z 不支持I/O读写断点 */
        ret.type = seL4_DataBreakpoint;
        ret.rw = seL4_BreakOnReadWrite;
        break;
    }
    return ret;
}
/*Z 返回断点对应的DR7中LENn域值 */
/** Converts an integer size number into an equivalent hardware register value.
 * @param n Breakpoint number.
 * @param type One value from seL4_BreakpointType.
 * @param size An integer for the operand size of the breakpoint.
 * @return Converted, hardware-specific value.
 */
PURE static inline word_t convertSizeToArch(uint16_t bp_num, word_t type, word_t size)
{
    if (type == seL4_InstructionBreakpoint) {
        /* Intel manual vol3 17.2.4:
         * "If the corresponding RWn field in register DR7 is 00 (instruction
         * execution), then the LENn field should also be 00"
         */
        size = 0;
    } else {
        switch (size) {
        case 1:
            size = X86_DEBUG_BP_SIZE_1B;
            break;
        case 2:
            size = X86_DEBUG_BP_SIZE_2B;
            break;
        case 8:
            size = X86_DEBUG_BP_SIZE_8B;
            break;
        default: /* 4B */
            assert(size == 4);
            size = X86_DEBUG_BP_SIZE_4B;
        }
    }

    switch (bp_num) {
    case 0:
        return size << X86_DEBUG_BP0_SIZE_SHIFT;
    case 1:
        return size << X86_DEBUG_BP1_SIZE_SHIFT;
    case 2:
        return size << X86_DEBUG_BP2_SIZE_SHIFT;
    default: /* 3 */
        assert(bp_num == 3);
        return size << X86_DEBUG_BP3_SIZE_SHIFT;
    }
}
/*Z 返回dr7中相应断点的数据区域大小 */
/** Reverse of convertSizeToArch(): converts a hardware-specific size value
 * into an integer representation.
 * @param dr7 Hardware register value as input.
 * @param n Breakpoint number.
 * @return Converted size value.
 */
PURE static inline word_t convertArchToSize(word_t dr7, uint16_t bp_num)
{
    word_t type;

    switch (bp_num) {
    case 0:
        type = dr7 & (0x3u << X86_DEBUG_BP0_TYPE_SHIFT);
        type >>= X86_DEBUG_BP0_TYPE_SHIFT;
        dr7 &= 0x3u << X86_DEBUG_BP0_SIZE_SHIFT;
        dr7 >>= X86_DEBUG_BP0_SIZE_SHIFT;
        break;
    case 1:
        type = dr7 & (0x3u << X86_DEBUG_BP1_TYPE_SHIFT);
        type >>= X86_DEBUG_BP1_TYPE_SHIFT;
        dr7 &= 0x3u << X86_DEBUG_BP1_SIZE_SHIFT;
        dr7 >>= X86_DEBUG_BP1_SIZE_SHIFT;
        break;
    case 2:
        type = dr7 & (0x3u << X86_DEBUG_BP2_TYPE_SHIFT);
        type >>= X86_DEBUG_BP2_TYPE_SHIFT;
        dr7 &= 0x3u << X86_DEBUG_BP2_SIZE_SHIFT;
        dr7 >>= X86_DEBUG_BP2_SIZE_SHIFT;
        break;
    default: /* 3 */
        assert(bp_num == 3);
        type = dr7 & (0x3u << X86_DEBUG_BP3_TYPE_SHIFT);
        type >>= X86_DEBUG_BP3_TYPE_SHIFT;
        dr7 &= 0x3u << X86_DEBUG_BP3_SIZE_SHIFT;
        dr7 >>= X86_DEBUG_BP3_SIZE_SHIFT;
    }

    /* Force size to 0 if type is instruction breakpoint. */
    if (type == X86_DEBUG_BP_TYPE_INSTR) {
        return 0;
    }

    switch (dr7) {
    case X86_DEBUG_BP_SIZE_1B:
        return 1;
    case X86_DEBUG_BP_SIZE_2B:
        return 2;
    case X86_DEBUG_BP_SIZE_8B:
        return 8;
    default: /* 4B */
        assert(dr7 == X86_DEBUG_BP_SIZE_4B);
        return 4;
    }
}
/*Z 使能TCB上下文中DR7相应断点位 */
/** Enables a breakpoint.
 * @param bp_num Hardware breakpoint ID. Usually an integer from 0..N.
 */
static void enableBreakpoint(tcb_t *t, uint16_t bp_num)
{
    word_t enable_bit;

    assert(t != NULL);
    assert(bp_num < X86_DEBUG_BP_N_REGS);

    switch (bp_num) {
    case 0:
        enable_bit = X86_DEBUG_BP0_ENABLE_BIT;
        break;
    case 1:
        enable_bit = X86_DEBUG_BP1_ENABLE_BIT;
        break;
    case 2:
        enable_bit = X86_DEBUG_BP2_ENABLE_BIT;
        break;
    default:
        enable_bit = X86_DEBUG_BP3_ENABLE_BIT;
        break;
    }

    bitwiseOrDr7Context(t, enable_bit);
}
/*Z 禁用TCB上下文中DR7相应断点位 */
/** Disables a breakpoint without clearing its configuration.
 * @param bp_num Hardware breakpoint ID. Usually an integer from 0..N.
 */
static void disableBreakpoint(tcb_t *t, uint16_t bp_num)
{
    word_t disable_mask;

    assert(t != NULL);
    assert(bp_num < X86_DEBUG_BP_N_REGS);

    switch (bp_num) {
    case 0:
        disable_mask = ~X86_DEBUG_BP0_ENABLE_BIT;
        break;
    case 1:
        disable_mask = ~X86_DEBUG_BP1_ENABLE_BIT;
        break;
    case 2:
        disable_mask = ~X86_DEBUG_BP2_ENABLE_BIT;
        break;
    default:
        disable_mask = ~X86_DEBUG_BP3_ENABLE_BIT;
        break;
    }

    bitwiseAndDr7Context(t, disable_mask);
}
/*Z TCB上下文中DR7相应断点是否使能 */
/** Returns a boolean for whether or not a breakpoint is enabled.
 * @param bp_num Hardware breakpoint ID. Usually an integer from 0..N.
 */
static bool_t breakpointIsEnabled(tcb_t *t, uint16_t bp_num)
{
    word_t dr7;

    assert(t != NULL);
    assert(bp_num < X86_DEBUG_BP_N_REGS);

    dr7 = readDr7Context(t);
    switch (bp_num) {
    case 0:
        return !!(dr7 & X86_DEBUG_BP0_ENABLE_BIT);
    case 1:
        return !!(dr7 & X86_DEBUG_BP1_ENABLE_BIT);
    case 2:
        return !!(dr7 & X86_DEBUG_BP2_ENABLE_BIT);
    default:
        return !!(dr7 & X86_DEBUG_BP3_ENABLE_BIT);
    }
}
/*Z 设置断点对应的TCB上下文中DR0~3的值(断点地址) */
static void setBpVaddrContext(tcb_t *t, uint16_t bp_num, word_t vaddr)
{
    assert(t != NULL);
    user_breakpoint_state_t *ubs = &t->tcbArch.tcbContext.breakpointState;

    switch (bp_num) {
    case 0:
        ubs->dr[0] = vaddr;
        break;
    case 1:
        ubs->dr[1] = vaddr;
        break;
    case 2:
        ubs->dr[2] = vaddr;
        break;
    default:
        assert(bp_num == 3);
        ubs->dr[3] = vaddr;
        break;
    }
    return;
}
/*Z 设置TCB上下文中断点条件、数据区域大小、地址、使能参数 */
/** Backend for the seL4_TCB_SetBreakpoint invocation.
 *
 * @param uds Arch TCB register context structure.
 * @param bp_num Hardware breakpoint ID.
 * @param vaddr USerspace virtual address on which you'd like this breakpoing
 *        to trigger.
 * @param types One of the seL4_BreakpointType values.
 * @param size positive integer indicating the byte-range size that should
 *        trigger the breakpoint. 0 is valid for Instruction breakpoints.
 * @param rw Access type that should trigger the BP (read/write).
 */
void setBreakpoint(tcb_t *t,
                   uint16_t bp_num, word_t vaddr, word_t types, word_t size, word_t rw)
{
    word_t dr7val;

    assert(t != NULL);

    dr7val = convertTypeAndAccessToArch(bp_num, types, rw);/*Z 获取断点编号对应的DR7断点条件值 */
    dr7val |= convertSizeToArch(bp_num, types, size);/*Z 返回断点对应的DR7中LENn域值 */

    setBpVaddrContext(t, bp_num, vaddr);/*Z 设置断点对应的TCB上下文中DR0~3的值(断点地址) */
    unsetDr7BitsFor(t, bp_num);/*Z 断点对应的TCB上下文中DR7断点条件、数据尺寸域置0 */
    bitwiseOrDr7Context(t, dr7val);/*Z 为TCB上下文中DR7赋位值 */
    enableBreakpoint(t, bp_num);/*Z 使能TCB上下文中DR7相应断点位 */
}
/*Z 返回断点号对应的断点地址 */
static word_t getBpVaddrContext(tcb_t *t, uint16_t bp_num)
{
    assert(t != NULL);
    user_breakpoint_state_t *ubs = &t->tcbArch.tcbContext.breakpointState;

    switch (bp_num) {
    case 0:
        return ubs->dr[0];
    case 1:
        return ubs->dr[1];
    case 2:
        return ubs->dr[2];
    default:
        assert(bp_num == 3);
        return ubs->dr[3];
    }
}
/*Z 获取断点有关参数 */
/** Backend for the x86 seL4_TCB_GetBreakpoint invocation.
 *
 * Returns information about a particular breakpoint ID, including whether or
 * not it's enabled.
 *
 * @param uds Arch TCB register context pointer.
 * @param bp_num Hardware breakpoint ID of the BP you'd like to query.
 * @return Structure containing information about the status of the breakpoint.
 */
getBreakpoint_t getBreakpoint(tcb_t *t, uint16_t bp_num)
{
    word_t dr7val;
    getBreakpoint_t ret;
    convertedTypeAndAccess_t res;

    dr7val = readDr7Context(t);/*Z 返回TCB上下文保存的DR7值 */
    ret.vaddr = getBpVaddrContext(t, bp_num);/*Z 返回断点号对应的断点地址 */
    ret.size = convertArchToSize(dr7val, bp_num);/*Z 返回dr7中相应断点的数据区域大小 */
    res = convertArchToTypeAndAccess(dr7val, bp_num);/*Z 获取断点号的断点及断点条件类型 */
    ret.type = res.type;
    ret.rw = res.rw;
    ret.is_enabled = breakpointIsEnabled(t, bp_num);/*Z TCB上下文中DR7相应断点是否使能 */
    return ret;
}
/*Z 清除断点对应的TCB上下文中保存值 */
/** Backend for the x86 seL4_TCB_UnsetBreakpoint invocation.
 *
 * Unsets and *clears* a hardware breakpoint.
 * @param uds Arch TCB register context pointer.
 * @param bp_num The hardware breakpoint ID you'd like to clear.
 */
void unsetBreakpoint(tcb_t *t, uint16_t bp_num)
{
    disableBreakpoint(t, bp_num);/*Z 禁用TCB上下文中DR7相应断点位 */
    unsetDr7BitsFor(t, bp_num);/*Z 断点对应的TCB上下文中DR7断点条件、数据尺寸域置0。这一点保证了杜绝Intel手册3第607页DR6:B0~B3误报可能 */
    setBpVaddrContext(t, bp_num, 0);/*Z 设置断点对应的TCB上下文中DR0~3的值(断点地址) */
}
/*Z 单步执行信息 */
/** Used in the exception path to determine if an exception was caused by
 * single-stepping being active.
 *
 * @param uc Arch TCB register context structure.
 * @return a structure stating whether or not the exception was caused by
 *          hardware single-stepping, and what the instruction vaddr was.
 */
typedef struct {
    bool_t ret;         /*Z 是否单步执行 */
    word_t instr_vaddr; /*Z 指令地址 */
} testAndResetSingleStepException_t;
/*Z 获取单步执行信息，掩掉DR6中相应的触发指示位 */
static testAndResetSingleStepException_t testAndResetSingleStepException(tcb_t *t)
{
    testAndResetSingleStepException_t ret;
    word_t dr6;
    /*Z 检测是否单步执行 */
    dr6 = readDr6Reg();
    if (!(dr6 & X86_DEBUG_DR6_SINGLE_STEP_FLAG)) {
        ret.ret = false;
        return ret;
    }

    ret.ret = true;
    ret.instr_vaddr = t->tcbArch.tcbContext.registers[FaultIP];
    bitwiseAndDr6Reg(~X86_DEBUG_DR6_SINGLE_STEP_FLAG);

    /* And that's not all: if the breakpoint is an instruction breakpoint, we
     * also need to set EFLAGS.RF. The processor raises the #DB exception BEFORE
     * the instruction executes. This means that when we IRET to userspace, the
     * SAME breakpoint will trigger again, and so on ad infinitum. EFLAGS.RF
     * solves this problem:
     *
     * When EFLAGS.RF is set, the processor will ignore instruction breakpoints
     * that should be raised, for one instruction. After that instruction
     * executes, the processor will also automatically unset EFLAGS.RF. See
     * Intel manuals, vol3, section 17.3.1.1.
     */
    /* This will automatically be popped by restore_user_context() */
    t->tcbArch.tcbContext.registers[FLAGS] |= X86_DEBUG_EFLAGS_RESUME_FLAG;
    return ret;
}
/*Z 设置单步中断参数：n_instr要跳过的指令数，0-禁用单步中断 */
bool_t configureSingleStepping(tcb_t *t, uint16_t bp_num, word_t n_instr,
                               UNUSED bool_t is_reply)
{
    /* On x86 no hardware breakpoints are needed for single stepping. */
    if (n_instr == 0) {
        /* If n_instr (number of instructions to single-step) is 0, that is the
          * same as requesting that single-stepping be disabled.
          *//*Z TCB上下文中记录禁用 */
        t->tcbArch.tcbContext.breakpointState.single_step_enabled = false;
        t->tcbArch.tcbContext.registers[FLAGS] &= ~X86_DEBUG_EFLAGS_TRAP_FLAG;
    } else {
        t->tcbArch.tcbContext.breakpointState.single_step_enabled = true;
    }

    t->tcbArch.tcbContext.breakpointState.n_instructions = n_instr;
    return false;
}
/*Z 断点信息 */
/** Used in the exception path to determine which breakpoint triggered the
 * exception.
 *
 * First, checks to see which hardware breakpoint was triggered, and saves
 * the ID of that breakpoint. Secondly, resets that breakpoint such that its
 * "triggered" bit is no longer in the asserted state -- whatever that means
 * for the arch. So on x86, that means clearing the indicator bit in DR6.
 *
 * Aside from the ID of the breakpoint that was raised, also returns
 * information about the breakpoint (vaddr, access, type, etc).
 *
 * @param uc Arch TCB register context pointer.
 * @return Structure with a "bp_num" member that states which hardware
 *          breakpoint was triggered, and gives information describing the
 *          breakpoint.
 */
typedef struct {
    int bp_num;             /*Z 断点序号：0～3，-1无效 */
    word_t vaddr, reason;   /*Z vaddr：断点地址，reason：断点类型 */
} getAndResetActiveBreakpoint_t;
/*Z 获取第1个激活的断点信息，掩掉DR6中相应的触发指示位 */
static getAndResetActiveBreakpoint_t getAndResetActiveBreakpoint(tcb_t *t)
{
    convertedTypeAndAccess_t tmp;
    getAndResetActiveBreakpoint_t ret;
    /*Z 获取第1个激活的断点序号。未首先检查DR7使能位是因为unsetBreakpoint中有考虑 */
    /* Read from the hardware regs, not user context */
    word_t dr6 = readDr6Reg();
    if (dr6 & BIT(0)) {
        ret.bp_num = 0;
    } else if (dr6 & BIT(1)) {
        ret.bp_num = 1;
    } else if (dr6 & BIT(2)) {
        ret.bp_num = 2;
    } else if (dr6 & BIT(3)) {
        ret.bp_num = 3;
    } else {
        ret.bp_num = -1;
        return ret;
    }
    /*Z 获取断点号的断点及断点条件类型、断点地址 */
    tmp = convertArchToTypeAndAccess(readDr7Context(t), ret.bp_num);
    ret.vaddr = getBpVaddrContext(t, ret.bp_num);
    ret.reason = tmp.type;
    /*Z 掩掉DR6中相应的断点触发指示位 */
    bitwiseAndDr6Reg(~BIT(ret.bp_num));
    return ret;
}
/*Z 处理用户空间DEBUG例外：将例外信息发送给当前线程的错误处理对象，线程阻塞并等待处理结果，引入调度 */
exception_t handleUserLevelDebugException(int int_vector)
{
    tcb_t *ct;
    getAndResetActiveBreakpoint_t active_bp;
    testAndResetSingleStepException_t single_step_info;

#if defined(CONFIG_DEBUG_BUILD) || defined(CONFIG_BENCHMARK_TRACK_KERNEL_ENTRIES)
    ksKernelEntry.path = Entry_UserLevelFault;
    ksKernelEntry.word = int_vector;
#else
    (void)int_vector;/*Z 不好：没用的花招 */
#endif /* DEBUG */

#ifdef CONFIG_BENCHMARK_TRACK_KERNEL_ENTRIES
    benchmark_track_start();
#endif

    ct = NODE_STATE(ksCurThread);

    /* Software break request (INT3) is detected by the vector number */
    if (int_vector == int_software_break_request) {/*Z INT3断点，生成一个专门错误 */
        current_fault = seL4_Fault_DebugException_new(getRestartPC(NODE_STATE(ksCurThread)),
                                                      0, seL4_SoftwareBreakRequest);
    } else {/*Z INT1(硬件DEBUG) */
        /* Hardware breakpoint trigger is detected using DR6 */
        active_bp = getAndResetActiveBreakpoint(ct);/*Z 获取第1个激活的断点信息，掩掉DR6中相应的触发指示位 */
        if (active_bp.bp_num >= 0) {/*Z 有断点触发，生成一个专门错误 */
            current_fault = seL4_Fault_DebugException_new(active_bp.vaddr,
                                                          active_bp.bp_num,
                                                          active_bp.reason);
        } else {/*Z 单步执行， */
            single_step_info = testAndResetSingleStepException(ct);/*Z 获取单步执行信息，掩掉DR6中相应的触发指示位 */
            if (single_step_info.ret == true) {
                /* If the caller asked us to skip over N instructions before
                 * generating the next single-step breakpoint, we shouldn't
                 * bother to construct a fault message until we've skipped N
                 * instructions.
                 *//*Z 递减单步执行跳过指令计数，不为0时不触发直接返回 */
                if (singleStepFaultCounterReady(ct) == false) {
                    return EXCEPTION_NONE;
                }
                current_fault = seL4_Fault_DebugException_new(single_step_info.instr_vaddr,
                                                              0, seL4_SingleStep);
            } else {
                return EXCEPTION_SYSCALL_ERROR;
            }
        }
    }
    /*Z 将DEBUG例外发送给当前线程的错误处理对象，线程阻塞并等待处理结果 */
    handleFault(NODE_STATE(ksCurThread));

    schedule();
    activateThread();

    return EXCEPTION_NONE;
}
/*Z 检查CPU是否支持8字节断点区域 */
BOOT_CODE bool_t Arch_initHardwareBreakpoints(void)
{
    x86_cpu_identity_t *modelinfo;

    modelinfo = x86_cpuid_get_model_info();
    /* Intel manuals, vol3, section 17.2.4, "NOTES". */
    if (modelinfo->family == 15) {
        if (modelinfo->model == 3 || modelinfo->model == 4
            || modelinfo->model == 6) {
            byte8_bps_supported = true;
        }
    }
    if (modelinfo->family == 6) {
        if (modelinfo->model == 15 || modelinfo->model == 23
            || modelinfo->model == 0x1C) {
            byte8_bps_supported = true;
        }
    }
    return true;
}
/*Z 设置上下文的DR6(状态)、DR7(控制)寄存器 */
void Arch_initBreakpointContext(user_breakpoint_state_t *uds)
{
    memset(uds, 0, sizeof(*uds));

    /* Preload reserved values into register context */
    uds->dr[4] = readDr6Reg() & /*Z 读取DR6的值 */
                 ~(BIT(0)       /*Z 当然没必要保存已触发的0~3断点标志 */
                   | BIT(1)
                   | BIT(2)
                   | BIT(3)
                   | X86_DEBUG_DR6_SINGLE_STEP_FLAG);/*Z 和单步执行标志 */

    uds->dr[5] = readDr7Reg() & /*Z 读取DR7的值 */
                 ~(X86_DEBUG_BP0_ENABLE_BIT | X86_DEBUG_BP1_ENABLE_BIT
                   | X86_DEBUG_BP2_ENABLE_BIT
                   | X86_DEBUG_BP3_ENABLE_BIT);/*Z 清除全局断点使能位1,3,5,7 */
}

#endif

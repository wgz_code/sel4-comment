/*Z OK */

/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>
#include <arch/machine/registerset.h>
/*Z 初始化上下文中cpu、FPU、DEBUG寄存器值 */
void Arch_initContext(user_context_t *context)
{   /*Z 初始化所有的上下文寄存器，大部分置0 */
    Mode_initContext(context);
    context->registers[FS_BASE] = 0;
    context->registers[GS_BASE] = 0;
    context->registers[Error] = 0;
    context->registers[FaultIP] = 0;
    context->registers[NextIP] = 0;            /* overwritten by setNextPC() later on */
    context->registers[CS] = SEL_CS_3;  /*Z CS指向用户权限CS段 */
    context->registers[FLAGS] = FLAGS_USER_DEFAULT;/*Z 默认使能中断 */
    context->registers[SS] = SEL_DS_3;  /*Z SS指向用户权限DS段 */
    /*Z 用静态配置的XSAVE保存区初始化FPU上下文 */
    Arch_initFpuContext(context);
#ifdef CONFIG_HARDWARE_DEBUG_API
    Arch_initBreakpointContext(&context->breakpointState);/*Z 设置上下文的DR6(状态)、DR7(控制)寄存器 */
#endif
}
/*Z 规范64位地址、cpu标志寄存器值 */
word_t sanitiseRegister(register_t reg, word_t v, bool_t archInfo)
{   /*Z 64位模式规范指定寄存器中的地址v：不规范的返回0 */
    /* First perform any mode specific sanitization */
    v = Mode_sanitiseRegister(reg, v);
    if (reg == FLAGS) {
        /* Set architecturally defined high and low bits */
        v |=  FLAGS_HIGH;
        v &= ~FLAGS_LOW;
        /* require user to have interrupts and no traps */
        v |=  FLAGS_IF;
        v &= ~FLAGS_TF;
        /* remove any other bits that shouldn't be set */
        v &=  FLAGS_MASK;
    }
    return v;
}

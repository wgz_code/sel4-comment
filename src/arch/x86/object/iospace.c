/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>

#ifdef CONFIG_IOMMU

#include <api/syscall.h>
#include <machine/io.h>
#include <kernel/thread.h>
#include <arch/api/invocation.h>
#include <arch/object/iospace.h>
#include <arch/model/statedata.h>
#include <linker.h>
#include <plat/machine/intel-vtd.h>


typedef struct lookupVTDContextSlot_ret {
    vtd_cte_t   *cte;
    word_t      index;
} lookupVTDContextSlot_ret_t;

/*Z 创建IO空间域0的控制能力（估计是关于IOMMU的）*/
BOOT_CODE cap_t master_iospace_cap(void)
{
    if (x86KSnumDrhu == 0) {
        return cap_null_cap_new();
    }

    return
        cap_io_space_cap_new(
            0,              /* capDomainID  */
            0              /* capPCIDevice */
        );
}
/*Z 返回能力对应的IOMMU上下文表中设备表项线性地址 */
static vtd_cte_t *lookup_vtd_context_slot(cap_t cap)
{
    uint32_t   vtd_root_index;
    uint32_t   vtd_context_index;
    uint32_t   pci_request_id;
    vtd_rte_t *vtd_root_slot;       /*Z struct{u64[2]} */
    vtd_cte_t *vtd_context;         /*Z struct{u64[2]} */
    vtd_cte_t *vtd_context_slot;

    switch (cap_get_capType(cap)) {
    case cap_io_space_cap:
        pci_request_id = cap_io_space_cap_get_capPCIDevice(cap);
        break;

    case cap_io_page_table_cap:
        pci_request_id = cap_io_page_table_cap_get_capIOPTIOASID(cap);
        break;

    case cap_frame_cap:
        pci_request_id = cap_frame_cap_get_capFMappedASID(cap);
        break;

    default:
        fail("Invalid cap type");
    }

    vtd_root_index = get_pci_bus(pci_request_id);
    vtd_root_slot = x86KSvtdRootTable + vtd_root_index;/*Z 根表项地址 */
    /*Z 上下文表指针 */
    vtd_context = (vtd_cte_t *)paddr_to_pptr(vtd_rte_ptr_get_ctp(vtd_root_slot));
    vtd_context_index = (get_pci_dev(pci_request_id) << 3) | get_pci_fun(pci_request_id);
    vtd_context_slot = &vtd_context[vtd_context_index];/*Z 上下文表项地址 */

    return vtd_context_slot;
}
/*Z 自指定页表开始，查找虚拟地址所在的指定级数页表项地址。注意只适用于4K页 */
static lookupIOPTSlot_ret_t lookupIOPTSlot_resolve_levels(vtd_pte_t *iopt, word_t translation,
                                                          word_t levels_to_resolve, word_t levels_remaining)
{/*Z iopt页表，translation虚拟地址>>12，levels_to_resolve要解析的级(0开始)，levels_remaining可用剩余的级数-1 */
    lookupIOPTSlot_ret_t ret;

    word_t      iopt_index = 0;
    vtd_pte_t   *iopt_slot = 0;
    vtd_pte_t   *next_iopt_slot = 0;

    if (iopt == 0) {
        ret.ioptSlot = 0;
        ret.level = levels_remaining;
        ret.status = EXCEPTION_LOOKUP_FAULT;
        return ret;
    }
    /*Z 计算页表内索引 */
    iopt_index = (translation  >> (VTD_PT_INDEX_BITS * (x86KSnumIOPTLevels - 1 - (levels_to_resolve - levels_remaining)))) &
                 MASK(VTD_PT_INDEX_BITS);
    iopt_slot = iopt + iopt_index;
    /*Z 注意：不可写意味着未映射，因为seL4在映射时均置可读可写 */
    if (!vtd_pte_ptr_get_write(iopt_slot) || levels_remaining == 0) {
        ret.ioptSlot = iopt_slot;
        ret.level = levels_remaining;
        ret.status = EXCEPTION_NONE;
        return ret;
    }
    next_iopt_slot = (vtd_pte_t *)paddr_to_pptr(vtd_pte_ptr_get_addr(iopt_slot));   /*Z 递归！*/
    return lookupIOPTSlot_resolve_levels(next_iopt_slot, translation, levels_to_resolve, levels_remaining - 1);
}

/*Z 在IOMMU页表查找虚拟地址的末级页表项地址 */
static inline lookupIOPTSlot_ret_t lookupIOPTSlot(vtd_pte_t *iopt, word_t io_address)
{                                               /*Z iopt一级页表，io_address虚拟地址 */
    lookupIOPTSlot_ret_t ret;

    if (iopt == 0) {
        ret.ioptSlot    = 0;
        ret.level       = 0;
        ret.status      = EXCEPTION_LOOKUP_FAULT;
        return ret;
    } else {
        return lookupIOPTSlot_resolve_levels(iopt, io_address >> PAGE_BITS,
                                             x86KSnumIOPTLevels - 1, x86KSnumIOPTLevels - 1);
    }
}
/*Z 删除IOMMU上下文表中能力对应的表项，刷新cache和IOTLB，置当前线程为重启动 */
void unmapVTDContextEntry(cap_t cap)
{   /*Z 能力对应的IOMMU上下文表项地址 */
    vtd_cte_t *cte = lookup_vtd_context_slot(cap);
    assert(cte != 0);
    *cte = vtd_cte_new(
               0,
               false,
               0,
               0,
               0,
               false
           );
    /*Z 刷出指针所指内存区域的各级cache */
    flushCacheRange(cte, VTD_CTE_SIZE_BITS);
    invalidate_iotlb();/*Z 失效所有IOMMU的IOTLB */
    setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
    return;
}
/*Z 清除IOMMU页表能力所指代的页表在上级页表(上下文项)中的页表项，失效所有IOMMU的IOTLB，能力置为无效 */
static exception_t performX86IOPTInvocationUnmap(cap_t cap, cte_t *ctSlot)
{   /*Z IOMMU页表能力设置成未映射(无效)，并清除其所在的页表项(或上下文项) */
    deleteIOPageTable(cap);
    cap = cap_io_page_table_cap_set_capIOPTIsMapped(cap, 0);
    ctSlot->cap = cap;

    return EXCEPTION_NONE;
}
/*Z 设置IOMMU上下文表项的值，设置IO页表能力的值 */
static exception_t performX86IOPTInvocationMapContextRoot(cap_t cap, cte_t *ctSlot, vtd_cte_t vtd_cte,
                                                          vtd_cte_t *vtd_context_slot)
{
    *vtd_context_slot = vtd_cte;
    flushCacheRange(vtd_context_slot, VTD_CTE_SIZE_BITS);
    ctSlot->cap = cap;

    return EXCEPTION_NONE;
}
/*Z 设置IOMMU页表项的值，设置IO页表能力的值 */
static exception_t performX86IOPTInvocationMapPT(cap_t cap, cte_t *ctSlot, vtd_pte_t iopte, vtd_pte_t *ioptSlot)
{
    *ioptSlot = iopte;
    flushCacheRange(ioptSlot, VTD_PTE_SIZE_BITS);
    ctSlot->cap = cap;

    return EXCEPTION_NONE;
}
/*Z 引用cap_io_page_table_cap的系统调用：映射、解映射IO页表 */
exception_t decodeX86IOPTInvocation(
    word_t invLabel,        /*Z 消息标签(错误类型) */
    word_t length,          /*Z 消息长度 */
    cte_t *slot,            /*Z 其CSlot */
    cap_t cap,              /*Z 其能力 */
    extra_caps_t excaps,    /*Z 额外能力 */
    word_t *buffer          /*Z IPC buffer */
)
{
    cap_t      io_space;
    paddr_t    paddr;
    uint32_t   pci_request_id;
    word_t   io_address;
    uint16_t   domain_id;
    vtd_cte_t *vtd_context_slot;
    vtd_pte_t *vtd_pte;

    if (invLabel == X86IOPageTableUnmap) {/*Z ----------------------------------------------------------------子功能：解映射。无消息传参 */

        setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
        return performX86IOPTInvocationUnmap(cap, slot);/*Z 清除IOMMU页表能力所指代的页表在上级页表(上下文项)中的页表项，失效缓存，能力置为无效 */
    }

    if (invLabel != X86IOPageTableMap) {/*Z -------------------------------------------------------------------------------子功能：映射 */
        userError("X86IOPageTable: Illegal operation.");
        current_syscall_error.type = seL4_IllegalOperation;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (excaps.excaprefs[0] == NULL || length < 1) {
        userError("X86IOPageTableMap: Truncated message.");
        current_syscall_error.type = seL4_TruncatedMessage;
        return EXCEPTION_SYSCALL_ERROR;
    }

    io_space     = excaps.excaprefs[0]->cap;    /*Z ------------------------------------------消息传参：extraCaps0-IO空间能力 */
    io_address   = getSyscallArg(0, buffer) & ~MASK(VTD_PT_INDEX_BITS + seL4_PageBits);             /*Z 0-要映射的首个虚拟地址 */

    if (cap_io_page_table_cap_get_capIOPTIsMapped(cap)) {/*Z 验证能力还未作映射 */
        userError("X86IOPageTableMap: IO page table is already mapped.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 0;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (cap_get_capType(io_space) != cap_io_space_cap) {/*Z 验证IO空间能力类型 */
        userError("X86IOPageTableMap: Invalid IO space capability.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 0;
        return EXCEPTION_SYSCALL_ERROR;
    }

    pci_request_id = cap_io_space_cap_get_capPCIDevice(io_space);
    domain_id = cap_io_space_cap_get_capDomainID(io_space);
    if (pci_request_id == asidInvalid) {/*Z 验证PCI设备编码 */
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 0;

        return EXCEPTION_SYSCALL_ERROR;
    }

    paddr = pptr_to_paddr(VTD_PTE_PTR(cap_io_page_table_cap_get_capIOPTBasePtr(cap)));
    vtd_context_slot = lookup_vtd_context_slot(io_space);/*Z 上下文表项线性地址 */

    if (!vtd_cte_ptr_get_present(vtd_context_slot)) {/*Z 不在内存。从这看，一级页表不能交换出内存 */

        /* 1st Level Page Table */
        vtd_cte_t vtd_cte = vtd_cte_new(
                                domain_id,                  /* domain ID                   */
                                false,                      /* RMRR                        */
                                x86KSnumIOPTLevels - 2,     /* addr width (x = levels - 2) */
                                paddr,                      /* address space root          */
                                0,                          /* translation type            */
                                true                        /* present                     */
                            );

        cap = cap_io_page_table_cap_set_capIOPTIsMapped(cap, 1);
        cap = cap_io_page_table_cap_set_capIOPTLevel(cap, 0);
        cap = cap_io_page_table_cap_set_capIOPTIOASID(cap, pci_request_id);

        setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
        return performX86IOPTInvocationMapContextRoot(cap, slot, vtd_cte, vtd_context_slot);/*Z 设置IOMMU上下文表项的值，设置IO页表能力的值 */
    } else {
        lookupIOPTSlot_ret_t lu_ret;
        vtd_pte_t   iopte;

        vtd_pte = (vtd_pte_t *)paddr_to_pptr(vtd_cte_ptr_get_asr(vtd_context_slot));
        lu_ret  = lookupIOPTSlot(vtd_pte, io_address);/*Z 最后一个有效页表项线性地址 */
                                                        /*Z BUG：如果传入的io_address的四级页表存在，以下的查找就误会了??? */
        if (lu_ret.status != EXCEPTION_NONE) {
            current_syscall_error.type = seL4_FailedLookup;
            current_syscall_error.failedLookupWasSource = false;
            return EXCEPTION_SYSCALL_ERROR;
        }

        lu_ret.level = x86KSnumIOPTLevels - lu_ret.level;
        if (vtd_pte_ptr_get_addr(lu_ret.ioptSlot) != 0) {
            current_syscall_error.type = seL4_DeleteFirst;

            return EXCEPTION_SYSCALL_ERROR;
        }

        iopte = vtd_pte_new(/*Z 描述一个页表的页表项 */
                    paddr,      /* physical addr            */
                    1,          /* write permission flag    */
                    1           /* read  permission flag    */
                );

        cap = cap_io_page_table_cap_set_capIOPTIsMapped(cap, 1);
        cap = cap_io_page_table_cap_set_capIOPTLevel(cap, lu_ret.level);
        cap = cap_io_page_table_cap_set_capIOPTIOASID(cap, pci_request_id);
        cap = cap_io_page_table_cap_set_capIOPTMappedAddress(cap, io_address);

        setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
        return performX86IOPTInvocationMapPT(cap, slot, iopte, lu_ret.ioptSlot);/*Z 设置IOMMU页表项的值，设置IO页表能力的值 */
    }
}
/*Z 设置CSlot的能力，设置IOMMU页表项的值，刷新页表项缓存 */
static exception_t performX86IOInvocationMap(cap_t cap, cte_t *ctSlot, vtd_pte_t iopte, vtd_pte_t *ioptSlot)
{
    ctSlot->cap = cap;
    *ioptSlot = iopte;
    flushCacheRange(ioptSlot, VTD_PTE_SIZE_BITS);

    return EXCEPTION_NONE;
}

/*Z 映射一个IOMMU内存页，更新CSlot的能力，建立页表项 */
exception_t decodeX86IOMapInvocation(
    word_t       length,    /*Z 消息长度 */
    cte_t       *slot,      /*Z 引用的CSlot */
    cap_t        cap,       /*Z 其能力cap_frame_cap */
    extra_caps_t excaps,    /*Z 传递的额外能力 */
    word_t      *buffer     /* IPC buffer */
)
{
    cap_t      io_space;            /*Z 要映射到的IOMMU空间能力 */
    word_t     io_address;          /*Z 要映射到的IOMMU虚拟地址 */
    uint32_t   pci_request_id;      /*Z PCI设备编码 */
    vtd_cte_t *vtd_context_slot;    /*Z IOMMU上下文表中的设备表项线性地址 */
    vtd_pte_t *vtd_pte;             /*Z 一级页表线性地址 */
    vtd_pte_t  iopte;
    paddr_t    paddr;               /*Z 该页物理地址 */
    lookupIOPTSlot_ret_t lu_ret;
    vm_rights_t frame_cap_rights;
    seL4_CapRights_t dma_cap_rights_mask;

    if (excaps.excaprefs[0] == NULL || length < 2) {
        userError("X86PageMapIO: Truncated message.");
        current_syscall_error.type = seL4_TruncatedMessage;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (cap_frame_cap_get_capFSize(cap) != X86_SmallPage) {
        userError("X86PageMapIO: Invalid page size.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 0;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (cap_frame_cap_get_capFMappedASID(cap) != asidInvalid) {
        userError("X86PageMapIO: Page already mapped.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 0;
        return EXCEPTION_SYSCALL_ERROR;
    }
    /*Z ---------------------------------------------------------------传递的消息参数：0-读写权限，1-要映射的IOMMU虚拟地址，extraCaps0-IOMMU空间能力 */
    io_space    = excaps.excaprefs[0]->cap;
    io_address  = getSyscallArg(1, buffer) & ~MASK(PAGE_BITS);
    paddr       = pptr_to_paddr((void *)cap_frame_cap_get_capFBasePtr(cap));

    if (cap_get_capType(io_space) != cap_io_space_cap) {
        userError("X86PageMapIO: Invalid IO space capability.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 0;
        return EXCEPTION_SYSCALL_ERROR;
    }

    pci_request_id = cap_io_space_cap_get_capPCIDevice(io_space);

    if (pci_request_id == asidInvalid) {
        userError("X86PageMapIO: Invalid PCI device.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 0;
        return EXCEPTION_SYSCALL_ERROR;
    }

    vtd_context_slot = lookup_vtd_context_slot(io_space);

    if (!vtd_cte_ptr_get_present(vtd_context_slot)) {
        /* 1st Level Page Table is not installed */
        current_syscall_error.type = seL4_FailedLookup;
        current_syscall_error.failedLookupWasSource = false;
        return EXCEPTION_SYSCALL_ERROR;
    }
    /*Z 一级页表线性地址 */
    vtd_pte = (vtd_pte_t *)paddr_to_pptr(vtd_cte_ptr_get_asr(vtd_context_slot));
    lu_ret  = lookupIOPTSlot(vtd_pte, io_address);/*Z 在IOMMU页表查找要映射的虚拟地址的末级页表项地址 */
    if (lu_ret.status != EXCEPTION_NONE || lu_ret.level != 0) {
        current_syscall_error.type = seL4_FailedLookup;
        current_syscall_error.failedLookupWasSource = false;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (vtd_pte_ptr_get_addr(lu_ret.ioptSlot) != 0) {/*Z 虚拟地址已被占用 */
        current_syscall_error.type = seL4_DeleteFirst;
        return EXCEPTION_SYSCALL_ERROR;
    }

    dma_cap_rights_mask = rightsFromWord(getSyscallArg(0, buffer));/*Z 要求的读写权限 */
    frame_cap_rights    = cap_frame_cap_get_capFVMRights(cap);/*Z 能力已有的权限 */

    bool_t write = seL4_CapRights_get_capAllowWrite(dma_cap_rights_mask) && (frame_cap_rights == VMReadWrite);
    bool_t read = seL4_CapRights_get_capAllowRead(dma_cap_rights_mask) && (frame_cap_rights != VMKernelOnly);
    if (write || read) {
        iopte = vtd_pte_new(paddr, !!write, !!read);/*Z 生成一个页表项 */
    } else {
        current_syscall_error.type = seL4_InvalidArgument;
        current_syscall_error.invalidArgumentNumber = 0;
        return EXCEPTION_SYSCALL_ERROR;
    }

    cap = cap_frame_cap_set_capFMapType(cap, X86_MappingIOSpace);
    cap = cap_frame_cap_set_capFMappedASID(cap, pci_request_id);
    cap = cap_frame_cap_set_capFMappedAddress(cap, io_address);

    setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
    return performX86IOInvocationMap(cap, slot, iopte, lu_ret.ioptSlot);/*Z 设置CSlot的能力，设置IOMMU页表项的值，刷新页表项缓存 */
}
/*Z 清除IOMMU页表能力所指代的页表在上级页表(上下文项)中的页表项，失效所有IOMMU的IOTLB */
void deleteIOPageTable(cap_t io_pt_cap)
{
    lookupIOPTSlot_ret_t lu_ret;
    uint32_t             level;
    word_t               io_address;
    vtd_cte_t           *vtd_context_slot;
    vtd_pte_t           *vtd_pte;

    if (cap_io_page_table_cap_get_capIOPTIsMapped(io_pt_cap)) {
        io_pt_cap = cap_io_page_table_cap_set_capIOPTIsMapped(io_pt_cap, 0);/*Z 不好：没用的语句 */
        level = cap_io_page_table_cap_get_capIOPTLevel(io_pt_cap);
        vtd_context_slot = lookup_vtd_context_slot(io_pt_cap);/*Z 上下文表项 */

        if (!vtd_cte_ptr_get_present(vtd_context_slot)) {/*Z 不在内存 */
            return;
        }
        /*Z 一级页表 */
        vtd_pte = (vtd_pte_t *)paddr_to_pptr(vtd_cte_ptr_get_asr(vtd_context_slot));

        if (level == 0) {
            /* if we have been overmapped or something */
            if (pptr_to_paddr(vtd_pte) != pptr_to_paddr((void *)cap_io_page_table_cap_get_capIOPTBasePtr(io_pt_cap))) {
                return;
            }
            *vtd_context_slot = vtd_cte_new(/*Z 清除上下文表项 */
                                    0,      /* Domain ID          */
                                    false,  /* RMRR               */
                                    0,      /* Address Width      */
                                    0,      /* Address Space Root */
                                    0,      /* Translation Type   */
                                    0       /* Present            */
                                );
            flushCacheRange(vtd_context_slot, VTD_CTE_SIZE_BITS);/*Z 刷新缓存 */
        } else {/*Z 查找映射的首个虚拟地址在最后一级页表的页表项 */
            io_address = cap_io_page_table_cap_get_capIOPTMappedAddress(io_pt_cap);
            lu_ret = lookupIOPTSlot_resolve_levels(vtd_pte, io_address >> PAGE_BITS, level - 1, level - 1);

            /* if we have been overmapped or something */
            if (lu_ret.status != EXCEPTION_NONE || lu_ret.level != 0) {
                return;
            }
            if (vtd_pte_ptr_get_addr(lu_ret.ioptSlot) != pptr_to_paddr((void *)cap_io_page_table_cap_get_capIOPTBasePtr(
                                                                           io_pt_cap))) {
                return;
            }
            *lu_ret.ioptSlot = vtd_pte_new(
                                   0,  /* Physical Address */
                                   0,  /* Read Permission  */
                                   0   /* Write Permission */
                               );
            flushCacheRange(lu_ret.ioptSlot, VTD_PTE_SIZE_BITS);
        }
        invalidate_iotlb();
    }
}
/*Z 清除能力代表的内存页在IOMMU中的页表项，清缓存 */
void unmapIOPage(cap_t cap)
{
    lookupIOPTSlot_ret_t lu_ret;
    word_t               io_address;
    vtd_cte_t           *vtd_context_slot;
    vtd_pte_t           *vtd_pte;
    /*Z 页映射的虚拟地址，上下文表项地址 */
    io_address  = cap_frame_cap_get_capFMappedAddress(cap);
    vtd_context_slot = lookup_vtd_context_slot(cap);

    /*Z 不在内存 */
    if (!vtd_cte_ptr_get_present(vtd_context_slot)) {
        return;
    }
    /*Z 一级页表 */
    vtd_pte = (vtd_pte_t *)paddr_to_pptr(vtd_cte_ptr_get_asr(vtd_context_slot));
    /*Z 虚拟地址的末级页表项地址 */
    lu_ret  = lookupIOPTSlot(vtd_pte, io_address);
    if (lu_ret.status != EXCEPTION_NONE || lu_ret.level != 0) {
        return;
    }

    if (vtd_pte_ptr_get_addr(lu_ret.ioptSlot) != pptr_to_paddr((void *)cap_frame_cap_get_capFBasePtr(cap))) {
        return;
    }

    *lu_ret.ioptSlot = vtd_pte_new(
                           0,  /* Physical Address */
                           0,  /* Read Permission  */
                           0   /* Write Permission */
                       );

    flushCacheRange(lu_ret.ioptSlot, VTD_PTE_SIZE_BITS);
    invalidate_iotlb();
}
/*Z 解映射一个IOMMU内存页：清页表、清缓存、复位能力。cap为cap-frame-cap能力，cte为其CSlot */
exception_t performX86IOUnMapInvocation(cap_t cap, cte_t *ctSlot)
{   /*Z 清除能力代表的内存页在IOMMU中的页表项，清缓存 */
    unmapIOPage(ctSlot->cap);

    ctSlot->cap = cap_frame_cap_set_capFMappedAddress(ctSlot->cap, 0);
    ctSlot->cap = cap_frame_cap_set_capFMapType(ctSlot->cap, X86_MappingNone);
    ctSlot->cap = cap_frame_cap_set_capFMappedASID(ctSlot->cap, asidInvalid);

    return EXCEPTION_NONE;
}
/*Z 引用cap_io_space_cap能力的系统调用：不支持 */
exception_t decodeX86IOSpaceInvocation(word_t invLabel, cap_t cap)
{
    userError("IOSpace capability has no invocations");
    current_syscall_error.type = seL4_IllegalOperation;
    return EXCEPTION_SYSCALL_ERROR;
}

#endif /* CONFIG_IOMMU */

/*Z OK */

/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>
#include <api/debug.h>
/*Z idle线程实际是所在cpu停止执行指令，除非通过中断调度走 */
void idle_thread(void)
{
    while (1) { /*Z NMI等可使hlt的cpu继续执行下条指令，但这里是while(1) */
        asm volatile("hlt");    /*Z 当前逻辑cpu停止执行指令 */
    }
}
/*Z hlt */
/** DONT_TRANSLATE */
void VISIBLE halt(void)
{
    /* halt is actually, idle thread without the interrupts */
    asm volatile("cli");    /*Z 屏蔽中断 */

#ifdef CONFIG_PRINTING
    printf("halting...");
#ifdef CONFIG_DEBUG_BUILD
    debug_printKernelEntryReason();
#endif
#endif
    idle_thread();
    UNREACHABLE();
}

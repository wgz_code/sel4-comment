/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>
#include <model/statedata.h>
#include <machine/fpu.h>
#include <arch/fastpath/fastpath.h>
#include <arch/kernel/traps.h>
#include <machine/debug.h>
#include <arch/object/vcpu.h>
#include <api/syscall.h>
#include <sel4/arch/vmenter.h>

#include <benchmark/benchmark_track.h>
#include <benchmark/benchmark_utilisation.h>
/*Z 6.2 标记等待处理的中断(嵌套中断) */
void VISIBLE c_nested_interrupt(int irq)
{
    /* This is not a real entry point, so we do not grab locks or
     * run c_entry/exit_hooks, since this occurs only if we're already
     * running inside the kernel. Just record the irq and return */
    assert(ARCH_NODE_STATE(x86KSPendingInterrupt) == int_invalid);
    ARCH_NODE_STATE(x86KSPendingInterrupt) = irq;
}
/*Z 6.3.1 处理来自用户空间、idle时的中断(例外)。之前已做：现场保存在TCB，加载内核栈，参数1向量号。
例外和硬件中断基本都需要用户空间自定义，seL4内核相当于只提供了机制。
参数syscall只在系统功能调用时使用，设计是用于DEBUG和BENCHMARK；中断路径不使用这个参数 */
void VISIBLE NORETURN c_handle_interrupt(int irq, int syscall)
{   /*Z 防攻击 */
    /* need to run this first as the NODE_LOCK code might end up as a function call
     * with a return, and we need to make sure returns are not exploitable yet
     * on x64 this code ran already */
    if (config_set(CONFIG_ARCH_IA32) && config_set(CONFIG_KERNEL_X86_IBRS_BASIC)) {
        x86_enable_ibrs();
    }
    /*Z 获取锁。不好：多核瓶颈，如果多个核中断进入，会竞争锁 */
    /* Only grab the lock if we are not handeling 'int_remote_call_ipi' interrupt
     * also flag this lock as IRQ lock if handling the irq interrupts. */
    NODE_LOCK_IF(irq != int_remote_call_ipi,
                 irq >= int_irq_min && irq <= int_irq_max);
    /*Z 将当前cpu的FSGS段基地址保存在ksCurThread TCB上下文中 */
    c_entry_hook();

    if (irq == int_unimpl_dev) {            /*Z --------------------------------------------------------------- #7 lazyFPU切换 */
        handleFPUFault();
#ifdef TRACK_KERNEL_ENTRIES
        ksKernelEntry.path = Entry_UnimplementedDevice;
        ksKernelEntry.word = irq;
#endif
    } else if (irq == int_page_fault) {     /*Z --------------------------------------------------------------- #14 页错误。seL4内核只传递错误 */
        /* Error code is in Error. Pull out bit 5, which is whether it was instruction or data */
        vm_fault_type_t type = (NODE_STATE(ksCurThread)->tcbArch.tcbContext.registers[Error] >> 4u) & 1u;
#ifdef TRACK_KERNEL_ENTRIES
        ksKernelEntry.path = Entry_VMFault;
        ksKernelEntry.word = type;
#endif  /*Z 处理页错误：将错误发送给当前线程的错误处理对象，线程阻塞并等待处理结果，引入调度 */
        handleVMFaultEvent(type);
#ifdef CONFIG_HARDWARE_DEBUG_API
    } else if (irq == int_debug || irq == int_software_break_request) {/*Z ------------------------------------ #1 #3 DEBUG。seL4内核只传递例外 */
        /* Debug exception */
#ifdef TRACK_KERNEL_ENTRIES
        ksKernelEntry.path = Entry_DebugFault;
        ksKernelEntry.word = NODE_STATE(ksCurThread)->tcbArch.tcbContext.registers[FaultIP];
#endif  /*Z 处理用户空间DEBUG例外：将例外信息发送给当前线程的错误处理对象，线程阻塞并等待处理结果，引入调度 */
        handleUserLevelDebugException(irq);
#endif /* CONFIG_HARDWARE_DEBUG_API */
    } else if (irq < int_irq_min) {/*Z ------------------------------------------------------------------------ #0~31 其它例外。seL4内核只传递例外 */
#ifdef TRACK_KERNEL_ENTRIES
        ksKernelEntry.path = Entry_UserLevelFault;
        ksKernelEntry.word = irq;
#endif  /*Z 处理<32的其它例外(中断)：将例外信息发送给当前线程的错误处理对象，线程阻塞并等待处理结果，引入调度 */
        handleUserLevelFault(irq, NODE_STATE(ksCurThread)->tcbArch.tcbContext.registers[Error]);
    } else if (likely(irq < int_trap_min)) {/*Z --------------------------------------------------------------- #32~159 硬件中断。多数需用户空间自定义 */
        ARCH_NODE_STATE(x86KScurInterrupt) = irq;/*Z 设置当前正处理的中断全局变量 */
#ifdef TRACK_KERNEL_ENTRIES
        ksKernelEntry.path = Entry_Interrupt;
        ksKernelEntry.word = irq;
#endif  /*Z 处理32~159硬件中断：普通中断发给用户自定义处理线程，定时器递减时间片，其它特定处理，引入调度 */
        handleInterruptEntry();
        /* check for other pending interrupts */
        receivePendingIRQ();/*Z 接收更新x86KSPendingInterrupt全局变量 */
    } else if (irq == int_spurious) {/*Z ---------------------------------------------------------------------- #255 伪中断。空 */
        /* fall through to restore_user_context and do nothing */
    } else {/*Z ----------------------------------------------------------------------------------------------- #160~254 用户自定义trap。目前用于DEBUG和BENCHMARK */
        /* Interpret a trap as an unknown syscall */
        /* Adjust FaultIP to point to trapping INT
         * instruction by subtracting 2 */
        int sys_num;        /*Z 将trap后本应执行的下条指令，改为重新执行trap指令(应该是修改后的非trap指令) */
        NODE_STATE(ksCurThread)->tcbArch.tcbContext.registers[FaultIP] -= 2;
        /* trap number is MSBs of the syscall number and the LSBS of EAX */
        sys_num = (irq << 24) | (syscall & 0x00ffffff);
#ifdef TRACK_KERNEL_ENTRIES
        ksKernelEntry.path = Entry_UnknownSyscall;
        ksKernelEntry.word = sys_num;
#endif  /*Z 处理#160~254自定义trap：目前是DEBUG和BENCHMARK */
        handleUnknownSyscall(sys_num);
    }
    restore_user_context();
    UNREACHABLE();
}
/*Z 系统调用慢(正常)路径第一部分 */
void NORETURN slowpath(syscall_t syscall)
{

#ifdef CONFIG_VTX
    if (syscall == SysVMEnter && NODE_STATE(ksCurThread)->tcbArch.tcbVCPU) {/*Z -------------------------------VMEnter系统调用 */
        vcpu_update_state_sysvmenter(NODE_STATE(ksCurThread)->tcbArch.tcbVCPU);/*Z 加载vcpu，更新VMCS关键域 */
        if (NODE_STATE(ksCurThread)->tcbBoundNotification
            && notification_ptr_get_state(NODE_STATE(ksCurThread)->tcbBoundNotification) == NtfnState_Active) {
            completeSignal(NODE_STATE(ksCurThread)->tcbBoundNotification, NODE_STATE(ksCurThread));/*Z 接收通知信号 */
            setRegister(NODE_STATE(ksCurThread), msgInfoRegister, SEL4_VMENTER_RESULT_NOTIF);
            /* Any guest state that we should return is in the same
             * register position as sent to us, so we can just return
             * and let the user pick up the values they put in */
            restore_user_context();/*Z 系统调用返回值协议：消息标记寄存器-通知标记，指示寄存器-0成功 */
        } else {
            setThreadState(NODE_STATE(ksCurThread), ThreadState_RunningVM);
            restore_user_context();
        }
    }
#endif
    /* check for undefined syscall */
    if (unlikely(syscall < SYSCALL_MIN || syscall > SYSCALL_MAX)) {
#ifdef TRACK_KERNEL_ENTRIES
        ksKernelEntry.path = Entry_UnknownSyscall;
        /* ksKernelEntry.word word is already set to syscall */
#endif /* TRACK_KERNEL_ENTRIES */
        handleUnknownSyscall(syscall);
    } else {
#ifdef TRACK_KERNEL_ENTRIES
        ksKernelEntry.is_fastpath = 0;
#endif /* TRACK KERNEL ENTRIES */
        handleSyscall(syscall);
    }

    restore_user_context();
    UNREACHABLE();
}

#ifdef CONFIG_KERNEL_MCS
void VISIBLE NORETURN c_handle_syscall(word_t cptr, word_t msgInfo, syscall_t syscall, word_t reply)
#else
/*Z 2. 系统调用C部分：发后要收的尽量使用快速调用路径。调用中持有锁
cptr使用的能力句柄，msgInfo指示性消息字，syscall系统调用号 */
void VISIBLE NORETURN c_handle_syscall(word_t cptr, word_t msgInfo, syscall_t syscall)
#endif
{
    /* need to run this first as the NODE_LOCK code might end up as a function call
     * with a return, and we need to make sure returns are not exploitable yet */
    if (config_set(CONFIG_KERNEL_X86_IBRS_BASIC)) {
        x86_enable_ibrs();
    }
    /*Z 获取内核锁 */
    NODE_LOCK_SYS;
    /*Z 将当前cpu的FSGS段基地址保存在ksCurThread TCB上下文中 */
    c_entry_hook();

#ifdef TRACK_KERNEL_ENTRIES
    benchmark_debug_syscall_start(cptr, msgInfo, syscall);
    ksKernelEntry.is_fastpath = 1;
#endif /* TRACK_KERNEL_ENTRIES */

    if (config_set(CONFIG_SYSENTER)) {
        /* increment NextIP to skip sysenter *//*Z 汇编入口处存的sysenter指令地址，这里加2得到其后的指令地址 */
        NODE_STATE(ksCurThread)->tcbArch.tcbContext.registers[NextIP] += 2;
    } else {
        /* set FaultIP */
        setRegister(NODE_STATE(ksCurThread), FaultIP, getRegister(NODE_STATE(ksCurThread), NextIP) - 2);
    }

#ifdef CONFIG_FASTPATH  /*Z 发后要求收的，可能通过快速路径直接使收方响应 */
    if (syscall == (syscall_t)SysCall) {/*Z Call系统调用 */
        fastpath_call(cptr, msgInfo);/*Z 快速路径系统调用 */
        UNREACHABLE();
    } else if (syscall == (syscall_t)SysReplyRecv) {/*Z ReplyRecv系统调用 */
#ifdef CONFIG_KERNEL_MCS
        fastpath_reply_recv(cptr, msgInfo, reply);
#else
        fastpath_reply_recv(cptr, msgInfo);/*Z 快速路径系统调用 */
#endif
        UNREACHABLE();
    }
#endif /* CONFIG_FASTPATH *//*Z 其它系统调用 */
    slowpath(syscall);
    UNREACHABLE();
}

#ifdef CONFIG_VTX
/*Z VMexit时执行的C第一部分 */
void VISIBLE NORETURN c_handle_vmexit(void)
{
#ifdef TRACK_KERNEL_ENTRIES
    ksKernelEntry.path = Entry_VMExit;
#endif
    /*Z 抹去当前栈顶下32个字。与防止推测执行攻击有关 */
    /* We *always* need to flush the rsb as a guest may have been able to train the rsb with kernel addresses */
    x86_flush_rsb();

    /* When we switched out of VMX mode the FS and GS registers were
     * clobbered and set to potentially undefined values. we need to
     * make sure we reload the correct values of FS and GS.
     * Unfortunately our cached values in x86KSCurrent[FG]SBase now
     * mismatch what is in the hardware. To force a reload to happen we
     * set the cached value to something that is guaranteed to not be
     * the target threads value, ensuring both the cache and the
     * hardware get updated.
     *
     * This needs to happen before the entry hook which will try to
     * restore the registers without having a means to determine whether
     * they may have been dirtied by a VM exit. */
    tcb_t *cur_thread = NODE_STATE(ksCurThread);
    ARCH_NODE_STATE(x86KSCurrentGSBase) = -(word_t)1;/*Z 因为程序的逻辑，这样赋值可以确保下面的函数调用会重新设置FS、GS段寄存器 */
    ARCH_NODE_STATE(x86KSCurrentFSBase) = -(word_t)1;
    x86_load_fsgs_base(cur_thread, SMP_TERNARY(getCurrentCPUIndex(), 0));/*Z 用指定TCB上下文中保存的值设置FS、GS段寄存器并保存在per cpu数据结构中 */
    /*Z 将当前cpu的FSGS段基地址保存在ksCurThread TCB上下文中 */
    c_entry_hook();
    /* NODE_LOCK will get called in handleVmexit */
    handleVmexit();
    restore_user_context();
    UNREACHABLE();
}
#endif

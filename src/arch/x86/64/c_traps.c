/*Z OK */

/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>
#include <model/statedata.h>
#include <machine/fpu.h>
#include <kernel/traps.h>
#include <arch/machine/debug.h>
#include <kernel/stack.h>

#include <api/syscall.h>

#ifdef CONFIG_VTX
/*Z 收集VMentry失败的全部17个(错误)信息，写入ksCurThread的消息寄存器和IPC buffer；控制权交回ksCurThread */
static void NORETURN vmlaunch_failed(word_t failInvalid, word_t failValid)
{   /*Z 获取内核锁 */
    NODE_LOCK_SYS;
    /*Z 将当前cpu的FSGS段基地址保存在ksCurThread TCB上下文中。有什么意义吧??? */
    c_entry_hook();

    if (failInvalid) {
        userError("current VMCS pointer is not valid");
    }
    if (failValid) {                                        /*Z VMX指令错误码 */
        userError("vmlaunch/vmresume error %d", (int)vmread(VMX_DATA_INSTRUCTION_ERROR));
    }
    /*Z 收集VMentry失败的全部17个(错误)信息，写入ksCurThread的消息寄存器和IPC buffer；控制权交回ksCurThread */
    handleVmEntryFail();
    restore_user_context();
}
/*Z 恢复ksCurThread的VMCS环境，加载或继续运行虚拟机；出错时写消息并将控制权交回ksCurThread */
static void NORETURN restore_vmx(void)
{
    restoreVMCS();/*Z 恢复ksCurThread的VMCS环境：vpid、EPT等 */
    tcb_t *cur_thread = NODE_STATE(ksCurThread);
#ifdef CONFIG_HARDWARE_DEBUG_API
    /* Do not support breakpoints in VMs, so just disable all breakpoints */
    loadAllDisabledBreakpointState(cur_thread);/*Z 禁用全局断点 */
#endif
    if (cur_thread->tcbArch.tcbVCPU->launched) {/*Z 已加载的就继续 */
        /* attempt to do a vmresume */
        asm volatile(
            // Set our stack pointer to the top of the tcb so we can efficiently pop
            "movq %[reg], %%rsp\n"  /*Z 栈指针指向通用寄存器数组的第0个元素 */
            "popq %%rax\n"
            "popq %%rbx\n"
            "popq %%rcx\n"
            "popq %%rdx\n"
            "popq %%rsi\n"
            "popq %%rdi\n"
            "popq %%rbp\n"
#ifdef ENABLE_SMP_SUPPORT
            "swapgs\n"              /*Z 交换设置GS段基地址 */
#endif
            // Now do the vmresume
            "vmresume\n"            /*Z 继续运行虚拟机。VMentry */
            "setb %%al\n"           /*Z 如果CF=1，AL=1，VMCS指针无效。不出错的话是执行不到这里的 */
            "sete %%bl\n"           /*Z 如果ZF=1，BL=1 */
            "movzx %%al, %%rdi\n"   /*Z 0扩展赋值 */
            "movzx %%bl, %%rsi\n"
            // if we get here we failed
#ifdef ENABLE_SMP_SUPPORT
            "swapgs\n"
            "movq %%gs:%c[stack_offset], %%rsp\n"   /*Z 栈指向内核栈顶 */
#else
            "leaq kernel_stack_alloc + %c[stack_size], %%rsp\n"
#endif
            "movq %[failed], %%rax\n"
            "jmp *%%rax\n"  /*Z 跳转到vmlaunch_failed() */
            :
            : [reg]"r"(&cur_thread->tcbArch.tcbVCPU->gp_registers[VCPU_EAX]),
            [failed]"i"(&vmlaunch_failed),
            [stack_size]"i"(BIT(CONFIG_KERNEL_STACK_BITS))
#ifdef ENABLE_SMP_SUPPORT
            , [stack_offset]"i"(OFFSETOF(nodeInfo_t, stackTop))
#endif
            // Clobber memory so the compiler is forced to complete all stores
            // before running this assembler
            : "memory"
        );
    } else {/*Z 未加载的就加载 */
        /* attempt to do a vmlaunch */
        asm volatile(
            // Set our stack pointer to the top of the tcb so we can efficiently pop
            "movq %[reg], %%rsp\n"
            "popq %%rax\n"
            "popq %%rbx\n"
            "popq %%rcx\n"
            "popq %%rdx\n"
            "popq %%rsi\n"
            "popq %%rdi\n"
            "popq %%rbp\n"
#ifdef ENABLE_SMP_SUPPORT
            "swapgs\n"
#endif
            // Now do the vmresume
            "vmlaunch\n"
            // if we get here we failed
            "setb %%al\n"
            "sete %%bl\n"
            "movzx %%al, %%rdi\n"
            "movzx %%bl, %%rsi\n"
#ifdef ENABLE_SMP_SUPPORT
            "swapgs\n"
            "movq %%gs:%c[stack_offset], %%rsp\n"
#else
            "leaq kernel_stack_alloc + %c[stack_size], %%rsp\n"
#endif
            "movq %[failed], %%rax\n"
            "jmp *%%rax\n"
            :
            : [reg]"r"(&cur_thread->tcbArch.tcbVCPU->gp_registers[VCPU_EAX]),
            [failed]"i"(&vmlaunch_failed),
            [stack_size]"i"(BIT(CONFIG_KERNEL_STACK_BITS))
#ifdef ENABLE_SMP_SUPPORT
            , [stack_offset]"i"(OFFSETOF(nodeInfo_t, stackTop))
#endif
            // Clobber memory so the compiler is forced to complete all stores
            // before running this assembler
            : "memory"
        );
    }
    UNREACHABLE();
}
#endif
/*Z 释放锁并恢复用户上下文：或处理等待的中断、或恢复虚拟机、或系统调用和中断返回 */
void VISIBLE NORETURN restore_user_context(void)
{
    NODE_UNLOCK_IF_HELD;/*Z 如果当前cpu持有锁，则释放锁 */
    c_exit_hook();/*Z 恢复ksCurThread上下文中保存的FS、GS段寄存器值 */

    /* we've now 'exited' the kernel. If we have a pending interrupt
     * we should 'enter' it again *//*Z 处理等待的中断----------------------------------------------------不再返回 */
    if (ARCH_NODE_STATE(x86KSPendingInterrupt) != int_invalid) {
        interrupt_t irq = servicePendingIRQ();/*Z 待处理的中断向量号 */
        /* reset our stack and jmp to the IRQ entry point */
        asm volatile(
            "movq %[stack_top], %%rsp\n"    /*Z rsp=当前cpu内核栈顶(数组外首字节) */
            "movq %[syscall], %%rsi\n"      /*Z rsi=0。C函数第二个参数(后续的是rdx, rcx, r8, r9) */
            "movq %[irq], %%rdi\n"          /*Z rdi=irq。C函数第一个参数 */
            "call c_handle_interrupt"
            :
            : [stack_top] "r"(&(kernel_stack_alloc[CURRENT_CPU_INDEX()][BIT(CONFIG_KERNEL_STACK_BITS)])),
            [syscall] "i"(0), /* syscall is unused for irq path */
            [irq] "r"((seL4_Word)irq)
            : "memory");
        UNREACHABLE();
    }

    tcb_t *cur_thread = NODE_STATE(ksCurThread);
    word_t *irqstack = x64KSIRQStack[CURRENT_CPU_INDEX()];/*Z 中断栈。8个字 */
#ifdef CONFIG_VTX   /*Z 成功恢复虚拟机后--------------------------------------------------------------------不再返回 */
    if (thread_state_ptr_get_tsType(&cur_thread->tcbState) == ThreadState_RunningVM) {
        restore_vmx();/*Z 恢复ksCurThread的VMCS环境，加载或继续运行虚拟机；出错时写消息并将控制权交回ksCurThread */
    }
#endif
    lazyFPURestore(cur_thread);/*Z 当前cpu有活跃FPU且属于线程时，使能FPU */

#ifdef CONFIG_HARDWARE_DEBUG_API
    restore_user_debug_context(cur_thread);/*Z 恢复断点、单步执行设置 */
#endif

#ifdef ENABLE_SMP_SUPPORT
#ifdef CONFIG_KERNEL_SKIM_WINDOW
    word_t user_cr3 = MODE_NODE_STATE(x64KSCurrentUserCR3);
#endif /* CONFIG_KERNEL_SKIM_WINDOW */
    swapgs();/*Z 恢复用户GS段寄存器 */
#endif /* ENABLE_SMP_SUPPORT */

    if (config_set(CONFIG_KERNEL_X86_IBRS_BASIC)) {
        x86_disable_ibrs();/*Z 禁用分支预测限制，阻止逻辑核间分支预测控制 */
    }

    // Check if we are returning from a syscall/sysenter or from an interrupt
    // There is a special case where if we would be returning from a sysenter,
    // but are current singlestepping, do a full return like an interrupt
    if (likely(cur_thread->tcbArch.tcbContext.registers[Error] == -1) &&    /*Z 有错误码 */
        (!config_set(CONFIG_SYSENTER) || !config_set(CONFIG_HARDWARE_DEBUG_API)
         || ((cur_thread->tcbArch.tcbContext.registers[FLAGS] & FLAGS_TF) == 0))) {
        if (config_set(CONFIG_KERNEL_SKIM_WINDOW)) {                        /*Z 清零中断栈 */
            /* if we are using the SKIM window then we are trying to hide kernel state from
             * the user in the case of Meltdown where the kernel region is effectively readable
             * by the user. To prevent a storage channel across threads through the irq stack,
             * which is idirectly controlled by the user, we need to clear the stack. We perform
             * this here since when we return *from* an interrupt we must use this stack and
             * cannot clear it. This means if we restore from interrupt, then enter from a syscall
             * and switch to a different thread we must either on syscall entry, or before leaving
             * the kernel, clear the irq stack. */
            irqstack[0] = 0;
            irqstack[1] = 0;
            irqstack[2] = 0;
            irqstack[3] = 0;
            irqstack[4] = 0;
            irqstack[5] = 0;
        }
        if (config_set(CONFIG_SYSENTER)) {/*Z 处理SYSEXIT返回--------------------------------------------不再返回 */
            cur_thread->tcbArch.tcbContext.registers[FLAGS] &= ~FLAGS_IF;   /*Z 清除保存的中断标志 */
#if defined(ENABLE_SMP_SUPPORT) && defined(CONFIG_KERNEL_SKIM_WINDOW)
            register word_t user_cr3_r11 asm("r11") = user_cr3;/*Z GCC扩展：定义变量存在于指定的寄存器 */
#endif
            asm volatile(
                // Set our stack pointer to the top of the tcb so we can efficiently pop
                "movq %0, %%rsp\n"      /*Z 依次弹出保存的寄存器值 */
                "popq %%rdi\n"
                "popq %%rsi\n"
                "popq %%rax\n"
                "popq %%rbx\n"
                "popq %%rbp\n"
                "popq %%r12\n"
                "popq %%r13\n"
                "popq %%r14\n"
                // skip RDX
                "addq $8, %%rsp\n"
                "popq %%r10\n"
                "popq %%r8\n"
                "popq %%r9\n"
                "popq %%r15\n"
                //restore RFLAGS
                "popfq\n"
                // reset interrupt bit
                "orq %[IF], -8(%%rsp)\n"
                // Restore NextIP
                "popq %%rdx\n"          /*Z RDX存的是NextIP值 */
                // Skip ERROR
                "addq $8, %%rsp\n"
                // Restore RSP
                "popq %%rcx\n"          /*Z RCX存RSP值 */
                // Skip FaultIP
                "addq $8, %%rsp\n"
#if defined(ENABLE_SMP_SUPPORT) && defined(CONFIG_KERNEL_SKIM_WINDOW)
                "popq %%rsp\n"
                "movq %%r11, %%cr3\n"   /*Z 恢复CR3 */
                "movq %%rsp, %%r11\n"
#else
                "popq %%r11\n"
#ifdef CONFIG_KERNEL_SKIM_WINDOW
                "movq (x64KSCurrentUserCR3), %%rsp\n"       /*Z GCC的什么用法???????????? */
                "movq %%rsp, %%cr3\n"
#endif /* CONFIG_KERNEL_SKIM_WINDOW */
#endif /* defined(ENABLE_SMP_SUPPORT) && defined(CONFIG_KERNEL_SKIM_WINDOW) */
                // More register but we can ignore and are done restoring
                // enable interrupt disabled by sysenter
                "sti\n"
                /* Return to user.
                 *
                 * SYSEXIT  0F 35     ; Return to compatibility mode from fast system call.
                 * SYSEXITQ 48 0F 35  ; Return to 64-bit mode from fast system call.
                 * */
                "sysexitq\n"
                :
                : "r"(&cur_thread->tcbArch.tcbContext.registers[RDI]),/*Z 保存的寄存器数组索引0处 */
#if defined(ENABLE_SMP_SUPPORT) && defined(CONFIG_KERNEL_SKIM_WINDOW)
                "r"(user_cr3_r11),
#endif
                [IF] "i"(FLAGS_IF)
                // Clobber memory so the compiler is forced to complete all stores
                // before running this assembler
                : "memory"
            );
        } else {/*Z 处理SYSRET返回------------------------------------------------------------------------不再返回 */
            asm volatile(
                // Set our stack pointer to the top of the tcb so we can efficiently pop
                "movq %0, %%rsp\n"
                "popq %%rdi\n"
                "popq %%rsi\n"
                "popq %%rax\n"
                "popq %%rbx\n"
                "popq %%rbp\n"
                "popq %%r12\n"
                "popq %%r13\n"
                "popq %%r14\n"
                "popq %%rdx\n"
                "popq %%r10\n"
                "popq %%r8\n"
                "popq %%r9\n"
                "popq %%r15\n"
                //restore RFLAGS
                "popq %%r11\n"          /*Z R11存RFLAGS */
                // Restore NextIP
#if defined(ENABLE_SMP_SUPPORT) && defined(CONFIG_KERNEL_SKIM_WINDOW)
                "popq %%rsp\n"
                "movq %%rcx, %%cr3\n"   /*Z 恢复CR3 */
                "movq %%rsp, %%rcx\n"   /*Z RCX存NextIP */
#else
                "popq %%rcx\n"
#ifdef CONFIG_KERNEL_SKIM_WINDOW
                "movq (x64KSCurrentUserCR3), %%rsp\n"
                "movq %%rsp, %%cr3\n"
#endif /* CONFIG_KERNEL_SKIM_WINDOW */
#endif /* defined(ENABLE_SMP_SUPPORT) && defined(CONFIG_KERNEL_SKIM_WINDOW) */
                // clear RSP to not leak information to the user
                "xor %%rsp, %%rsp\n"
                // More register but we can ignore and are done restoring
                // enable interrupt disabled by sysenter
                "sysretq\n"
                :
                : "r"(&cur_thread->tcbArch.tcbContext.registers[RDI])
#if defined(ENABLE_SMP_SUPPORT) && defined(CONFIG_KERNEL_SKIM_WINDOW)
                , "c"(user_cr3)
#endif
                // Clobber memory so the compiler is forced to complete all stores
                // before running this assembler
                : "memory"
            );
        }
    } else {/*Z 处理IRET中断返回------------------------------------------------------------------------不再返回 */
        /* construct our return from interrupt frame */
#ifdef CONFIG_KERNEL_SKIM_WINDOW
        /* Have to zero this to prevent storage channel */
        irqstack[0] = 0;
#endif  /*Z 构建IRET返回前的中断栈内容 */
        irqstack[1] = getRegister(cur_thread, NextIP);
        irqstack[2] = getRegister(cur_thread, CS);
        irqstack[3] = getRegister(cur_thread, FLAGS);
        irqstack[4] = getRegister(cur_thread, RSP);
        irqstack[5] = getRegister(cur_thread, SS);
        asm volatile(
            // Set our stack pointer to the top of the tcb so we can efficiently pop
            "movq %0, %%rsp\n"
            "popq %%rdi\n"
            "popq %%rsi\n"
            "popq %%rax\n"
            "popq %%rbx\n"
            "popq %%rbp\n"
            "popq %%r12\n"
            "popq %%r13\n"
            "popq %%r14\n"
            "popq %%rdx\n"
            "popq %%r10\n"
            "popq %%r8\n"
            "popq %%r9\n"
            "popq %%r15\n"
            /* skip RFLAGS, Error, NextIP, RSP, and FaultIP */
            "addq $40, %%rsp\n"
            "popq %%r11\n"

#if defined(ENABLE_SMP_SUPPORT) && defined(CONFIG_KERNEL_SKIM_WINDOW)
            /* pop into rsp as we're done with the stack for now and we need to
             * preserve our rcx value as it has our next cr3 value */
            "popq %%rsp\n"      /*Z RSP暂存RCX */
#else
            "popq %%rcx\n"
#endif /* defined(ENABLE_SMP_SUPPORT) && defined(CONFIG_KERNEL_SKIM_WINDOW) */

#ifdef ENABLE_SMP_SUPPORT
            // Swapping gs twice here is worth it as it allows us to efficiently
            // set the user gs base previously
            "swapgs\n"              /*Z 暂时恢复到内核GS段 */
#ifdef CONFIG_KERNEL_SKIM_WINDOW
            /* now we stash rcx in the scratch space that we can access once we've
             * loaded the user cr3 */
            "movq %%rsp, %%gs:%c[scratch_offset]\n"
#endif /* CONFIG_KERNEL_SKIM_WINDOW */
            "movq %%gs:8, %%rsp\n"  /*Z 恢复RSP指向中断栈 */
#ifdef CONFIG_KERNEL_SKIM_WINDOW
            /* change to the user address space and then load the value of rcx that
             * we stashed */
            "movq %%rcx, %%cr3\n"   /*Z 恢复CR3 */
            "movq %%gs:%c[scratch_offset], %%rcx\n"
#endif /* CONFIG_KERNEL_SKIM_WINDOW */
            "addq $8, %%rsp\n"      /*Z 跳过错误码 */
            // Switch to the user GS value
            "swapgs\n"              /*Z 恢复用户GS段 */
#else /* !ENABLE_SMP_SUPPORT */
#ifdef CONFIG_KERNEL_SKIM_WINDOW
            "movq (x64KSCurrentUserCR3), %%rsp\n"
            "movq %%rsp, %%cr3\n"
#endif /* CONFIG_KERNEL_SKIM_WINDOW */
            "leaq x64KSIRQStack + 8, %%rsp\n"
#endif /* ENABLE_SMP_SUPPORT */
            "iretq\n"
            :
            : "r"(&cur_thread->tcbArch.tcbContext.registers[RDI])
#if defined(ENABLE_SMP_SUPPORT) && defined(CONFIG_KERNEL_SKIM_WINDOW)
            , "c"(user_cr3)
            , [scratch_offset] "i"(nodeSkimScratchOffset)
#endif
            // Clobber memory so the compiler is forced to complete all stores
            // before running this assembler
            : "memory"
        );
    }
    UNREACHABLE();
}
/*Z 6.3 处理来自用户空间、idle时的中断(例外。之前已做：现场保存在TCB，加载内核栈，参数1向量号，参数2调用号) */
void VISIBLE NORETURN c_x64_handle_interrupt(int irq, int syscall);
void VISIBLE NORETURN c_x64_handle_interrupt(int irq, int syscall)
{
    if (config_set(CONFIG_KERNEL_X86_IBRS_BASIC)) {
        x86_enable_ibrs();
    }/*Z 将中断栈中的现场也保存在TCB中 */
    word_t *irq_stack = x64KSIRQStack[CURRENT_CPU_INDEX()];
    setRegister(NODE_STATE(ksCurThread), Error, irq_stack[0]);
    /* In the case of an interrupt the NextIP and the FaultIP should be the same value,
     * i.e. the address of the instruction the CPU was about to execute before the
     * interrupt. This is the 5th value pushed on by the hardware, so indexing from
     * the bottom is x64KSIRQStack[1] */
    setRegister(NODE_STATE(ksCurThread), NextIP, irq_stack[1]);
    setRegister(NODE_STATE(ksCurThread), FaultIP, irq_stack[1]);
    setRegister(NODE_STATE(ksCurThread), FLAGS, irq_stack[3]);
    setRegister(NODE_STATE(ksCurThread), RSP, irq_stack[4]);
    c_handle_interrupt(irq, syscall);
    UNREACHABLE();
}

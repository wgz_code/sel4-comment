/*Z 建立内核页表：
一级共1页
第511项 ->          二级共1页
0xffffff8000000000  第0-509项 ->                        PML4最后一项 + PDPT头510个巨页项 = 510G线性地址空间，所有物理地址一一线性映射

PPTR_BASE           第510项   ->                        PML4最后一项 + PDPT第510个巨页项 = 1G内核代码空间
低39位随意          0xffffffff80000000
高位全1             KERNEL_BASE
                    低30位随意
                    第30位为0
                    高位全1

                    第511项   ->        三级共1页       PML4最后一项 + PDPT第511个及PD、PT正常页项 = 1G设备内存空间
                    0xffffffffc0000000
                    KDEV_BASE
                    你30位随意
                    第30位为1
                    高位全1
                                        第0项        -> 四级共1页   设备内存空间2M
                                                        第0项       APIC
                                                        第1-m项     I/O APIC
                                                        第m-n项     IOMMU
                                                        第n-511项   置空

如果未配置巨页而是大页，基本与前面类同。
如果配置了TRACE选项，最后一张三级页表的最后一项指向2M的benchmark log空间。
一级页表的第510项及往前，表示TLB(cpu)位图。
*/

/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>
#include <api/syscall.h>
#include <machine/io.h>
#include <kernel/boot.h>
#include <model/statedata.h>
#include <arch/kernel/vspace.h>
#include <arch/kernel/boot.h>
#include <arch/kernel/boot_sys.h>
#include <arch/api/invocation.h>
#include <mode/kernel/tlb.h>
#include <arch/kernel/tlb_bitmap.h>
#include <object/structures.h>

/* When using the SKIM window to isolate the kernel from the user we also need to
 * not use global mappings as having global mappings and entries in the TLB is
 * equivalent, for the purpose of exploitation, to having the mappings in the
 * kernel window */
#define KERNEL_IS_GLOBAL() (config_set(CONFIG_KERNEL_SKIM_WINDOW) ? 0 : 1)

/* For the boot code we create two windows into the physical address space
 * One is at the same location as the kernel window, and is placed up high
 * The other is a 1-to-1 mapping of the first 512gb of memory. The purpose
 * of this is to have a 1-to-1 mapping for the low parts of memory, so that
 * when we switch paging on, and are still running at physical addresses,
 * we don't explode. Then we also want the high mappings so we can start
 * running at proper kernel virtual addresses *//*Z 引导时用的内核一、二级页表 */
pml4e_t boot_pml4[BIT(PML4_INDEX_BITS)] ALIGN(BIT(seL4_PageBits)) VISIBLE PHYS_BSS;
pdpte_t boot_pdpt[BIT(PDPT_INDEX_BITS)] ALIGN(BIT(seL4_PageBits)) VISIBLE PHYS_BSS;

/* 'gdt_idt_ptr' is declared globally because of a C-subset restriction.
 * It is only used in init_drts(), which therefore is non-reentrant.
 */
gdt_idt_ptr_t gdt_idt_ptr;
/*Z 建立静态内核页表，映射所有可能的物理地址(实际不一定都存在)，映射APIC、IOAPIC、IOMMU设备内存，
清空TLB、paging缓存 */
BOOT_CODE bool_t map_kernel_window(
    uint32_t num_ioapic,
    paddr_t   *ioapic_paddrs,
    uint32_t   num_drhu,
    paddr_t   *drhu_list
)
{

    uint64_t paddr;
    uint64_t vaddr;

#ifdef CONFIG_HUGE_PAGE
    /* using 1 GiB page size */

    /* verify that the kernel window as at the last entry of the PML4 */
    assert(GET_PML4_INDEX(PPTR_BASE) == BIT(PML4_INDEX_BITS) - 1);
    /* verify that the kernel_base is located in the last entry of the PML4,
     * the second last entry of the PDPT, is 1gb aligned and 1gb in size */
    assert(GET_PML4_INDEX(KERNEL_ELF_BASE) == BIT(PML4_INDEX_BITS) - 1);
    assert(GET_PDPT_INDEX(KERNEL_ELF_BASE) == BIT(PML4_INDEX_BITS) - 2);/*Z 不好：此项和下一项应使用PDPT_INDEX_BITS */
    assert(GET_PDPT_INDEX(KDEV_BASE) == BIT(PML4_INDEX_BITS) - 1);
    assert(IS_ALIGNED(KERNEL_ELF_BASE - KERNEL_ELF_PADDR_BASE, seL4_HugePageBits));
    assert(IS_ALIGNED(KDEV_BASE, seL4_HugePageBits));
    /* place the PDPT into the PML4 *//*Z 在一级页表最后一项建立到二级页表的映射，可管理顶端512G */
    x64KSKernelPML4[GET_PML4_INDEX(PPTR_BASE)] = pml4e_new(
                                                     0, /* xd */
                                                     kpptr_to_paddr(x64KSKernelPDPT),
                                                     0, /* accessed */
                                                     0, /* cache_disabled */
                                                     0, /* write_through */
                                                     0, /* super_user *//*Z 访问控制：0-特权，1-普通 */
                                                     1, /* read_write *//*Z 读写控制：0-只读，1-读写 */
                                                     1  /* present *//*Z 是否在内存中：0-不在，1-在 */
                                                 );
    /* put the 1GB kernel_base mapping into the PDPT *//*Z 在二级页表倒数第二项建立到物理内存头1G巨页(位7=1)的映射，内核映像所在地 */
    x64KSKernelPDPT[GET_PDPT_INDEX(KERNEL_ELF_BASE)] = pdpte_pdpte_1g_new(
                                                           0, /* xd */
                                                           PADDR_BASE,
                                                           0, /* PAT */
                                                           KERNEL_IS_GLOBAL(), /* global */
                                                           0, /* dirty */
                                                           0, /* accessed */
                                                           0, /* cache_disabled */
                                                           0, /* write_through */
                                                           0, /* super_user */
                                                           1, /* read_write */
                                                           1  /* present */
                                                       );
    /* also map the physical memory into the big kernel window */
    /*Z 在二级页表从表项0到509映射510G可能的所有物理巨页(位7=1)，表项0和510均指向物理头1G */
    paddr = 0;
    vaddr = PPTR_BASE;
    for (paddr = 0; paddr < PADDR_TOP;
         paddr += BIT(seL4_HugePageBits)) {

        int pdpte_index = GET_PDPT_INDEX(vaddr);
        x64KSKernelPDPT[pdpte_index] = pdpte_pdpte_1g_new(
                                           0,          /* xd               */
                                           paddr,      /* physical address */
                                           0,          /* PAT              */
                                           KERNEL_IS_GLOBAL(), /* global   */
                                           0,          /* dirty            */
                                           0,          /* accessed         */
                                           0,          /* cache_disabled   */
                                           0,          /* write_through    */
                                           0,          /* super_user       */
                                           1,          /* read_write       */
                                           1           /* present          */
                                       );

        vaddr += BIT(seL4_HugePageBits);
    }

    /* put the PD into the PDPT *//*Z 在二级页表最后一项建立到三级页表(位7=0)的映射，可管理顶端1G */
    x64KSKernelPDPT[GET_PDPT_INDEX(KDEV_BASE)] = pdpte_pdpte_pd_new(
                                                     0, /* xd */
                                                     kpptr_to_paddr(x64KSKernelPD),
                                                     0, /* accessed */
                                                     0, /* cache_disabled */
                                                     0, /* write_through */
                                                     0, /* super_user */
                                                     1, /* read_write */
                                                     1  /* present */
                                                 );
    /* put the PT into the PD *//*Z 在三级页表第一项建立到四级页表(位7=0)的映射，可管理2M空间 */
    x64KSKernelPD[0] = pde_pde_pt_new(
                           0, /* xd */
                           kpptr_to_paddr(x64KSKernelPT),
                           0, /* accessed */
                           0, /* cache_disabled */
                           0, /* write_through */
                           0, /* super_user */
                           1, /* read_write */
                           1  /* present */
                       );
#else

    int pd_index = 0;
    /* use 2 MiB page size */
    /* verify that the kernel window as at the last entry of the PML4 */
    assert(GET_PML4_INDEX(PPTR_BASE) == BIT(PML4_INDEX_BITS) - 1);/*Z 不好：这些断言不是与前面的一模一样吗 */
    /* verify that the kernel_base is located in the last entry of the PML4,
     * the second last entry of the PDPT, is 1gb aligned and 1gb in size */
    assert(GET_PML4_INDEX(KERNEL_ELF_BASE) == BIT(PML4_INDEX_BITS) - 1);
    assert(GET_PDPT_INDEX(KERNEL_ELF_BASE) == BIT(PML4_INDEX_BITS) - 2);
    assert(GET_PDPT_INDEX(KDEV_BASE) == BIT(PML4_INDEX_BITS) - 1);
    assert(IS_ALIGNED(KERNEL_ELF_BASE - KERNEL_ELF_PADDR_BASE, seL4_HugePageBits));
    assert(IS_ALIGNED(KDEV_BASE, seL4_HugePageBits));

    /* place the PDPT into the PML4 *//*Z 在一级页表最后一项建立到二级页表的映射，可管理顶端512G */
    x64KSKernelPML4[GET_PML4_INDEX(PPTR_BASE)] = pml4e_new(
                                                     0, /* xd */
                                                     kpptr_to_paddr(x64KSKernelPDPT),
                                                     0, /* accessed */
                                                     0, /* cache_disabled */
                                                     0, /* write_through */
                                                     0, /* super_user */
                                                     1, /* read_write */
                                                     1  /* present */
                                                 );
    /*Z 在二级页表从表项0到509映射三级页表(位7=0)，每项可管理1G */
    for (pd_index = 0; pd_index < PADDR_TOP >> seL4_HugePageBits; pd_index++) {
        /* put the 1GB kernel_base mapping into the PDPT *//*Z 不好：这注释是下句的 */
        x64KSKernelPDPT[GET_PDPT_INDEX(PPTR_BASE) + pd_index] = pdpte_pdpte_pd_new(
                                                                    0, /* xd */
                                                                    kpptr_to_paddr(&x64KSKernelPDs[pd_index][0]),
                                                                    0, /* accessed */
                                                                    0, /* cache disabled */
                                                                    0, /* write through */
                                                                    0, /* super user */
                                                                    1, /* read write */
                                                                    1 /* present */
                                                                );
    }
    /*Z 在二级页表第510项映射内核代码空间，与第0项相重 */
    x64KSKernelPDPT[GET_PDPT_INDEX(KERNEL_ELF_BASE)] = pdpte_pdpte_pd_new(
                                                           0, /* xd */
                                                           kpptr_to_paddr(&x64KSKernelPDs[0][0]),
                                                           0, /* accessed */
                                                           0, /* cache disable */
                                                           1, /* write through */
                                                           0, /* super user */
                                                           1, /* read write */
                                                           1  /* present */
                                                       );

    paddr = 0;
    vaddr = PPTR_BASE;
    /*Z 在三级页表所有表、项建立到物理可能的510G内存映射，每页2M */
    for (paddr = 0; paddr < PADDR_TOP;
         paddr += 0x200000) {

        int pd_index = GET_PDPT_INDEX(vaddr) - GET_PDPT_INDEX(PPTR_BASE);
        int pde_index = GET_PD_INDEX(vaddr);

        x64KSKernelPDs[pd_index][pde_index] = pde_pde_large_new(
                                                  0, /* xd */
                                                  paddr,
                                                  0, /* pat */
                                                  KERNEL_IS_GLOBAL(), /* global */
                                                  0, /* dirty */
                                                  0, /* accessed */
                                                  0, /* cache disabled */
                                                  0, /* write through */
                                                  0, /* super user */
                                                  1, /* read write */
                                                  1  /* present */
                                              );
        vaddr += 0x200000;
    }
    /*Z 在二级页表的最后一项建立到最后一张三级页表(设备)的映射 */
    /* put the PD into the PDPT */
    x64KSKernelPDPT[GET_PDPT_INDEX(KDEV_BASE)] = pdpte_pdpte_pd_new(
                                                     0, /* xd */
                                                     kpptr_to_paddr(&x64KSKernelPDs[BIT(PDPT_INDEX_BITS) - 1][0]),
                                                     0, /* accessed */
                                                     0, /* cache_disabled */
                                                     0, /* write_through */
                                                     0, /* super_user */
                                                     1, /* read_write */
                                                     1  /* present */
                                                 );
    /*Z 在三级页表的最后一张的第0项建立到四级页表的映射 */
    /* put the PT into the PD */
    x64KSKernelPDs[BIT(PDPT_INDEX_BITS) - 1][0] = pde_pde_pt_new(
                                                      0, /* xd */
                                                      kpptr_to_paddr(x64KSKernelPT),
                                                      0, /* accessed */
                                                      0, /* cache_disabled */
                                                      0, /* write_through */
                                                      0, /* super_user */
                                                      1, /* read_write */
                                                      1  /* present */
                                                  );
#endif

#if CONFIG_MAX_NUM_TRACE_POINTS > 0
    /* use the last PD entry as the benchmark log storage.
     * the actual backing physical memory will be filled
     * later by using alloc_region *//*Z 三级页表最后一页最后一项所指的最大2M空间(顶端2M)，用于benchmark */
    ksLog = (ks_log_entry_t *)(KDEV_BASE + 0x200000 * (BIT(PD_INDEX_BITS) - 1));
#endif
    /*Z 将APIC、IOAPIC、IOMMU内存区加入保留区，并顺序加入到四级页表中 */
    /* now map in the kernel devices */
    if (!map_kernel_window_devices(x64KSKernelPT, num_ioapic, ioapic_paddrs, num_drhu, drhu_list)) {
        return false;
    }

#ifdef ENABLE_SMP_SUPPORT
    /* initialize the TLB bitmap *//*Z 一级页表倒数若干项（除最后一项）全置0 */
    tlb_bitmap_init(x64KSKernelPML4);
#endif /* ENABLE_SMP_SUPPORT */
    /*Z 清空所有的TLB和paging缓存，为进入用户空间做准备 */
    /* In boot code, so fine to just trash everything here */
    invalidateLocalTranslationAll();
    printf("Mapping kernel window is done\n");
    return true;
}

#ifdef CONFIG_KERNEL_SKIM_WINDOW
/*Z 将给定的地址区域视为内核滑动窗口，建立专门的页表（2M页），特权权限 */
BOOT_CODE bool_t map_skim_window(vptr_t skim_start, vptr_t skim_end)
{
    /* place the PDPT into the PML4 *//*Z 一级页表第511项指向二级页表 */
    x64KSSKIMPML4[GET_PML4_INDEX(PPTR_BASE)] = pml4e_new(
                                                   0, /* xd */
                                                   kpptr_to_paddr(x64KSSKIMPDPT),
                                                   0, /* accessed */
                                                   0, /* cache_disabled */
                                                   0, /* write_through */
                                                   0, /* super_user */
                                                   1, /* read_write */
                                                   1  /* present */
                                               );
    /* place the PD into the kernel_base slot of the PDPT *//*Z 二级页表第510项指向三级页表 */
    x64KSSKIMPDPT[GET_PDPT_INDEX(KERNEL_ELF_BASE)] = pdpte_pdpte_pd_new(
                                                         0, /* xd */
                                                         kpptr_to_paddr(x64KSSKIMPD),
                                                         0, /* accessed */
                                                         0, /* cache_disabled */
                                                         0, /* write_through */
                                                         0, /* super_user */
                                                         1, /* read_write */
                                                         1  /* present */
                                                     );
    /* map the skim portion into the PD. we expect it to be 2M aligned */
    assert((skim_start % BIT(seL4_LargePageBits)) == 0);
    assert((skim_end % BIT(seL4_LargePageBits)) == 0);
    uint64_t paddr = kpptr_to_paddr((void *)skim_start);/*Z 三级页表项指向给定的地址区域页（2M）*/
    for (int i = GET_PD_INDEX(skim_start); i < GET_PD_INDEX(skim_end); i++) {
        x64KSSKIMPD[i] = pde_pde_large_new(
                             0, /* xd */
                             paddr,
                             0, /* pat */
                             KERNEL_IS_GLOBAL(), /* global */
                             0, /* dirty */
                             0, /* accessed */
                             0, /* cache_disabled */
                             0, /* write_through */
                             0, /* super_user */
                             1, /* read_write */
                             1  /* present */
                         );
        paddr += BIT(seL4_LargePageBits);
    }
    return true;
}
#endif
/*Z 初始化内核TSS段内容，设置IDT可索引的内核中断栈，禁用所有低权限I/O端口操作 */
BOOT_CODE void init_tss(tss_t *tss)
{   /*Z 中断栈顶，上面还空了2个机器字 */
    word_t base = (word_t)&x64KSIRQStack[CURRENT_CPU_INDEX()][IRQ_STACK_SIZE];
    *tss = tss_new(
               sizeof(*tss),   /* io map base *//*Z I/O端口许可位图相对于TSS的偏移量 */
               0, 0,       /* ist 7 *//*Z interrupt stack table (IST) pointers */
               0, 0,
               0, 0,
               0, 0,
               0, 0,
               0, 0,
               /* ist 1 is the stack frame we use for interrupts */
               base >> 32, base & 0xffffffff,  /* ist 1 *//*Z IDT中的向量可以索引IST，从而使用其所指栈 */
               0, 0,       /* rsp 2 */
               0, 0,       /* rsp 1 */
               0, 0        /* rsp 0 */
           );
    /* set the IO map to all 1 to block user IN/OUT instructions */
    memset(&x86KSGlobalState[CURRENT_CPU_INDEX()].x86KStss.io_map[0], 0xff,
           sizeof(x86KSGlobalState[CURRENT_CPU_INDEX()].x86KStss.io_map));
}
/*Z 初始化syscall/sysret指令有关MSR寄存器 */
BOOT_CODE void init_syscall_msrs(void)
{   /*Z RIP：写入系统调用入口地址 */
    x86_wrmsr(IA32_LSTAR_MSR, (uint64_t)&handle_fastsyscall);
    // mask bit 9 in the kernel (which is the interrupt enable bit)
    // also mask bit 8, which is the Trap Flag, to prevent the kernel
    // from single stepping     /*Z syscall后清中断和单步标志 */
    x86_wrmsr(IA32_FMASK_MSR, FLAGS_TF | FLAGS_IF);
    x86_wrmsr(IA32_STAR_MSR, ((uint64_t)SEL_CS_0 << 32) | ((uint64_t)SEL_CS_3 << 48));
}
/*Z 初始化gdt指向的GDT表，包含tss指向的TSS段描述符 */
BOOT_CODE void init_gdt(gdt_entry_t *gdt, tss_t *tss)
{

    uint64_t tss_base = (uint64_t)tss;
    gdt_tss_t gdt_tss;
    /*Z 第0项全0未用 */
    gdt[GDT_NULL] = gdt_entry_gdt_null_new();
    /*Z 第1项特权级代码段，起始地址0 */
    gdt[GDT_CS_0] = gdt_entry_gdt_code_new(
                        0,                  /* base high *//*Z 段地址-高8位 */
                        1,                  /* granularity *//*Z 1-段长单位4K，0-字节 */
                        0,                  /* operation size, must be 0 when 64-bit is set */
                        1,                  /* long mode *//*Z 1-64位代码段 */
                        0,                  /* avl */
                        0xf,                /* limit high *//*Z 段长-高4位 */
                        1,                  /* present *//*Z 1-在内存，0-不在 */
                        0,                  /* dpl *//*Z 特权等级要求，0-3 */
                        1,                  /* always 1 for segment *//*Z 1-代码/数据段，0-系统段 */
                        0,                  /* base middle *//*Z 段地址-中8位 */
                        0,                  /* base low *//*Z 段地址-低16位 */
                        0xffff              /* limit low *//*Z 段长-低16位 */
                    );
    /*Z 第2项特权级数据段，起始地址0 */
    gdt[GDT_DS_0] = gdt_entry_gdt_data_new(
                        0,                  /* base high */
                        1,                  /* granularity */
                        1,                  /* operation size */
                        0,                  /* avl */
                        0xf,                /* seg limit high */
                        1,                  /* present */
                        0,                  /* dpl */
                        1,                  /* always 1 */
                        0,                  /* base mid */
                        0,                  /* base low */
                        0xffff              /* seg limit low */
                    );
    /*Z 第5项普通级代码段，起始地址0 */
    gdt[GDT_CS_3] = gdt_entry_gdt_code_new(
                        0,                  /* base high */
                        1,                  /* granularity */
                        0,                  /* operation size, must be 0 when 64-bit is set */
                        1,                  /* long mode */
                        0,                  /* avl */
                        0xf,                /* limit high */
                        1,                  /* present */
                        3,                  /* dpl */
                        1,                  /* always 1 */
                        0,                  /* base middle */
                        0,                  /* base low */
                        0xffff              /* limit low */
                    );
    /*Z 第6项普通级数据段，起始地址0 */
    gdt[GDT_DS_3] = gdt_entry_gdt_data_new(
                        0,
                        1,
                        1,
                        0,
                        0xf,
                        1,
                        3,
                        1,
                        0,
                        0,
                        0xffff
                    );
    /*Z 第7项普通级数据段（FS），起始地址0 */
    gdt[GDT_FS] = gdt_entry_gdt_data_new(
                      0,
                      1,
                      1,
                      0,
                      0xf,
                      1,
                      3,
                      1,
                      0,
                      0,
                      0xffff
                  );
    /*Z 第8项普通级数据段（FS），起始地址0 */
    gdt[GDT_GS] = gdt_entry_gdt_data_new(
                      0,
                      1,
                      1,
                      0,
                      0xf,
                      1,
                      3,
                      1,
                      0,
                      0,
                      0xffff
                  );
    /*Z 第3项特权级TSS段（16字节），起始地址0 */
    gdt_tss = gdt_tss_new(
                  tss_base >> 32,                     /* base 63 - 32 */
                  (tss_base & 0xff000000UL) >> 24,    /* base 31 - 24 */
                  1,                                  /* granularity */
                  0,                                  /* avl */
                  0,                                  /* limit high */
                  1,                                  /* present */
                  0,                                  /* dpl */
                  9,                                  /* desc type *//*Z TSS段 */
                  (tss_base & 0xff0000UL) >> 16,      /* base 23-16 */
                  (tss_base & 0xffffUL),              /* base 15 - 0 */
                  sizeof(tss_io_t) - 1                  /*Z 段长-低16位（含） */
              );

    gdt[GDT_TSS].words[0] = gdt_tss.words[0];
    gdt[GDT_TSS + 1].words[0] = gdt_tss.words[1];
}
/*Z 将handler作为interrupt的中断向量，填入到IDT表中 */
BOOT_CODE void init_idt_entry(idt_entry_t *idt, interrupt_t interrupt, void(*handler)(void))
{
    uint64_t handler_addr = (uint64_t)handler;
    uint64_t dpl = 3;/*Z 为3时所有特权等级空间都可以通过INT n、INT3、INTO指令触发中断 */

    if (interrupt < int_trap_min && interrupt != int_software_break_request) {
        dpl = 0;    /*Z 为0时只在特权等级为0的内核能软件触发 */
    }
    /*Z seL4没有区分trap gate，都当成自动清中断标志的interrupt gate */
    idt[interrupt] = idt_entry_interrupt_gate_new(
                         handler_addr >> 32,                 /* offset 63 - 32 *//*Z handler地址高32位 */
                         ((handler_addr >> 16) & 0xffff),                       /*Z handler地址中16位 */
                         1,                                  /* present *//*Z 1-在内存 */
                         dpl,                                /* dpl *//*Z 特权等级 */
                         1,                                  /* ist *//*Z TSS中IST索引-中断栈表索引 */
                         SEL_CS_0,                           /* segment selector *//*Z 代码段选择子 */
                         (handler_addr & 0xffff)               /* offset 15 - 0 *//*Z handler地址低16位 */
                     );
}
/*Z 把线程的页表设置为当前页表 */
void setVMRoot(tcb_t *tcb)
{
    cap_t threadRoot;
    asid_t asid;
    pml4e_t *pml4;
    findVSpaceForASID_ret_t find_ret;
    cr3_t cr3;
    /*Z TCB VSpace能力 */
    threadRoot = TCB_PTR_CTE_PTR(tcb, tcbVTable)->cap;
    /*Z 无VSpace能力或未映射的，设置内核页表为当前页表。这种情况肯定不是用户空间线程 */
    if (cap_get_capType(threadRoot) != cap_pml4_cap ||
        !cap_pml4_cap_get_capPML4IsMapped(threadRoot)) {/*Z 设置内核页表为当前页表 */
        setCurrentUserVSpaceRoot(kpptr_to_paddr(X86_GLOBAL_VSPACE_ROOT), 0);
        return;
    }
    /*Z 有VSpace能力但与ASID(PCID)映射不符的，设置内核页表为当前页表。这是什么情况???? */
    pml4 = PML4E_PTR(cap_pml4_cap_get_capPML4BasePtr(threadRoot));
    asid = cap_pml4_cap_get_capPML4MappedASID(threadRoot);
    find_ret = findVSpaceForASID(asid);/*Z 在pool中查找ASID(PCID)对应的一级页表线性地址 */
    if (unlikely(find_ret.status != EXCEPTION_NONE || find_ret.vspace_root != pml4)) {
        setCurrentUserVSpaceRoot(kpptr_to_paddr(X86_GLOBAL_VSPACE_ROOT), 0);
        return;
    }
    cr3 = makeCR3(pptr_to_paddr(pml4), asid);
    if (getCurrentUserCR3().words[0] != cr3.words[0]) {
        SMP_COND_STATEMENT(tlb_bitmap_set(pml4, getCurrentCPUIndex());)/*Z 置位cpu的TLB位图 */
        setCurrentUserCR3(cr3);
    }
}

/*Z 加载内存管理寄存器GDTR、IDTR、LDTR、TR */
BOOT_CODE void init_dtrs(void)
{
    gdt_idt_ptr.limit = (sizeof(gdt_entry_t) * GDT_ENTRIES) - 1;/*Z GDT表大小 */
    gdt_idt_ptr.base = (uint64_t)x86KSGlobalState[CURRENT_CPU_INDEX()].x86KSgdt;/*Z GDT表基地址 */

    /* When we install the gdt it will clobber any value of gs that
     * we have. Since we might be using it for TLS we can stash
     * and unstash any gs value using swapgs
     *//*Z 加载GDTR寄存器 */
    swapgs();
    x64_install_gdt(&gdt_idt_ptr);
    swapgs();

    gdt_idt_ptr.limit = (sizeof(idt_entry_t) * (int_max + 1)) - 1;/*Z IDT表大小 */
    gdt_idt_ptr.base = (uint64_t)x86KSGlobalState[CURRENT_CPU_INDEX()].x86KSidt;/*Z IDT表基地址 */
    x64_install_idt(&gdt_idt_ptr);/*Z 加载IDTR寄存器 */
    /*Z 禁用LDT */
    x64_install_ldt(SEL_NULL);
    /*Z 加载TR寄存器 */
    x64_install_tss(SEL_TSS);
}
/*Z 在pd_cap所指的页表层级中，为frame_cap所指的物理页，建立普通权限的页表项 */
BOOT_CODE void map_it_frame_cap(cap_t pd_cap, cap_t frame_cap)
{
    pml4e_t *pml4 = PML4_PTR(pptr_of_cap(pd_cap));/*Z 一级页表起始地址 */
    pdpte_t *pdpt;
    pde_t *pd;
    pte_t *pt;
    vptr_t vptr = cap_frame_cap_get_capFMappedAddress(frame_cap);/*Z 要映射到的虚拟地址 */
    void *pptr = (void *)cap_frame_cap_get_capFBasePtr(frame_cap);/*Z 物理线性地址 */

    assert(cap_frame_cap_get_capFMapType(frame_cap) == X86_MappingVSpace);
    assert(cap_frame_cap_get_capFMappedASID(frame_cap) != asidInvalid);
    pml4 += GET_PML4_INDEX(vptr);/*Z 对应的一级页表项 */
    assert(pml4e_ptr_get_present(pml4));
    pdpt = paddr_to_pptr(pml4e_ptr_get_pdpt_base_address(pml4));/*Z 不好：生成的代码与的是0x7fffffffff000，51位 */
    pdpt += GET_PDPT_INDEX(vptr);/*Z 对应的二级页表项 */
    assert(pdpte_pdpte_pd_ptr_get_present(pdpt));
    pd = paddr_to_pptr(pdpte_pdpte_pd_ptr_get_pd_base_address(pdpt));/*Z 不好：生成的代码与的是0x7fffffffff000，51位 */
    pd += GET_PD_INDEX(vptr);/*Z 对应的三级页表项 */
    assert(pde_pde_pt_ptr_get_present(pd));
    pt = paddr_to_pptr(pde_pde_pt_ptr_get_pt_base_address(pd));/*Z 不好：生成的代码与的是0x7fffffffff000，51位 */
    *(pt + GET_PT_INDEX(vptr)) = pte_new(
                                     0,                      /* xd                   */
                                     pptr_to_paddr(pptr),    /* page_base_address    */
                                     0,                      /* global               */
                                     0,                      /* pat                  */
                                     0,                      /* dirty                */
                                     0,                      /* accessed             */
                                     0,                      /* cache_disabled       */
                                     0,                      /* write_through        */
                                     1,                      /* super_user           *//*Z 用户空间可访问 */
                                     1,                      /* read_write           */
                                     1                       /* present              */
                                 );
}
/*Z 在vspace_cap所指的一级页表中，为pdpt_cap所指的二级页表，建立普通权限的页表项 */
static BOOT_CODE void map_it_pdpt_cap(cap_t vspace_cap, cap_t pdpt_cap)
{
    pml4e_t *pml4 = PML4_PTR(pptr_of_cap(vspace_cap));/*Z 一级页表 */
    pdpte_t *pdpt = PDPT_PTR(cap_pdpt_cap_get_capPDPTBasePtr(pdpt_cap));/*Z 二级页表 */
    vptr_t vptr = cap_pdpt_cap_get_capPDPTMappedAddress(pdpt_cap);/*Z 虚拟地址首地址 */

    assert(cap_pdpt_cap_get_capPDPTIsMapped(pdpt_cap));
    *(pml4 + GET_PML4_INDEX(vptr)) = pml4e_new(
                                         0,                      /* xd                   */
                                         pptr_to_paddr(pdpt),    /* pdpt_base_address    */
                                         0,                      /* accessed             */
                                         0,                      /* cache_disabled       */
                                         0,                      /* write_through        */
                                         1,                      /* super_user           *//*Z 用户空间可访问 */
                                         1,                      /* read_write           */
                                         1                       /* present              */
                                     );
}
/*Z 在vspace_cap所指的页表层级中，为pd_cap所指的三级页表，建立普通权限的页表项 */
BOOT_CODE void map_it_pd_cap(cap_t vspace_cap, cap_t pd_cap)
{
    pml4e_t *pml4 = PML4_PTR(pptr_of_cap(vspace_cap));
    pdpte_t *pdpt;
    pde_t *pd = PD_PTR(cap_page_directory_cap_get_capPDBasePtr(pd_cap));
    vptr_t vptr = cap_page_directory_cap_get_capPDMappedAddress(pd_cap);

    assert(cap_page_directory_cap_get_capPDIsMapped(pd_cap));
    pml4 += GET_PML4_INDEX(vptr);
    assert(pml4e_ptr_get_present(pml4));
    pdpt = paddr_to_pptr(pml4e_ptr_get_pdpt_base_address(pml4));/*Z 不好：生成的代码与的是0x7fffffffff000，51位 */
    *(pdpt + GET_PDPT_INDEX(vptr)) = pdpte_pdpte_pd_new(
                                         0,                      /* xd                   */
                                         pptr_to_paddr(pd),      /* pd_base_address      */
                                         0,                      /* accessed             */
                                         0,                      /* cache_disabled       */
                                         0,                      /* write_through        */
                                         1,                      /* super_user           *//*Z 用户空间可访问 */
                                         1,                      /* read_write           */
                                         1                       /* present              */
                                     );
}
/*Z 在vspace_cap所指的页表层级中，为pt_cap所指的四级页表，建立普通权限的页表项 */
BOOT_CODE void map_it_pt_cap(cap_t vspace_cap, cap_t pt_cap)
{
    pml4e_t *pml4 = PML4_PTR(pptr_of_cap(vspace_cap));
    pdpte_t *pdpt;
    pde_t *pd;
    pte_t *pt = PT_PTR(cap_page_table_cap_get_capPTBasePtr(pt_cap));
    vptr_t vptr = cap_page_table_cap_get_capPTMappedAddress(pt_cap);

    assert(cap_page_table_cap_get_capPTIsMapped(pt_cap));
    pml4 += GET_PML4_INDEX(vptr);
    assert(pml4e_ptr_get_present(pml4));
    pdpt = paddr_to_pptr(pml4e_ptr_get_pdpt_base_address(pml4));
    pdpt += GET_PDPT_INDEX(vptr);
    assert(pdpte_pdpte_pd_ptr_get_present(pdpt));
    pd = paddr_to_pptr(pdpte_pdpte_pd_ptr_get_pd_base_address(pdpt));/*Z 不好：生成的代码与的是0x7fffffffff000，51位 */
    *(pd + GET_PD_INDEX(vptr)) = pde_pde_pt_new(
                                     0,                      /* xd                   */
                                     pptr_to_paddr(pt),      /* pt_base_address      */
                                     0,                      /* accessed             */
                                     0,                      /* cache_disabled       */
                                     0,                      /* write_through        */
                                     1,                      /* super_user           *//*Z 用户空间可访问 */
                                     1,                      /* read_write           */
                                     1                       /* present              */
                                 );
}
/*Z 对64位系统，4G以下的物理内存已映射入内核虚拟地址空间（顶端512G）*/
BOOT_CODE void *map_temp_boot_page(void *entry, uint32_t large_pages)
{
    /* this function is for legacy 32-bit systems where the ACPI tables might
     * collide with the kernel window. Here we just assert that the table is
     * in fact in the lower 4GiB region (which is already 1-to-1 mapped) and
     * continue */
    assert((word_t)entry < BIT(32));    /*Z 不好：内容要是超过了4G边界呢 */
    return entry;
}
/*Z 为虚拟地址vptr对应的二级页表pptr创建访问能力，并做页表映射 */
static BOOT_CODE cap_t create_it_pdpt_cap(cap_t vspace_cap, pptr_t pptr, vptr_t vptr, asid_t asid)
{
    cap_t cap;
    cap = cap_pdpt_cap_new(/*Z 不好：生成的代码中，vptr保留了高10位而不是9位 */
              asid,   /* capPDPTMappedASID    */
              pptr,   /* capPDPTBasePtr       */
              1,      /* capPDPTIsMapped      */
              vptr    /* capPDPTMappedAddress */
          );
    map_it_pdpt_cap(vspace_cap, cap);/*Z 在一级页表中为二级页表建立普通权限的页表项 */
    return cap;
}
/*Z 为虚拟地址vptr对应的三级页表pptr创建访问能力，并做页表映射 */
static BOOT_CODE cap_t create_it_pd_cap(cap_t vspace_cap, pptr_t pptr, vptr_t vptr, asid_t asid)
{
    cap_t cap;
    cap = cap_page_directory_cap_new(/*Z 不好：生成的代码中，vptr保留了高19位而不是18位 */
              asid,   /* capPDMappedASID      */
              pptr,   /* capPDBasePtr         */
              1,      /* capPDIsMapped        */
              vptr    /* capPDMappedAddress   */
          );
    map_it_pd_cap(vspace_cap, cap);
    return cap;
}
/*Z 为虚拟地址vptr对应的四级页表pptr创建访问能力，并做页表映射 */
static BOOT_CODE cap_t create_it_pt_cap(cap_t vspace_cap, pptr_t pptr, vptr_t vptr, asid_t asid)
{
    cap_t cap;
    cap = cap_page_table_cap_new(/*Z 不好：生成的代码中，vptr保留了高28位而不是27位 */
              asid,   /* capPTMappedASID      */
              pptr,   /* capPTBasePtr         */
              1,      /* capPTIsMapped        */
              vptr    /* capPTMappedAddress   */
          );
    map_it_pt_cap(vspace_cap, cap);
    return cap;
}

/*Z 区域需要的页表页总数（除一级页表页）+ 老旧设备DMA保留区占用的IOMMU页表。注意：每次调用都包含IOMMU部分 */
BOOT_CODE word_t arch_get_n_paging(v_region_t it_v_reg)
{
    word_t n = get_n_paging(it_v_reg, PD_INDEX_OFFSET); /*Z 该区域需要的三级页表条目数(四级页表页数) */
    n += get_n_paging(it_v_reg, PDPT_INDEX_OFFSET);     /*Z 该区域需要的二级页表条目数(三级页表页数) */
    n += get_n_paging(it_v_reg, PML4_INDEX_OFFSET);     /*Z 该区域需要的一级页表条目数(二级页表页数) */
#ifdef CONFIG_IOMMU
    n += vtd_get_n_paging(&boot_state.rmrr_list);       /*Z 老旧设备DMA保留区占用的IOMMU页表页数 */
#endif
    return n;
}
/*Z 根据传入的映像区域，构建rootserver的完整页表及其访问能力，返回根VSpace能力 */
BOOT_CODE cap_t create_it_address_space(cap_t root_cnode_cap, v_region_t it_v_reg)
{
    cap_t      vspace_cap;
    vptr_t     vptr;
    seL4_SlotPos slot_pos_before;
    seL4_SlotPos slot_pos_after;
    /*Z 将内核滑动窗口的一级页表(包含内核的.text、.rodata、.skim_data、.skim_bss段内容，
    及可能配置的TLB位图表项)拷贝给rootserver的根VSpace。内核(特权）可访问，initrd(普通)不能访问 */
    slot_pos_before = ndks_boot.slot_pos_cur;
    copyGlobalMappings(PML4_PTR(rootserver.vspace));
    vspace_cap = cap_pml4_cap_new(
                     IT_ASID,        /* capPML4MappedASID */
                     rootserver.vspace,           /* capPML4BasePtr   */
                     1               /* capPML4IsMapped   */
                 );

    /*Z 赋予rootserver的VSapce(一级页表)访问能力 */
    write_slot(SLOT_PTR(pptr_of_cap(root_cnode_cap), seL4_CapInitThreadVSpace), vspace_cap);
    /*Z 赋予rootserver二级页表访问能力(写入CNode的空闲CSlot中)，并分配建立相应的一级页表项 */
    /* Create any PDPTs needed for the user land image */
    for (vptr = ROUND_DOWN(it_v_reg.start, PML4_INDEX_OFFSET);
         vptr < it_v_reg.end;
         vptr += BIT(PML4_INDEX_OFFSET)) {
        if (!provide_cap(root_cnode_cap,/*Z 为虚拟地址vptr对应的二级页表pptr创建访问能力，并做页表映射 */
                         create_it_pdpt_cap(vspace_cap, it_alloc_paging(), vptr, IT_ASID))
           ) {
            return cap_null_cap_new();/*Z CNode空间不足 */
        }
    }
    /*Z 赋予rootserver三级页表访问能力(写入CNode的空闲CSlot中)，并分配建立相应的二级页表项 */
    /* Create any PDs needed for the user land image */
    for (vptr = ROUND_DOWN(it_v_reg.start, PDPT_INDEX_OFFSET);
         vptr < it_v_reg.end;
         vptr += BIT(PDPT_INDEX_OFFSET)) {
        if (!provide_cap(root_cnode_cap,
                         create_it_pd_cap(vspace_cap, it_alloc_paging(), vptr, IT_ASID))
           ) {
            return cap_null_cap_new();
        }
    }
    /*Z 赋予rootserver四级页表访问能力(写入CNode的空闲CSlot中)，并分配建立相应的三级页表项 */
    /* Create any PTs needed for the user land image */
    for (vptr = ROUND_DOWN(it_v_reg.start, PD_INDEX_OFFSET);
         vptr < it_v_reg.end;
         vptr += BIT(PD_INDEX_OFFSET)) {
        if (!provide_cap(root_cnode_cap,
                         create_it_pt_cap(vspace_cap, it_alloc_paging(), vptr, IT_ASID))
           ) {
            return cap_null_cap_new();
        }
    }

    slot_pos_after = ndks_boot.slot_pos_cur;
    ndks_boot.bi_frame->userImagePaging = (seL4_SlotRegion) {
        slot_pos_before, slot_pos_after
    };
    return vspace_cap;
}
/*Z 将内核(滑动窗口)的一级页表(包含内核的.text、.rodata、.skim_data、.skim_bss段内容，
及可能配置的TLB位图表项)拷贝给传入的参数 */
void copyGlobalMappings(vspace_root_t *new_vspace)
{
    unsigned long i;
    pml4e_t *vspace = (pml4e_t *)new_vspace;

    /* Copy from the tlbbitmap_pptr so that we copy the default entries of the
     * tlb bitmap (if it exists). If it doesn't exist then this loop
     * will be equivalent to copying from PPTR_BASE
     */
    for (i = GET_PML4_INDEX(TLBBITMAP_PPTR); i < BIT(PML4_INDEX_BITS); i++) {
        vspace[i] = X86_GLOBAL_VSPACE_ROOT[i];
    }
}
/*Z 创建物理线性地址映射到虚拟地址的能力 */
static BOOT_CODE cap_t create_it_frame_cap(pptr_t pptr, vptr_t vptr, asid_t asid, bool_t use_large, seL4_Word map_type)
{                                                                       /*Z 不好：没用其定义的vm_page_map_type_t类型 */
    vm_page_size_t frame_size;

    if (use_large) {
        frame_size = X86_LargePage;
    } else {
        frame_size = X86_SmallPage;
    }

    return
        cap_frame_cap_new(
            asid,                          /* capFMappedASID     */
            pptr,                          /* capFBasePtr        */
            frame_size,                    /* capFSize           */
            map_type,                      /* capFMapType        */
            vptr,                          /* capFMappedAddress  */
            wordFromVMRights(VMReadWrite), /* capFVMRights       */
            0                              /* capFIsDevice       */
        );
}
/*Z 为物理页创建未映射的能力 */
BOOT_CODE cap_t create_unmapped_it_frame_cap(pptr_t pptr, bool_t use_large)
{
    return create_it_frame_cap(pptr, 0, asidInvalid, use_large, X86_MappingNone);
}
/*Z 为物理页创建映射能力，建立普通权限的页表项 */
BOOT_CODE cap_t create_mapped_it_frame_cap(cap_t vspace_cap, pptr_t pptr, vptr_t vptr, asid_t asid, bool_t use_large,
                                           bool_t executable UNUSED)
{   /*Z 创建物理线性地址映射到虚拟地址的能力 */
    cap_t cap = create_it_frame_cap(pptr, vptr, asid, use_large, X86_MappingVSpace);
    map_it_frame_cap(vspace_cap, cap);/*Z 在pd_cap所指的页表层级中，为frame_cap所指的物理页，建立普通权限的页表项 */
    return cap;
}

/* ====================== BOOT CODE FINISHES HERE ======================== */


/*Z 将ASID赋予(EPT)一级页表，设置相应的ASID pool项 */
exception_t performASIDPoolInvocation(asid_t asid, asid_pool_t *poolPtr, cte_t *vspaceCapSlot)
{
    asid_map_t asid_map;
#ifdef CONFIG_VTX
    if (cap_get_capType(vspaceCapSlot->cap) == cap_ept_pml4_cap) {
        cap_ept_pml4_cap_ptr_set_capPML4MappedASID(&vspaceCapSlot->cap, asid);
        cap_ept_pml4_cap_ptr_set_capPML4IsMapped(&vspaceCapSlot->cap, 1);
        asid_map = asid_map_asid_map_ept_new(cap_ept_pml4_cap_get_capPML4BasePtr(vspaceCapSlot->cap));
    } else
#endif
    {
        assert(cap_get_capType(vspaceCapSlot->cap) == cap_pml4_cap);
        cap_pml4_cap_ptr_set_capPML4MappedASID(&vspaceCapSlot->cap, asid);
        cap_pml4_cap_ptr_set_capPML4IsMapped(&vspaceCapSlot->cap, 1);
        asid_map = asid_map_asid_map_vspace_new(cap_pml4_cap_get_capPML4BasePtr(vspaceCapSlot->cap));
    }
    poolPtr->array[asid & MASK(asidLowBits)] = asid_map;
    return EXCEPTION_NONE;
}
/*Z 能力是否一级页表访问能力 */
bool_t CONST isVTableRoot(cap_t cap)
{
    return cap_get_capType(cap) == cap_pml4_cap;
}
/*Z 能力是否一级页表访问能力且已映射(有效) */
bool_t CONST isValidNativeRoot(cap_t cap)
{
    return isVTableRoot(cap) &&
           cap_pml4_cap_get_capPML4IsMapped(cap);
}
/*Z 新建一个一级页表项数据 */
static pml4e_t CONST makeUserPML4E(paddr_t paddr, vm_attributes_t vm_attr)
{
    return pml4e_new(
               0,
               paddr,
               0,
               vm_attributes_get_x86PCDBit(vm_attr),
               vm_attributes_get_x86PWTBit(vm_attr),
               1,
               1,
               1
           );
}
/*Z 创建一个全0的页表项 */
static pml4e_t CONST makeUserPML4EInvalid(void)
{
    return pml4e_new(
               0,                  /* xd               */
               0,                  /* pdpt_base_addr   */
               0,                  /* accessed         */
               0,                  /* cache_disabled   */
               0,                  /* write through    */
               0,                  /* super user       */
               0,                  /* read_write       */
               0                   /* present          */
           );
}
/*Z 新建一个巨页二级页表项数据 */
static pdpte_t CONST makeUserPDPTEHugePage(paddr_t paddr, vm_attributes_t vm_attr, vm_rights_t vm_rights)
{
    return pdpte_pdpte_1g_new(
               0,          /* xd               */
               paddr,      /* physical address */
               0,          /* PAT              */
               0,          /* global           */
               0,          /* dirty            */
               0,          /* accessed         */
               vm_attributes_get_x86PCDBit(vm_attr),  /* cache disabled */
               vm_attributes_get_x86PWTBit(vm_attr),  /* write through  */
               SuperUserFromVMRights(vm_rights),       /* super user     */
               WritableFromVMRights(vm_rights),        /* read write     */
               1                                       /* present        */
           );
}
/*Z 新建一个指代三级页表的二级页表项数据 */
static pdpte_t CONST makeUserPDPTEPageDirectory(paddr_t paddr, vm_attributes_t vm_attr)
{
    return pdpte_pdpte_pd_new(
               0,                      /* xd       */
               paddr,                  /* paddr    */
               0,                      /* accessed */
               vm_attributes_get_x86PCDBit(vm_attr),  /* cache disabled */
               vm_attributes_get_x86PWTBit(vm_attr),  /* write through  */
               1,                      /* super user */
               1,                      /* read write */
               1                       /* present    */
           );
}
/*Z 生成一个全0二级页表项 */
static pdpte_t CONST makeUserPDPTEInvalid(void)
{
    return pdpte_pdpte_pd_new(
               0,          /* xd               */
               0,          /* physical address */
               0,          /* accessed         */
               0,          /* cache disabled */
               0,          /* write through  */
               0,          /* super user     */
               0,          /* read write     */
               0           /* present        */
           );
}
/*Z 新建一个大页三级页表项数据 */
pde_t CONST makeUserPDELargePage(paddr_t paddr, vm_attributes_t vm_attr, vm_rights_t vm_rights)
{
    return pde_pde_large_new(
               0,                                              /* xd                   */
               paddr,                                          /* page_base_address    */
               vm_attributes_get_x86PATBit(vm_attr),          /* pat                  */
               0,                                              /* global               */
               0,                                              /* dirty                */
               0,                                              /* accessed             */
               vm_attributes_get_x86PCDBit(vm_attr),          /* cache_disabled       */
               vm_attributes_get_x86PWTBit(vm_attr),          /* write_through        */
               SuperUserFromVMRights(vm_rights),               /* super_user           */
               WritableFromVMRights(vm_rights),                /* read_write           */
               1                                               /* present              */
           );
}
/*Z 新建一个指代四级页表的三级页表项数据 */
pde_t CONST makeUserPDEPageTable(paddr_t paddr, vm_attributes_t vm_attr)
{

    return  pde_pde_pt_new(
                0,                                  /* xd               */
                paddr,                              /* pt_base_address  */
                0,                                  /* accessed         */
                vm_attributes_get_x86PCDBit(vm_attr), /* cache_disabled   */
                vm_attributes_get_x86PWTBit(vm_attr), /* write_through    */
                1,                                  /* super_user       *//*Z 1-用户权限 */
                1,                                  /* read_write       *//*Z 1-允许写 */
                1                                   /* present          */
            );
}
/*Z 生成一个全0三级页表项 */
pde_t CONST makeUserPDEInvalid(void)
{
    /* The bitfield only declares two kinds of PDE entries (page tables or large pages)
     * and an invalid entry should really be a third type, but we can simulate it by
     * creating an invalid (present bit 0) entry of either of the defined types */
    return pde_pde_pt_new(
               0,      /* xd               */
               0,      /* pt_base_addr     */
               0,      /* accessed         */
               0,      /* cache_disabled   */
               0,      /* write_through    */
               0,      /* super_user       */
               0,      /* read_write       */
               0       /* present          */
           );
}
/*Z 新建一个四级页表项数据 */
pte_t CONST makeUserPTE(paddr_t paddr, vm_attributes_t vm_attr, vm_rights_t vm_rights)
{
    return pte_new(
               0,                                              /* xd                   */
               paddr,                                          /* page_base_address    */
               0,                                              /* global               */
               vm_attributes_get_x86PATBit(vm_attr),          /* pat                  */
               0,                                              /* dirty                */
               0,                                              /* accessed             */
               vm_attributes_get_x86PCDBit(vm_attr),          /* cache_disabled       */
               vm_attributes_get_x86PWTBit(vm_attr),          /* write_through        */
               SuperUserFromVMRights(vm_rights),               /* super_user           */
               WritableFromVMRights(vm_rights),                /* read_write           */
               1                                               /* present              */
           );
}
/*Z 新建一个全0的页表项数据 */
pte_t CONST makeUserPTEInvalid(void)
{
    return pte_new(
               0,                   /* xd                   */
               0,                   /* page_base_address    */
               0,                   /* global               */
               0,                   /* pat                  */
               0,                   /* dirty                */
               0,                   /* accessed             */
               0,                   /* cache_disabled       */
               0,                   /* write_through        */
               0,                   /* super_user           */
               0,                   /* read_write           */
               0                    /* present              */
           );
}

/*Z 获取虚拟地址的一级页表项地址 */
static pml4e_t *lookupPML4Slot(vspace_root_t *pml4, vptr_t vptr)
{
    pml4e_t *pml4e = PML4E_PTR(pml4);
    word_t pml4Index = GET_PML4_INDEX(vptr);
    return pml4e + pml4Index;
}
/*Z 获取虚拟地址的二级页表项地址 */
static lookupPDPTSlot_ret_t lookupPDPTSlot(vspace_root_t *pml4, vptr_t vptr)
{
    pml4e_t *pml4Slot = lookupPML4Slot(pml4, vptr);/*Z 获取虚拟地址的一级页表项地址 */
    lookupPDPTSlot_ret_t ret;

    if (!pml4e_ptr_get_present(pml4Slot)) {/*Z 不在内存，报错 */
        current_lookup_fault = lookup_fault_missing_capability_new(PML4_INDEX_OFFSET);

        ret.pdptSlot = NULL;
        ret.status = EXCEPTION_LOOKUP_FAULT;
        return ret;
    } else {
        pdpte_t *pdpt;
        pdpte_t *pdptSlot;
        word_t pdptIndex = GET_PDPT_INDEX(vptr);
        pdpt = paddr_to_pptr(pml4e_ptr_get_pdpt_base_address(pml4Slot));
        pdptSlot = pdpt + pdptIndex;

        ret.status = EXCEPTION_NONE;
        ret.pdptSlot = pdptSlot;
        return ret;
    }
}
/*Z 查找虚拟地址的三级页表项地址 */
lookupPDSlot_ret_t lookupPDSlot(vspace_root_t *pml4, vptr_t vptr)
{
    lookupPDPTSlot_ret_t pdptSlot;
    lookupPDSlot_ret_t ret;

    pdptSlot = lookupPDPTSlot(pml4, vptr);

    if (pdptSlot.status != EXCEPTION_NONE) {
        ret.pdSlot = NULL;
        ret.status = pdptSlot.status;
        return ret;
    }
    if ((pdpte_ptr_get_page_size(pdptSlot.pdptSlot) != pdpte_pdpte_pd) ||
        !pdpte_pdpte_pd_ptr_get_present(pdptSlot.pdptSlot)) {/*Z 不在内存或所指是巨页 */
        current_lookup_fault = lookup_fault_missing_capability_new(PDPT_INDEX_OFFSET);

        ret.pdSlot = NULL;
        ret.status = EXCEPTION_LOOKUP_FAULT;
        return ret;
    } else {
        pde_t *pd;
        pde_t *pdSlot;
        word_t pdIndex = GET_PD_INDEX(vptr);
        pd = paddr_to_pptr(pdpte_pdpte_pd_ptr_get_pd_base_address(pdptSlot.pdptSlot));
        pdSlot = pd + pdIndex;

        ret.status = EXCEPTION_NONE;
        ret.pdSlot = pdSlot;
        return ret;
    }
}
/*Z 失效TLB cpu位图指定的cpu上ASID(PCID)所属的TLB和paging缓存，如果不是当前在用页表还清除TLB位图中的当前cpu位，等待所有的核处理完毕 */
static void flushPD(vspace_root_t *vspace, word_t vptr, pde_t *pd, asid_t asid)
{
    /* clearing the entire PCID vs flushing the virtual addresses
     * one by one using invplg.
     * choose the easy way, invalidate the PCID
     */
    invalidateASID(vspace, asid, SMP_TERNARY(tlb_bitmap_get(vspace), 0));

}
/*Z 失效TLB cpu位图指定的cpu上ASID(PCID)所属的TLB和paging缓存，如果不是当前在用页表还清除TLB位图中的当前cpu位，等待所有的核处理完毕 */
static void flushPDPT(vspace_root_t *vspace, word_t vptr, pdpte_t *pdpt, asid_t asid)/*Z 不好：中间两个参数均未使用 */
{
    /* similar here *//*Z 返回一级页表中存储的TLB cpu位图 */
    invalidateASID(vspace, asid, SMP_TERNARY(tlb_bitmap_get(vspace), 0));
    return;
}
/*Z 失效TLB cpu位图指定的cpu上ASID(PCID)所属的TLB和paging缓存，如果不是当前在用页表还清除TLB位图中的当前cpu位，等待所有的核处理完毕 */
void hwASIDInvalidate(asid_t asid, vspace_root_t *vspace)
{
    invalidateASID(vspace, asid, SMP_TERNARY(tlb_bitmap_get(vspace), 0));
}
/*Z 在二级页表中删除三级页表pd所在的项，失效相关(TLB位图指示)的TLB和paging缓存 */
void unmapPageDirectory(asid_t asid, vptr_t vaddr, pde_t *pd)
{
    findVSpaceForASID_ret_t find_ret;
    lookupPDPTSlot_ret_t    lu_ret;
    /*Z 在全局pool中查找ASID(PCID)对应的一级页表线性地址 */
    find_ret = findVSpaceForASID(asid);
    if (find_ret.status != EXCEPTION_NONE) {
        return;
    }
    /*Z 获取虚拟地址的二级页表项地址 */
    lu_ret = lookupPDPTSlot(find_ret.vspace_root, vaddr);
    if (lu_ret.status != EXCEPTION_NONE) {
        return;
    }

    /* check if the PDPT has the PD */
    if (!(pdpte_ptr_get_page_size(lu_ret.pdptSlot) == pdpte_pdpte_pd &&
          pdpte_pdpte_pd_ptr_get_present(lu_ret.pdptSlot) &&
          (pdpte_pdpte_pd_ptr_get_pd_base_address(lu_ret.pdptSlot) == pptr_to_paddr(pd)))) {
        return;
    }
    /*Z 失效TLB cpu位图指定的cpu上ASID(PCID)所属的TLB和paging缓存，如果不是当前在用页表还清除TLB位图中的当前cpu位，等待所有的核处理完毕 */
    flushPD(find_ret.vspace_root, vaddr, pd, asid);
    /*Z 生成一个全0二级页表项并写入要失效的二级页表项处 */
    *lu_ret.pdptSlot = makeUserPDPTEInvalid();
    /*Z 失效TLB cpu位图指定的cpu页缓存，根据配置也可能失效ASID(PCID)相关的TLB */
    invalidatePageStructureCacheASID(pptr_to_paddr(find_ret.vspace_root), asid,
                                     SMP_TERNARY(tlb_bitmap_get(find_ret.vspace_root), 0));
}

/*Z 解映射三级页表：置空二级页表项，清缓存，清内存，CSlot能力置未映射标记 */
static exception_t performX64PageDirectoryInvocationUnmap(cap_t cap, cte_t *ctSlot)
{

    if (cap_page_directory_cap_get_capPDIsMapped(cap)) {
        pde_t *pd = PDE_PTR(cap_page_directory_cap_get_capPDBasePtr(cap));
        unmapPageDirectory(/*Z 在二级页表中删除三级页表pd所在的项，失效相关(TLB位图指示)的TLB和paging缓存 */
            cap_page_directory_cap_get_capPDMappedASID(cap),
            cap_page_directory_cap_get_capPDMappedAddress(cap),
            pd
        );
        clearMemory((void *)pd, cap_get_capSizeBits(cap));/*Z 清内存 */
    }

    cap_page_directory_cap_ptr_set_capPDIsMapped(&(ctSlot->cap), 0);

    return EXCEPTION_NONE;
}
/*Z 设置CSlot能力，设置二级页表项的值，清缓存 */
static exception_t performX64PageDirectoryInvocationMap(cap_t cap, cte_t *ctSlot, pdpte_t pdpte, pdpte_t *pdptSlot,
                                                        vspace_root_t *vspace)
{
    ctSlot->cap = cap;
    *pdptSlot = pdpte;
    invalidatePageStructureCacheASID(pptr_to_paddr(vspace), cap_page_directory_cap_get_capPDMappedASID(cap),
                                     SMP_TERNARY(tlb_bitmap_get(vspace), 0));
    return EXCEPTION_NONE;
}

/*Z 引用cap_page_directory_cap能力与MMU有关的系统调用处理函数：映射、解映射三级页表 */
static exception_t decodeX64PageDirectoryInvocation(
    word_t label,           /*Z 消息标签(错误类型) */
    word_t length,          /*Z 消息长度 */
    cte_t *cte,             /*Z 其CSlot */
    cap_t cap,              /*Z 其能力 */
    extra_caps_t extraCaps, /*Z 传递的额外能力 */
    word_t *buffer          /*Z IPC buffer */
)
{
    word_t              vaddr;
    vm_attributes_t     vm_attr;
    cap_t               vspaceCap;
    vspace_root_t      *vspace;
    pdpte_t             pdpte;
    paddr_t             paddr;
    asid_t              asid;
    lookupPDPTSlot_ret_t pdptSlot;

    if (label == X86PageDirectoryUnmap) {/*Z -----------------------------------------------------------------子功能：解映射三级页表，无消息传参 */
        if (!isFinalCapability(cte)) {/*Z CSlot必须是该页表的最后一个能力 */
            current_syscall_error.type = seL4_RevokeFirst;
            userError("X86PageDirectory: Cannot unmap if more than one cap exist.");
            return EXCEPTION_SYSCALL_ERROR;
        }
        setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
        /*Z 解映射三级页表：置空二级页表项，清缓存，清内存，CSlot能力置未映射标记 */
        return performX64PageDirectoryInvocationUnmap(cap, cte);
    }

    if (label != X86PageDirectoryMap) {/*Z --------------------------------------------------------------------------------子功能：映射三级页表 */
        userError("X64Directory: Illegal operation.");
        current_syscall_error.type = seL4_IllegalOperation;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (length < 2 || extraCaps.excaprefs[0] == NULL) {
        userError("X64PageDirectory: Truncated message.");
        current_syscall_error.type = seL4_TruncatedMessage;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (cap_page_directory_cap_get_capPDIsMapped(cap)) {/*Z 页表不能已映射 */
        userError("X64PageDirectory: PD is already mapped to a PML4.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 0;

        return EXCEPTION_SYSCALL_ERROR;
    }

    vaddr = getSyscallArg(0, buffer) & (~MASK(PDPT_INDEX_OFFSET));/*Z ----------------消息传参：0-负责映射的首个虚拟地址 */
    vm_attr = vmAttributesFromWord(getSyscallArg(1, buffer));                               /*Z 1-页属性 */
    vspaceCap = extraCaps.excaprefs[0]->cap;                                                /*Z extraCaps0-要映射到的VSpace */

    if (!isValidNativeRoot(vspaceCap)) {
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 1;

        return EXCEPTION_SYSCALL_ERROR;
    }

    vspace = (vspace_root_t *)pptr_of_cap(vspaceCap);
    asid = cap_get_capMappedASID(vspaceCap);

    if (vaddr > USER_TOP) {
        userError("X64PageDirectory: Mapping address too high.");
        current_syscall_error.type = seL4_InvalidArgument;
        current_syscall_error.invalidArgumentNumber = 0;

        return EXCEPTION_SYSCALL_ERROR;
    }

    findVSpaceForASID_ret_t find_ret;

    find_ret = findVSpaceForASID(asid);
    if (find_ret.status != EXCEPTION_NONE) {
        current_syscall_error.type = seL4_FailedLookup;
        current_syscall_error.failedLookupWasSource = false;

        return EXCEPTION_SYSCALL_ERROR;
    }

    if (find_ret.vspace_root != vspace) {
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 1;

        return EXCEPTION_SYSCALL_ERROR;
    }

    pdptSlot = lookupPDPTSlot(vspace, vaddr);
    if (pdptSlot.status != EXCEPTION_NONE) {
        current_syscall_error.type = seL4_FailedLookup;
        current_syscall_error.failedLookupWasSource = false;

        return EXCEPTION_SYSCALL_ERROR;
    }
    /*Z 不好：直接用present测试就够了 */
    if ((pdpte_ptr_get_page_size(pdptSlot.pdptSlot) == pdpte_pdpte_pd &&
         pdpte_pdpte_pd_ptr_get_present(pdptSlot.pdptSlot)) ||
        (pdpte_ptr_get_page_size(pdptSlot.pdptSlot) == pdpte_pdpte_1g
         && pdpte_pdpte_1g_ptr_get_present(pdptSlot.pdptSlot))) {
        current_syscall_error.type = seL4_DeleteFirst;

        return EXCEPTION_SYSCALL_ERROR;
    }

    paddr = pptr_to_paddr(PDE_PTR(cap_page_directory_cap_get_capPDBasePtr(cap)));
    pdpte = makeUserPDPTEPageDirectory(paddr, vm_attr);

    cap = cap_page_directory_cap_set_capPDIsMapped(cap, 1);
    cap = cap_page_directory_cap_set_capPDMappedASID(cap, asid);
    cap = cap_page_directory_cap_set_capPDMappedAddress(cap, vaddr);

    setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
    return performX64PageDirectoryInvocationMap(cap, cte, pdpte, pdptSlot.pdptSlot, vspace);/*Z 设置CSlot能力，设置二级页表项的值，清缓存 */
}
/*Z 置空二级页表的一级页表项，清缓存 */
static void unmapPDPT(asid_t asid, vptr_t vaddr, pdpte_t *pdpt)
{
    findVSpaceForASID_ret_t find_ret;
    pml4e_t *pml4Slot;
    /*Z 在全局pool中查找ASID(PCID)对应的一级页表线性地址 */
    find_ret = findVSpaceForASID(asid);
    if (find_ret.status != EXCEPTION_NONE) {
        return;
    }
    /*Z 获取虚拟地址的一级页表项地址 */
    pml4Slot = lookupPML4Slot(find_ret.vspace_root, vaddr);

    /* check if the PML4 has the PDPT */
    if (!(pml4e_ptr_get_present(pml4Slot) &&
          pml4e_ptr_get_pdpt_base_address(pml4Slot) == pptr_to_paddr(pdpt))) {
        return;
    }
    /*Z 失效TLB cpu位图指定的cpu上ASID(PCID)所属的TLB和paging缓存，如果不是当前在用页表还清除TLB位图中的当前cpu位，等待所有的核处理完毕 */
    flushPDPT(find_ret.vspace_root, vaddr, pdpt, asid);
                /*Z 创建一个全0的页表项 */
    *pml4Slot = makeUserPML4EInvalid();
}
/*Z 解映射二级页表：置空一级页表项，清缓存，清内存，CSlot能力置未映射标记 */
static exception_t performX64PDPTInvocationUnmap(cap_t cap, cte_t *ctSlot)
{
    if (cap_pdpt_cap_get_capPDPTIsMapped(cap)) {
        pdpte_t *pdpt = PDPTE_PTR(cap_pdpt_cap_get_capPDPTBasePtr(cap));
        unmapPDPT(cap_pdpt_cap_get_capPDPTMappedASID(cap),/*Z 置空二级页表的一级页表项，清缓存 */
                  cap_pdpt_cap_get_capPDPTMappedAddress(cap),
                  pdpt);
        clearMemory((void *)pdpt, cap_get_capSizeBits(cap));/*Z 清内存 */
    }

    cap_pdpt_cap_ptr_set_capPDPTIsMapped(&(ctSlot->cap), 0);

    return EXCEPTION_NONE;
}
/*Z 设置CSlot的能力值，设置一级页表项的值，清缓存 */
static exception_t performX64PDPTInvocationMap(cap_t cap, cte_t *ctSlot, pml4e_t pml4e, pml4e_t *pml4Slot,
                                               vspace_root_t *vspace)
{
    ctSlot->cap = cap;
    *pml4Slot = pml4e;
    invalidatePageStructureCacheASID(pptr_to_paddr(vspace), cap_pdpt_cap_get_capPDPTMappedASID(cap),
                                     SMP_TERNARY(tlb_bitmap_get(vspace), 0));

    return EXCEPTION_NONE;
}
/*Z 引用cap_pdpt_cap能力与MMU有关的系统调用处理函数：映射、解映射二级页表 */
static exception_t decodeX64PDPTInvocation(/*Z 系统调用传入的当前线程参数： */
    word_t label,           /*Z 消息标签(错误类型) */
    word_t length,          /*Z 消息长度 */
    cte_t *cte,             /*Z 其CSlot */
    cap_t cap,              /*Z 其能力 */
    extra_caps_t extraCaps, /*Z 传递的额外能力 */
    word_t *buffer)         /*Z IPC buffer */
{
    word_t                  vaddr;
    vm_attributes_t         attr;
    pml4e_t                *pml4Slot;
    cap_t                   vspaceCap;
    vspace_root_t          *vspace;
    pml4e_t                 pml4e;
    paddr_t                 paddr;
    asid_t                  asid;

    if (label == X86PDPTUnmap) {/*Z -----------------------------------------------------------------子功能：解映射二级页表，无消息传参 */
        if (!isFinalCapability(cte)) {/*Z CSlot必须是该二级页表的最后一个能力 */
            current_syscall_error.type = seL4_RevokeFirst;
            userError("X86PDPT: Cannot unmap if more than one cap exist.");
            return EXCEPTION_SYSCALL_ERROR;
        }

        setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
        /*Z 解映射二级页表：置空一级页表项，清缓存，清内存，CSlot能力置未映射标记 */
        return performX64PDPTInvocationUnmap(cap, cte);
    }

    if (label != X86PDPTMap) {/*Z --------------------------------------------------------------------------------子功能：映射二级页表 */
        userError("X86PDPT: Illegal operation.");
        current_syscall_error.type = seL4_IllegalOperation;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (length < 2 || extraCaps.excaprefs[0] == NULL) {
        userError("X64PDPT: Truncated message.");
        current_syscall_error.type = seL4_TruncatedMessage;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (cap_pdpt_cap_get_capPDPTIsMapped(cap)) {/*Z 二级页表不能已映射 */
        userError("X64PDPT: PDPT is already mapped to a PML4.");
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 0;

        return EXCEPTION_SYSCALL_ERROR;
    }

    vaddr = getSyscallArg(0, buffer) & (~MASK(PML4_INDEX_OFFSET));/*Z ----------------消息传参：0-负责映射的首个虚拟地址 */
    attr = vmAttributesFromWord(getSyscallArg(1, buffer));                                  /*Z 1-页属性 */
    vspaceCap = extraCaps.excaprefs[0]->cap;                                                /*Z extraCaps0-要映射到的VSpace */

    if (!isValidNativeRoot(vspaceCap)) {/*Z 验证VSpace有效性 */
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 1;

        return EXCEPTION_SYSCALL_ERROR;
    }

    vspace = (vspace_root_t *)pptr_of_cap(vspaceCap);
    asid = cap_get_capMappedASID(vspaceCap);

    if (vaddr > USER_TOP) {
        userError("X64PDPT: Mapping address too high.");
        current_syscall_error.type = seL4_InvalidArgument;
        current_syscall_error.invalidArgumentNumber = 0;

        return EXCEPTION_SYSCALL_ERROR;
    }

    findVSpaceForASID_ret_t find_ret;

    find_ret = findVSpaceForASID(asid);
    if (find_ret.status != EXCEPTION_NONE) {/*Z 验证ASID的有效性 */
        current_syscall_error.type = seL4_FailedLookup;
        current_syscall_error.failedLookupWasSource = false;

        return EXCEPTION_SYSCALL_ERROR;
    }

    if (find_ret.vspace_root != vspace) {/*Z 验证一级页表地址有效性 */
        current_syscall_error.type = seL4_InvalidCapability;
        current_syscall_error.invalidCapNumber = 1;

        return EXCEPTION_SYSCALL_ERROR;
    }

    pml4Slot = lookupPML4Slot(vspace, vaddr);

    if (pml4e_ptr_get_present(pml4Slot)) {/*Z 一级页表项不能已被占用 */
        current_syscall_error.type = seL4_DeleteFirst;

        return EXCEPTION_SYSCALL_ERROR;
    }

    paddr = pptr_to_paddr(PDPTE_PTR((cap_pdpt_cap_get_capPDPTBasePtr(cap))));
    pml4e = makeUserPML4E(paddr, attr);

    cap = cap_pdpt_cap_set_capPDPTIsMapped(cap, 1);
    cap = cap_pdpt_cap_set_capPDPTMappedASID(cap, asid);
    cap = cap_pdpt_cap_set_capPDPTMappedAddress(cap, vaddr);

    setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
    return performX64PDPTInvocationMap(cap, cte, pml4e, pml4Slot, vspace);/*Z 设置CSlot的能力值，设置一级页表项的值，清缓存 */
}
/*Z MMU有关的系统调用处理函数：映射、解映射二级或三级页表 */
exception_t decodeX86ModeMMUInvocation(/*Z 系统调用传入的当前线程参数： */
    word_t label,           /*Z 消息标签(错误类型) */
    word_t length,          /*Z 消息长度 */
    cptr_t cptr,            /*Z 引用的CSlot句柄 */
    cte_t *cte,             /*Z 其CSlot */
    cap_t cap,              /*Z 其能力 */
    extra_caps_t extraCaps, /*Z 传递的额外能力 */
    word_t *buffer          /*Z IPC buffer */
)
{
    switch (cap_get_capType(cap)) {

    case cap_pml4_cap:
        current_syscall_error.type = seL4_IllegalOperation;
        return EXCEPTION_SYSCALL_ERROR;

    case cap_pdpt_cap:/*Z 映射、解映射二级页表 */
        return decodeX64PDPTInvocation(label, length, cte, cap, extraCaps, buffer);

    case cap_page_directory_cap:/*Z 映射、解映射三级页表 */
        return decodeX64PageDirectoryInvocation(label, length, cte, cap, extraCaps, buffer);

    default:
        fail("Invalid arch cap type");
    }
}
/*Z 清除内存页的页表映射。page_size页大小，vroot一级页表线性地址，vptr该页虚拟地址，pptr该页线性地址 */
bool_t modeUnmapPage(vm_page_size_t page_size, vspace_root_t *vroot, vptr_t vaddr, void *pptr)
{
    if (config_set(CONFIG_HUGE_PAGE) && page_size == X64_HugePage) {
        pdpte_t     *pdpte;/*Z 获取虚拟地址的二级页表项地址 */
        lookupPDPTSlot_ret_t pdpt_ret = lookupPDPTSlot(vroot, vaddr);
        if (pdpt_ret.status != EXCEPTION_NONE) {
            return false;
        }
        pdpte = pdpt_ret.pdptSlot;


        if (!(pdpte_ptr_get_page_size(pdpte) == pdpte_pdpte_1g
              && pdpte_pdpte_1g_ptr_get_present(pdpte)
              && (pdpte_pdpte_1g_ptr_get_page_base_address(pdpte)
                  == pptr_to_paddr(pptr)))) {
            return false;
        }

        *pdpte = makeUserPDPTEInvalid();
        return true;
    }
    fail("Invalid page type");
    return false;
}
/*Z 更新页表项，失效页缓存 */
static exception_t updatePDPTE(asid_t asid, pdpte_t pdpte, pdpte_t *pdptSlot, vspace_root_t *vspace)
{
    *pdptSlot = pdpte;
    invalidatePageStructureCacheASID(pptr_to_paddr(vspace), asid,
                                     SMP_TERNARY(tlb_bitmap_get(vspace), 0));
    return EXCEPTION_NONE;
}
/*Z 更新CSlot的能力，更新页表项，失效页缓存 */
static exception_t performX64ModeMap(cap_t cap, cte_t *ctSlot, pdpte_t pdpte, pdpte_t *pdptSlot, vspace_root_t *vspace)
{
    ctSlot->cap = cap;
    return updatePDPTE(cap_frame_cap_get_capFMappedASID(cap), pdpte, pdptSlot, vspace);
}

struct create_mapping_pdpte_return {
    exception_t status;
    pdpte_t pdpte;      /*Z 页表项数据 */
    pdpte_t *pdptSlot;  /*Z 页表项线性地址 */
};
typedef struct create_mapping_pdpte_return create_mapping_pdpte_return_t;/*Z 新建二级页表项结果 */
/*Z 新建二级页表项数据，各级页表必须已经存在。参数依次是：物理地址、虚拟地址、读写权限、属性、一级页表线性地址 */
static create_mapping_pdpte_return_t createSafeMappingEntries_PDPTE(paddr_t base, word_t vaddr, vm_rights_t vmRights,
                                                                    vm_attributes_t attr,
                                                                    vspace_root_t *vspace)
{
    create_mapping_pdpte_return_t ret;
    lookupPDPTSlot_ret_t          lu_ret;

    lu_ret = lookupPDPTSlot(vspace, vaddr);
    if (lu_ret.status != EXCEPTION_NONE) {
        current_syscall_error.type = seL4_FailedLookup;
        current_syscall_error.failedLookupWasSource = false;
        ret.status = EXCEPTION_SYSCALL_ERROR;
        /* current_lookup_fault will have been set by lookupPDSlot */
        return ret;
    }
    ret.pdptSlot = lu_ret.pdptSlot;

    /* check for existing page directory *//*Z 现有项指代的是个页表 */
    if ((pdpte_ptr_get_page_size(ret.pdptSlot) == pdpte_pdpte_pd) &&
        (pdpte_pdpte_pd_ptr_get_present(ret.pdptSlot))) {
        current_syscall_error.type = seL4_DeleteFirst;
        ret.status = EXCEPTION_SYSCALL_ERROR;
        return ret;
    }
    /*Z 新建一个巨页二级页表项数据 */
    ret.pdpte = makeUserPDPTEHugePage(base, attr, vmRights);
    ret.status = EXCEPTION_NONE;
    return ret;
}
/*Z 重映射一个巨页。类同src/arch/x86/kernel/vspace.c::decodeX86FrameInvocation */
exception_t decodeX86ModeMapPage(word_t label, vm_page_size_t page_size, cte_t *cte, cap_t cap,
                                 vspace_root_t *vroot, vptr_t vaddr, paddr_t paddr, vm_rights_t vm_rights, vm_attributes_t vm_attr)
{
    if (config_set(CONFIG_HUGE_PAGE) && page_size == X64_HugePage) {
        create_mapping_pdpte_return_t map_ret;
        /*Z 新建一个巨页二级页表项数据 */
        map_ret = createSafeMappingEntries_PDPTE(paddr, vaddr, vm_rights, vm_attr, vroot);
        if (map_ret.status != EXCEPTION_NONE) {
            return map_ret.status;
        }

        setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);

        switch (label) {
        case X86PageMap:/*Z 更新CSlot的能力，更新页表项，失效页缓存 */
            return performX64ModeMap(cap, cte, map_ret.pdpte, map_ret.pdptSlot, vroot);

        default:
            current_syscall_error.type = seL4_IllegalOperation;
            return EXCEPTION_SYSCALL_ERROR;
        }
    }
    fail("Invalid Page type");
}

#ifdef CONFIG_PRINTING
/*Z 读虚拟地址空间字的结果 */
typedef struct readWordFromVSpace_ret {
    exception_t status;
    word_t value;
} readWordFromVSpace_ret_t;
/*Z 通过指定的一级页表(线性地址)读指定虚拟地址处的一个字 */
static readWordFromVSpace_ret_t readWordFromVSpace(vspace_root_t *vspace, word_t vaddr)
{
    readWordFromVSpace_ret_t ret;
    lookupPTSlot_ret_t ptSlot;
    lookupPDSlot_ret_t pdSlot;
    lookupPDPTSlot_ret_t pdptSlot;
    paddr_t paddr;
    word_t offset;
    pptr_t kernel_vaddr;
    word_t *value;

    pdptSlot = lookupPDPTSlot(vspace, vaddr);/*Z 获取虚拟地址的二级页表项地址 */
    if (pdptSlot.status == EXCEPTION_NONE &&
        pdpte_ptr_get_page_size(pdptSlot.pdptSlot) == pdpte_pdpte_1g &&
        pdpte_pdpte_1g_ptr_get_present(pdptSlot.pdptSlot)) {/*Z 如果是巨页，查找结束 */

        paddr = pdpte_pdpte_1g_ptr_get_page_base_address(pdptSlot.pdptSlot);
        offset = vaddr & MASK(seL4_HugePageBits);
    } else {
        pdSlot = lookupPDSlot(vspace, vaddr);/*Z 获取虚拟地址的三级页表项地址 */
        if (pdSlot.status == EXCEPTION_NONE &&
            ((pde_ptr_get_page_size(pdSlot.pdSlot) == pde_pde_large) &&
             pde_pde_large_ptr_get_present(pdSlot.pdSlot))) {/*Z 如果是大页，查找结束 */

            paddr = pde_pde_large_ptr_get_page_base_address(pdSlot.pdSlot);
            offset = vaddr & MASK(seL4_LargePageBits);
        } else {
            ptSlot = lookupPTSlot(vspace, vaddr);/*Z 查找虚拟地址的四级页表项地址 */
            if (ptSlot.status == EXCEPTION_NONE && pte_ptr_get_present(ptSlot.ptSlot)) {
                paddr = pte_ptr_get_page_base_address(ptSlot.ptSlot);
                offset = vaddr & MASK(seL4_PageBits);
            } else {
                ret.status = EXCEPTION_LOOKUP_FAULT;
                return ret;
            }
        }
    }


    kernel_vaddr = (word_t)paddr_to_pptr(paddr);/*Z 转换成线性地址 */
    value = (word_t *)(kernel_vaddr + offset);
    ret.status = EXCEPTION_NONE;
    ret.value = *value;
    return ret;
}
/*Z 打印线程的栈内容 */
void Arch_userStackTrace(tcb_t *tptr)
{
    cap_t threadRoot;
    vspace_root_t *vspace_root;
    word_t sp;
    int i;
    /*Z 线程一级页表访问能力 */
    threadRoot = TCB_PTR_CTE_PTR(tptr, tcbVTable)->cap;

    /* lookup the PD */
    if (cap_get_capType(threadRoot) != cap_pml4_cap) {
        printf("Invalid vspace\n");
        return;
    }
    /*Z 线程一级页表线性地址 */
    vspace_root = (vspace_root_t *)pptr_of_cap(threadRoot);
    /*Z TCB上下文保存的RSP寄存器值 */
    sp = getRegister(tptr, RSP);
    /* check for alignment so we don't have to worry about accessing
     * words that might be on two different pages */
    if (!IS_ALIGNED(sp, seL4_WordSizeBits)) {
        printf("RSP not aligned\n");
        return;
    }

    for (i = 0; i < CONFIG_USER_STACK_TRACE_LENGTH; i++) {
        word_t address = sp + (i * sizeof(word_t));
        readWordFromVSpace_ret_t result;
        result = readWordFromVSpace(vspace_root, address);/*Z 通过一级页表直接读虚拟地址处的字 */
        if (result.status == EXCEPTION_NONE) {
            printf("0x%lx: 0x%lx\n", (long)address, (long)result.value);
        } else {
            printf("0x%lx: INVALID\n", (long)address);
        }
    }
}
#endif /* CONFIG_PRINTING */

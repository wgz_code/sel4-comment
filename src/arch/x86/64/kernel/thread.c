/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <object.h>
#include <machine.h>
#include <arch/model/statedata.h>
#include <arch/kernel/vspace.h>
#include <arch/kernel/thread.h>
#include <linker.h>
/*Z 切换到指定线程页表 */
void Arch_switchToThread(tcb_t *tcb)
{
    /* set PD *//*Z 设置页表 */
    setVMRoot(tcb);
#ifdef ENABLE_SMP_SUPPORT   /*Z 设置本cpu的现场保存地址。为什么就SMP有???? */
    asm volatile("movq %[value], %%gs:%c[offset]"
                 :
                 : [value] "r"(&tcb->tcbArch.tcbContext.registers[Error + 1]),
                 [offset] "i"(OFFSETOF(nodeInfo_t, currentThreadUserContext)));
#endif
    if (config_set(CONFIG_KERNEL_X86_IBPB_ON_CONTEXT_SWITCH)) {
        x86_ibpb();/*Z 阻止分支预测 */
    }

    if (config_set(CONFIG_KERNEL_X86_RSB_ON_CONTEXT_SWITCH)) {
        x86_flush_rsb();/*Z 抹去当前栈顶下32个字。与防止推测执行攻击有关 */
    }
}
/*Z 在TCB中保存上下文相关寄存器的指定值 */
BOOT_CODE void Arch_configureIdleThread(tcb_t *tcb)
{
    setRegister(tcb, FLAGS, FLAGS_USER_DEFAULT);        /*Z 标志寄存器使能中断 */
    setRegister(tcb, NextIP, (uint64_t)idleThreadStart);/*Z 伪RIP寄存器指向idle线程函数 */
    setRegister(tcb, CS, SEL_CS_0);                     /*Z CS设置为syscall能自动加载的特权代码段 */
    setRegister(tcb, SS, SEL_DS_0);                     /*Z SS设置为syscall能自动加载的特权堆栈段 */
    /* We set the RSP to 0, even though the idle thread will never touch it, as it
     * allows us to distinguish an interrupt from the idle thread from an interrupt
     * from kernel execution, just by examining the saved RSP value (since the kernel
     * thread will have a valid RSP, and never 0). See traps.S for the other side of this
     */
    setRegister(tcb, RSP, 0);                           /*Z RSP置0 */
}
/*Z 切换到idle线程页表 */
void Arch_switchToIdleThread(void)
{
    tcb_t *tcb = NODE_STATE(ksIdleThread);
    /* Force the idle thread to run on kernel page table */
    setVMRoot(tcb);/*Z 将当前页表设置为线程对应的页表 */
#ifdef ENABLE_SMP_SUPPORT       /*Z %c指示生成一个立即数 */
    asm volatile("movq %[value], %%gs:%c[offset]"           /*Z 为什么就SMP有????? */
                 :              /*Z 将上下文中RSP的地址写入当前cpu的全局变量中，以便于切换上下文时保存现场 */
                 : [value] "r"(&tcb->tcbArch.tcbContext.registers[Error + 1]),
                 [offset] "i"(OFFSETOF(nodeInfo_t, currentThreadUserContext)));
#endif
}
/*Z 啥也没做 */
void Arch_activateIdleThread(tcb_t *tcb)
{
    /* Don't need to do anything */
}
/*Z 如果不是当前线程，置TCB上下文错误码寄存器值0 */
void Mode_postModifyRegisters(tcb_t *tptr)
{
    /* Setting Error to 0 will force a return by the interrupt path, which
     * does a full restore. Unless we're the current thread, in which case
     * we still have to go back out via a sysret */
    if (tptr != NODE_STATE(ksCurThread)) {
        setRegister(tptr, Error, 0);
    }
}

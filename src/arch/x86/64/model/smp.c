/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>
#include <mode/model/smp.h>

#ifdef ENABLE_SMP_SUPPORT
/*Z 各core信息 */
nodeInfo_t node_info[CONFIG_MAX_NUM_NODES] ALIGN(L1_CACHE_LINE_SIZE) VISIBLE;
char nodeSkimScratch[CONFIG_MAX_NUM_NODES][sizeof(nodeInfo_t)] ALIGN(L1_CACHE_LINE_SIZE);/*Z node_info影子，就c_traps.c中临时用了8个字节。不好：搞这个小tricky有什么意义 */
extern char kernel_stack_alloc[CONFIG_MAX_NUM_NODES][BIT(CONFIG_KERNEL_STACK_BITS)];
/*Z 初始化一个core的内核全局数据结构：栈、中断栈、GS段寄存器 */
BOOT_CODE void mode_init_tls(cpu_id_t cpu_index)
{
    node_info[cpu_index].stackTop = kernel_stack_alloc[cpu_index + 1];/*Z 栈顶 */
    node_info[cpu_index].irqStack = &x64KSIRQStack[cpu_index][0];/*Z 栈尾 */
    node_info[cpu_index].index = cpu_index; /*Z 该核node_info的地址就是其GS段基地址 */
    x86_wrmsr(IA32_KERNEL_GS_BASE_MSR, (word_t)&node_info[cpu_index]);
    swapgs();   /*Z 设置GS段寄存器 */
}

#endif /* ENABLE_SMP_SUPPORT */

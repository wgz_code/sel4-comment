/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>
#include <util.h>
#include <api/types.h>
#include <arch/types.h>
#include <arch/model/statedata.h>
#include <arch/object/structures.h>

/* ==== read/write kernel state not preserved across kernel entries ==== */

/* Interrupt currently being handled */
UP_STATE_DEFINE(interrupt_t, x86KScurInterrupt VISIBLE);

UP_STATE_DEFINE(interrupt_t, x86KSPendingInterrupt);

/* ==== proper read/write kernel state ==== */
/*Z per cpu硬件自动操作的TSS、GDT、IDT数据 */
x86_arch_global_state_t x86KSGlobalState[CONFIG_MAX_NUM_NODES] ALIGN(L1_CACHE_LINE_SIZE) SKIM_BSS;
/*Z ASID(PCID) pools。2^3个pools */
/* The top level ASID table */
asid_pool_t *x86KSASIDTable[BIT(asidHighBits)];

/* Current user value of the fs/gs base */
UP_STATE_DEFINE(word_t, x86KSCurrentFSBase);
UP_STATE_DEFINE(word_t, x86KSCurrentGSBase);

UP_STATE_DEFINE(word_t, x86KSGPExceptReturnTo);

/*Z cpu索引到真正cpuid的映射 */
/* ==== read-only kernel state (only written during bootstrapping) ==== */
/* Defines a translation of cpu ids from an index of our actual CPUs */
SMP_STATE_DEFINE(cpu_id_mapping_t, cpu_mapping);
/*Z 6。对数表示的cacheline大小 */
/* CPU Cache Line Size */
uint32_t x86KScacheLineSizeBits;
/*Z FPU的XSAVE保存区，保存了FPU的各种状态信息 */
/* A valid initial FPU state, copied to every new thread. */
user_fpu_state_t x86KSnullFpuState ALIGN(MIN_FPU_ALIGNMENT);
/*Z ACPI提供的IOMMU数量 */
/* Number of IOMMUs (DMA Remapping Hardware Units) */
uint32_t x86KSnumDrhu;

#ifdef CONFIG_IOMMU
/* Intel VT-d Root Entry Table */
vtd_rte_t *x86KSvtdRootTable;           /*Z 所有IOMMU共同的根表线性地址。每项数据结构是struct{u64[2]} */
uint32_t x86KSnumIOPTLevels;            /*Z 所有IOMMU共同支持的最少页表级数。与位宽指示有减2关系 */
uint32_t x86KSnumIODomainIDBits;        /*Z 所有IOMMU共同支持的最少域数量（对数表示）*/
uint32_t x86KSFirstValidIODomain;       /*Z 第一个有效的IOMMU域标识符 */
#endif

#ifdef CONFIG_VTX
UP_STATE_DEFINE(vcpu_t *, x86KSCurrentVCPU);
#endif

#ifdef CONFIG_PRINTING
uint16_t x86KSconsolePort;  /*Z 控制台端口。但无人使用！！！ */
#endif
#ifdef CONFIG_DEBUG_BUILD
uint16_t x86KSdebugPort;
#endif
/*Z 硬件中断状态，intStateIRQTable基本属性决定了本变量是否能用。每个元素是struct{u32[2]}，
    type = 0，空闲（类型为该值，下同）
    u32[0]：置0
    u32[1]：31 28 27 0
            类型   置0

    type = 1，I/O APIC
    u32[0]：置0
    u32[1]：31 28  27               23    22              18    17       16          15
            类型   IOAPIC编号(id, 0基)    中断线号(pin, 0基)   level  polarity_low  masked

    type = 2，Message signalled interrupts(MSI)，属于PCI规范
    u32[0]：31   0
            handle
    u32[1]：31 28  27 20  19 15  14 12
            类型    bus    dev   func
    
    type =3，保留，格式同type=0
*/
/* State data tracking what IRQ source is related to each
 * CPU vector */
x86_irq_state_t x86KSIRQState[maxIRQ + 1];
/*Z 已分配的I/O端口位图 */
word_t x86KSAllocatedIOPorts[NUM_IO_PORTS / CONFIG_WORD_SIZE];
#ifdef CONFIG_KERNEL_MCS
uint32_t x86KStscMhz;       /*Z APIC TSC的计数频率MHZ，也是：次/微秒 */
uint32_t x86KSapicRatio;    /*Z APIC TSC频率:timer频率的比例。0表示使用TSC最后期限模式 */
#endif

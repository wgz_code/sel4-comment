/*Z OK 串口控制台初始化及IO函数。但没搞清串口与控制台是如何关联的??????? */

/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>
#include <arch/kernel/boot_sys.h>
#include <arch/model/statedata.h>
#include <machine/io.h>
#include <plat/machine/io.h>
/*Z 初始化串行端口 */
#if defined(CONFIG_DEBUG_BUILD) || defined(CONFIG_PRINTING)
void serial_init(uint16_t port)
{   /*Z 读状态寄存器（实际是另一个端口），等待收发器空闲或收发缓冲区空 */
    while (!(in8(port + 5) & 0x60)); /* wait until not busy */

    out8(port + 1, 0x00); /* disable generating interrupts */
    out8(port + 3, 0x80); /* line control register: command: set divisor */
    out8(port,     0x01); /* set low byte of divisor to 0x01 = 115200 baud */
    out8(port + 1, 0x00); /* set high byte of divisor to 0x00 */
    out8(port + 3, 0x03); /* line control register: set 8 bit, no parity, 1 stop bit */
    out8(port + 4, 0x0b); /* modem control register: set DTR/RTS/OUT2 *//*Z 估计是握手 */

    in8(port);     /* clear receiver port *//*Z 估计是清除握手反馈信息 */
    in8(port + 5); /* clear line status port */
    in8(port + 6); /* clear modem status port */
}
/*Z 向debug控制台输出一个字符 */
void putDebugChar(unsigned char a)
{                               /*Z 收发缓冲区不为空，等待 */
    while (x86KSdebugPort && (in8(x86KSdebugPort + 5) & 0x20) == 0);
    out8(x86KSdebugPort, a);
}

#endif /* CONFIG_PRINTING || CONFIG_DEBUG_BUILD */
/*Z 从debug控制台读入一个字符 */
#ifdef CONFIG_DEBUG_BUILD
unsigned char getDebugChar(void)
{               /*Z 收发缓冲区没有可读数据，等待 */
    while ((in8(x86KSdebugPort + 5) & 1) == 0);
    return in8(x86KSdebugPort);
}
#endif /* CONFIG_DEBUG_BUILD */

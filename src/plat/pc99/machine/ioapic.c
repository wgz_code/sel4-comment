/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>

#include <linker.h>
#include <machine/io.h>
#include <plat/machine/hardware.h>
#include <plat/machine/ioapic.h>

#define IOAPIC_REGSEL 0x00          /*Z 虽然I/O APIC映射了内存，但似乎都通过此选择寄存器(索引)进行IO */
#define IOAPIC_WINDOW 0x10          /*Z 统一的数据读写寄存器，值均是u32 */

#define IOAPIC_REG_IOAPICID 0x00    /*Z APIC ID寄存器索引：位27～24 */
#define IOAPIC_REG_IOREDTBL 0x10    /*Z 第1个中断线重定位表寄存器(低32位)，与0x11一起构成一个64位寄存器。其它中断线依此序 */
/*Z 第1个中断线重定位表寄存器值格式：
    0x11                                                   0x10
63(31) 56(24)      16          15       14        13          12            11         10     8    7      0
目的APIC ID     禁用标志    触发电平    IRR    管脚极化    传送状态    目的寻址模式    传送模式    中断向量
                                                           1-传送中    0-物理 1-逻辑
*/

#define IOREDTBL_LOW(reg) (IOAPIC_REG_IOREDTBL + (reg) * 2) /*Z 第reg个中断线重定位表寄存器(低32位) */
#define IOREDTBL_HIGH(reg) (IOREDTBL_LOW(reg) + 1)          /*Z 第reg个中断线重定位表寄存器(高32位) */

#define IOREDTBL_LOW_INTERRUPT_MASK BIT(16)                 /*Z 禁用标志位(位值) */
#define IOREDTBL_LOW_TRIGGER_MODE_LEVEL BIT(15)             /*Z 触发电平位(位值) */
#define IOREDTBL_LOW_TRIGGER_MODE_SHIFT    15               /*Z 触发电平位(位索引) */
#define IOREDTBL_LOW_POLARITY_LOW BIT(13)                   /*Z 管脚极化位(位值) */
#define IOREDTBL_LOW_POLARITY_SHIFT         13              /*Z 管脚极化位(位索引) */
#define IOREDTBL_LOW_DEST_MODE_LOGCIAL BIT(11)              /*Z 目的地寻址模式位(位值) */

#define IOAPICID_ID_BITS 4                                  /*Z 物理寻址模式下APIC ID位数 */
#define IOAPICID_ID_OFFSET 24                               /*Z 目的地APIC ID位(位索引) */

#define IOREDTBL_HIGH_RESERVED_BITS 24                      /*Z 中断线重定位表寄存器(高32位)保留的位数(右数) */
/*Z 所有I/O APIC的中断线重定位表寄存器低32位值(缓存) */
/* Cache what we believe is in the low word of the IOREDTBL. This
 * has all the state of trigger modes etc etc */
static uint32_t ioredtbl_state[IOAPIC_IRQ_LINES * CONFIG_MAX_NUM_IOAPIC];
/*Z I/O APIC数量 */
/* Number of IOAPICs in the system */
static uint32_t num_ioapics = 0;
/*Z 向第ioapic个I/O APIC的第reg个寄存器（内存映射）写入u32值 */
static void ioapic_write(uint32_t ioapic, word_t reg, uint32_t value)
{
    *(volatile uint32_t *)((word_t)(PPTR_IOAPIC_START + ioapic * BIT(PAGE_BITS)) + reg) = value;
}
/*Z 读第ioapic个I/O APIC的第reg个寄存器（内存映射）u32值 */
static uint32_t ioapic_read(uint32_t ioapic, word_t reg)
{
    return *(volatile uint32_t *)((word_t)(PPTR_IOAPIC_START + ioapic * BIT(PAGE_BITS)) + reg);
}
/*Z 设置该I/O APIC的各中断线目标cpu，并屏蔽各中断 */
static void single_ioapic_init(word_t ioapic, cpu_id_t delivery_cpu)
{
    uint32_t i;
    /*Z 设置各中断线的目标cpu，禁用中断 */
    /* Mask all the IRQs. In doing so we happen to set
     * the vector to 0, which we can assert against in
     * mask_interrupt to ensure a vector is assigned
     * before we unmask */
    for (i = 0; i < IOAPIC_IRQ_LINES; i++) {/*Z 遍历该I/OAPIC的各中断线 */
        /* Send to desired cpu */
        ioapic_write(ioapic, IOAPIC_REGSEL, IOREDTBL_HIGH(i));/*Z 选择中断线重定位表寄存器(高32位) */
        ioapic_write(ioapic, IOAPIC_WINDOW, (ioapic_read(ioapic,/*Z 读原值，保留保留位，置入目标cpu值，写进去 */
                                                         IOAPIC_WINDOW) & MASK(IOREDTBL_HIGH_RESERVED_BITS)) | (delivery_cpu << IOREDTBL_HIGH_RESERVED_BITS));
        /* mask and set 0 vector */
        ioredtbl_state[i] = IOREDTBL_LOW_INTERRUPT_MASK;/*Z 禁用中断 */
        ioapic_write(ioapic, IOAPIC_REGSEL, IOREDTBL_LOW(i));/*Z 选择中断线重定位表寄存器(低32位) */
        /* The upper 16 bits are reserved, so we make sure to preserve them */
        ioredtbl_state[i] |= ioapic_read(ioapic, IOAPIC_WINDOW) & ~MASK(16);/*Z 保留左16位原值。不好：应该是17 */
        ioapic_write(ioapic, IOAPIC_WINDOW, ioredtbl_state[i]);
    }
}
/*Z 临时记录I/O中断的目标cpu */
static  cpu_id_t ioapic_target_cpu = 0;
/*Z 设置所有I/O APIC的各中断线目标cpu，并屏蔽各中断 */
void ioapic_init(uint32_t num_nodes, cpu_id_t *cpu_list, uint32_t num_ioapic)
{                       /*Z num_nodes未用，cpu_list只用了第1个 */
    uint32_t ioapic;
    num_ioapics = num_ioapic;
    ioapic_target_cpu = cpu_list[0];/*Z 目前只支持一个cpu处理中断 */

    for (ioapic = 0; ioapic < num_ioapic; ioapic++) {
        /* Init this ioapic *//*Z 设置该I/O APIC的各中断线目标cpu，并屏蔽各中断 */
        single_ioapic_init(ioapic, cpu_list[0]);
    }
}
/*Z 设置第ioapic个I/O APIC的第pin个中断线禁用mask，1-禁用，0-启用 */
void ioapic_mask(bool_t mask, uint32_t ioapic, uint32_t pin)
{
    int index = ioapic * IOAPIC_IRQ_LINES + pin;
    if (ioapic >= num_ioapics || pin >= IOAPIC_IRQ_LINES) {
        /* silently ignore requests to non existent parts of the interrupt space */
        return;
    }
    if (mask) {
        ioredtbl_state[index] |= IOREDTBL_LOW_INTERRUPT_MASK;
    } else {
        ioredtbl_state[index] &= ~IOREDTBL_LOW_INTERRUPT_MASK;
        /* it should not be possible to be unmasking an interrupt, without
         * it having been mapped to a vector, assert that this is the case */
        assert((ioredtbl_state[index] & 0xff) != 0);
    }
    ioapic_write(ioapic, IOAPIC_REGSEL, IOREDTBL_LOW(pin));
    ioapic_write(ioapic, IOAPIC_WINDOW, ioredtbl_state[index]);
}
/*Z 检查I/O APIC参数合法性 */
exception_t ioapic_decode_map_pin_to_vector(word_t ioapic, word_t pin, word_t level,
                                            word_t polarity, word_t vector)
{
    if (num_ioapics == 0) {
        userError("System has no IOAPICs");
        current_syscall_error.type = seL4_IllegalOperation;
        return EXCEPTION_SYSCALL_ERROR;
    }
    if (ioapic >= num_ioapics) {
        userError("Invalid IOAPIC %ld, only have %ld", (long)ioapic, (long)num_ioapics);
        current_syscall_error.type = seL4_RangeError;
        current_syscall_error.rangeErrorMin = 0;
        current_syscall_error.rangeErrorMax = num_ioapics - 1;
        return EXCEPTION_SYSCALL_ERROR;
    }
    if (pin >= IOAPIC_IRQ_LINES) {
        userError("Invalid IOAPIC pin %ld, there are %d pins", (long)pin, IOAPIC_IRQ_LINES);
        current_syscall_error.type = seL4_RangeError;
        current_syscall_error.rangeErrorMin = 0;
        current_syscall_error.rangeErrorMax = IOAPIC_IRQ_LINES - 1;
        return EXCEPTION_SYSCALL_ERROR;
    }

    if (level != 0 && level != 1) {
        userError("Level should be 0 or 1, not %d", (int)level);
        current_syscall_error.type = seL4_RangeError;
        current_syscall_error.rangeErrorMin = 0;
        current_syscall_error.rangeErrorMax = 1;
        return EXCEPTION_SYSCALL_ERROR;
    }
    if (polarity != 0 && polarity != 1) {
        userError("Polarity should be 0 or 1, not %d", (int)polarity);
        current_syscall_error.type = seL4_RangeError;
        current_syscall_error.rangeErrorMin = 0;
        current_syscall_error.rangeErrorMax = 1;
        return EXCEPTION_SYSCALL_ERROR;
    }
    return EXCEPTION_NONE;
}
/*Z 设置I/O APIC的中断线及其向量 */
void ioapic_map_pin_to_vector(word_t ioapic, word_t pin, word_t level,
                              word_t polarity, word_t vector)
{
    uint32_t ioredtbl_high = 0;
    uint32_t index = 0;

    index = ioapic * IOAPIC_IRQ_LINES + pin;/*Z 中断线总序号 */
    ioapic_write(ioapic, IOAPIC_REGSEL, IOREDTBL_HIGH(pin));
    ioredtbl_high = ioapic_read(ioapic, IOAPIC_WINDOW) & MASK(IOREDTBL_HIGH_RESERVED_BITS);
    /* delivery mode: physical mode only, using APIC ID */
    ioredtbl_high |= (ioapic_target_cpu << IOREDTBL_HIGH_RESERVED_BITS);
    ioapic_write(ioapic, IOAPIC_WINDOW, ioredtbl_high);
    /* we do not need to add IRQ_INT_OFFSET to the vector here */
    ioredtbl_state[index] = IOREDTBL_LOW_INTERRUPT_MASK |
                            (level << IOREDTBL_LOW_TRIGGER_MODE_SHIFT) |
                            (polarity << IOREDTBL_LOW_POLARITY_SHIFT) |
                            vector;

    ioapic_write(ioapic, IOAPIC_REGSEL, IOREDTBL_LOW(pin));
    /* the upper 16 bits are reserved */
    ioredtbl_state[index] |= ioapic_read(ioapic, IOAPIC_WINDOW) & ~MASK(16);
    ioapic_write(ioapic, IOAPIC_WINDOW, ioredtbl_state[index]);
}

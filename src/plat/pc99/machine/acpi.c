/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>
#include <util.h>
#include <assert.h>
#include <machine/io.h>
#include <linker.h>
#include <plat/machine.h>
#include <plat/machine/acpi.h>
#include <plat/machine/devices.h>

enum acpi_type {
    ACPI_RSDP,
    ACPI_RSDT
};
/*Z 见http://www.intel.com/content/dam/www/public/us/en/documents/product-specifications/vt-directed-io-spec.pdf 8.1 */
/* DMA Remapping Reporting Table *//*Z DMA重映射报告表：IOMMU、中断重映射 */
typedef struct acpi_dmar {
    acpi_header_t header;           /*Z length包括后面紧邻结构的大小 */
    uint8_t       host_addr_width;  /*Z 加1=DMA物理地址最大宽度（位数） */
    uint8_t       flags;
    uint8_t       reserved[10];     /*Z 后面紧邻DRHD、RMRR等子结构 */
} acpi_dmar_t;
compile_assert(acpi_dmar_packed,
               sizeof(acpi_dmar_t) == sizeof(acpi_header_t) + 12)
/*Z 见上述链接#8.2。每个remapping结构都以此开头 */
/* DMA Remapping Structure Header */
typedef struct acpi_dmar_header {
    uint16_t type;      /*Z 重映射结构类型 */
    uint16_t length;    /*Z 包括此头在内的一个重映射结构大小（字节）*/
} acpi_dmar_header_t;
compile_assert(acpi_dmar_header_packed, sizeof(acpi_dmar_header_t) == 4)

/* DMA Remapping Structure Types */
enum acpi_table_dmar_struct_type {
    DMAR_DRHD = 0,  /*Z DMA Remapping Hardware Unit Definition Structure */
    DMAR_RMRR = 1,  /*Z Reserved Memory Region Reporting Structure */
    DMAR_ATSR = 2,  /*Z Root Port ATS Capability Reporting Structure */
};
/*Z 见上述链接#8.3。重映射硬件单元(用于IOMMU等)，每个PCI段至少应有一个 */
/* DMA Remapping Hardware unit Definition */
typedef struct acpi_dmar_drhd {
    acpi_dmar_header_t header;      /*Z length包含紧邻的Device Scope结构 */
    uint8_t            flags;
    uint8_t            reserved;
    uint16_t           segment;     /*Z PCI段号 */
    uint32_t           reg_base[2]; /*Z 此硬件寄存器集的64位起始内存地址。后面紧邻设备描述列表 */
} acpi_dmar_drhd_t;
compile_assert(acpi_dmar_drhd_packed,
               sizeof(acpi_dmar_drhd_t) == sizeof(acpi_dmar_header_t) + 12)
/*Z 见上述链接#8.3.1。一个设备的描述 */
/* Reserved Memory Region Reporting structure Definition */
typedef struct acpi_dmar_devscope {
    uint8_t  type;          /*Z 这两项都是1字节，因此不适用acpi_dmar_header_t */
    uint8_t  length;
    uint16_t reserved;
    uint8_t  enum_id;
    uint8_t  start_bus;     /*Z 总线号 */
    struct {                /*Z 此struct可有多项，表示到达该设备的“路径” */
        uint8_t dev;        /*Z 设备号 */
        uint8_t fun;        /*Z 功能号 */
    } path_0;
} acpi_dmar_devscope_t;
compile_assert(acpi_dmar_devscope_packed, sizeof(acpi_dmar_devscope_t) == 8)
/*Z 见上述链接#8.4。保留内存区描述（用于DMA等），适用于管理老旧设备，如USB、集成显卡等 */
/* Reserved Memory Region Reporting structure Definition */
typedef struct acpi_dmar_rmrr {
    acpi_dmar_header_t   header;        /*Z length包含后缀的所有设备结构 */
    uint16_t             reserved;
    uint16_t             segment;       /*Z PCI段号 */
    uint32_t             reg_base[2];   /*Z 保留内存区起始物理地址 */
    uint32_t             reg_limit[2];  /*Z 结束地址（包含）*/
    acpi_dmar_devscope_t devscope_0;    /*Z 可有多项。必须同时出现在DRHD中 */
} acpi_dmar_rmrr_t;
compile_assert(acpi_dmar_rmrr_packed, sizeof(acpi_dmar_rmrr_t) ==
               sizeof(acpi_dmar_header_t) + 20 + sizeof(acpi_dmar_devscope_t))
/*Z ACPI-FADT表部分内容，实际只需要flags域 */
/* Fixed ACPI Description Table (FADT), partial as we only need flags */
typedef struct acpi_fadt {
    acpi_header_t  header;
    uint8_t        reserved[76];
    uint32_t       flags;
} acpi_fadt_t;
compile_assert(acpi_fadt_packed,
               sizeof(acpi_fadt_t) == sizeof(acpi_header_t) + 80)
/*Z ACPI-MADT表内容 */
/* Multiple APIC Description Table (MADT) */
typedef struct acpi_madt {
    acpi_header_t header;       /*Z length包含紧邻数据大小 */
    uint32_t      apic_addr;    /*Z 本地中断控制器物理地址 */
    uint32_t      flags;        /*Z 后面紧邻若干中断控制器数据结构 */
} acpi_madt_t;
compile_assert(acpi_madt_packed,
               sizeof(acpi_madt_t) == sizeof(acpi_header_t) + 8)
/*Z 一个中断控制器数据结构头 */
typedef struct acpi_madt_header {
    uint8_t type;
    uint8_t length;     /*Z 本结构长度，包括后面紧邻的具体信息 */
} acpi_madt_header_t;
compile_assert(acpi_madt_header_packed, sizeof(acpi_madt_header_t) == 2)
/*Z 中断控制器数据结构类型 */
enum acpi_table_madt_struct_type {
    MADT_APIC   = 0,    /*Z 处理器本地APIC */
    MADT_IOAPIC = 1,    /*Z I/O APIC */
    MADT_ISO    = 2,    /*Z Interrupt Source Override */
    MADT_x2APIC = 9     /*Z 处理器本地x2APIC */
};
/*Z MADT子表：处理器本地APIC表 */
typedef struct acpi_madt_apic {
    acpi_madt_header_t header;
    uint8_t            cpu_id;      /*Z 与某个cpu关联 */
    uint8_t            apic_id;     /*Z seL4说这个就是cpu id，那上个是什么 */
    uint32_t           flags;
} acpi_madt_apic_t;
compile_assert(acpi_madt_apic_packed,
               sizeof(acpi_madt_apic_t) == sizeof(acpi_madt_header_t) + 6)
/*Z MADT子表：处理器本地x2APIC表 */
typedef struct acpi_madt_x2apic {
    acpi_madt_header_t  header;
    uint16_t            reserved;
    uint32_t            x2apic_id;
    uint32_t            flags;              /*Z 与MADT_APIC的相同 */
    uint32_t            acpi_processor_uid; /*Z 与某个cpu关联 */
} acpi_madt_x2apic_t;
compile_assert(acpi_madt_x2apic_packed,
               sizeof(acpi_madt_x2apic_t) == sizeof(acpi_madt_header_t) + 14)
/*Z MADT子表：I/O APIC表 */
typedef struct acpi_madt_ioapic {
    acpi_madt_header_t header;
    uint8_t            ioapic_id;
    uint8_t            reserved[1];
    uint32_t           ioapic_addr; /*Z 该PIC物理地址 */
    uint32_t           gsib;        /*Z 该PIC中断线在全局中断号中的起始号 */
} acpi_madt_ioapic_t;
compile_assert(acpi_madt_ioapic_packed,
               sizeof(acpi_madt_ioapic_t) == sizeof(acpi_madt_header_t) + 10)
/*Z MADT子表：Interrupt Source Override表。默认老旧的8259一一对应第一个I/O APIC，如果不是就需要此项 */
typedef struct acpi_madt_iso {
    acpi_madt_header_t header;
    uint8_t            bus; /* always 0 (ISA) */
    uint8_t            source;  /*Z IRQ# */
    uint32_t           gsi;     /*Z 用此全局中断号覆盖原号 */
    uint16_t           flags;
} acpi_madt_iso_t;
/* We can't assert on the sizeof acpi_madt_iso because it contains trailing
 * padding.
 */
unverified_compile_assert(acpi_madt_iso_packed,
                          OFFSETOF(acpi_madt_iso_t, flags) == sizeof(acpi_madt_header_t) + 6)
/*Z BIOS中有关ACPI表的特征串 */
/* workaround because string literals are not supported by C parser */
const char acpi_str_rsd[]  = {'R', 'S', 'D', ' ', 'P', 'T', 'R', ' ', 0};/*Z BIOS中ACPI-RSDP的特征串，位置对齐16字节 */
const char acpi_str_fadt[] = {'F', 'A', 'C', 'P', 0};/*Z FADT表的特征串 */
const char acpi_str_apic[] = {'A', 'P', 'I', 'C', 0};/*Z MADT表的特征串 */
const char acpi_str_dmar[] = {'D', 'M', 'A', 'R', 0};/*Z DMAR表的特征串 */
/*Z 验证ACPI各种表，简单累加各字节，结果低字节要为0 */
BOOT_CODE static uint8_t acpi_calc_checksum(char *start, uint32_t length)
{
    uint8_t checksum = 0;

    while (length > 0) {
        checksum += *start;
        start++;
        length--;
    }
    return checksum;
}
/*Z 通过搜索BIOS查找RSDP信息 */
BOOT_CODE static acpi_rsdp_t *acpi_get_rsdp(void)
{
    char *addr; /*Z BUG：没有查找BIOS EBDA的头1K区域??????????????? */

    /*Z https://uefi.org/sites/default/files/resources/ACPI_6_3_final_Jan30.pdf 5.2.5.1
    https://wiki.osdev.org/Memory_Map_(x86)
    unsigned long ebda = *(uint16_t *)0x40e;
    ebda <<= 4;
    if (ebda > 0x80000)     // Linux use 0x400
        for (addr = (char *)ebda; addr < (char *)ebda + 1024; addr += 16) {
            if (strncmp(addr, acpi_str_rsd, 8) == 0) {
                if (acpi_calc_checksum(addr, ACPI_V1_SIZE) == 0) {
                    return (acpi_rsdp_t *)addr;
                }
            }
        }
    */

    for (addr = (char *)BIOS_PADDR_START; addr < (char *)BIOS_PADDR_END; addr += 16) {
        if (strncmp(addr, acpi_str_rsd, 8) == 0) {
            if (acpi_calc_checksum(addr, ACPI_V1_SIZE) == 0) {
                return (acpi_rsdp_t *)addr;
            }
        }
    }
    return NULL;
}
/*Z 仅对32位有意义，为entry指向的ACPI内存在固定位置建立临时页表，并返回虚拟地址。对64位已作映射，仅返回entry原值 */
BOOT_CODE static void *acpi_table_init(void *entry, enum acpi_type table_type)
{
    void *acpi_table;
    unsigned int pages_for_table;
    unsigned int pages_for_header = 1;
    /*Z 对32位机器，这时entry指向的地址可能尚未建立页表，无法访问，因此需先为表头建立页表 */
    /* if we need to map another page to read header *//*Z 跨过大页边界时要两个物理页映射该表 */
    unsigned long offset_in_page = (unsigned long)entry & MASK(LARGE_PAGE_BITS);
    if (MASK(LARGE_PAGE_BITS) - offset_in_page < sizeof(acpi_rsdp_t)) {/*Z 这里利用了和_header_t字节相同的特点，不好 */
        pages_for_header++;
    }

    /* map in table's header */
    acpi_table = map_temp_boot_page(entry, pages_for_header);
    /*Z 读取表头，计算entry指向的整个表占用的大页数 */
    switch (table_type) {
    case ACPI_RSDP: {
        acpi_rsdp_t *rsdp_entry = (acpi_rsdp_t *)entry;
        pages_for_table = (rsdp_entry->length + offset_in_page) / MASK(LARGE_PAGE_BITS) + 1;
        break;
    }
    case ACPI_RSDT: { // RSDT, MADT, DMAR etc.
        acpi_rsdt_t *rsdt_entry = (acpi_rsdt_t *)entry;
        pages_for_table = (rsdt_entry->header.length + offset_in_page) / MASK(LARGE_PAGE_BITS) + 1;
        break;
    }
    default:
        printf("Error: Mapping unknown ACPI table type\n");
        assert(false);
        return NULL;
    }

    /* map in full table */
    acpi_table = map_temp_boot_page(entry, pages_for_table);

    return acpi_table;
}
/*Z 获取并保存RSDP表（在rsdp_data中），验证检验和。返回值1成功，0失败 */
BOOT_CODE bool_t acpi_init(acpi_rsdp_t *rsdp_data)
{   /*Z 通过BIOS获取RSDP地址 */
    acpi_rsdp_t *acpi_rsdp = acpi_get_rsdp();

    if (acpi_rsdp == NULL) {
        printf("BIOS: No ACPI support detected\n");
        return false;
    }
    printf("ACPI: RSDP paddr=%p\n", acpi_rsdp);
    acpi_rsdp = acpi_table_init(acpi_rsdp, ACPI_RSDP);/*Z 为其建立临时页表项 */
    printf("ACPI: RSDP vaddr=%p\n", acpi_rsdp);

    /* create a copy of the rsdp data */
    *rsdp_data = *acpi_rsdp;

    /* perform final validation */
    return acpi_validate_rsdp(rsdp_data);
}
/*Z 验证RSDP、RSDT校验和 */
BOOT_CODE bool_t acpi_validate_rsdp(acpi_rsdp_t *acpi_rsdp)
{
    acpi_rsdt_t *acpi_rsdt;
    acpi_rsdt_t *acpi_rsdt_mapped;
    /*Z 验证ACPI 1.0 */
    if (acpi_calc_checksum((char *)acpi_rsdp, ACPI_V1_SIZE) != 0) {
        printf("BIOS: ACPIv1 information corrupt\n");
        return false;
    }
    /*Z 验证ACPI 2.0以上 */
    if (acpi_rsdp->revision > 0 && acpi_calc_checksum((char *)acpi_rsdp, sizeof(*acpi_rsdp)) != 0) {
        printf("BIOS: ACPIv2 information corrupt\n");
        return false;
    }
    /*Z 为RSDT表建立临时页表项。不好：规范说先用XSDT */
    /* verify the rsdt, even though we do not actually make use of the mapping right now */
    acpi_rsdt = (acpi_rsdt_t *)(word_t)acpi_rsdp->rsdt_address;
    printf("ACPI: RSDT paddr=%p\n", acpi_rsdt);
    acpi_rsdt_mapped = (acpi_rsdt_t *)acpi_table_init(acpi_rsdt, ACPI_RSDT);
    printf("ACPI: RSDT vaddr=%p\n", acpi_rsdt_mapped);
    /*Z 验证RSDT检验和 */
    assert(acpi_rsdt_mapped->header.length > 0);
    if (acpi_calc_checksum((char *)acpi_rsdt_mapped, acpi_rsdt_mapped->header.length) != 0) {
        printf("ACPI: RSDT checksum failure\n");
        return false;
    }

    return true;
}
/*Z 通过ACPI获取并保存CPU、APIC信息，返回可用cpu数量 */
BOOT_CODE uint32_t acpi_madt_scan(
    acpi_rsdp_t *acpi_rsdp,
    cpu_id_t    *cpu_list,
    uint32_t    *num_ioapic,
    paddr_t     *ioapic_paddrs
)
{
    unsigned int entries;
    uint32_t            num_cpu;
    uint32_t            count;
    acpi_madt_t        *acpi_madt;
    acpi_madt_header_t *acpi_madt_header;

    acpi_rsdt_t *acpi_rsdt_mapped;
    acpi_madt_t *acpi_madt_mapped;      /*Z 建立临时页表项 */
    acpi_rsdt_mapped = (acpi_rsdt_t *)acpi_table_init((acpi_rsdt_t *)(word_t)acpi_rsdp->rsdt_address, ACPI_RSDT);

    num_cpu = 0;
    *num_ioapic = 0;
    /*Z 遍历RSDT查找MADT表 */
    assert(acpi_rsdt_mapped->header.length >= sizeof(acpi_header_t));
    /* Divide by uint32_t explicitly as this is the size as mandated by the ACPI standard */
    entries = (acpi_rsdt_mapped->header.length - sizeof(acpi_header_t)) / sizeof(uint32_t);
    for (count = 0; count < entries; count++) {
        acpi_madt = (acpi_madt_t *)(word_t)acpi_rsdt_mapped->entry[count];
        acpi_madt_mapped = (acpi_madt_t *)acpi_table_init(acpi_madt, ACPI_RSDT);/*Z 建立临时页表项 */

        if (strncmp(acpi_str_apic, acpi_madt_mapped->header.signature, 4) == 0) {
            printf("ACPI: MADT paddr=%p\n", acpi_madt);
            printf("ACPI: MADT vaddr=%p\n", acpi_madt_mapped);
            printf("ACPI: MADT apic_addr=0x%x\n", acpi_madt_mapped->apic_addr);
            printf("ACPI: MADT flags=0x%x\n", acpi_madt_mapped->flags);

            acpi_madt_header = (acpi_madt_header_t *)(acpi_madt_mapped + 1);/*Z 指向PIC结构信息列表 */
            /*Z 遍历各个PIC信息结构 */
            while ((char *)acpi_madt_header < (char *)acpi_madt_mapped + acpi_madt_mapped->header.length) {
                switch (acpi_madt_header->type) {
                /* ACPI specifies the following rules when listing APIC IDs:
                 *  - Boot processor is listed first
                 *  - For multi-threaded processors, BIOS should list the first logical
                 *    processor of each of the individual multi-threaded processors in MADT
                 *    before listing any of the second logical processors.
                 *  - APIC IDs < 0xFF should be listed in APIC subtable, APIC IDs >= 0xFF
                 *    should be listed in X2APIC subtable */
                case MADT_APIC: {   /*Z 核内嵌中断控制器 */
                    /* what Intel calls apic_id is what is called cpu_id in seL4! */
                    uint8_t  cpu_id = ((acpi_madt_apic_t *)acpi_madt_header)->apic_id;
                    uint32_t flags  = ((acpi_madt_apic_t *)acpi_madt_header)->flags;
                    if (flags == 1) {   /*Z 处理器就绪 */
                        printf("ACPI: MADT_APIC apic_id=0x%x\n", cpu_id);
                        if (num_cpu == CONFIG_MAX_NUM_NODES) {
                            printf("ACPI: Not recording this APIC, only support %d\n", CONFIG_MAX_NUM_NODES);
                        } else {
                            cpu_list[num_cpu] = cpu_id;
                            num_cpu++;
                        }
                    }
                    break;
                }
                case MADT_x2APIC: {   /*Z 升级的核内嵌中断控制器 */
                    uint32_t cpu_id = ((acpi_madt_x2apic_t *)acpi_madt_header)->x2apic_id;
                    uint32_t flags  = ((acpi_madt_x2apic_t *)acpi_madt_header)->flags;
                    if (flags == 1) {   /*Z 处理器就绪 */
                        printf("ACPI: MADT_x2APIC apic_id=0x%x\n", cpu_id);
                        if (num_cpu == CONFIG_MAX_NUM_NODES) {
                            printf("ACPI: Not recording this APIC, only support %d\n", CONFIG_MAX_NUM_NODES);
                        } else {
                            cpu_list[num_cpu] = cpu_id;
                            num_cpu++;
                        }
                    }
                    break;
                }
                case MADT_IOAPIC:   /*Z 核外中断控制器 */
                    printf(
                        "ACPI: MADT_IOAPIC ioapic_id=%d ioapic_addr=0x%x gsib=%d\n",
                        ((acpi_madt_ioapic_t *)acpi_madt_header)->ioapic_id,
                        ((acpi_madt_ioapic_t *)acpi_madt_header)->ioapic_addr,
                        ((acpi_madt_ioapic_t *)acpi_madt_header)->gsib
                    );
                    if (*num_ioapic == CONFIG_MAX_NUM_IOAPIC) {
                        printf("ACPI: Not recording this IOAPIC, only support %d\n", CONFIG_MAX_NUM_IOAPIC);
                    } else {
                        ioapic_paddrs[*num_ioapic] = ((acpi_madt_ioapic_t *)acpi_madt_header)->ioapic_addr;
                        (*num_ioapic)++;
                    }
                    break;
                case MADT_ISO:  /*Z 默认老旧的8259一一对应第一个I/O APIC，如果不是就需要此项 */
                    printf("ACPI: MADT_ISO bus=%d source=%d gsi=%d flags=0x%x\n",
                           ((acpi_madt_iso_t *)acpi_madt_header)->bus,
                           ((acpi_madt_iso_t *)acpi_madt_header)->source,
                           ((acpi_madt_iso_t *)acpi_madt_header)->gsi,
                           ((acpi_madt_iso_t *)acpi_madt_header)->flags);
                    break;
                default:
                    break;
                }
                acpi_madt_header = (acpi_madt_header_t *)((char *)acpi_madt_header + acpi_madt_header->length);
            }
        }
    }

    printf("ACPI: %d CPU(s) detected\n", num_cpu);

    return num_cpu;
}
/*Z 映射并检查FADT表中IPI目的core标记方式是否符合配置 */
BOOT_CODE bool_t acpi_fadt_scan(
    acpi_rsdp_t *acpi_rsdp
)
{
    unsigned int entries;
    uint32_t            count;
    acpi_fadt_t        *acpi_fadt;

    acpi_rsdt_t *acpi_rsdt_mapped;
    acpi_fadt_t *acpi_fadt_mapped;
    acpi_rsdt_mapped = (acpi_rsdt_t *)acpi_table_init((acpi_rsdt_t *)(word_t)acpi_rsdp->rsdt_address, ACPI_RSDT);
    /*Z 在RSDT表中查找FADT表的地址 */
    assert(acpi_rsdt_mapped->header.length >= sizeof(acpi_header_t));
    /* Divide by uint32_t explicitly as this is the size as mandated by the ACPI standard */
    entries = (acpi_rsdt_mapped->header.length - sizeof(acpi_header_t)) / sizeof(uint32_t);
    for (count = 0; count < entries; count++) {
        acpi_fadt = (acpi_fadt_t *)(word_t)acpi_rsdt_mapped->entry[count];
        acpi_fadt_mapped = (acpi_fadt_t *)acpi_table_init(acpi_fadt, ACPI_RSDT);

        if (strncmp(acpi_str_fadt, acpi_fadt_mapped->header.signature, 4) == 0) {
            printf("ACPI: FADT paddr=%p\n", acpi_fadt);
            printf("ACPI: FADT vaddr=%p\n", acpi_fadt_mapped);
            printf("ACPI: FADT flags=0x%x\n", acpi_fadt_mapped->flags);
            /*Z 检查IPI目的core标记方式是否与配置相符 */
            if (config_set(CONFIG_USE_LOGICAL_IDS) &&   /*Z 配置的是逻辑模式 */
                acpi_fadt_mapped->flags & BIT(19)) {    /*Z 获取的是物理模式 */
                printf("system requires apic physical mode\n");
                return false;
            }
        }
    }

    return true;
}
/*Z 查找并保存IOMMU设置 */
BOOT_CODE void acpi_dmar_scan(
    acpi_rsdp_t *acpi_rsdp,
    paddr_t     *drhu_list,
    uint32_t    *num_drhu,
    uint32_t     max_drhu_list_len,
    acpi_rmrr_list_t *rmrr_list
)
{
    word_t i;
    unsigned int entries;
    uint32_t count;
    uint32_t reg_basel, reg_baseh;
    int rmrr_count;
    dev_id_t dev_id;

    acpi_dmar_t          *acpi_dmar;
    acpi_dmar_header_t   *acpi_dmar_header;
    acpi_dmar_rmrr_t     *acpi_dmar_rmrr;
    acpi_dmar_devscope_t *acpi_dmar_devscope;

    acpi_rsdt_t *acpi_rsdt_mapped;
    acpi_dmar_t *acpi_dmar_mapped;

    acpi_rsdt_mapped = (acpi_rsdt_t *)acpi_table_init((acpi_rsdt_t *)(word_t)acpi_rsdp->rsdt_address, ACPI_RSDT);

    *num_drhu = 0;
    rmrr_count = 0;
    /*Z 在RSDT表中查找DMAR表 */
    assert(acpi_rsdt_mapped->header.length >= sizeof(acpi_header_t));
    entries = (acpi_rsdt_mapped->header.length - sizeof(acpi_header_t)) / sizeof(uint32_t);
    for (count = 0; count < entries; count++) {
        acpi_dmar = (acpi_dmar_t *)(word_t)acpi_rsdt_mapped->entry[count];
        acpi_dmar_mapped = (acpi_dmar_t *)acpi_table_init(acpi_dmar, ACPI_RSDT);

        if (strncmp(acpi_str_dmar, acpi_dmar_mapped->header.signature, 4) == 0) {
            printf("ACPI: DMAR paddr=%p\n", acpi_dmar);
            printf("ACPI: DMAR vaddr=%p\n", acpi_dmar_mapped);
            printf("ACPI: IOMMU host address width: %d\n", acpi_dmar_mapped->host_addr_width + 1);
            acpi_dmar_header = (acpi_dmar_header_t *)(acpi_dmar_mapped + 1);/*Z 指向紧邻的是remapping结构列表 */
            /*Z 遍历DMAR表中的remapping结构 */
            while ((char *)acpi_dmar_header < (char *)acpi_dmar_mapped + acpi_dmar_mapped->header.length) {
                switch (acpi_dmar_header->type) {

                case DMAR_DRHD: /*Z 主要部分就是IOMMU */
                    if (*num_drhu == max_drhu_list_len) {
                        printf("ACPI: too many IOMMUs, disabling IOMMU support\n");
                        /* try to increase MAX_NUM_DRHU in config.h */
                        *num_drhu = 0; /* report zero IOMMUs */
                        return;
                    }
                    reg_basel = ((acpi_dmar_drhd_t *)acpi_dmar_header)->reg_base[0];
                    reg_baseh = ((acpi_dmar_drhd_t *)acpi_dmar_header)->reg_base[1];
                    /* check if value fits into uint32_t */
                    if (reg_baseh != 0) {
                        printf("ACPI: DMAR_DRHD reg_base exceeds 32 bit, disabling IOMMU support\n");
                        /* try to make BIOS map it below 4G */
                        *num_drhu = 0; /* report zero IOMMUs */
                        return;
                    }
                    drhu_list[*num_drhu] = (paddr_t)reg_basel;
                    (*num_drhu)++;
                    break;

                case DMAR_RMRR: /*Z 用于老旧设备的DMA保留内存区 */
                    /* loop through all device scopes of this RMRR */
                    acpi_dmar_rmrr = (acpi_dmar_rmrr_t *)acpi_dmar_header;
                    if (acpi_dmar_rmrr->reg_base[1] != 0 ||
                        acpi_dmar_rmrr->reg_limit[1] != 0) {
                        printf("ACPI: RMRR device above 4GiB, disabling IOMMU support\n");
                        *num_drhu = 0;
                        return ;
                    }
                    /*Z 遍历该保留区涉及的所有设备。这意味着rmrr_list列表中同一内存区域可有多项(设备不同但共用内存区) */
                    for (i = 0; i <= (acpi_dmar_header->length - sizeof(acpi_dmar_rmrr_t)) / sizeof(acpi_dmar_devscope_t); i++) {
                        acpi_dmar_devscope = &acpi_dmar_rmrr->devscope_0 + i;

                        if (acpi_dmar_devscope->type != 1) {    /*Z 目前不支持非端点设备 */
                            /* FIXME - bugzilla bug 170 */
                            printf("ACPI: RMRR device scope: non-PCI-Endpoint-Devices not supported yet, disabling IOMMU support\n");
                            *num_drhu = 0; /* report zero IOMMUs */
                            return;
                        }

                        if (acpi_dmar_devscope->length > sizeof(acpi_dmar_devscope_t)) {/*Z 目前不支持多路径 */
                            /* FIXME - bugzilla bug 170 */
                            printf("ACPI: RMRR device scope: devices behind bridges not supported yet, disabling IOMMU support\n");
                            *num_drhu = 0; /* report zero IOMMUs */
                            return;
                        }

                        dev_id =
                            get_dev_id(
                                acpi_dmar_devscope->start_bus,
                                acpi_dmar_devscope->path_0.dev,
                                acpi_dmar_devscope->path_0.fun
                            );

                        if (rmrr_count == CONFIG_MAX_RMRR_ENTRIES) {
                            printf("ACPI: Too many RMRR entries, disabling IOMMU support\n");
                            *num_drhu = 0;
                            return;
                        }
                        printf("\tACPI: registering RMRR entry for region for device: bus=0x%x dev=0x%x fun=0x%x\n",
                               acpi_dmar_devscope->start_bus,
                               acpi_dmar_devscope->path_0.dev,
                               acpi_dmar_devscope->path_0.fun
                              );

                        rmrr_list->entries[rmrr_count].device = dev_id;
                        rmrr_list->entries[rmrr_count].base = acpi_dmar_rmrr->reg_base[0];
                        rmrr_list->entries[rmrr_count].limit = acpi_dmar_rmrr->reg_limit[0];
                        rmrr_count++;
                    }
                    break;

                case DMAR_ATSR:
                    /* not implemented yet */
                    break;

                default:
                    printf("ACPI: Unknown DMA remapping structure type: %x\n", acpi_dmar_header->type);
                }
                acpi_dmar_header = (acpi_dmar_header_t *)((char *)acpi_dmar_header + acpi_dmar_header->length);
            }
        }
    }
    rmrr_list->num = rmrr_count;
    printf("ACPI: %d IOMMUs detected\n", *num_drhu);
}

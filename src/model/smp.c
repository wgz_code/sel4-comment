/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>
#include <model/smp.h>
#include <object/tcb.h>

#ifdef ENABLE_SMP_SUPPORT
/*Z 设置线程新的亲和cpu，迁移DEBUG队列。没有显式调用，内核不会迁移线程 */
void migrateTCB(tcb_t *tcb, word_t new_core)
{
#ifdef CONFIG_DEBUG_BUILD
    tcbDebugRemove(tcb);/*Z 将线程从DEBUG队列中摘出 */
#endif
    Arch_migrateTCB(tcb);/*Z 确保线程在当前亲和cpu上不是FPU活跃指针属主 */
    tcb->tcbAffinity = new_core;
#ifdef CONFIG_DEBUG_BUILD
    tcbDebugAppend(tcb);/*Z 将线程插入到亲和cpu的DEBUG线程双向链表头 */
#endif
}

#endif /* ENABLE_SMP_SUPPORT */
